<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//User
// $form_regis= DB::table('tbl_config')->select('*')->first();
// Auth::routes(['reset' => false,'verify' => false,'register' => $form_regis->status]);

Auth::routes(['reset' => false,'verify' => false,'register' => true]);

Route::get('/', function () {return view('auth.login');})->name('landingPage')->middleware('guest');

// ADMIN
Route::prefix('dashboard')->group(function () {
    //DASHBOARD
    Route::get('/', 'Admin\HomeController@index')->name('adminGetLab');
    Route::get('/report', 'Admin\HomeController@showReport')->name('adminGetStat');
    Route::post('/rawdata', 'Admin\HomeController@exportAllRevenue')->name('adminRevenueExportAll');

    // RUMAHSAKIT
    Route::group([
        'prefix' => 'rs/{tahun}/{id_rs}',
        'where' =>  [
                        'tahun' => '[0-9]+',
                        'id_rs' => '[0-9]+'
                    ],

    ], function() {
        Route::get('/', 'Admin\RumahsakitController@index')->name('adminRsIndex');
        Route::get('revenue', 'Admin\RumahsakitController@showRevenue')->name('adminRevenueShow');
        Route::get('revenue/download', 'Admin\RumahsakitController@exportRevenue')->name('adminRevenueExport');
        Route::get('log', 'Admin\RumahsakitController@showLog')->name('adminLogShow');
    });

    //forms
    Route::prefix('form')->group(function() {
        Route::get('/', 'Admin\FormController@index')->name('adminFormIndex');
        Route::post('/', 'Admin\FormController@create')->name('adminFormCreate');
        Route::post('update', 'Admin\FormController@update')->name('adminFormUpdate');
        Route::post('post', 'Admin\FormController@destroy')->name('adminFormDestroy');
    });

    Route::prefix('settings')->group(function() {
        Route::get('/', 'Admin\SettingsController@index')->name('adminSettingsIndex');
        Route::post('/', 'Admin\SettingsController@update')->name('adminSettingsupdate');
    });
});

// USER ROUTE
Route::group([
        'prefix' => 'rs/{keytahun}/{keyrs}',
        'where' => ['keytahun' => '[0-9]+'],
    ],
    function() {
        // DASHBOARD
        Route::get('/', 'User\HomeController@index')->name('userHome');
        Route::get('/getURL', 'User\HomeController@getURL')->name('userHomeURL');

        // RESPONDEN
        Route::prefix('respondent')->group(function(){
            Route::get('/', 'User\RespondenController@index')->name('respondenIndex');
            Route::post('/', 'User\RespondenController@updateOrCreate')->name('respondenUpdate');
            Route::post('/photo/upload', 'User\RespondenController@uploadPhoto')->name('respondenUploadPhoto');
        });

        // KUESIONER
        Route::prefix('questionnaire')->group(function(){
            Route::get('/', 'User\KuesionerController@index')->name('kuesionerIndex');
            Route::post('/', 'User\KuesionerController@updateOrCreate')->name('kuesionerUpdate');
        });

        // DOKUMENTASI
        Route::prefix('gallery')->group(function(){
            Route::get('/', 'User\DokumentasiController@index')->name('dokumentasiIndex');
            Route::post('/', 'User\DokumentasiController@create')->name('dokumentasiUpdate');
        });

        // REVENUE
        Route::prefix('revenue')->group(function(){
            Route::get('/', 'User\RevenueController@index')->name('revenueIndex');
            Route::post('/', 'User\RevenueController@updateOrCreate')->name('revenueUpdate');
        });
    }
);

// GENERAL FORM
Route::group([
    'prefix' => 'form/{keytahun}',
    'where' => ['keytahun' => '[0-9]+']
],
function () {
    Route::get('/', 'User\GeneralFormController@index')->name('generalIndex');
    Route::post('/', 'User\GeneralFormController@update')->name('generalUpdate');
    Route::get('/add', 'User\GeneralFormController@index_alt')->name('generalIndexAlt');
    Route::post('/add', 'User\GeneralFormController@create')->name('generalCreate');
}
);

Route::prefix('data')->group(function () {
// AJAX ROUTE
Route::get('/', function () {return abort(404);});
Route::get('/grup_2/{id_grup_1}/{keyid_rs?}', 'AjaxDataController@getDataGrup2')->name('getDataGrup2');
Route::get('/grup_3/{id_grup_1}/{id_grup_2}/{keyid_rs?}', 'AjaxDataController@getDataGrup3')->name('getDataGrup3');
Route::get('/rs/{kota}/', 'AjaxDataController@liveSearch')->name('getDataRS');
});

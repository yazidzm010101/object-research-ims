-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2020 at 08:53 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ims_objectresearch_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `id_config` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_config`
--

INSERT INTO `tbl_config` (`id_config`, `status`) VALUES
(1, 1),
(2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form`
--

CREATE TABLE `tbl_form` (
  `id_form` bigint(20) UNSIGNED NOT NULL,
  `tahun` int(11) NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_form`
--

INSERT INTO `tbl_form` (`id_form`, `tahun`, `link`, `status`) VALUES
(1, 2020, 'http://localhost:8000/form/7698', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_1`
--

CREATE TABLE `tbl_grup_1` (
  `id_grup_1` int(10) UNSIGNED NOT NULL,
  `nama_grup_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_1`
--

INSERT INTO `tbl_grup_1` (`id_grup_1`, `nama_grup_1`) VALUES
(1, 'ACUTE WOUND CARE'),
(2, 'ADVANCE WOUND CARE '),
(3, 'PHYSIOTHERAPY & BANDAGIING'),
(4, 'FRACTURE MANAGEMENT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_2`
--

CREATE TABLE `tbl_grup_2` (
  `id_grup_2` int(10) UNSIGNED NOT NULL,
  `nama_grup_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lini` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_grup_2` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_2`
--

INSERT INTO `tbl_grup_2` (`id_grup_2`, `nama_grup_2`, `nama_lini`, `status_grup_2`) VALUES
(1, 'Wide Area Fixation (Plester penutup luka lebar berwarna putih)', 'FIXATION', 0),
(2, 'Rubber Tapes ( Plester medis roll berwarna coklat)', 'FIXATION', 0),
(3, 'Special Fixation ( Pleseter Infus)', 'FIXATION', 0),
(4, 'Retention Bandage (Cohesive bandage) Perban Elastis Kohesif', 'FIXATION', 0),
(5, 'Low Allergies Tape  ( Plester  roll kertas untuk kulit sensitif) ', 'FIXATION', 0),
(6, 'Sterile Post Op (Plester Pasca Operasi)', 'DRESSING', 0),
(7, 'Surgical dressing ( Perban operasi)', 'DRESSING', 0),
(8, 'Urology/Folley Catheter ( Selang Kateter)', 'MISCELLANEOUS WOUNDCARE ', 0),
(9, 'Antimicrobial (Dressing Luka Anti Baketri)', 'WOUND BED PREPARATION', 0),
(10, 'Impregnated sterile dressing ( Tulle dressing berlapis bahan cair )', 'Advanced Wound Dressings', 0),
(11, 'Hydrocolloid/hydropolymers (Dressing hidrokoloid)', 'Advanced Wound Dressings', 0),
(12, 'Hydrogels ', 'Advanced Wound Dressings', 0),
(13, 'Alginates/hydrofibers (Pembalut Alginat)', 'Advanced Wound Dressings', 0),
(14, 'Foam Dressing (Pembalut penyerap eksudat)', 'Advanced Wound Dressings', 0),
(15, 'Non Adhesive Bandage/Crepe Bandage (Elastik perban tidak berperekat)', 'NON ADHESIVE SUPPORT ', 0),
(16, 'STRAPPING TAPES (Taping Kinesiologi)', 'Actimove', 0),
(17, 'Plaster Of Paris/POP Cast (Gips POP)', 'CAST', 0),
(18, 'Rigid-fiberglass casting (Casting Sintetik tahan air)', 'CAST', 0),
(19, 'Syntetic Splints (Splinting sintetik)', 'SPLINT', 0),
(20, 'Padding (Padding untuk gips)', 'Accessories', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_3`
--

CREATE TABLE `tbl_grup_3` (
  `id_grup_3` int(10) UNSIGNED NOT NULL,
  `nama_grup_3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_grup_3` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_3`
--

INSERT INTO `tbl_grup_3` (`id_grup_3`, `nama_grup_3`, `brand`, `status_grup_3`) VALUES
(1, 'Hypafix', 'BSN MEDICAL', 0),
(2, 'Polifix', 'SNAM', 0),
(3, 'Pharmafix ', 'Pharmaplast/ Global Dispo Medika', 0),
(4, 'Plesterin', 'OneMed', 0),
(5, 'Soft Cloth', '3M', 0),
(6, 'Fixomull Stretch', 'BSN Medical', 0),
(7, 'Ultrafix', 'OneMed', 0),
(8, 'Leukoplast', 'BSN MEDICAL', 0),
(9, 'Hansaplast', 'Beirsdorf', 0),
(10, 'Fescoplast ', 'FRESCO', 0),
(11, 'Leukomed IV Film', 'BSN MEDICAL', 0),
(12, 'Opsite IV 3000', 'Smith & Nephew', 0),
(13, '1624R Tegaderm IV Film', '3M', 0),
(14, 'Onemed Dermafix S IV', 'OneMed', 0),
(15, 'BBRAUN Askina Soft Clear IV', 'BBraun', 0),
(16, 'CANNOFIX-PU 6 CM X 8 CM', 'PHARMAPLAST S.A.E,EGYPT', 0),
(17, 'Elastomull Haft', 'BSN MEDICAL', 0),
(18, 'Pdsive', 'Pro Device', 0),
(19, 'Mediflex', 'Onemed', 0),
(20, 'Leukopor', 'BSN Medical', 0),
(21, 'Micropore', '3M', 0),
(22, 'Masterplast', '', 0),
(23, 'Leukomed T Plus ', 'BSN MEDICAL', 0),
(24, 'Opsite Post op ', 'Smith & Nephew', 0),
(25, 'Tegaderm? Absorbent Clear Acrylic Dressing', '3M', 0),
(26, 'Askina Soft & Clear', 'BBraun', 0),
(27, 'Dermafix', 'Onemed', 0),
(28, 'Curapor Transparent (Lohmann & Rauscher)            ', 'Lohmann & Rauscher', 0),
(29, 'Transparent Wound Dressing with Pad', 'Wayson Medical Semillas', 0),
(30, 'Leukomed Sorbact', 'BSN MEDICAL', 0),
(31, 'Aquacel Ag Surgical - Hydrofiber technology with polyurethane film', 'Convatec', 0),
(32, 'Postofix-PU', 'Pharmaplast', 0),
(33, 'Askina Soft', 'Bbraun', 0),
(34, 'Cutisorb Sterile', 'BSN Medical', 0),
(35, 'Askina Absorb plus ', 'Bbraun', 0),
(36, 'Super Absorbent', 'Winner', 0),
(37, 'Norta ', 'BSN MEDICAL', 0),
(38, 'Urocare Folley Catheter 2 Way ', 'Onemed', 0),
(39, '2 Ways, Folley Catheter', 'Remedi', 0),
(40, 'Euromed Foley Catheter', 'Uromed', 0),
(41, 'Folley Catheter 2 Way', 'Eskamed', 0),
(42, 'Latex Foley Catheter 2 Way ', 'Fesco', 0),
(43, 'Foleycath, 2 way latex ', 'Aximed', 0),
(44, 'Foley Catheter 2 Way', 'Bard Bardia', 0),
(45, '2Way Standard Foley Catheter Silicone Coated ', 'Idealcare', 0),
(46, 'Welford Medical Foley catheter 2 way ', 'Welforf Medical', 0),
(47, 'Foley Balloon Catheter 2 Way ', 'WRP', 0),
(48, 'RUSCH foley catheter ', 'RUSCH', 0),
(49, 'Cutimed Sorbact ', 'BSN MEDICAL', 0),
(50, 'Acticoat Flex 3 ', 'Smith & Nephew', 0),
(51, 'Askina Calgitrol Ag ', 'Bbraun', 0),
(52, 'Aquacell Ag Extra ', 'Convatec', 0),
(53, 'Pharma Super Foam Carbon Silver ', 'Pharmaplast', 0),
(54, 'Kill Bac ', 'KALBE', 0),
(55, 'CUTIMED SORBACT Gel ', 'BSN MEDICAL', 0),
(56, 'Idoform ', 'KALBE', 0),
(57, 'Cuticell Classic ', 'BSN MEDICAL', 0),
(58, 'Bactigrass ', 'Smith & Nephew', 0),
(59, 'Sofratulle ', 'Sanofi', 0),
(60, 'Daryantulle ', 'Darya-Varia', 0),
(61, 'Lomatulle ', 'Lohmann & Rauscher', 0),
(62, 'Framycetin Sulfat ', 'Darya Varia', 0),
(63, 'Excel Tulle ', 'Exceltis', 0),
(64, 'Cutimed Hydro L ', 'BSN MEDICAL', 0),
(65, 'Duoderm ', 'Convatec', 0),
(66, 'Suprasorb H ', 'Lohmann Rauscher', 0),
(67, 'Elect Hydro ', 'Smith Nephew', 0),
(68, 'Exelcare extra thin ', 'Exeltis', 0),
(69, 'Modress hydrocolloid ', 'SOHO', 0),
(70, 'CUTIMED Gel ', 'BSN MEDICAL', 0),
(71, 'Intrasite Gel ', 'Smith Nephew', 0),
(72, 'Askina Gel ', 'Bbraun', 0),
(73, 'Prontosan Wound Gel ', 'Bbraun', 0),
(74, 'Duoderm hydro active gel ', 'Convatec', 0),
(75, 'Exelcare hydrogel ', 'Exeltis', 0),
(76, 'Cavidagel ', 'Global Dispomedika', 0),
(77, 'Cutimed Alginate ', 'BSN MEDICAL', 0),
(78, 'Durafiber ', 'Smith Nephew', 0),
(79, 'Modress alginate', 'SOHO', 0),
(80, 'Kaltostat ', 'Convatec', 0),
(81, 'Suprasorb A ', 'Lohmann & Rauscher', 0),
(82, 'Cutimed Siltec ', 'BSN MEDICAL', 0),
(83, 'Allevyn non adhesive, Allevyn life ', 'Smith & Nephew', 0),
(84, 'Askina Foam ', 'Bbraun', 0),
(85, 'Aquacell Foam Dressing non adhesive  ', 'Convatec', 0),
(86, 'Tegaderm? High Performance Foam Non-Adhesive Dressing ', '3M', 0),
(87, 'Silicone Foam Dressing ', 'Exeltis', 0),
(88, 'Exelcare dressing foam ', 'Winner', 0),
(89, 'Super Foam Carbon ', 'Winner', 0),
(90, 'Modress foam ', 'SOHO', 0),
(91, 'Suprasorb P ', 'Lohmann & Rauscher', 0),
(92, 'Therasorb ', 'Darya Varia', 0),
(93, 'W-Care  ', 'Mahakam', 0),
(94, 'Tensocrepe ', 'BSN MEDICAL', 0),
(95, 'Leukocrepe ', 'BSN MEDICAL', 0),
(96, 'Policrepe ', 'SNA Medika', 0),
(97, 'FM Crepe ', 'SNA Medika', 0),
(98, 'Pro Crepe ', 'SNA Medika', 0),
(99, ' LR Crepe ', 'Lohmann & Rauscher', 0),
(100, 'High Elastic Bandage', 'Winner', 0),
(101, 'Uniflex ', 'BSN MEDICAL', 0),
(102, 'PD crepe ', 'Pro Device', 0),
(103, 'Leukotape - K ', 'BSN MEDICAL', 0),
(104, 'KT Gold ', 'Kinesiology Corp', 0),
(105, ' Kindmax Kinesiology Tape ', 'Kindmax', 0),
(106, ' SPOL ', 'Spol', 0),
(107, ' N- Tape ', 'Gandasari', 0),
(108, 'Gypsona ', 'BSN MEDICAL', 0),
(109, 'Polygip ', 'SNA Medika', 0),
(110, 'Eko Gips ', 'NewMaw Medical Ltd', 0),
(111, 'Cellona ', 'Pro Device', 0),
(112, 'Delta lite Conformable ', 'BSN MEDICAL', 0),
(113, 'SNA Cast ', 'SNA Medika', 0),
(114, 'Orthocast ', 'SOHO', 0),
(115, 'Cellacast ', 'Pro Device', 0),
(116, 'Dynacast Prelude ', 'BSN MEDICAL', 0),
(117, 'Ortho Splint', 'SOHO', 0),
(118, 'Medisplint ', 'MEDISPLIN', 0),
(119, 'Soffban', 'BSN MEDICAL', 0),
(120, 'Poliban', 'SNA Medika', 0),
(121, 'Mediban', 'Onemed', 0),
(122, 'Delta Dry', 'BSN MEDICAL', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_all`
--

CREATE TABLE `tbl_grup_all` (
  `id_grup_all` int(10) UNSIGNED NOT NULL,
  `id_grup_1` int(10) UNSIGNED NOT NULL,
  `id_grup_2` int(10) UNSIGNED NOT NULL,
  `id_grup_3` int(10) UNSIGNED NOT NULL,
  `ukuran` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_all`
--

INSERT INTO `tbl_grup_all` (`id_grup_all`, `id_grup_1`, `id_grup_2`, `id_grup_3`, `ukuran`, `tipe`) VALUES
(1, 1, 1, 1, '5 CM X 1 M ( NEW )', 0),
(2, 1, 1, 1, '5 CM X 5 M (NEW)', 0),
(3, 1, 1, 1, '10 CM X 5 M ( NEW )', 0),
(4, 1, 1, 1, '15 CM X 5 M ( NEW )', 0),
(5, 1, 1, 1, '20 CM X 5 M (NEW)', 0),
(6, 1, 1, 2, '10 CM X 5 M', 0),
(7, 1, 1, 2, '15 CM X 5 M', 0),
(8, 1, 1, 2, '21 CM X 5 M', 0),
(9, 1, 1, 3, '5 CM X 5 M', 0),
(10, 1, 1, 3, '10 CM X 5 M', 0),
(11, 1, 1, 3, '10 CM X 10 M', 0),
(12, 1, 1, 3, '15 CM X 10 M', 0),
(13, 1, 1, 4, '5 CM X 5 M', 0),
(14, 1, 1, 4, '10 CM X 5 M', 0),
(15, 1, 1, 5, '5cm x 5m', 0),
(16, 1, 1, 5, '10cm x 5m', 0),
(17, 1, 1, 6, '5cm x 5m', 0),
(18, 1, 1, 6, '10cm x 5m', 0),
(19, 1, 1, 6, '15cm x 5m', 0),
(20, 1, 1, 7, '5cm x 5m', 0),
(21, 1, 1, 7, '10cm x 5m', 0),
(22, 1, 1, 7, '15cm x 5m', 0),
(23, 1, 2, 8, '1621H (0.5cm x 5m)', 0),
(24, 1, 2, 8, '1562 (1cm x 1m)', 0),
(25, 1, 2, 8, '1622 (1cm x 5m)', 0),
(26, 1, 2, 8, '1622H (1cm x 5m)', 0),
(27, 1, 2, 8, '1624 (2cm x 5m)', 0),
(28, 1, 2, 8, '1624H (2cm x 5m)', 0),
(29, 1, 2, 8, '1625 (3cm x 5m)', 0),
(30, 1, 2, 8, '1625H (3cm x 5m)', 0),
(31, 1, 2, 8, '1624H (5cm x 4,5m)', 0),
(32, 1, 2, 8, '7.5 CM x 4.5 M', 0),
(33, 1, 2, 8, '1625 H (5cm x 4 5m)', 0),
(34, 1, 2, 8, '1562 (5cmx1m) EDITION', 0),
(35, 1, 2, 9, '1,25 CM X 1 M', 0),
(36, 1, 2, 9, '1,25 CM X 5 M', 0),
(37, 1, 2, 10, '1.25 cm X 4.5 M', 0),
(38, 1, 2, 10, '2,5cm x 4,5m', 0),
(39, 1, 2, 10, '5cmx4.5m', 0),
(40, 1, 3, 11, '6 x 8 cm', 0),
(41, 1, 3, 12, '5 CM X 6 cm', 0),
(42, 1, 3, 12, '7 cm x 9 cm', 0),
(43, 1, 3, 13, '6cm x 7 cm', 0),
(44, 1, 3, 14, '6cm x 7 cm', 0),
(45, 1, 3, 15, '8cm x 6cm', 0),
(46, 1, 3, 16, '6cm x 8cm', 0),
(47, 1, 4, 17, '6cm x 4m', 0),
(48, 1, 4, 17, '8cm x 4m\n', 0),
(49, 1, 4, 17, '10cm x 4m\n', 0),
(50, 1, 4, 17, '6cm x 20m', 0),
(51, 1, 4, 17, '8cm x 20m', 0),
(52, 1, 4, 17, '10cm x 20m', 0),
(53, 1, 4, 18, '10cmx4m', 0),
(54, 1, 4, 19, '2,5cm', 0),
(55, 1, 4, 19, '5cm', 0),
(56, 1, 4, 19, '7,5cm', 0),
(57, 1, 4, 19, '10cm', 0),
(58, 1, 5, 20, '2453 ( 1.25CM x 9.2M (NEW)\n', 0),
(59, 1, 5, 20, '2454 (2,5CM x 9.2M)\n', 0),
(60, 1, 5, 20, '2455 (5CM x 9.2M (NEW)\n', 0),
(61, 1, 5, 20, '2456 (7,5cm x 9,2m)', 0),
(62, 1, 5, 21, '2.5 CM X 9.1 M', 0),
(63, 1, 5, 22, '0,5 x 10yds', 0),
(64, 1, 6, 23, '7.2cm x 5cm\n', 0),
(65, 1, 6, 23, '8cm x 10cm\n', 0),
(66, 1, 6, 23, '8cm x 15cm\n', 0),
(67, 1, 6, 23, '10cm x 25cm\n', 0),
(68, 1, 6, 23, '10cm x 30cm\n', 0),
(69, 1, 6, 23, '10cm x 35cm', 0),
(70, 1, 6, 24, '6.5 CM X 5 CM', 0),
(71, 1, 6, 24, '9.5 CM X 8.5 CM', 0),
(72, 1, 6, 24, '15 CM X 10 CM', 0),
(73, 1, 6, 24, '20 CM X 10 CM', 0),
(74, 1, 6, 25, '4,5 x 5cm', 0),
(75, 1, 6, 25, '7.6 x 9.5cm', 0),
(76, 1, 6, 26, '7.5 X 5 CM', 0),
(77, 1, 6, 26, '9 X 10 CM', 0),
(78, 1, 6, 26, '9 X 15 CM', 0),
(79, 1, 6, 26, '9 X 20CM', 0),
(80, 1, 6, 26, '9 X 25CM', 0),
(81, 1, 6, 26, '9 X 30CM', 0),
(82, 1, 6, 27, '5cm x 7cm', 0),
(83, 1, 6, 27, '10cm x 12cm', 0),
(84, 1, 6, 27, '10cm x 20cm', 0),
(85, 1, 6, 28, '10 x 15cm', 0),
(86, 1, 6, 28, '10 x 20cm', 0),
(87, 1, 6, 29, '6 x 7 cm', 0),
(88, 1, 6, 30, '8x15cm', 0),
(89, 1, 6, 31, '9cm x 150cm', 0),
(90, 1, 6, 31, '9cm x 15cm', 0),
(91, 1, 6, 31, '9cm x 20cm', 0),
(92, 1, 6, 32, '10 X 10CM', 0),
(93, 1, 6, 32, '10 X 15CM', 0),
(94, 1, 6, 32, '10 X 20CM', 0),
(95, 1, 6, 33, '7,5 x 5cm', 0),
(96, 1, 6, 33, '9 X 10 CM', 0),
(97, 1, 6, 33, '9 X 15 CM', 0),
(98, 1, 6, 33, '9 X 20CM', 0),
(99, 1, 6, 33, '9 X 25CM', 0),
(100, 1, 6, 33, '9 X 30CM', 0),
(101, 1, 7, 34, '10 x 10cm', 0),
(102, 1, 7, 34, '10 x 20cm', 0),
(103, 1, 7, 35, '10cm x 10cm', 0),
(104, 1, 7, 35, '10cm x 20cm', 0),
(105, 1, 7, 35, '20cm x 20cm', 0),
(106, 1, 7, 36, '7,5 X 7,5CM', 0),
(107, 1, 7, 36, '10 X 10CM', 0),
(108, 1, 7, 36, '10 X 20CM', 0),
(109, 1, 8, 37, '2WAY CH.14 (9385)', 0),
(110, 1, 8, 37, '2WAY CH.16 (9385)', 0),
(111, 1, 8, 37, '2WAY CH.18 (9385)', 0),
(112, 1, 8, 37, '2WAY CH.20 (9385)', 0),
(113, 1, 8, 37, '3WAY CH.22 (9388)', 0),
(114, 1, 8, 37, '3WAY CH.24 (9388)', 0),
(115, 1, 8, 37, '2WAY PED. CH6 (9413)', 0),
(116, 1, 8, 37, '2WAY PED. CH8 (9413)', 0),
(117, 1, 8, 37, '2WAY PED. CH10 (9413)', 0),
(118, 1, 8, 38, 'ch14', 0),
(119, 1, 8, 38, 'ch-16', 0),
(120, 1, 8, 38, 'ch-18', 0),
(121, 1, 8, 39, '10FR', 0),
(122, 1, 8, 39, '12FR', 0),
(123, 1, 8, 39, '16FR', 0),
(124, 1, 8, 39, '18FR', 0),
(125, 1, 8, 39, '20FR', 0),
(126, 1, 8, 40, '6FR', 0),
(127, 1, 8, 40, '8FR', 0),
(128, 1, 8, 40, '10FR', 0),
(129, 1, 8, 40, '12FR', 0),
(130, 1, 8, 40, '14FR', 0),
(131, 1, 8, 40, '16FR', 0),
(132, 1, 8, 40, '18FR', 0),
(133, 1, 8, 40, '20FR', 0),
(134, 1, 8, 40, '22FR', 0),
(135, 1, 8, 40, '24FR', 0),
(136, 1, 8, 41, '', 0),
(137, 1, 8, 42, '16FR', 0),
(138, 1, 8, 42, '18FR', 0),
(139, 1, 8, 43, '14FR', 0),
(140, 1, 8, 43, '16FR', 0),
(141, 1, 8, 43, '18FR', 0),
(142, 1, 8, 44, '', 0),
(143, 1, 8, 45, '14FR', 0),
(144, 1, 8, 45, '16FR', 0),
(145, 1, 8, 45, '18FR', 0),
(146, 1, 8, 46, '', 0),
(147, 1, 8, 47, '', 0),
(148, 1, 8, 48, '', 0),
(149, 2, 9, 49, '7 cm x 9 cm', 0),
(150, 2, 9, 50, '10 x 10cm', 0),
(151, 2, 9, 51, '10 x 10cm', 0),
(152, 2, 9, 51, '15 x 15cm', 0),
(153, 2, 9, 51, '20 x 20cm', 0),
(154, 2, 9, 52, '', 0),
(155, 2, 9, 53, '10 cm x 10 cm', 0),
(156, 2, 9, 53, '10cm x 20cm', 0),
(157, 2, 9, 54, '', 0),
(158, 2, 9, 55, '7,5cm x 7,5cm', 0),
(159, 2, 9, 55, '7,5cm x 15cm', 0),
(160, 2, 9, 56, '', 0),
(161, 2, 10, 57, '10cm x 10cm', 0),
(162, 2, 10, 57, '10cm x 40cm', 0),
(163, 2, 10, 58, '10cm x 10cm', 0),
(164, 2, 10, 59, '10cm x 10cm', 0),
(165, 2, 10, 60, '', 0),
(166, 2, 10, 61, '10cm x 10cm', 0),
(167, 2, 10, 62, '', 0),
(168, 2, 10, 63, '10cm x 10cm', 0),
(169, 2, 11, 64, '10cm x 10cm', 0),
(170, 2, 11, 65, '', 0),
(171, 2, 11, 66, '10 x 10cm', 0),
(172, 2, 11, 67, '10 x 10cm', 0),
(173, 2, 11, 68, '10 x 10cm', 0),
(174, 2, 11, 69, '', 0),
(175, 2, 12, 70, '1 x 25gr', 0),
(176, 2, 12, 71, '1 x15gr', 0),
(177, 2, 12, 72, '1 x15gr', 0),
(178, 2, 12, 73, '1 x 30ml', 0),
(179, 2, 12, 74, '1 x 30gr', 0),
(180, 2, 12, 75, '1 x 30gr', 0),
(181, 2, 12, 76, '1 x 15gr', 0),
(182, 2, 12, 76, '1x 30gr', 0),
(183, 2, 13, 77, '10cm x 10cm', 0),
(184, 2, 13, 78, '10cm x 10cm', 0),
(185, 2, 13, 79, '', 0),
(186, 2, 13, 80, '5x5cm', 0),
(187, 2, 13, 80, '7.5x12cm', 0),
(188, 2, 13, 80, '10x20cm', 0),
(189, 2, 13, 81, '5 x 5cm', 0),
(190, 2, 13, 81, '10 x 10cm', 0),
(191, 2, 14, 82, '10cm x 10cm', 0),
(192, 2, 14, 83, '5cm x 5cm', 0),
(193, 2, 14, 83, '10cm x 10cm', 0),
(194, 2, 14, 83, '15cm x 15cm', 0),
(195, 2, 14, 83, '20cm x 20cm', 0),
(196, 2, 14, 84, '5 cm x 7 cm', 0),
(197, 2, 14, 84, '10 cm x 10 cm', 0),
(198, 2, 14, 84, '10 cm x 20 cm', 0),
(199, 2, 14, 84, '20 cm x 20 cm', 0),
(200, 2, 14, 85, '10cm x 10cm', 0),
(201, 2, 14, 85, '17,5cm x 17,5cm', 0),
(202, 2, 14, 86, '', 0),
(203, 2, 14, 87, '10 x 10 cm', 0),
(204, 2, 14, 87, '15 x 15 cm', 0),
(205, 2, 14, 87, '20 x 20 cm', 0),
(206, 2, 14, 88, '10x10cm', 0),
(207, 2, 14, 88, '15 x 15 cm', 0),
(208, 2, 14, 89, '', 0),
(209, 2, 14, 90, '', 0),
(210, 2, 14, 91, '7,5 X 7,5cm', 0),
(211, 2, 14, 91, '10 X 10cm', 0),
(212, 2, 14, 91, '15 X 20cm', 0),
(213, 2, 14, 92, '10 x 10cm', 0),
(214, 2, 14, 93, '10 x 10cm', 0),
(215, 3, 15, 94, '7,5 x 2,3m', 0),
(216, 3, 15, 94, '10cm x 4.5m', 0),
(217, 3, 15, 94, '15cm x 4,5m', 0),
(218, 3, 15, 94, '7,5cm x 4,5m', 0),
(219, 3, 15, 94, '10cm x 4,5m', 0),
(220, 3, 15, 94, '15cm x 4,5m', 0),
(221, 3, 15, 95, '7,5cm x 4,5m', 0),
(222, 3, 15, 95, '10cm x 4,5m', 0),
(223, 3, 15, 95, '15cm x 4,5m', 0),
(224, 3, 15, 96, '7,5cm x 4,5m', 0),
(225, 3, 15, 96, '10cm x 4,5m', 0),
(226, 3, 15, 97, '7,5cm x 4,5cm', 0),
(227, 3, 15, 97, '7,5? cm X 4,5-4,55 m', 0),
(228, 3, 15, 97, '10 ? cm X 4,5-4,55 m', 0),
(229, 3, 15, 97, '15 ? cm X 4,5-4,55 m', 0),
(230, 3, 15, 97, '10cm x 4,5m', 0),
(231, 3, 15, 98, '', 0),
(232, 3, 15, 99, '', 0),
(233, 3, 15, 100, '5CMx5M BLUE 72978.21', 0),
(234, 3, 15, 100, '5CMx5M RED 72978.16', 0),
(235, 3, 15, 100, '5CMx5M SKIN 72978.11', 0),
(236, 3, 15, 100, 'Blue 5CMx5M 72978.28', 0),
(237, 3, 15, 100, 'Light Red 5CMx5M 72978.25', 0),
(238, 3, 15, 101, '', 0),
(239, 3, 15, 102, '5CM X 5M', 0),
(240, 3, 16, 103, '', 0),
(241, 3, 16, 104, '', 0),
(242, 3, 16, 105, '7,5cm x 2,75m', 0),
(243, 3, 16, 105, '7cm x 3,5m', 0),
(244, 3, 16, 105, '10cm x 3,5m', 0),
(245, 3, 16, 105, '15cm x 3,5m', 0),
(246, 3, 16, 105, '20cm x 3,5m', 0),
(247, 3, 16, 106, '', 0),
(248, 3, 16, 107, '', 0),
(249, 4, 17, 108, '', 0),
(250, 4, 17, 109, '5cm X 3.6m', 0),
(251, 4, 17, 109, '7.5cm x 3.6m', 0),
(252, 4, 17, 109, '10cm x 3.6m', 0),
(253, 4, 17, 109, '12.5cm x 3.6m', 0),
(254, 4, 17, 110, '16cm x 16cm', 0),
(255, 4, 17, 111, '', 0),
(256, 4, 18, 112, '', 0),
(257, 4, 18, 113, '2.5cm x 4.6m', 0),
(258, 4, 18, 113, '5cm x 4.6m', 0),
(259, 4, 18, 113, '7.5cm x 4.6m', 0),
(260, 4, 18, 113, '10cm x 4.6m', 0),
(261, 4, 18, 113, '12.5cm x 4.6m', 0),
(262, 4, 18, 113, '15cm x 4.6m', 0),
(263, 4, 18, 114, '', 0),
(264, 4, 18, 115, '', 0),
(265, 4, 19, 116, '5cm x 2.7m', 0),
(266, 4, 19, 116, '7.5cm x 2.7m', 0),
(267, 4, 19, 116, '10cm x 2.7m', 0),
(268, 4, 19, 116, '15cm x 2.7m', 0),
(269, 4, 19, 117, '7.5 cm x 2.7 m?', 0),
(270, 4, 19, 117, '10 cm x 2.7 m', 0),
(271, 4, 19, 117, '15 cm x 2.7 m', 0),
(272, 4, 20, 118, '7,5cm x 2,75m', 0),
(273, 4, 20, 119, '15cm x 2,7m', 0),
(274, 4, 20, 120, '5cm x 2.4m', 0),
(275, 4, 20, 120, '7.5cm x 2.4m', 0),
(276, 4, 20, 120, '10cm x 2.4m', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_edit`
--

CREATE TABLE `tbl_log_edit` (
  `id_log_edit` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_grup_2` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_audit`
--

CREATE TABLE `tbl_main_audit` (
  `id_audit` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `id_grup_all` int(10) UNSIGNED NOT NULL,
  `ketersediaan` tinyint(1) NOT NULL DEFAULT 0,
  `jumlah` int(10) UNSIGNED DEFAULT NULL,
  `harga` bigint(20) UNSIGNED DEFAULT NULL,
  `jumlah_q1` int(10) UNSIGNED DEFAULT NULL,
  `harga_q1` bigint(20) UNSIGNED DEFAULT NULL,
  `jumlah_q2` int(10) UNSIGNED DEFAULT NULL,
  `harga_q2` bigint(20) UNSIGNED DEFAULT NULL,
  `id_log_edit` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_rs`
--

CREATE TABLE `tbl_main_rs` (
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `id_rawdata` bigint(20) UNSIGNED NOT NULL,
  `tahun` smallint(5) UNSIGNED DEFAULT NULL,
  `nama_rs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelompok_kota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_pengelolaan` smallint(5) UNSIGNED DEFAULT NULL,
  `kelas` smallint(5) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_main_rs`
--

INSERT INTO `tbl_main_rs` (`id_rs`, `id_rawdata`, `tahun`, `nama_rs`, `alamat`, `kota`, `kelompok_kota`, `status_pengelolaan`, `kelas`, `status`) VALUES
(1, 143, 2020, 'RS MEDISTRA', 'JL GATOT SUBROTO KAV.59 JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(2, 903, 2020, 'SANSANI', 'JL. SOEKARNO HATTA ATAS PKU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(3, 145, 2020, 'RS HARAPAN BUNDA', 'JL. RAYA BOGOR KM.22 NO. 24 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(4, 1002, 2020, 'RSUD SITI FATIMAH PROVINSI SUMSEL', 'Jl. Kol. H. Burlian Km 6', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(5, 821, 2020, 'RSU ROYAL PRIMA', 'JL AGAHANTA NO 68 A MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(6, 818, 2020, 'RS RIDOS', 'JL MENTENG 7 NO 62', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(7, 160, 2020, 'RSUD TARAKAN', 'JL. KYAI CARINGIN NO.7   ', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(8, 1302, 2020, 'RSU PERMATA BUNDA', 'DI PINGKA KLADUIDAJ', 'Manado', 'Makassar+Manado', 0, 0, 0),
(9, 822, 2020, 'COLUMBIA ASIA MEDAN', 'JL LISTRIK NO 6A', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(10, 1428, 2020, 'RS. PARU DR M GUNAWAN CISARUA', 'JL. PUNCAK RAYA KM 83 CIBEREM CISARUA', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(11, 910, 2020, 'RSIA BUDHI MULIA', 'PEKAN BARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(12, 202, 2020, 'RS Umum Tk IV 03.07.03Sariningsih', 'JL. LL. RR MARTADINATA. BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(13, 402, 2020, 'RSK TUGU REJO', 'JL RAYA TUGUREJO TAMBAK AJI NGALINGAN', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(14, 206, 2020, 'RSIA MELINDA', 'Jl. Pajajaran No.46 Kel Pasirkaliki, Kec. Cicendo, Kota Bandung', 'Bandung', 'Bandung', 0, 0, 0),
(15, 825, 2020, 'RS MARTHA FRISKA', 'KOMPLEK MULTATULI', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(16, 1011, 2020, 'RS KARYA ASIH CHARITAS', 'PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(17, 819, 2020, 'RSU ESTOMIHI', 'JL SM RAJA NO 235', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(18, 146, 2020, 'RSU KARTIKA PULOMAS JAKARTA TIMUR', 'JL. PULOMAS BARAT IV C NO. 1 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(19, 403, 2020, 'RS KARIADI', 'JL DR SUTOMO NO 16 RANDUSARI', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(20, 308, 2020, 'RS WIYUNG SEJAHTERA', 'JL WARENGAN', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(21, 1403, 2020, 'RSIA TUMBUH KEMBANG ANAK', 'JL. RAYA BOGOR KM 31 NO. 23 CIMANGGIS DEPOK', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(22, 1402, 2020, 'BOGOR SENIOR HOSPITAL', 'JALAN RAYA TAJUR NO. 168 BOGOR', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(23, 318, 2020, 'RS PREMIERE SURABAYA', 'JL NGINDEN INTAN BARAT BLOK B JAWA TIMUR SUKOLILO', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(24, 811, 2020, 'RS SUFIWA AZIZ', 'JL KARYA BARU NO 1', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(25, 817, 2020, 'RSU IMELDA', 'JL. BILAL NO. 616', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(26, 606, 2020, 'RSIA PERMATA BUNDA', 'JL. NGEKSIGONDO NO. 56 YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(27, 101, 2020, 'RS J DR SOEHARTO HERDJAN', 'JL PROF LATUMENJEN NO 1 JAKARTA BARAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(28, 1101, 2020, 'RS PERMATA HATI', 'JL IMAM BONJOL NO 1', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(29, 397, 2020, 'RS DARMO', 'JL RAYA DARMO NO 90 DARMO', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(30, 1401, 2020, 'RS. UMUM HARAPAN DEPOK', 'JL. PEMUDA NO. 10 DEPOK', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(31, 162, 2020, 'RS SINT CAROLLUS', 'JL. SALEMBARAYA NO. 41 JAKARTA PUSAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(32, 324, 2020, 'RS WIJAYA', 'JL. MENGAUDI 398 WIYUNG SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(33, 812, 2020, 'RSU SUNDARI', 'JL TB SIMATUPANG/ JL PINANG BARAT NO 31 MEDAN SUNGGAL', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(34, 1425, 2020, 'RS MULIA PAJAJARAN', 'JL RAYA PAJAJARAN NO 98 RT02/RW03 BANTARJATI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(35, 127, 2020, 'RS UMUM TEBET', 'JL HARYONO MT NO 8 JAKSEL', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(36, 1009, 2020, 'RSIA AZ-ZAHRA PALEMBANG', 'Jl.Brigjend Hasan Kasim No. 1-2 Kel. Bukit Sangkal Kec. Kalidoni Palembang', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(37, 106, 2020, 'RS. SILOAM CILANDAK JAKARTA SELATAN', 'JL. RA. KARTINI NO. 8 CILANDAK RT.010/RW.004. CILANDAK BARAT. JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(38, 110, 2020, 'RS PATRIA IKKT', 'KOMP HAMKAM JL CENDRAWASIH NO 1 SLIPI PALMERAH RT 5 RW2', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(39, 115, 2020, 'RS Umum Daerah Kecamatan Tebet', 'JL PROF SOEPOMO SH NO 54 TEBET RT 13 RW02', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(40, 1404, 2020, 'RSU JATI SAMPURNA', 'JL STUDIO ANTEVE JATIRADEN JATI SAMPURNA KOTA BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(41, 503, 2020, 'CAKRA HUSADA', 'Jl. Merbabu klaten', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(42, 805, 2020, 'RSU MARTHA  FIRISKA P BRAYAN', 'JLN YOS SUDARSO', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(43, 808, 2020, 'rsu bakti medan', 'jl hm jhoni no 64', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(44, 820, 2020, 'RSU VINA ESTETIKA', 'JL. ISKANDAR MUDA NO. 119 MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(45, 904, 2020, 'RS BINA KASIH', 'JL DR SAMANHUDI NO 3 . 5 SAGO', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(46, 163, 2020, 'RS PANTAI INDAH KAPUK', 'JL PANTAI INDAH UTARA 3 JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(47, 209, 2020, 'RSUD AL IHSAN BALE ENDAH', 'JL. KI ASTRA MANGALA KAMPUNG BALE BALE BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(48, 1303, 2020, 'SITI MARYAM', 'JL PAGIDAN TUMITING', 'Manado', 'Makassar+Manado', 0, 0, 0),
(49, 304, 2020, 'RSI JEMUR SARI', 'RSI JEMUR SARI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(50, 139, 2020, 'RSU CINTA KASIH TZU CHI', 'JL KAMAL RAYA UOTER RING ROAD CENGKARENG JAKARTA BARAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(51, 111, 2020, 'RSUPN DR. CIPTO MANGUNKUSUMO', 'JL. PANGERAN DIPONEGORO NO.71 RW. 05 KENARI. SENEN KENARI. JAKARTA PUSAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(52, 205, 2020, 'RSU KARISMA', 'Jl. Raya Cimareme No. 235 RT. 03 RW. 01 Kec.Ngamprah', 'Bandung', 'Bandung', 0, 0, 0),
(53, 1012, 2020, 'RS KHUSUS MATA PROVINSI SUMATERA SELATAN', 'JL. ANGKATAN 45 IR. HARAPAN NO. 10 PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(54, 322, 2020, 'RS DELTA SURYA', 'RS DELTA SURYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(55, 806, 2020, 'rsu prof boloni', 'jl walter mongonsidi no 11', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(56, 807, 2020, 'RSU PERMATA BUNDA', 'JL. SISINGAMANGARAJA NO. 7 MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(57, 1417, 2020, 'RS ANANDA BEKASI', 'JL. SULTAN AGUNG NO. 173 BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(58, 909, 2020, 'RSU UNIVERSITAS RIAU', 'Kompleks Bina Widya KM 12,5 Simpang Baru, Panam PEKAN BARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(59, 161, 2020, 'RS ATMAJAYA', 'JL. PLUIT RAYA NO.2 PENJARINGAN JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(60, 330, 2020, 'RS ROYAL SURABAYA', 'JL. RUNGKUT INDUSTRI III SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(61, 702, 2020, 'RS JASEM', 'JL. SAMANHUDI NO. 85 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(62, 335, 2020, 'RS JIWA MENUR', 'JL RAYA MENUR SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(63, 504, 2020, 'RS PKU MUH SOLO', 'JL RONGGO WARSITO', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(64, 133, 2020, 'RSUD BUDI ASIH CAWANG', 'JL DEWI SARTIKA CAWANG', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(65, 108, 2020, 'RS Haji Pondok Gede', 'jl raya pondok gede pinang ranti makassar jaktim', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(66, 1305, 2020, 'RUMKIT TK II R.W MONGONSIDI', 'JL 14 FEBRUARI', 'Manado', 'Makassar+Manado', 0, 0, 0),
(67, 401, 2020, 'RS YAY KES TELOGOREDJO', 'JL KH. AHMAD DAHLAN PEKUNDEN SEMARANG', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(68, 117, 2020, 'RS UMUM TARUMA JAYA', 'JL RAYA TARUMA JAYA NO 18 PANTAI MAKMUR BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(69, 309, 2020, 'RS MANYAR MEDICAL CENTRE', 'JL MANYAR RAYA NO 9', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(70, 329, 2020, 'RS BHAKTI DARMA HUSADA', 'JL RAYA KENDUANG 115 SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(71, 501, 2020, 'RS DENTATAMA', 'JL PERINTIS KEMERDEKAAN SEAGEN', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(72, 1409, 2020, 'RS UMUM ASYIFAH', 'JL RAYA LEUWILIANG CIBEBER 1 KABUPATEN BOGOR', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(73, 712, 2020, 'RSUD SIDOARJO', 'JL MAJAPAHIT 9 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(74, 1307, 2020, 'RSUD PROV SULUT', 'JL BETHESDA NO 77', 'Manado', 'Makassar+Manado', 0, 0, 0),
(75, 204, 2020, 'RSU CAHYA KAWALUYAN', 'JL. PARAHIANGAN KM.3 BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(76, 406, 2020, 'RS COLUMBIA ASIA SMG', 'JL SILIWANGI NO 143 SEMARANG BARAT', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(77, 1004, 2020, 'RSIA BUNDA NONI', 'Jl. Srijaya Negara No.1 Rt.72 Rw.11 Kel. Bukit Lama', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(78, 157, 2020, 'RSUD KECAMATAN KRAMATJATI', 'JL.ROYA INPRES NO.48', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(79, 1005, 2020, 'RSIA RIKA AMELIA', 'JL. SULTAN MACHMUD BADARUDIN II NO. 18 RT.017/RW.004 PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(80, 1007, 2020, 'RSIA YK MADIRA PALEMBANG', 'Jl. Jendral Sudirman no. 1051 C-D-E KM 3.5', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(81, 405, 2020, 'RS PANTI WILASA CITARUM', 'JL CITARUM NO 98 KEBON AGUNG SEMARANG TIMUR', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(82, 907, 2020, 'RSU LANCANG KUNING', 'Jl Ronggowarsito Ujung No.5A Gobah Pekanbaru', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(83, 305, 2020, 'RUMAH SAKIT ISLAM SURABAYA', 'JL A YANI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(84, 311, 2020, 'RS  ISLAM SURABAYA', 'JL A YANI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(85, 1432, 2020, 'RS SANTHA ELISABETH', 'JL. RAYA NAROGONG NO. 202 KEMANG BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(86, 323, 2020, 'RS BUNDA SURABAYA', 'JL. KANDANGAN NO.23-24', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(87, 1427, 2020, 'RS BHAKTI YUDA', 'JL SAWANGAN NO2A PANCORAN MAS', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(88, 1204, 2020, 'RS KHODIJAH', 'Jl. RA. Kartini No. 15-17 Makassar', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(89, 1411, 2020, 'RS TIARA', 'JL BERINGIN RAYA NO 3-5', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(90, 107, 2020, 'RS ANDIKA', 'JL WARUNG SILAH NO 8 RT 6 RW4', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(91, 709, 2020, 'RS ISLAM SITI HAJAR', 'JL. RADEN PATAH NO. 70-72 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(92, 713, 2020, 'RSU MITRA KELUARGA WARU SIDOARJO', 'JL RAYA KALIJATEN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(93, 711, 2020, 'RSU ANWAR MEDIKA', 'JL RAYA BY PASS KRIAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(94, 156, 2020, 'RSU KECAMATAN MATRAMAN', 'JL KEBON KELAPA RAYA NO 29, UTAN KAYU UTARA, MATRAMAN RT 1/RW 10 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(95, 158, 2020, 'RS ROYAL TARUNA', 'JL. DAAN MOGOT NO.34 GROGOT PETAMBUTAN JAWA BARAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(96, 703, 2020, 'RSU ASSAKINAH MEDIKA', 'JL. BOGON KEBON AGUNG NO. 65 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(97, 714, 2020, 'RUMKITBAN 05 08 03 SIDOARJO', 'JL DR SOETOMO NO 2 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(98, 506, 2020, 'RSIA RESTU IBU', 'JL NGRAMPEL SRAGEN', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(99, 505, 2020, 'RSU KUMALA SIWI MIJEN KUDUS', 'Jl. Jepara', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(100, 327, 2020, 'RS MUJI RAHAYU', 'JL RAYA MANUKAN WETAN SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(101, 312, 2020, 'RS PURA RAHARJA', 'JL PUCANG ADI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(102, 313, 2020, 'RS SILOAM', 'JL RAYA GUBAG NO 70', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(103, 701, 2020, 'RS RAHMAN RAHIM', 'JL. SAUBANG. DESA KEBONAGUNG. SIDOARJO', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(104, 1206, 2020, 'RSU ALIYAH I', 'JL. BUNGGASI', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(105, 1406, 2020, 'RS AMINAH', 'jl hos cokroaminoto no4 larangan banten 15156', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(106, 1006, 2020, 'RS RK CHARITAS', ' Jl. Sudirman 1054,Palembang', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(107, 171, 2020, 'RS. SIAGA RAYA', 'JL. SIAGA RAYA NO KAV 4-8 PASAR MINGGU JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(108, 603, 2020, 'RSIA PKU MUHAMADIYAH', 'JL. KEMASAN NO. 43 PURBAYANI KOTA GEDE YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(109, 1433, 2020, 'RS SENTOSA', 'JL RAYA KEMANG PONDOK UDIK KECAMATAN KEMANG BOGOR', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(110, 326, 2020, 'RS GOTONG ROYONG', 'JL RAYA MAMKA WETAN 68A', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(111, 710, 2020, 'RSIA PRIMA HASADA', 'JL LETJEN SOETOOYO 03', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(112, 1415, 2020, 'rs umum bayangkara tk IV Bogor', 'jl kaptent nuslihad no 18 paledang bogor', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(113, 502, 2020, 'RS PANTI WALUYO', 'JL.A YANI NO. 01 RT. 004 RW. 014 KERTEN-LAWEYAN', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(114, 328, 2020, 'RS HAJI SURABAYA', 'JL MANYAR KESTOADI SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(115, 203, 2020, 'RS AL-ISLAM BANDUNG', 'JL. SOEKARNO HATTA NO. 644 KOTA BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(116, 1208, 2020, 'RS AWAL BROS', 'JL. URIP SUMOHARJO NO. 47 KARUWISI UTARA - PANAKUKANG', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(117, 1102, 2020, 'RS PERTAMINA', 'JL JENDRAL SUDIRMAN NO 1 BALIKPAPAN', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(118, 307, 2020, 'RS BERSALIN ADI GUNA', 'JL ALUN ALUN RANGKAH NO 3', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(119, 708, 2020, 'RS MATA FATIMA', 'JL KALI JATEN 40 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(120, 407, 2020, 'RS ELIZABETH', 'JL. KAWI NO.1 WONOTINGGAL KANDISARI', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(121, 1431, 2020, 'RS MELATI', 'JL. MERDEKA TANGERANG NO. 92', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(122, 906, 2020, 'RS KHUSUS MATA SMEC', 'Jl Arifin Ahmad No. 92 Kecamatan Marpoyan Damai Kota Pekanbaru', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(123, 1301, 2020, 'PANCARAN KASIH', 'JL SAMRATULANGI', 'Manado', 'Makassar+Manado', 0, 0, 0),
(124, 604, 2020, 'RSU TRIHARSI', 'JL. MONGINSIDI NO. 82 SURAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(125, 1003, 2020, 'RS MUSI MEDIKA CENDIKIA', 'Jl. Demang Lebar Daun Rt.035 Rw.010 Ilir Barat I Kec. Demang Lebar Daun', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(126, 336, 2020, 'RS Ibu dan Anak Bantuan 05.08.05 Surabaya', 'JL RAYA GUBENG POJOK NO 21', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(127, 130, 2020, 'RS MEDIKA PERMATA HIJAU', 'JL KEB LAMA NO 64 RT6 RW8 SUKABUMI SELATAN KEBON JERUK', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(128, 114, 2020, 'RS PURI INDAH', 'JL PURI INDAH RAYA BLOK S-2 KEMBANGAN RT 1 RW 2', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(129, 605, 2020, 'RS. PRATAMA KOTA YOGYAKARTA', 'JL. KOLONEL SUGIYONO BRONTOKUSUNAB YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(130, 908, 2020, 'RS ERIA BUNDA', 'KH AHMAD DAHLAN NO 163 PEKAN BARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(131, 1416, 2020, 'SENTRA MEDIKA', 'JL RAYA BOGOR KM 33', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(132, 334, 2020, 'RS JIWA MENUR', 'JL RAYA MENUR SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(133, 154, 2020, 'RSUD MAMPANG PRAPATAN', 'JL. MAMPANG PRAPATAN RAYA NO. 9 RT.001/RW.013 MAMPANG PRAPATAN JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(134, 404, 2020, 'RS PERMATA MEDIKA', 'JL MOCH ICHSAN NO 93 -97 NGALINGAN', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(135, 913, 2020, 'RS TK 4 PEKANBARU', 'JL. KESEHATAN NO. 2 PEKANBARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(136, 1216, 2020, 'RS SAYANG RAKYAT', 'JL PAHLAWAN NO 10000 MAKASAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(137, 1103, 2020, 'RDS HARDJANTO', 'JL TANJUNG PURA NO 1 BALIKPAPAN', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(138, 705, 2020, 'RS UMUM BUNDA', 'JL KUNDI NO 70 KEPUH KIRIMAN WARU SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(139, 128, 2020, 'RS SUKMUL', 'JL TAWES NO 18-20 RT4 RW8 TANJUNG PRIOK', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(140, 201, 2020, 'RS UMUM BUNGSU', 'JL VETERAN NO 6 BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(141, 1213, 2020, 'RS FAISAL', 'JL AP PEHARANI MAKASAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(142, 1207, 2020, 'RS BUDI MULIA', 'JL. NIKEL', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(143, 1104, 2020, 'RSB KASIH BUNDA', 'JL LETJEND S.PARMAN NO 06 RT23 KEL GUNUNG SARI', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(144, 608, 2020, 'RS PANTI RAPIH', 'JL. TEUKU CIKDITIRO 30 YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(145, 319, 2020, 'RSIA LOMBOK DUA DUA', 'JL RAYA Lontar', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(146, 320, 2020, 'RSIA LOMBOK DUA DUA', 'JL RAYA Lontar', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(147, 704, 2020, 'RS KIRANA', 'JL NGELOM NO 87 TAMAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(148, 601, 2020, 'RS KHUSUS IBU DAN ANAK FAJAR', 'JL BUGISAN NO 6/9', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(149, 1422, 2020, 'RS BHAKTI ASIH ', 'JL. RADEN SALEH NO.10 CILEDUK KARANG TENGAH', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(150, 1008, 2020, 'RS BHAYANGKARA', 'JL. JENDRAL SUDIRMAN KM. 45 PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(151, 1304, 2020, 'RS SILOAM MANADO', 'JL SAM RATULANGI NO 22 WENANG UTARA', 'Manado', 'Makassar+Manado', 0, 0, 0),
(152, 911, 2020, 'RS PROF DR TABRANI', 'JL JENDRAL SUDIRMAN 410 PEKANBARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(153, 1105, 2020, 'RS UMUM DAERAH BERIMAN', 'JL MS SUTOYO NO30 KEL GUNUNG ARI ULU', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(154, 166, 2020, 'RS POLRI BHAYANGKARA SOEKAMTO', 'JL. KRAMATJATI', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(155, 105, 2020, 'RS PS REBO', 'JL TB SIMATUPANG NO 30 PS REBO JAKTIM\\', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(156, 125, 2020, 'RSIA RESTI MULYA', 'JL PAHLAWAN KOMARUDIN RAYA NO 5 RT 011 RW 05 PENGGILINGAN CAKUNG JAKTIM', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(157, 1414, 2020, 'RS CENTRA MEDIKA CIBINONG', 'JL MAYOR O KING JAYA ANDAYA NO 9', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(158, 507, 2020, 'RS KASIH IBU', 'JL BRIG JEND SLAMET RIYADI NO 404 RT01/RW10', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(159, 165, 2020, 'RS FATMAWATI', 'JL. RS FATMAWATI NO. 4 CILANDAK. CILANDAK BARAT JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(160, 1217, 2020, 'RS GRESTELINA', 'JL HERSTANING NO 51 MAKASAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(161, 1205, 2020, 'RSUP DR WAHIDIN SUDIRO HUSODO', 'JL PERINTIS KEMERDEKAAN KM 10 TAMALAHREA', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(162, 901, 2020, 'IBNU SINA', 'JL MELATI NO 60 PKU HARJOSARI', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(163, 815, 2020, 'RUMAH SAKIT SITI HAJAR', 'JL LETJEN JAMIUN GINTING', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(164, 816, 2020, 'RS IBNU SALEH', 'JL H M JONI NO 64 63', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(165, 333, 2020, 'RSIA IBI', 'JL DUPAK 15A', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(166, 706, 2020, 'RS IBU DAN ANAK SOERYA', 'JL RAYA KALIJATEN 11-15 TAMAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(167, 707, 2020, 'RSU ANWAR MEDIKA', 'JL RAYA BY PASS KRIAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(168, 1203, 2020, 'RS PELAMONIA', 'JENDRAL SUDERMAN NO 27', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(169, 208, 2020, 'BHAYANGKARA TKII SARTIKA ASIH', 'JL MOH TOHA 369', 'Bandung', 'Bandung', 0, 0, 0),
(170, 1215, 2020, 'RSA PERMATA HATI', 'JL BTP BLOK M NO.9 MAKASSAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(171, 153, 2020, 'RS UMUM TRIA DIPA', 'JL. RAYA PASAR MINGGU34 JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(172, 151, 2020, 'RS. SETIA MITRA', 'JL. FATMAWATI NO. 80-82 JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(173, 152, 2020, 'RS. KESDAM JAYA', 'JL. MAHONI CIJANTUNG JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(174, 167, 2020, 'RS SATYA NEGARA', 'JL. AGUNG UTARA RAYA SUNTER JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(175, 170, 2020, 'RS ANTAM MEDIKA', 'JL. RAYA PEMUDA NO. 1A PULOGADUNG', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(176, 210, 2020, 'RSIA GRAHA BUNDA', 'JL. TERUSAN JAKARTA NO. 15-17 BABAKAN BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(177, 212, 2020, 'RSIA HANDAYANI', 'JL. GM BATU NO 7 BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(178, 339, 2020, 'RSIA KENDANGSARI MERR SURABAYA', 'JL DR IR H SOEKAMO NO 2 SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(179, 340, 2020, 'RS TNI AU SOEMITRO', 'JL RAYA SERAYU NO 17 SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(180, 607, 2020, 'RSIA RACHMI', 'JL. WAHID HASYIM NO. 47 NOTOPRAJAN NGAMJILAN YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(181, 169, 2020, 'RS AQIDAH', 'JL. RADEN FATAH NO. 40', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(182, 804, 2020, 'RSU TERE MARGARETH', 'JL RINGREAD', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(183, 809, 2020, 'RSU TERE MARGARETH', 'JL RINGROAD', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(184, 814, 2020, 'RSU MADANI', 'JL AR HAKIM NO 168', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(185, 914, 2020, 'RS PRIMA PEKANBARU', 'JL. BIMA NO. 1 TUANKU TAMBUSAI KELURAHAN DELIMA KECAMATAN TAMPAN KOTA PEKANBARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(186, 823, 2020, 'RSU MURNI TEGUH', 'JL JAWA NO 2 GG BUNTU', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(187, 1209, 2020, 'RSP UNHAS MAKASSAR', 'JL. PERINTIS KEMERDEKAAN KM. 11 MAKASSAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(188, 1210, 2020, 'RSIA SENTOSA', 'JL. JENDRAL SUDIRMAN NO. 52 SAWERIGADING MAKASSAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(189, 131, 2020, 'RS ISLAM PONDOK KOPI', 'JL RAYA PONDOK KOPI JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(190, 132, 2020, 'RS PELABUHAN JAKARTA', 'JL KERAMAT JAYA 1 TUGU UTARA KOJA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(191, 303, 2020, 'RSIA PUTRI', 'JL ARIF RAHMAN HAKIM', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(192, 207, 2020, 'RS RAJAWALI', 'JL. RAJAWALI BARAT NO. 38 MALEBER. BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(193, 902, 2020, 'RS ANNISA', 'JL GARUDA NO 66 TANGERANG MAPOYAN DAMAI', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(194, 810, 2020, 'RSU H ADAM MALIK', 'JL BUNGA LAU NO 17', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(195, 1202, 2020, 'RSUP DR WAHIDIN SUDIRO HUSODO', 'JL PERINTIS KEMERDEKAAN KM 10 TAMALAHREA', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(196, 1413, 2020, 'RS BUAH HATI CIPUTAT', 'JL ARIA PUTRA NO 399 SERUA INDAH CIPUTAT TANGSEL', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(197, 302, 2020, 'RSIA NUR UMMI NUMBI', 'RSIA NUR UMMI NUMBI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(198, 103, 2020, 'RS ZAHIRA', 'JL SIRSAK NO 21 RT9 RW 1 JAGAKARSA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(199, 1201, 2020, 'RS BHYANGKARA MAKASSSAR', 'JL.LETJEN ANDI MAPPAODDANG JONGAYA - TAMALATE', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(200, 211, 2020, 'RS PARU DR. HA ROTINSULU', 'JL. BUKIT JARIAN NO. 40 CIPAGANTI BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(201, 508, 2020, 'RS UMUM DR KUSTATI', 'JL KAPT MULYADI 249 PS KLIWON SOLO', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(202, 316, 2020, 'RSIA PUSURA TEGALSARI', 'JL TEGALSARI JAWA TIMUR 60261', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(203, 144, 2020, 'RSJ DHARMAWANGSA', 'JL. DHARMAWANGSA RAYA NO. 13', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(204, 155, 2020, 'RS FIRDAUS', 'KOMPLEK BEA CUKAI JL SIAK NO 14 CILINCING JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(205, 915, 2020, 'RS SYAFIRA', 'JL. JENDRAL SUDIRMAN NO. 134 PEKANBARU 28252', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(206, 802, 2020, 'RS BUNDA THAMRIN', 'JL SAI BATANG HARI NO 28-30 MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(207, 150, 2020, 'RS. HARAPAN JAYAKARTA', 'JL. BEKASI TIMUR KM. 18 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_1_all`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_1_all` (
`id_grup_all` int(10) unsigned
,`id_grup_1` int(10) unsigned
,`nama_grup_1` varchar(191)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_2_all`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_2_all` (
`id_grup_all` int(10) unsigned
,`id_grup_1` int(10) unsigned
,`id_grup_2` int(10) unsigned
,`nama_grup_2` varchar(191)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_all`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_all` (
`id_grup_all` int(10) unsigned
,`id_grup_1` int(10) unsigned
,`nama_grup_1` varchar(191)
,`id_grup_2` int(10) unsigned
,`nama_grup_2` varchar(191)
,`nama_lini` varchar(191)
,`id_grup_3` int(10) unsigned
,`nama_grup_3` varchar(191)
,`brand` varchar(191)
,`ukuran` varchar(191)
,`tipe` smallint(5) unsigned
,`nama_tipe` varchar(3)
,`status_grup_2` tinyint(1)
,`status_grup_3` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_fill_revenue`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_fill_revenue` (
`id_grup_all` int(10) unsigned
,`id_grup_3` int(10) unsigned
,`nama_grup_3` varchar(191)
,`nama_brand` varchar(385)
,`ukuran` varchar(191)
,`tipe` smallint(5) unsigned
,`nama_tipe` varchar(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_last_progress`
-- (See below for the actual view)
--
CREATE TABLE `vw_last_progress` (
`id_rs` bigint(20)
,`id_grup_2` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_log_edit`
-- (See below for the actual view)
--
CREATE TABLE `vw_log_edit` (
`id_log_edit` bigint(20) unsigned
,`id_rs` bigint(20)
,`id_rawdata` bigint(20) unsigned
,`nama_grup_2` varchar(191)
,`nama` varchar(191)
,`tahun` smallint(5) unsigned
,`lokasi` varchar(191)
,`ip` varchar(191)
,`tanggal_akses` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rawdata`
-- (See below for the actual view)
--
CREATE TABLE `vw_rawdata` (
`ketersediaan` tinyint(1)
,`id_grup_all` int(10) unsigned
,`tahun` smallint(5) unsigned
,`id_rs` bigint(20) unsigned
,`nama_rs` varchar(191)
,`alamat` mediumtext
,`kota` varchar(191)
,`kelompok_kota` varchar(191)
,`status_pengelolaan` smallint(5) unsigned
,`kelas` smallint(5) unsigned
,`nama_grup_1` varchar(191)
,`nama_lini` varchar(191)
,`nama_grup_2` varchar(191)
,`nama_grup_3` varchar(387)
,`nama_grup_3_ukuran` text
,`brand` varchar(191)
,`satuan` varchar(3)
,`jumlah` int(10) unsigned
,`harga` bigint(20) unsigned
,`total` bigint(30) unsigned
,`jumlah_q1` int(10) unsigned
,`harga_q1` bigint(20) unsigned
,`total_q1` bigint(30) unsigned
,`proporsi` varchar(18)
,`status_grup_2` varchar(3)
,`status_produk` varchar(3)
,`status_rs` varchar(3)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_1_all`
--
DROP TABLE IF EXISTS `vw_grup_1_all`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grup_1_all`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_1` AS `id_grup_1`,`tbl_grup_1`.`nama_grup_1` AS `nama_grup_1` from (`tbl_grup_all` join `tbl_grup_1` on(`tbl_grup_all`.`id_grup_1` = `tbl_grup_1`.`id_grup_1`)) order by `tbl_grup_1`.`id_grup_1` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_2_all`
--
DROP TABLE IF EXISTS `vw_grup_2_all`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grup_2_all`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_1` AS `id_grup_1`,`tbl_grup_all`.`id_grup_2` AS `id_grup_2`,`tbl_grup_2`.`nama_grup_2` AS `nama_grup_2` from (`tbl_grup_all` join `tbl_grup_2` on(`tbl_grup_all`.`id_grup_2` = `tbl_grup_2`.`id_grup_2`)) order by `tbl_grup_all`.`id_grup_2` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_all`
--
DROP TABLE IF EXISTS `vw_grup_all`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grup_all`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_1` AS `id_grup_1`,`tbl_grup_1`.`nama_grup_1` AS `nama_grup_1`,`tbl_grup_all`.`id_grup_2` AS `id_grup_2`,`tbl_grup_2`.`nama_grup_2` AS `nama_grup_2`,`tbl_grup_2`.`nama_lini` AS `nama_lini`,`tbl_grup_all`.`id_grup_3` AS `id_grup_3`,`tbl_grup_3`.`nama_grup_3` AS `nama_grup_3`,`tbl_grup_3`.`brand` AS `brand`,`tbl_grup_all`.`ukuran` AS `ukuran`,`tbl_grup_all`.`tipe` AS `tipe`,case `tbl_grup_all`.`tipe` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `nama_tipe`,`tbl_grup_2`.`status_grup_2` AS `status_grup_2`,`tbl_grup_3`.`status_grup_3` AS `status_grup_3` from (((`tbl_grup_all` join `tbl_grup_1` on(`tbl_grup_all`.`id_grup_1` = `tbl_grup_1`.`id_grup_1`)) join `tbl_grup_2` on(`tbl_grup_all`.`id_grup_2` = `tbl_grup_2`.`id_grup_2`)) join `tbl_grup_3` on(`tbl_grup_all`.`id_grup_3` = `tbl_grup_3`.`id_grup_3`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_fill_revenue`
--
DROP TABLE IF EXISTS `vw_grup_fill_revenue`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_grup_fill_revenue`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_3` AS `id_grup_3`,`tbl_grup_3`.`nama_grup_3` AS `nama_grup_3`,concat(`tbl_grup_3`.`nama_grup_3`,' (',`tbl_grup_3`.`brand`,')') AS `nama_brand`,`tbl_grup_all`.`ukuran` AS `ukuran`,`tbl_grup_all`.`tipe` AS `tipe`,case `tbl_grup_all`.`tipe` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `nama_tipe` from (`tbl_grup_all` join `tbl_grup_3` on(`tbl_grup_all`.`id_grup_3` = `tbl_grup_3`.`id_grup_3`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_last_progress`
--
DROP TABLE IF EXISTS `vw_last_progress`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_last_progress`  AS  select `tbl_log_edit`.`id_rs` AS `id_rs`,max(`tbl_log_edit`.`id_grup_2`) AS `id_grup_2` from `tbl_log_edit` group by `tbl_log_edit`.`id_rs` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_log_edit`
--
DROP TABLE IF EXISTS `vw_log_edit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_log_edit`  AS  select `tbl_log_edit`.`id_log_edit` AS `id_log_edit`,`tbl_log_edit`.`id_rs` AS `id_rs`,`tbl_main_rs`.`id_rawdata` AS `id_rawdata`,`tbl_grup_2`.`nama_grup_2` AS `nama_grup_2`,`tbl_main_rs`.`nama_rs` AS `nama`,`tbl_main_rs`.`tahun` AS `tahun`,`tbl_log_edit`.`lokasi` AS `lokasi`,`tbl_log_edit`.`ip` AS `ip`,`tbl_log_edit`.`updated_at` AS `tanggal_akses` from ((`tbl_log_edit` join `tbl_main_rs` on(`tbl_log_edit`.`id_rs` = `tbl_main_rs`.`id_rs`)) join `tbl_grup_2` on(`tbl_grup_2`.`id_grup_2` = `tbl_grup_2`.`id_grup_2`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rawdata`
--
DROP TABLE IF EXISTS `vw_rawdata`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rawdata`  AS  select `tbl_main_audit`.`ketersediaan` AS `ketersediaan`,`vw_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_main_rs`.`tahun` AS `tahun`,`tbl_main_audit`.`id_rs` AS `id_rs`,`tbl_main_rs`.`nama_rs` AS `nama_rs`,`tbl_main_rs`.`alamat` AS `alamat`,`tbl_main_rs`.`kota` AS `kota`,`tbl_main_rs`.`kelompok_kota` AS `kelompok_kota`,`tbl_main_rs`.`status_pengelolaan` AS `status_pengelolaan`,`tbl_main_rs`.`kelas` AS `kelas`,`vw_grup_all`.`nama_grup_1` AS `nama_grup_1`,`vw_grup_all`.`nama_lini` AS `nama_lini`,`vw_grup_all`.`nama_grup_2` AS `nama_grup_2`,concat(`vw_grup_all`.`nama_grup_3`,' ( ',`vw_grup_all`.`brand`,' )') AS `nama_grup_3`,concat(`vw_grup_all`.`nama_grup_3`,' ( ',`vw_grup_all`.`brand`,' ) ',' ',`vw_grup_all`.`ukuran`) AS `nama_grup_3_ukuran`,`vw_grup_all`.`brand` AS `brand`,case `vw_grup_all`.`tipe` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `satuan`,`tbl_main_audit`.`jumlah` AS `jumlah`,`tbl_main_audit`.`harga` AS `harga`,`tbl_main_audit`.`jumlah` * `tbl_main_audit`.`harga` AS `total`,`tbl_main_audit`.`jumlah_q1` AS `jumlah_q1`,`tbl_main_audit`.`harga_q1` AS `harga_q1`,`tbl_main_audit`.`jumlah_q1` * `tbl_main_audit`.`harga_q1` AS `total_q1`,concat(`tbl_main_audit`.`jumlah` / `tbl_main_audit`.`jumlah_q1`,' % ') AS `proporsi`,case `vw_grup_all`.`status_grup_2` when '0' then '' when '1' then 'NEW' end AS `status_grup_2`,case `vw_grup_all`.`status_grup_3` when '0' then '' when '1' then 'NEW' end AS `status_produk`,case `vw_grup_all`.`tipe` when '0' then '' when '1' then 'NEW' end AS `status_rs` from ((`tbl_main_audit` join `vw_grup_all` on(`tbl_main_audit`.`id_grup_all` = `vw_grup_all`.`id_grup_all`)) join `tbl_main_rs` on(`tbl_main_audit`.`id_rs` = `tbl_main_rs`.`id_rs`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`id_config`);

--
-- Indexes for table `tbl_form`
--
ALTER TABLE `tbl_form`
  ADD PRIMARY KEY (`id_form`);

--
-- Indexes for table `tbl_grup_1`
--
ALTER TABLE `tbl_grup_1`
  ADD PRIMARY KEY (`id_grup_1`);

--
-- Indexes for table `tbl_grup_2`
--
ALTER TABLE `tbl_grup_2`
  ADD PRIMARY KEY (`id_grup_2`);

--
-- Indexes for table `tbl_grup_3`
--
ALTER TABLE `tbl_grup_3`
  ADD PRIMARY KEY (`id_grup_3`);

--
-- Indexes for table `tbl_grup_all`
--
ALTER TABLE `tbl_grup_all`
  ADD PRIMARY KEY (`id_grup_all`),
  ADD KEY `tbl_grup_all_id_grup_1_foreign` (`id_grup_1`),
  ADD KEY `tbl_grup_all_id_grup_2_foreign` (`id_grup_2`),
  ADD KEY `tbl_grup_all_id_grup_3_foreign` (`id_grup_3`);

--
-- Indexes for table `tbl_log_edit`
--
ALTER TABLE `tbl_log_edit`
  ADD PRIMARY KEY (`id_log_edit`);

--
-- Indexes for table `tbl_main_audit`
--
ALTER TABLE `tbl_main_audit`
  ADD PRIMARY KEY (`id_audit`),
  ADD KEY `tbl_main_audit_id_rs_foreign` (`id_rs`),
  ADD KEY `tbl_main_audit_id_grup_all_foreign` (`id_grup_all`),
  ADD KEY `tbl_main_audit_id_log_edit_foreign` (`id_log_edit`);

--
-- Indexes for table `tbl_main_rs`
--
ALTER TABLE `tbl_main_rs`
  ADD PRIMARY KEY (`id_rs`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_config`
--
ALTER TABLE `tbl_config`
  MODIFY `id_config` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_form`
--
ALTER TABLE `tbl_form`
  MODIFY `id_form` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_grup_1`
--
ALTER TABLE `tbl_grup_1`
  MODIFY `id_grup_1` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_grup_2`
--
ALTER TABLE `tbl_grup_2`
  MODIFY `id_grup_2` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_grup_3`
--
ALTER TABLE `tbl_grup_3`
  MODIFY `id_grup_3` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `tbl_grup_all`
--
ALTER TABLE `tbl_grup_all`
  MODIFY `id_grup_all` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- AUTO_INCREMENT for table `tbl_log_edit`
--
ALTER TABLE `tbl_log_edit`
  MODIFY `id_log_edit` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_main_audit`
--
ALTER TABLE `tbl_main_audit`
  MODIFY `id_audit` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_main_rs`
--
ALTER TABLE `tbl_main_rs`
  MODIFY `id_rs` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_grup_all`
--
ALTER TABLE `tbl_grup_all`
  ADD CONSTRAINT `tbl_grup_all_id_grup_1_foreign` FOREIGN KEY (`id_grup_1`) REFERENCES `tbl_grup_1` (`id_grup_1`),
  ADD CONSTRAINT `tbl_grup_all_id_grup_2_foreign` FOREIGN KEY (`id_grup_2`) REFERENCES `tbl_grup_2` (`id_grup_2`),
  ADD CONSTRAINT `tbl_grup_all_id_grup_3_foreign` FOREIGN KEY (`id_grup_3`) REFERENCES `tbl_grup_3` (`id_grup_3`);

--
-- Constraints for table `tbl_main_audit`
--
ALTER TABLE `tbl_main_audit`
  ADD CONSTRAINT `tbl_main_audit_id_grup_all_foreign` FOREIGN KEY (`id_grup_all`) REFERENCES `tbl_grup_all` (`id_grup_all`),
  ADD CONSTRAINT `tbl_main_audit_id_log_edit_foreign` FOREIGN KEY (`id_log_edit`) REFERENCES `tbl_log_edit` (`id_log_edit`),
  ADD CONSTRAINT `tbl_main_audit_id_rs_foreign` FOREIGN KEY (`id_rs`) REFERENCES `tbl_main_rs` (`id_rs`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

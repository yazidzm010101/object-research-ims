<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblGrup2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_grup_2');
        Schema::create('tbl_grup_2', function (Blueprint $table) {
            $table->Increments('id_grup_2');
            $table->string('nama_grup_2');
            $table->string('nama_lini');
            $table->boolean('status_grup_2')->default(1);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_grup_2');
    }
}

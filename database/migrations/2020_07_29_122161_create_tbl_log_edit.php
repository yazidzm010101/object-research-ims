<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblLogEdit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_log_edit');
        Schema::create('tbl_log_edit', function (Blueprint $table) {
            $table->bigIncrements('id_log_edit');
            $table->bigInteger('id_rs');
            $table->string('lokasi');
            $table->unsignedInteger('id_grup_2');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_log_edit');
    }
}

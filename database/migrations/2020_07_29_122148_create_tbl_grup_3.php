<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblGrup3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_grup_3');
        Schema::create('tbl_grup_3', function (Blueprint $table) {
            $table->Increments('id_grup_3');
            $table->string('nama_grup_3');
            $table->string('brand');
            $table->boolean('status_grup_3')->default(1);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_grup_3');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblMainRs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_main_rs');
        Schema::create('tbl_main_rs', function (Blueprint $table) {
            $table->BigIncrements('id_rs');
            $table->unsignedBigInteger('id_rawdata');
            $table->unsignedSmallInteger('tahun')->nullable();
            $table->string('nama_rs')->nullable();
            $table->mediumText('alamat')->nullable();
            $table->string('kota')->nullable();
            $table->string('kelompok_kota')->nullable();
            $table->unsignedSmallInteger('status_pengelolaan')->nullable();
            $table->unsignedSmallInteger('kelas')->nullable();
            $table->boolean('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_main_rs');
    }
}

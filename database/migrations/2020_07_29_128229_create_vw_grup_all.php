<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwGrupAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_grup_all AS
            SELECT
            id_grup_all,
            tbl_grup_all.id_grup_1, tbl_grup_1.nama_grup_1,
            tbl_grup_all.id_grup_2, tbl_grup_2.nama_grup_2, nama_lini,
            tbl_grup_all.id_grup_3, tbl_grup_3.nama_grup_3, brand,
            ukuran, tipe,
            CASE tipe
                when '0' then 'Box'
                when '1' then 'Rol'
                when '2' then 'Pcs'
            END as nama_tipe ,
            tbl_grup_2.status_grup_2, tbl_grup_3.status_grup_3 from tbl_grup_all
            INNER JOIN tbl_grup_1 ON tbl_grup_all.id_grup_1 = tbl_grup_1.id_grup_1
            INNER JOIN tbl_grup_2 ON tbl_grup_all.id_grup_2 = tbl_grup_2.id_grup_2
            INNER JOIN tbl_grup_3 ON tbl_grup_all.id_grup_3 = tbl_grup_3.id_grup_3;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vw_grup_all");
    }
}

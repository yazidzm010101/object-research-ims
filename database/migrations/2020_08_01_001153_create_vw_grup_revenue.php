<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwGrupRevenue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_grup_fill_revenue AS
            SELECT
            id_grup_all,
            tbl_grup_all.id_grup_3, tbl_grup_3.brand as nama_manufacture,  CONCAT (tbl_grup_3.nama_grup_3 ,' (',brand,')') as nama_brand,
            ukuran, tipe,
            CASE tipe
                when '0' then 'Box'
                when '1' then 'Rol'
                when '2' then 'Pcs'
            END as nama_tipe from tbl_grup_all
            INNER JOIN tbl_grup_3 ON tbl_grup_all.id_grup_3 = tbl_grup_3.id_grup_3;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vw_grup_fill_revenue");
    }
}

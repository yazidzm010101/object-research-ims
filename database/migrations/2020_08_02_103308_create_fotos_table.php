<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_quest_foto');
        Schema::create('tbl_quest_foto', function (Blueprint $table) {
            // id , id rs, id form
            $table->bigIncrements('id_foto');
            $table->unsignedBigInteger('id_rs');

            $table->tinyInteger('jenis');
            $table->text('foto');

            //timestamps
            $table->timestamps();

            // foreign keys
            $table->foreign('id_rs')->references('id_rs')->on('tbl_main_rs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_quest_foto');
    }
}

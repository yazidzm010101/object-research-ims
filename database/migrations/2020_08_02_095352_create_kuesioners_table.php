<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKuesionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_quest_kuesioner');
        Schema::create('tbl_quest_kuesioner', function (Blueprint $table) {
            // id, id rumah sakit, id formulir pengisian
            $table->bigIncrements('id_kuesioner');
            $table->unsignedBigInteger('id_rs')->unique();

            // data wawancara
            $table->string('interviewer')->nullable();
            $table->date('tanggal')->nullable();
            $table->time('waktu_mulai', 0)->nullable();
            $table->time('waktu_selesai', 0)->nullable();

            // identifikasi rumah sakit
            $table->year('tahun_berdiri')->nullable();
            $table->unsignedDecimal('luas', 7, 2)->nullable();

            // data sales
            $table->unsignedSmallInteger('frekuensi')->nullable();
            $table->text('program')->nullable();
            $table->decimal('pendapatan', 5, 2)->nullable();

            // timestamp
            $table->timestamps();

            // foreign keys
            $table->foreign('id_rs')->references('id_rs')->on('tbl_main_rs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_quest_kuesioner');
    }
}

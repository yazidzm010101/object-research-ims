<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwRawData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_rawdata AS
            SELECT
            CASE tbl_main_audit.ketersediaan
                WHEN '0' THEN 'Tidak'
                WHEN '1' THEN 'Iya'
            END AS ketersediaan,
            vw_grup_all.id_grup_all, tbl_main_rs.tahun, tbl_main_rs.id_rs, tbl_main_rs.id_rawdata, nama_rs, alamat, kota, kelompok_kota,
            CASE status_pengelolaan
                WHEN '1' THEN 'Public'
                WHEN '2' THEN 'Private'
            END as status_pengelolaan,
            CASE kelas
                WHEN '1' THEN 'A'
                WHEN '2' THEN 'B'
                WHEN '3' THEN 'C'
                WHEN '4' THEN 'D'
            END as kelas,
            vw_grup_all.nama_grup_1, vw_grup_all.nama_lini, vw_grup_all.nama_grup_2,
            CONCAT (vw_grup_all.nama_grup_3 ,' ( ',vw_grup_all.brand,' )') as nama_grup_3,
            ukuran, vw_grup_all.brand,
            CASE vw_grup_all.tipe
                when '0' then 'Box'
                when '1' then 'Rol'
                when '2' then 'Pcs'
            END as satuan_tipe,
            jumlah,
            CASE satuan
                when '0' then 'Box'
                when '1' then 'Rol'
                when '2' then 'Pcs'
            END as satuan,
            harga, total,
            jumlah_q1,
            CASE satuan_q1
                when '0' then 'Box'
                when '1' then 'Rol'
                when '2' then 'Pcs'
            END as satuan_q1,
            harga_q1, total_q1,
            jumlah_q2,
            CASE satuan_q2
                when '0' then 'Box'
                when '1' then 'Rol'
                when '2' then 'Pcs'
            END as satuan_q2,
            harga_q2, total_q2,
            CONCAT (jumlah/jumlah_q1 ,' % ') as proporsi,
            CASE vw_grup_all.status_grup_2
                when '0' then ''
                when '1' then 'NEW'
            END as status_grup_2
            ,
            CASE vw_grup_all.status_grup_3
                when '0' then ''
                when '1' then 'NEW'
            END as status_produk
            , CASE vw_grup_all.tipe
                when '0' then ''
                when '1' then 'NEW'
            END as status_rs
            from tbl_main_audit
            INNER JOIN vw_grup_all ON tbl_main_audit.id_grup_all = vw_grup_all.id_grup_all
            INNER JOIN tbl_main_rs ON tbl_main_audit.id_rs = tbl_main_rs.id_rs;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vw_rawdata");
    }
}

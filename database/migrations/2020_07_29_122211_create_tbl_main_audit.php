<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblMainAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_main_audit');
        Schema::create('tbl_main_audit', function (Blueprint $table) {
            $table->BigIncrements('id_audit');
            $table->unsignedBigInteger('id_rs');
            $table->unsignedInteger('id_grup_all');
            $table->boolean('ketersediaan')->default(0);
            $table->unsignedInteger('jumlah')->nullable();
            $table->unsignedSmallInteger('satuan')->nullable();
            $table->unsignedBigInteger('harga')->nullable();
            $table->unsignedBigInteger('total')->nullable();
            $table->unsignedInteger('jumlah_q1')->nullable();
            $table->unsignedSmallInteger('satuan_q1')->nullable();
            $table->unsignedBigInteger('harga_q1')->nullable();
            $table->unsignedBigInteger('total_q1')->nullable();
            $table->unsignedInteger('jumlah_q2')->nullable();
            $table->unsignedSmallInteger('satuan_q2')->nullable();
            $table->unsignedBigInteger('harga_q2')->nullable();
            $table->unsignedBigInteger('total_q2')->nullable();
            $table->unsignedBigInteger('id_log_edit');
            $table->foreign('id_rs')->references('id_rs')->on('tbl_main_rs');
            $table->foreign('id_grup_all')->references('id_grup_all')->on('tbl_grup_all');
            $table->foreign('id_log_edit')->references('id_log_edit')->on('tbl_log_edit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_main_audit');
    }
}

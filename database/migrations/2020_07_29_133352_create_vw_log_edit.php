<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwLogEdit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_log_edit AS
            select tbl_log_edit.id_log_edit,
            tbl_log_edit.id_rs AS id_rs,
            id_rawdata, nama_grup_2,
            tbl_main_rs.nama_rs AS `nama`,
            tbl_main_rs.tahun,
            tbl_log_edit.lokasi,
            tbl_log_edit.ip,
            tbl_log_edit.updated_at AS `tanggal_akses`
            from tbl_log_edit
            INNER JOIN tbl_main_rs ON tbl_log_edit.id_rs = tbl_main_rs.id_rs
            LEFT JOIN tbl_grup_2 ON tbl_log_edit.id_grup_2 = tbl_grup_2.id_grup_2;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vw_log_edit");
    }
}

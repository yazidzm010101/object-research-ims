<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwGrup1All extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_grup_1_All AS
            SELECT
            tbl_grup_all.id_grup_all,
            tbl_grup_all.id_grup_1, tbl_grup_1.nama_grup_1 from tbl_grup_all
            INNER JOIN tbl_grup_1 ON tbl_grup_all.id_grup_1 = tbl_grup_1.id_grup_1
            ORDER BY tbl_grup_1.id_grup_1;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vw_grup_1_All");
    }
}

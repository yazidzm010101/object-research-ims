<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespondensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_quest_responden');
        Schema::create('tbl_quest_responden', function (Blueprint $table) {
            // id
            $table->bigIncrements('id_responden');
            $table->unsignedBigInteger('id_rs')->unique();

            // attr
            $table->string('nama')->nullable();
            $table->string('jabatan', 32)->nullable();
            $table->string('bagian', 32)->nullable();
            $table->string('telp', 16)->nullable();
            $table->string('hp', 16)->nullable();

            // timestamp
            $table->timestamps();

            // foreign keys
            $table->foreign('id_rs')->references('id_rs')->on('tbl_main_rs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_quest_responden');
    }
}

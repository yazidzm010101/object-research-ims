<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblGrupAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_grup_all');
        Schema::create('tbl_grup_all', function (Blueprint $table) {
            $table->Increments('id_grup_all');
            $table->unsignedInteger('id_grup_1');
            $table->unsignedInteger('id_grup_2');
            $table->unsignedInteger('id_grup_3');
            $table->string('ukuran')->nullable();
            $table->unsignedSmallInteger('tipe')->nullable();
            $table->foreign('id_grup_1')->references('id_grup_1')->on('tbl_grup_1');
            $table->foreign('id_grup_2')->references('id_grup_2')->on('tbl_grup_2');
            $table->foreign('id_grup_3')->references('id_grup_3')->on('tbl_grup_3');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_grup_all');
    }
}

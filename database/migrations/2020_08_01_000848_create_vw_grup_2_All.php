<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVwGrup2All extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_grup_2_All AS
            SELECT
            tbl_grup_all.id_grup_all,
            tbl_grup_all.id_grup_1,
            tbl_grup_all.id_grup_2, tbl_grup_2.nama_grup_2
            from tbl_grup_all
            INNER JOIN tbl_grup_2 ON tbl_grup_all.id_grup_2 = tbl_grup_2.id_grup_2
            ORDER BY tbl_grup_all.id_grup_2;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vw_grup_2_All");
    }
}

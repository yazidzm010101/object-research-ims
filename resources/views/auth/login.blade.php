@extends('layouts.auth')
@section('title','Login · ')

@section('content')
<section class="section center-align" style="padding-bottom: 4em;">
    <img src="{{asset('img/logo.png')}}" alt="" class="responsive-img" style="max-height: 2em;">
    <h5 class="light">Selamat Datang</h5>
</section>

<form method="POST" action="{{ route('login') }}" autocomplete="off">
    @csrf
    <div class="input-field">
        <i class="material-icons prefix">alternate_email</i>
        <input id="email" type="email" class="@error('email') invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        <label for="email">{{ __('E-Mail Address') }}</label>
        @error('email')
        <span class="helper-text" data-error="{{ $message }}"></span>
        @enderror
    </div>
    <div class="input-field">
        <i class="material-icons prefix">vpn_key</i>
        <input id="password" type="password" class="@error('password') invalid @enderror" name="password" required autocomplete="current-password">
        <label for="password">{{ __('Password') }}</label>
        @error('password')
        <span class="helper-text" data-error="{{ $message }}"></span>
        @enderror
    </div>

    {{-- change-in-production --}}
    <section class="section">
        <div class="center-align">
            {!! NoCaptcha::renderJs() !!}
            {!! NoCaptcha::display() !!}
            <span class="red-text">{{ $errors->first('g-recaptcha-response') }}</span>
        </div>
    </section>

    <section class="section" style="display: flex;">
        <div style="display:inline-block; margin-right: auto;">
            @if (Route::has('register'))
                <a class="btn btn-flat white blue-text text-darken-1 waves-effect waves-dark" href="{{ route('register') }}">
                    Belum punya akun?
                </a>
            @endif
            @if (Route::has('password.request'))
                <a class="btn btn-flat white blue-text text-darken-1 waves-effect waves-dark" href="{{ route('password.request') }}">
                    Lupa kata sandi?
                </a>
            @endif
        </div>
        <div style="display:inline-block; margin-left: auto;">
            <button type="submit" class="btn btn-flat blue darken-1 white-text waves-effect waves-light">
                {{ __('Login') }}
            </button>
        </div>
    </section>

</form>
@endsection

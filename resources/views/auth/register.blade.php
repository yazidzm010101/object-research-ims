@extends('layouts.auth')
@section('title','Register · ')

@section('content')
<section class="section center-align">
    <img src="{{asset('img/logo.png')}}" alt="" class="responsive-img" style="max-height: 2em;">
    <h5 class="light">{{ __('Register') }}</h5>
</section>
<form method="POST" action="{{ route('register') }}" autocomplete="off">
    @csrf

    <div class="input-field">
        <i class="material-icons prefix">person</i>
        <input id="name" type="text" class="@error('name') invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
        @error('name')
        <span class="helper-text" data-error="{{ $message }}"></span>
        @enderror
        <label for="name">{{ __('Name') }}</label>
    </div>

    <div class="input-field">
        <i class="material-icons prefix">alternate_email</i>
        <input id="email" type="email" class="@error('email') invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
        @error('email')
        <span class="helper-text" data-error="{{ $message }}"></span>
        @enderror
        <label for="email">{{ __('E-Mail Address') }}</label>
    </div>

    <div class="input-field">
        <i class="material-icons prefix">vpn_key</i>
        <input id="password" type="password" class="@error('password') invalid @enderror" name="password" required autocomplete="new-password">
        @error('password')
        <span class="helper-text" data-error="{{ $message }}"></span>
        @enderror
        <label for="password">{{ __('Password') }}</label>
    </div>

    <div class="input-field">
        <i class="material-icons prefix">lock</i>
        <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
        <label for="password-confirm">{{ __('Confirm Password') }}</label>
    </div>

    <section class="section">
        <section class="section" style="display: flex;">
            <div style="display:inline-block; margin-right: auto;">
                <a class="btn btn-flat white blue-text text-darken-1 waves-effect waves-dark" href="{{ route('login') }}">
                    Sudah punya akun?
                </a>
            </div>
            <div style="display:inline-block; margin-left: auto;">
                <button type="submit" class="btn btn-flat blue darken-1 white-text waves-effect waves-light">
                    {{ __('Register') }}
                </button>
            </div>
        </section>
    </section>
</form>
@endsection

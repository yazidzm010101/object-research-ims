@extends('layouts.admin')
@section('title','Rumah Sakit · ')
@section('id', 'dashboard')

@section('navbar')
<div id="navbar" class="navbar-fixed{{ (count($data['dataRumahSakit']) > 0) ? ' double-height' : ''  }}">
    <nav class="white grey-text text-darken-4">
        <form class="nav-wrapper" style="margin: unset;" method="GET">
            @csrf
            <div style="position:relative">
                <a href="#" data-target="slide-out" class="grey-text text-darken-1 sidenav-trigger hide-on-large-only"
                    style="margin-left: 1em; position: absolute; top:50%; left:0; transform:translateY(-50%)">
                    <i class="material-icons">menu</i>
                </a>
                <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-med-and-down sidenav-toggle"
                    style="margin-left: 1em; position: absolute; top:50%; left:0; transform:translateY(-50%)">
                    <i class="material-icons">menu</i>
                </a>
                <input placeholder="Rumah Sakit" name="k" type="text" style="margin:0;"
                value="{{ $data['keyword'] ?? '' }}">
                <button type="submit" class="btn-floating btn-flat grey "
                    style="margin-right: 1em; position: absolute; top:50%; right:0; transform:translateY(-50%); background: transparent !important; padding:unset">
                    <i class="material-icons grey-text text-darken-1"
                    style="line-height: 40px !important; heigth: 40px !important">search</i>
                </button>
            </div>
        </form>
        @if(count($data['dataRumahSakit']) > 0)
            <div class="nav-wrapper hide-on-small-only">
                <div class="row" style="margin: unset;">
                    <div class="col s2 m1">Id</div>
                    <div class="col s8 m10">
                        <div class="row" style="margin: unset;">
                            <div class="col s12 m5">Nama</div>
                            <div class="col s12 m3">Tahun</div>
                            <div class="col s12 m4">Tanggal masuk</div>
                        </div>
                    </div>
                    <div class="col s2 m1 right-align"></div>
                </div>
            </div>
        @endif
    </nav>
</div>
@stop

@section('contents')
@if(count($data['dataRumahSakit']) == 0)
    <h5 class="center-align light" style="margin: 4rem 0.75rem 0 0.75rem">
        @if($item = $data['keyword'] ?? FALSE)
        Hasil pencarian tidak ditemukan
        @else
        Belum ada data rumah sakit yang masuk
        @endif
    </h5>
@else
<ul class="collection row grey-text text-darken-3">
    @foreach ($data['dataRumahSakit'] as $item)
    <li class="collection-item row">
        <div class="col s2 m1">
            <b>{{ $item->id_rawdata }}</b>
        </div>
        <div class="col s8 m10">
            <div class="row">
                <div class="col s12 m5 item-header">
                    {{ $item->nama }}
                </div>
                <div class="col s12 m3">
                    {{ $item->tahun }}
                </div>
                <div class="col s12 m4 item-detail">
                    <span class="hide-on-med-and-up">Tanggal masuk </span>
                    <span>{{ substr($item->tanggal_akses, 0, 10) }}</span>
                    <span class="hide-on-small-only"> {{ substr($item->tanggal_akses, -8, 5) }}</span>
                </div>
            </div>
        </div>
        <div class="col s2 m1 right-align item-action">
            <!-- Dropdown Trigger -->
            <a class='dropdown-trigger btn-floating btn-flat white waves-effect' href='#' data-target='dropdown-{{ $item->id_rs }}'>
                <i class="material-icons grey-text text-darken-1">more_vert</i>
            </a>

            <!-- Dropdown Structure -->
            <ul id='dropdown-{{ $item->id_rs }}' class='dropdown-content'>
                <li><a href="{{ url("dashboard/rs/{$item->tahun}/{$item->id_rs}") }}"><i class="material-icons">launch</i>Detail</a></li>
                @php
                    $key = config('global.key_crypt');
                    $keyform = $item->tahun * $key;
                    $keyrs = base64_encode($item->id_rs * $key);
                @endphp
                <li><a href="{{ url("rs/{$keyform}/{$keyrs}") }}"><i class="material-icons">link</i>Buka Link</a></li>
                <li><a href="{{ url("dashboard/rs/{$item->tahun}/{$item->id_rs}/revenue/download") }}"><i class="green-text text-darken-1 material-icons">assignment_returned</i>Export</a></li>
            </ul>
        </div>
        </li>
        @endforeach
</ul>

<div class="center-align">
    {{ $data['dataRumahSakit']->links('vendor.pagination.default') }}
</div>

@endif

<div class="fixed-action-btn">
    <a href="#modal1" class="btn-floating btn-large green darken-1 modal-trigger"><i
            class="large material-icons">assignment_returned</i></a>
</div>



<div id="modal1" class="modal">
    <form action="{{ route('adminRevenueExportAll') }}" method="POST">
        @csrf
        <div class="modal-content">
            <h5>Generate Raw-Data</h5>
            <br>
            <div class="row">
                <div class="input-field">
                    <select class="browser-default" id="tahun" name="tahun" required>
                        <option value="" disabled selected>Pilih tahun</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                    </select>
                    <label for="tahun">Tahun</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="modal-close green darken-1 white-text waves-effect waves-dark btn-flat"><i
                    class="large material-icons">assignment_returned</i>
                    <span class="d-inline-block" style="line-height: 36px; vertical-align:top;">Generate</span>
                </button>
        </div>
    </form>
</div>


@endsection

<div class="mx-3">
    @if ($data['log'] ?? FALSE)
    <table class="hightlight">
        <thead class="grey darken-2 white-text light" style="z-index: 2">
            <tr>
                <th scope="col" class="center-align align-middle th-lg">No</th>
                <th scope="col" class="center-align align-middle th-lg">Progress</th>
                <th scope="col" class="center-align align-middle th-lg">Terakhir diakses</th>
                <th scope="col" class="center-align align-middle th-lg">Lokasi</th>
                <th scope="col" class="center-align align-middle th-lg" >Alamat IP</th>
            </tr>
        </thead>

        <tbody id="grup_3">
            @foreach ($data['log'] as $log)
            <tr>
                <td class="center-align">
                    {{$loop->iteration}}
                </td>
                <td class="center-align">
                    {{ $log->nama_grup_2 ?? 'Kuesioner' }}
                </td>
                <td class="center-align">
                    {{ $log->tanggal_akses }}
                </td>
                <td class="center-align">
                    {{ $log->lokasi }}
                </td>
                <td class="center-align">
                    {{ $log->ip }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="center-align">
        {{ $data['log']->links('vendor.pagination.default') }}
    </div>
    @else
    <h5 class="center-align light" style="padding-top: 4rem">Belum ada catatan log</h5>
    @endif

</div>

@extends('layouts.admin')
@section('title','Pengaturan · ')
@section('id', 'settings')

@section('nav-wrapper')
<div class="brand-logo" style="display: flex; width: 100%; align-items:center; padding: 0 1rem">
    <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-large-only sidenav-trigger"
        style="margin: 0;">
        <i class="material-icons">menu</i>
    </a>
    <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-med-and-down sidenav-toggle"
        style="margin: 0;">
        <i class="material-icons">menu</i>
    </a>
    <span class="grey-text text-darken-2 light">Pengaturan</span>
</div>
@stop

@section('contents')
<form action="{{ url()->current() }}" method="POST">
    @csrf

    <ul class="collection row grey-text text-darken-3">

        <li class="collection-item row">
            <div class="col s9">
                <h5 style="margin: unset">Registrasi Administrator</h5>
            </div>
            <div class="col s3 right-align">
                <div class="switch">
                    <label>
                        <input type="checkbox" name="register_status" id="register_status" value="1"
                        @if($item = $data['register']->status ?? FALSE)
                        {{ ($item == '1') ? 'checked' : '' }}
                        @endif>
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
            <div class="col s12">
                <p class="light">Aktifkan mode ini untuk membuat lebih dari satu akun administrator dan matikan kembali apabila akun tersebut selesai dibuat.</p>
            </div>
        </li>

        <li class="collection-item row">
            <div class="col s9">
                <h5 style="margin: unset">Validasi Link Khusus</h5>
            </div>
            <div class="col s3 right-align">
                <div class="switch">
                    <label>
                        <input type="checkbox" name="validate_status" id="validate_status" value="1"
                        @if($item = $data['validate']->status ?? FALSE)
                        {{ ($item == '1') ? 'checked' : '' }}
                        @endif>
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
            <div class="col s12">
                <p class="light">Jika mode ini aktif, data rumah yang sudah pernah diisi sebelumnya hanya dapat diakses melalui link khusus.</p>
            </div>
        </li>

    </ul>
</form>
@endsection


@section('body-end-extends')
<script>
    $('[name=register_status], [name=validate_status]').on('change', function(e){
        $(e.target).parents('form:eq(0)').submit();
    })
</script>
@endsection

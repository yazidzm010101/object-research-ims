<div id="modal1" class="modal" style="max-width: 480px !important">
    <form action="{{ route('adminFormCreate') }}" method="POST">
        @csrf
        <div class="modal-content">
            <h5>Buat Formulir Baru</h5>
            <div class="input-field">
                <select class="browser-default" id="tahun" name="tahun" required>
                    <option value="" disabled selected>Pilih tahun</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                </select>
                <label for="tahun">Tahun</label>
            </div>
            <div class="input-field">
                <select class="browser-default" id="status" name="status" required>
                    <option value="" disabled selected>Tentukan status</option>
                    <option value="0">Nonaktif</option>
                    <option value="1">Aktif</option>
                </select>
                <label for="tahun">Tahun</label>
            </div>
        </div>
        <div class="modal-footer right-align">
            <button type="submit" class="btn btn-flat blue darken-1 white-text waves-effect waves-dark">
                <i class="material-icons">add</i> Tambah
            </button>
        </div>
    </form>
</div>

<div class="fixed-action-btn">
    <a href="#modal1" class="btn-floating btn-large blue darken-1 modal-trigger"><i
            class="large material-icons">add</i></a>
</div>

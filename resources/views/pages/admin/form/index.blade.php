@extends('layouts.admin')
@section('title','Formulir · ')
@section('id', 'form-man settings grey lighten-5')
@section('nav-title', 'Formulir')
@section('head-end')
<style>
    .card-form{
        border-radius: 0.5rem;
    }
    .card-header{
        border-radius: 0.5rem 0.5rem 0 0 !important;
    }
    .card-content:not(.card-header){
        border-radius: 0 0 0.5rem 0.5rem !important;
    }
</style>
@endsection

@section('contents')
@include('pages.admin.form.data')
@include('pages.admin.form.modals')
@endsection


@section('body-end-extends')
<script>
    $('.card-form [name=status]').on('change', function(e){
        $(e.target).parents('form:eq(0)').submit();
    })
</script>
@endsection

@if(count($data['form']) > 0)
<section class="section">
    <div class="row">
        @foreach($data['form'] as $item)
        <div class="col s12 m6 xl4">
            <div class="card card-form">
                <form action="{{ route('adminFormUpdate', ['id_form' => $item->id_form, 'tahun' => $item->tahun]) }}" method="POST">
                @csrf
                    <div class="card-content card-header blue darken-1 white-text">
                        <h5 class="light" style="margin: 0;">{{ $item->tahun }}</h5>
                    </div>
                    <div class="divider"></div>
                    <div class="card-content">
                        <div class="input-field">
                            <i class="material-icons grey-text prefix">toggle_on</i>
                            <select name="status" id="status" class="browser-default">
                                <option value="" selected disabled>Pilih status</option>
                                @foreach(['Nonaktif', 'Aktif'] as $id => $option)
                                <option value="{{ $id }}"
                                    {{ $item->status == $id ? 'selected' : '' }}>{{ $option }}</option>
                                @endforeach
                            </select>
                            <label for="status">STATUS</label>
                        </div>
                        <div class="input-field">
                            <i class="material-icons grey-text prefix">link</i>
                            <div class="just-text truncate">
                                @php
                                    $key = config('global.key_crypt');
                                    $keytahun = $item->tahun * $key;
                                    $url = url("form/{$keytahun}")
                                @endphp
                                <a href="{{ $url }}">{{ $url }}</a>
                            </div>
                            <label>LINK</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</section>
@else
<h5 class="light center-align" style="margin: 4rem 0.75rem 0 0.75rem">Belum ada formulir</h5>
@endif

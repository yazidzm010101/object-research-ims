<script>
    var ctx = document.getElementById('all-revenue').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: [['Mengisi', 'Revenue'], ['Mengisi', 'Profil']],
            datasets: [{
                label: 'Jumlah Rumah Sakit',
                data: [
                        "{{ $data['totalMengisiAudit'] }}",
                        "{{ $data['totalMengisiProfil'] }}",
                    ],
                backgroundColor: [
                    '#43a047',
                    '#1e88e5',
                ]
            }]
        },
        options: {
            title: {
                display: true,
                fontSize: 18,
                text: 'Total Pengisian Rumah Sakit',
                fontStyle: 'normal',
                padding: 20,
            },
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    });
</script>

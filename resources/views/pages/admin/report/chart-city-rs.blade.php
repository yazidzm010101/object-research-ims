<script>
    var ctx = document.getElementById('city-rs').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [@foreach($data['totalPerKota'] as $item) '{{ $item->kota }}', @endforeach],
            datasets: [{
                label: 'Jumlah Rumah Sakit',
                data: [@foreach($data['totalPerKota'] as $item) '{{ $item->total }}', @endforeach],
                backgroundColor: randomColor({{ count($data['totalPerKota']) }})
            }]
        },
        options: {
            title: {
                display: true,
                fontSize: 18,
                text: ['Total Rumah Sakit Yang', 'Mengisi Revenue'],
                fontStyle: 'normal',
                padding: 20,
            },
            maintainAspectRatio: false,
        }
    });

    function randomColor(n){
        var hsl = [];
        var h = 200;
        var hMax = 360;
        var range = hMax - h;
        var s = 77;
        var l = 55;
        var range = Math.floor(range / n);
        while(hsl.length <= n){
            hsl.push("hsl(" + h + ", " + s + "%, " + l + "%)");
            h += range;
        }
        return hsl;
    }
</script>

<script>
    var ctx = document.getElementById('city-revenue').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [@foreach($data['totalRevenuePerKota'] as $item) '{{ $item->kota }}', @endforeach],
            datasets: [{
                label: 'Total Entri Raw Data',
                data: [@foreach($data['totalRevenuePerKota'] as $item) '{{ $item->total }}', @endforeach],
                backgroundColor: randomColor({{ count($data['totalPerKota']) }})
            }]
        },
        options: {
            title: {
                display: true,
                fontSize: 18,
                text: ['Total Entri Raw Data', 'Per Kota'],
                fontStyle: 'normal',
                padding: 20,
            },
            maintainAspectRatio: false,
        }
    });

    function randomColor(n){
        var hsl = [];
        var h = 200;
        var hMax = 360;
        var range = hMax - h;
        var s = 77;
        var l = 55;
        var range = Math.floor(range / n);
        while(hsl.length <= n){
            hsl.push("hsl(" + h + ", " + s + "%, " + l + "%)");
            h += range;
        }
        return hsl;
    }
</script>

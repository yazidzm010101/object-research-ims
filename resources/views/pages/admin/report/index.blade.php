@extends('layouts.admin')
@section('title','Formulir · ')
@section('id', 'report grey lighten-4')
@section('head-end')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
@endsection
@section('nav-title', 'Perolehan')

@section('contents')
<div class="container" style="width: 100%; max-width: 1280px; padding: 1rem">
    <div class="card-panel">
        <div class="row">
            <div class="col s12 m6">
                <canvas id="all-rs" height="300"></canvas>
            </div>
            <div class="col s12 m6">
                <canvas id="all-revenue" height="300"></canvas>
            </div>
        </div>
        @if(count($data['totalPerKota']) > 0)
        <div class="row">
            <div class="col s12 m6">
                <canvas id="city-rs" width="400" height="400"></canvas>
            </div>
            <div class="col s12 m6">
                <canvas id="city-revenue" width="400" height="400"></canvas>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
@section('body-end-extends')
@include('pages.admin.report.chart-all-rs')
@include('pages.admin.report.chart-all-revenue')
@include('pages.admin.report.chart-city-rs')
@include('pages.admin.report.chart-city-revenue')
@endsection

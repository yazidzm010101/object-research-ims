<script>
    var ctx = document.getElementById('all-rs').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: ['Lama', 'Baru'],
            datasets: [{
                label: 'Jumlah Rumah Sakit',
                data: [
                        "{{ $data['totalRSLama'] }}",
                        "{{ $data['totalRSBaru'] }}",
                    ],
                backgroundColor: [
                    '#1e88e5',
                    '#5e35b1',
                    '#d81b60',
                ]
            }]
        },
        options: {
            title: {
                display: true,
                fontSize: 18,
                text: 'Total Rumah Sakit',
                fontStyle: 'normal',
                padding: 20,
            },
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max: parseInt("{{ $data['totalRSCeil'] }}")
                    }
                }]
            }
        }
    });
</script>

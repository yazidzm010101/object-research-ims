<div class="mx-3">
    @if (count($data['audit']) > 0)
        <div class="table-responsive">
            <div style="overflow: auto; min-width: 100%; max-height: 65vh;">
                <table class="hightlight">
                    <thead class="grey darken-2 white-text light" style="z-index: 2">
                        <tr>
                            <th scope="col" class="center-align align-middle th-sm border-right-light" rowspan="2">#</th>
                            <th scope="col" class="center-align align-middle th-sm border-right-light" rowspan="2">Ketersediaan</th>
                            <th scope="col" class="center-align align-middle th-md" rowspan="2" id="deskripsiTest">Manufacture</th>
                            <th scope="col" class="center-align align-middle border-right-light" colspan="3">Product</th>
                            <th scope="col" class="center-align align-middle border-right-light" colspan="4" id="rev">Revenue
                                {{$data['rs']->tahun - 1}}</th>
                            <th scope="col" class="center-align align-middle border-right-light" colspan="4" id="rev-q1">Revenue
                                Q1-{{$data['rs']->tahun}}<br>January - March</th>
                            <th scope="col" class="center-align align-middle" colspan="4" id="rev-q1">Revenue
                                Q2-{{$data['rs']->tahun}}<br>January - March</th>
                        </tr>
                        <tr>
                            <th scope="col" class="center-align align-middle th-lg" rowspan="2">Brand</th>
                            <th scope="col" class="center-align align-middle th-md" rowspan="2">Size</th>
                            <th scope="col" class="center-align align-middle th-sm border-right-light" rowspan="2">Pack</th>
                            <th scope="col" class="center-align align-middle th-sm">Jumlah Unit</th>
                            <th scope="col" class="center-align align-middle th-sm">Satuan</th>
                            <th scope="col" class="center-align align-middle th-md">Harga Perunit</th>
                            <th scope="col" class="center-align align-middle th-md border-right-light">Total</th>
                            <th scope="col" class="center-align align-middle th-sm">Jumlah Unit</th>
                            <th scope="col" class="center-align align-middle th-sm">Satuan</th>
                            <th scope="col" class="center-align align-middle th-md">Harga Perunit</th>
                            <th scope="col" class="center-align align-middle th-md border-right-light">Total</th>
                            <th scope="col" class="center-align align-middle th-sm">Jumlah Unit</th>
                            <th scope="col" class="center-align align-middle th-sm">Satuan</th>
                            <th scope="col" class="center-align align-middle th-md">Harga Perunit</th>
                            <th scope="col" class="center-align align-middle th-md">Total</th>
                        </tr>
                    </thead>

                    <tbody id="grup_3">
                        @php $row_id = 0 @endphp
                        @foreach($data['audit'] as $id => $item)
                        @php $row_id += 1 @endphp
                        <tr class="{{ $item->status_produk == 1 ? 'data-new' : '' }}">
                            <td class="center-align">
                                {{$loop->iteration}}
                            </td>
                            <td class="center-align">
                                {{$item->ketersediaan}}
                            </td>
                            <td class="center-align">{{$item->nama_manufacture}}</td>
                            <td class="center-align th-lg">
                                {{$item->nama_brand}}
                            </td>
                            <td class="center-align th-md">
                                {{$item->ukuran}}
                            </td>
                            <td>
                                {{$item->nama_tipe}}
                            </td>
                            <td>
                                {{$item->jumlah ?? ''}}
                            </td>
                            <td>
                                {{$item->satuan ?? '-'}}
                            </td>
                            <td>
                                {{$item->harga ?? ''}}
                            </td>
                            <td>
                                {{$item->total ?? ''}}
                            </td>
                            <td>
                                {{$item->jumlah_q1 ?? ''}}
                            </td>
                            <td>
                                {{$item->satuan_q1 ?? '-'}}
                            </td>
                            <td>
                                {{$item->harga_q1 ?? ''}}
                            </td>
                            <td>
                                {{$item->total_q1 ?? ''}}
                            </td>
                            <td>
                                {{$item->jumlah_q2 ?? ''}}
                            </td>
                            <td>
                                {{$item->satuan_q2 ?? '-'}}
                            </td>
                            <td>
                                {{$item->harga_q2 ?? ''}}
                            </td>
                            <td>
                                {{$item->total_q2 ?? ''}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="center-align">
            {{ $data['audit']->links('vendor.pagination.default') }}
        </div>
    @else
        <h5 class="center-align light" style="padding-top: 4rem;">Belum ada data revenue pada laboratorium ini</h5>
    @endif
</div>

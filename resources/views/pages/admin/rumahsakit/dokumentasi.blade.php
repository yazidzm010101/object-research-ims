<div style="padding: 1rem;">
    @if(count($data['foto_rs']) == 0)
        <h5 class="light center-align" style="padding: 15vh 0 20vh 0">Belum ada foto</h5>
    @else
        <div class="row" id="images">
            @foreach ($data['foto_rs'] as $item)
                <div class="col s12 m6 l4" style="padding: 0.75rem;">
                    <div style="position: relative">
                        <img class="materialboxed responsive-img" src="{{ url("storage/photo/{$item->foto}") }}" alt=""
                        data-caption="Diupload pada {{ $item->created_at->format('d F Y') }}"
                        style="width: 100%;">
                        <div class="light white-text" style="position: absolute; bottom: 0; padding: 0.25rem 0.75rem; right: 0; left: 0; background: rgba(0, 0, 0, 0.35)">
                            {{ $item->created_at->isoFormat('d MMM Y HH:mm') }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

@extends('layouts.admin')
@section('title','Rumah Sakit · ')
@section('id', 'detail grey lighten-3')
@section('head-end')
    <style>
        @media only screen and (max-width: 719px) {
            #app {
                background: white !important;
                background-color: white !important;
            }
        }
    </style>
@endsection

@section('nav-wrapper')
    <div class="brand-logo" style="display: flex; width: 100%; align-items:center; padding: 0 1rem">
        <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-large-only sidenav-trigger"
            style="margin: 0;">
            <i class="material-icons">menu</i>
        </a>
        <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-med-and-down sidenav-toggle"
            style="margin: 0;">
            <i class="material-icons">menu</i>
        </a>
        <span class="grey-text text-darken-2 light truncate">{{ $data['rs']->nama_rs }}</span>
        <a href="{{ url("rs/{$data['key']['keytahun']}/{$data['key']['keyrs']}") }}" data-target="slide-out" class="grey-text text-darken-1 nav-action"
            style="margin-left: auto !important;">
            <i class="material-icons">launch</i>
        </a>
    </div>
@stop

@section('contents')
<div class="card lighten-3 no-shadow" style="margin: unset">
    <div class="card-image indigo lighten-3">
        <div class="card-image-bg">
            @if($item = $data['foto_rs'][0]->foto ?? FALSE)
            <img src="{{ url("storage/photo/{$item}") }}"/>
            @else
            <i class="material-icons large indigo-text text-lighten-5">local_hospital</i>
            @endif

        </div>
        <div class="container" style="position: relative; max-width: 720px; width: 100%; height: 100%;">
            <a class="btn-floating halfway-fab btn-large waves-effect waves-light blue darken-1"
                href="{{ url("rs/{$data['key']['keytahun']}/{$data['key']['keyrs']}") }}">
                <i class="material-icons">launch</i>
            </a>
        </div>
    </div>
    <div class="card-container container white">
        <div class="card-tabs">
            <ul class="tabs">
                <li class="tab"><a href="#tab1" class="waves-effect">Responden</a></li>
                <li class="tab"><a class="waves-effect active" href="#tab2">Identifikasi</a></li>
                <li class="tab"><a href="#tab3" class="waves-effect">Kuesioner</a></li>
                <li class="tab"><a href="#tab4" class="waves-effect">Dokumentasi</a></li>
            </ul>
        </div>
        <div class="card-content" style="padding-left: unset; padding-right: unset">
            <div id="tab1">@include('pages.admin.rumahsakit.responden')</div>
            <div id="tab2">@include('pages.admin.rumahsakit.identifikasi')</div>
            <div id="tab3">@include('pages.admin.rumahsakit.kuesioner')</div>
            <div id="tab4">@include('pages.admin.rumahsakit.dokumentasi')</div>
        </div>
    </div>
</div>
@endsection

@section('body-end')
    <script>
        nav();
        slideToggle();

        function slideToggle(){
            $('.sidenav-toggle').on('click', function(e){
                var targetId = '#' + $(e.target).attr('data-target');
                $(targetId).toggleClass('sidenav-toggled');
                $('.contents, #navbar nav, .modal-wrapper .modal').toggleClass('side-toggled');
            })
        }

        function nav(){
            var y = 0;
            var y2 = 0;
            $('#navbar nav').addClass('transparent');
            $(window).scroll(function(){
                var scrollTop = $(document).scrollTop();
                var startHeight = $('.card-image').height() - $('#navbar nav').height();
                if(scrollTop >= startHeight){
                    $('#navbar nav').removeClass('transparent');
                    if(scrollTop > y){
                        if(y2 >= 64){
                            $('#navbar nav').css('transform', 'translateY(-64px)');
                            $('.fixed-action-btn').css('transform', 'scale(0)');
                        }else{
                            y2 += (scrollTop - y);
                        }
                    }else{
                        y2 = 0;
                        $('#navbar nav').css('transform', 'translateY(0)');
                        $('.fixed-action-btn').css('transform', 'scale(1)');
                    }
                    y = scrollTop;
                }else{
                    $('#navbar nav').addClass('transparent');
                }
            })
        }
    </script>
@stop

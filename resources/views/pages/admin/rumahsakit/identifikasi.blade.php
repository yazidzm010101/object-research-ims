{{-- GEOGRAFI --}}
<ul class="collection">
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">local_hospital</i>
        <span class="title">Nama Rumah Sakit</span>
        <p class="light">{{ $data['rs']->nama_rs ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">place</i>
        <span class="title">Alamat</span>
        <p class="light">{{ $data['rs']->alamat ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"></i>
        <span class="title">Kota</span>
        <p class="light">{{ $data['rs']->kota ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"></i>
        <span class="title">Kelompok Kota</span>
        <p class="light">{{ $data['rs']->kelompok_kota ?? '-' }}</p>
    </li>
</ul>
{{-- KELAS --}}
<ul class="collection">
    <li class="collection-item avatar" style="border-bottom: unset; padding-bottom:unset">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">apartment</i>
        <span class="title">Status Pengelolaan</span>
        <p class="light">
            @if($item = $data['rs']->status_pengelolaan ?? FALSE)
            {{ $item == '1' ? 'Pemerintah' : 'Swasta' }}
            @else -
            @endif
        </p>
    </li>
    <li class="collection-item avatar" style="border-bottom: unset; padding-bottom:unset">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em"></i>
        <span class="title">Kelas</span>
        <p class="light">
            @if($item = $data['rs']->kelas ?? FALSE)
                @php $kelas = ['A', 'B', 'C', 'D']; @endphp
                {{ $kelas[$item - 1] ?? ' - ' }}
            @else -
            @endif
        </p>
    </li>
</ul>
{{-- BANGUNAN --}}
<ul class="collection">
    <li class="collection-item avatar" style="border-bottom: unset; padding-bottom:unset">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">apartment</i>
        <span class="title">Luas</span>
        <p class="light">
            {{ $data['rs']->luas ?? ' - ' }}
        </p>
    </li>
    <li class="collection-item avatar" style="border-bottom: unset; padding-bottom:unset">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em"></i>
        <span class="title">Tahun Berdiri</span>
        <p class="light">
            {{ $data['kuesioner']->tahun_berdiri ?? ' - ' }}
        </p>
    </li>
</ul>

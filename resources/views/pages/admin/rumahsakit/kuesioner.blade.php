<ul class="collection">
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">person</i>
        <span class="title">Interviewer</span>
        <p class="light">{{ $data['kuesioner']->interviewer ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">event</i>
        <span class="title">Tanggal Wawancara</span>
        <p class="light">{{ $data['kuesioner']->tanggal ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">schedule</i>
        <span class="title">Waktu</span>
        <p class="light">
            {{ $data['kuesioner']->waktu_mulai ?? '-' }} s.d. {{ $data['kuesioner']->waktu_selesai ?? '-' }}
        </p>
    </li>
</ul>
<ul class="collection">
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">equalizer</i>
        <span class="title">Frekuensi Kunjungan Sales</span>
        <p class="light">{{ $data['kuesioner']->frekuensi ?? '-' }} / Bulan</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">event</i>
        <span class="title">Program Sales Tahun {{ $data['rs']->tahun }}</span>
        <p class="light">{{ $data['kuesioner']->program ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">show_chart</i>
        <span class="title">Kondisi Penjualan</span>
        <p class="light">
            @if($item = $data['kuesioner']->pendapatan ?? FALSE)
            {{ $item > 0 ? 'Meningkat,' : ($item < 0 ? 'Menurun, ' : '') }}
            {{ abs($item) }} %
            @else - @endif
        </p>
    </li>
</ul>

<ul class="collection">
    <div class="collection-item center-align">
        <img src="
        @if($item = $data['foto_responden']-> foto ?? FALSE)
        {{ url("storage/photo/{$item}") }}
        @else
        https://www.sunsetlearning.com/wp-content/uploads/2019/09/User-Icon-Grey-300x300.png
        @endif"
        class="circle z-depth-1 responsive-img" style="max-height: 12rem;">
    </div>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"
        style="font-size: 1.5em">person</i>
        <span class="title">Nama</span>
        <p class="light">{{ $data['responden']->nama ?? '-' }}</p>
    </li>
</ul>
<ul class="collection">
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1">call</i>
        <span class="title">No. Telepon</span>
        <p class="light">{{ $data['responden']->telp ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"></i>
        <span class="title">No. Handphone</span>
        <p class="light">{{ $data['responden']->hp ?? '-' }}</p>
    </li>
</ul>
<ul class="collection">
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1">work</i>
        <span class="title">Jabatan</span>
        <p class="light">{{ $data['responden']->jabatan ?? '-' }}</p>
    </li>
    <li class="collection-item avatar">
        <i class="material-icons circle white grey-text text-darken-1"></i>
        <span class="title">Bagian</span>
        <p class="light">{{ $data['responden']->bagian ?? '-' }}</p>
    </li>
</ul>

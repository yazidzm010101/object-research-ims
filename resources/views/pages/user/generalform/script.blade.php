<script>
    var kota = $('#kota').val() == null;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });

    $('#keyword').prop('readonly', kota).prop('disabled', kota);

    $('#kota').change(function(e) {
        var value = $(e.target).val() == null;
        $('#keyword').prop('readonly', value).prop('disabled', value);
    })

    initLiveSearch();


    function initLiveSearch(elem) {
        var elem = $('#keyword')[0];
        var instance = M.Autocomplete.init(elem);
        updateLiveSearch(elem, instance);
    }

    function updateLiveSearch(elem, instance) {
        var data = [];
        var total = 0;
        var tooltip = $('#result-message');
        var current_match = 0;
        var current_count = 0;
        $(elem).on('focusin.search keyup.search change.search', function(e) {
            var value = $(elem).val();
            var kota = $('#kota').val();
            ajaxLiveSearch(kota, value,
            function(res){
                var new_data = {};
                total = res['total'];
                data = res['data'];
                match = $(data).filter(function(id, item) {
                    return item.nama.toLowerCase().indexOf(value.toLowerCase()) >= 0;
                });
                if(match.length == 1){
                    $('#id_rs').val(match[0].id_rs);
                    current_count = 0;
                }else if((match.length <= 0) && (current_match > 0)){
                    M.toast({ html: '<strong class="red-text">Rumah Sakit tidak ditemukan, silahkan klik</strong><strong class="blue-text"> Registrasi Rumah Sakit</strong>' })
                }
                current_match = match.length;

                $.each(data, function(id, item){
                    new_data[item.nama] = null;
                });
                $(tooltip).text(match.length + ' / ' + total + ' data')
                instance.updateData(new_data);
            },
            function(res){
                // M.toast({html: 'Terjadi kesalahan!'});
            })
        });

    }

    function ajaxLiveSearch(kota, value, callback, onError){
        var url = "{{url('data/rs/')}}/" + kota;
        $.ajax({
            type : 'get',
            url : url,
            dataType: 'json',
            data: {'keyword': value},
            success: function(res){
                callback(res);
            },
            error: function(res){
                onError(res);
            }
        });
    }
</script>

@extends('layouts.user')
@section('title','General Form · ')
@section('id', 'questionnaire respondent')

@section('navbar')
@stop

@section('contents')
<form class="form" style="margin: unset; display:flex; flex-direction: column; min-height: calc(100vh - 56px);" method="POST"
autocomplete="off">
    @csrf
    <input type="text" name="id_rs" id="id_rs" readonly hidden/>

    <div class="form-content" style="margin-bottom: 0">
        <div class="form-content-wrapper" style="margin: 0 auto; max-width: 420px; width: 100%">
            <section class="section center-align animate__animated animate__fadeInUp">
                <h4><img src="{{asset('img/logo.png')}}" alt="iqvia" class="responsive-img" style="max-height: 1em"></h4>
                <h4 class="light">
                    Medical Survey {{ $data['form']->tahun ?? ''}}
                </h4>
            </section>
            <section class="section animate__animated animate__fadeInUp">
                <div class="input-field" style="padding-bottom: 1rem;">
                    <i class="material-icons prefix">location_city</i>
                    <select name="kota" id="kota" class="browser-default">
                        <option {{ ($data['kota']->kota ?? NULL) == NULL ? 'selected' : '' }} disabled>Pilih Kota</option>
                        @foreach($data['kota'] as $id => $item)
                            <option value="{{$id}}">{{$item}}</option>
                        @endforeach
                    </select>
                    <label for="kota">KOTA</label>
                </div>
                <div class="input-field" id="keyword-field">
                    <i class="material-icons prefix">local_hospital</i>
                    <input type="text" name="keyword" id="keyword" class="autocomplete" required>
                    <label for="keyword">RUMAH SAKIT</label>
                    <span id="result-message" style="position: absolute; top: 0; right: 0; transform:translateY(-100%)"></span>
                </div>

                <div class="input-field" style="display: flex">
                    <a href="{{ url("form/{$data['keytahun']}/add") }}" class="btn btn-flat white blue-text text-darken-1 waves-effect waves-dark"
                    style="margin-right: auto;">
                        Registrasi Rumah Sakit
                    </a>
                    <button type="submit" class="btn btn-flat blue blue-darken-1 white-text waves-effect waves-light">LANJUT</button>
                </div>
            </section>
        </div>
    </div>
</form>
@endsection
@section('body-end')
    @include('pages.user.generalform.script')
@endsection

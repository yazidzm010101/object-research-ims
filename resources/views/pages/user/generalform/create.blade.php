@extends('layouts.user')
@section('title','General Form · ')
@section('id', 'questionnaire respondent')

@section('navbar')
@stop

@section('contents')
<form class="form" style="margin: unset; display:flex; flex-direction: column; min-height: calc(100vh - 56px);" method="POST">
    @csrf
    <div class="form-content" style="margin-bottom: 0">
        <div class="form-content-wrapper" style="margin: 0 auto; max-width: 420px; width: 100%">
            <section class="section center-align animate__animated animate__fadeInUp">
                <h4><img src="{{asset('img/logo.png')}}" alt="iqvia" class="responsive-img" style="max-height: 1em"></h4>
                <h4 class="light">Medical Survey {{ $data['form']->tahun ?? ''}}</h4>
                <h5 class="light">Registrasi Rumah Sakit</h5>
            </section>
            <section class="section animate__animated animate__fadeInUp">
                <div class="input-field" style="padding-bottom: 1rem;">
                    <i class="material-icons prefix">location_city</i>
                    <select name="kota" id="kota" class="browser-default">
                        <option {{ ($data['form']->kota ?? NULL) == NULL ? 'selected' : '' }} disabled>Pilih Kota</option>
                        @foreach($data['kota'] as $id => $item)
                            <option value="{{$id}}">{{$item}}</option>
                        @endforeach
                    </select>
                    <label for="kota">KOTA</label>
                </div>
                <div class="input-field" id="keyword-field">
                    <i class="material-icons prefix">local_hospital</i>
                    <input type="text" name="nama_rs" id="nama_rs" class="validate" required>
                    <label for="nama_rs">NAMA RUMAH SAKIT</label>
                </div>

                <div class="input-field" style="display: flex">
                    <a href="{{ url("form/{$data['keytahun']}") }}" class="btn btn-flat white blue-text text-darken-1 waves-effect waves-dark"
                    style="margin-right: auto;">
                        Cari Rumah Sakit
                    </a>
                    <button type="submit" class="btn btn-flat blue text-darken-1 white-text waves-effect waves-light">DAFTAR</button>
                </div>
            </section>
        </div>
    </div>
</form>
@endsection

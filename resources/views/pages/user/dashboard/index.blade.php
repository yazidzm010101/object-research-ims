@extends('layouts.user')
@section('title','Dashboard · ')
@section('id', 'dashboard')

@section('contents')
<style>
    #navbar nav{
        transition: all 0.2s ease-in-out;
    }
    body{
        position: relative;
    }
    footer{
        padding: 1.5em 1em;
        position: absolute;
        border-top: 1px solid #e0e0e0;
        bottom: 0;
        left: 0;
        width: 100%;
    }
    a.card {
        display: block !important;
    }
    a.card .badge{
        padding: 0 0.5rem;
        margin: 0.25rem 0.25rem 0 0;
        border-radius: 1rem;
    }
    #menu{
        padding: 1em 1em 3em 1em;
    }
    #content{
        padding: 1em;
        padding-left: 2em;
        padding-right: 2em;
        padding-bottom: 5em !important;
    }
    .copyright{
        text-align: right;
    }
    .powered{
        text-align: left;
    }
    @media only screen and (max-width: 600px){
        .copyright{
            text-align: center;
        }
        .powered{
            text-align: center;
        }
        #content{
            padding-left: 1em;
            padding-right: 1em;
        }
    }
    @media only screen and (max-width: 800px){
        #content {
            width: 100% !important;
            padding-left: 1.5em;
            padding-right: 1.5em;
        }
    }
</style>
<div class="container" id="content" style="max-width: 800px; width: 100% !important">

    <section class="section center-align" id="user">
        <img alt="rumahsakit" class="circle responsive-img z-depth-1"
            style="max-width: 100% !important; max-height: 8em !important; margin: 0.5em;"
            src="https://almightycs.com/web/image/product.template/133/image">
        <h5>Selamat datang
            , {{ $data['rs']->nama_rs }}
        </h5>
        <h5 class="light">
            {{ $data['rs']->alamat }}
        </h5>
    </section>

    <section class="grey-text text-darken-3" id="menu">
        <div class="row" style="margin-bottom: 0;">

            <div class="col s12">
                <a class="card amber darken-3 white-text waves-effect waves-light"
                    href="{{ url(url()->current().'/getURL')}}">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s8">
                                <span class="card-title"><strong>Simpan Link</strong></span>
                                <p>Halaman ini hanya dapat diakses melalui link khusus</p>
                            </div>
                            <div class="col s4 center-align">
                                <i class="medium material-icons">link</i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m6">
                <a class="card blue darken-1 white-text waves-effect waves-light"
                    href="{{ url(url()->current().'/respondent')}}">
                    @if($item = ($data['badge']['responden'] < 100))
                    <span class="badge yellow darken-3 white-text text-darken-2 z-depth-1"
                    >{{ $data['badge']['responden'] }} %</span>
                    @else
                    <span class="badge green darken-1 white-text z-depth-1">100 %</span>
                    @endif
                    <div class="card-content">
                        <div class="row">
                            <div class="col s8">
                                <span class="card-title"><strong>Responden</strong></span>
                                <p>Lengkapi profil serta foto responden</p>
                            </div>
                            <div class="col s4 center-align">
                                <i class="medium material-icons">assignment_ind</i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m6">
                <a class="card deep-purple darken-1 white-text waves-effect waves-light"
                    href="{{ url(url()->current().'/questionnaire')}}">
                    @if($item = ($data['badge']['kuesioner'] < 100))
                    <span class="badge yellow darken-3 white-text text-darken-2 z-depth-1"
                    >{{ $data['badge']['kuesioner'] }} %</span>
                    @else
                    <span class="badge green darken-1 white-text z-depth-1">100 %</span>
                    @endif
                    <div class="card-content">
                        <div class="row">
                            <div class="col s8">
                                <span class="card-title"><strong>Kuesioner</strong></span>
                                <p>Isi dan lengkapi kuesioner Medical Survey {{ $data['rs']->tahun }}</p>
                            </div>
                            <div class="col s4 center-align">
                                <i class="medium material-icons">assignment_turned_in</i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m6">
                <a class="card red lighten-1 white-text waves-effect waves-light"
                    href="{{ url(url()->current().'/gallery')}}">
                    <span class="badge yellow darken-3 white-text text-darken-2 z-depth-1"
                    >{{ $data['badge']['dokumentasi'] }}</span>
                    <div class="card-content">
                        <div class="row">
                            <div class="col s8">
                                <span class="card-title"><strong>Dokumentasi</strong></span>
                                <p>Tambahkan dokumentasi foto rumah sakit anda</p>
                            </div>
                            <div class="col s4 center-align">
                                <i class="medium material-icons">image</i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m6">
                <a class="card green darken-1 white-text waves-effect waves-light"
                    href="{{ url(url()->current().'/revenue')}}">
                    <span class="badge yellow darken-3 white-text text-darken-2 z-depth-1"
                    >{{ $data['badge']['revenue'] }}</span>
                    <div class="card-content">
                        <div class="row">
                            <div class="col s8">
                                <span class="card-title"><strong>Revenue</strong></span>
                                <p>Lengkapi Revenue {{ $data['rs']->tahun }} Rumah Sakit Anda</p>
                            </div>
                            <div class="col s4 center-align">
                                <i class="medium material-icons">show_chart</i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </section>

</div>

@endsection

@section('body-end')
    <footer class="page-footer white grey-text">
        <div class="footer-wrapper">
            <div class="container">
                <div class="row" style="margin: 0;">
                    <div class="col s12 m6 copyright">
                        © {{ $data['rs']->tahun }} IQVIA Medical Survey
                    </div>
                    <div class="col s12 m6 powered">
                        Powered by Object Research Indonesia
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
        nav();

        function nav(){
            var y = 0;
            var y2 = 0;
            $(window).scroll(function(){
                var scrollTop = $(document).scrollTop();
                if(scrollTop > y){
                    if(y2 >= 64){
                        $('#navbar nav').css('transform', 'translateY(-64px)');
                        $('.fixed-action-btn').css('transform', 'scale(0)');
                    }else{
                        y2 += (scrollTop - y);
                    }
                }else{
                    y2 = 0;
                    $('#navbar nav').css('transform', 'translateY(0)');
                    $('.fixed-action-btn').css('transform', 'scale(1)');
                }
                y = scrollTop;
            })
        }
    </script>
@endsection

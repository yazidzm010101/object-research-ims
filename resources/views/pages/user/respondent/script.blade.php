<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });
    var check = {
        'html5': function(){
            var test_canvas = document.createElement("canvas");
            var canvascheck= (test_canvas.getContext) ? true : false;
            return canvascheck;
        }
    }
    var image = {
        'b64toBlob': function (src, sliceSize) {
                var block = src.split(";");
                var b64Data = block[1].split(",")[1];
                var contentType = block[0].split(":")[1];
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        },

        'resize': function(src, quality, maxWidth, callback){
            loading.draw('Mengkompresi gambar');
            var img = new Image();

            img.onload = function(){
                maxWidth = maxWidth || 1024;
                var mime_type = src.substring(src.indexOf(":")+1, src.indexOf(";"));
                var natW = img.width;
                var natH = img.height;
                var ratio = natH / natW;
                var maxHeight = ratio * maxWidth;

                if((natW < maxWidth) && (natH < maxHeight)){
                    maxWidth = natW;
                    maxHeight = natH;
                }

                var cvs = document.createElement('canvas');
                cvs.width = maxWidth;
                cvs.height = maxHeight;
                console.log(maxWidth, maxHeight);

                cvs.getContext("2d").drawImage(img,
                    0, 0,
                    natW, natH,
                    0, 0,
                    maxWidth, maxHeight
                );

                return callback(cvs.toDataURL(mime_type,quality/100));

            }
            img.src = src;
        },

        'load': function(input, callback){
            if(input.files.length == 0)
                // return callback(null, 'Tidak ada file yang terpilih');
                return callback(null, null, 'Tidak ada file yang terpilih');

            var filename = input.files[0].name,
                ext = filename.split(".").slice(-1)[0].toLowerCase();
                size = input.files[0].size;
                console.log(size);

            if(['jpg', 'jpeg', 'png'].indexOf(ext) < 0)
                return callback(null, null, 'Jenis file .' + ext + ' tidak didukung, silahkan gunakan file .jpg, .jpeg, atau .png');

            if(size == 0){
                return callback(null, null, 'Ukuran file tidak valid (0B)');
            }

            if(size >= 10485760){
                return callback(null, null, 'Ukuran terlalu besar(melebihi 10MB)');
            }


            try{
                var reader = new FileReader();
                $(reader).on('load', function(e) {
                    var src = e.target.result;

                    if(check.html5){
                        image.resize(src, 90, 512, function(src){
                            var blob = image.b64toBlob(src);
                            exif.copyExif(input.files[0], blob, function(blob_result){
                                // var file = new File([blob_result], filename);
                                // return callback(file, false);
                                return callback(blob_result, filename, false);
                            });
                        })
                    }else{
                        loading.draw('Browser tidak mendukung HTML5, melewati proses kompresi', 25);
                        var blob_result = image.b64toBlob(src);
                        // var file = new File([blob_result], filename)
                        return callback(blob_result, filename, false);
                    }
                });

                reader.readAsDataURL(input.files[0]);
            }catch(error){
                return callback(null, null, 'File yang anda upload corrupt!')
            }

        },

        'upload': function(name, filename, file, square, callback){
            var form = document.getElementsByTagName('form')[0];
            var formData = new FormData(form);
            formData.append('foto', file, filename);
            formData.append('square', square);

            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = (evt.loaded / evt.total) * 100;
                            loading.draw('Mengunggah gambar', percentComplete);
                        }
                    }, false);
                    return xhr;
                },
                url: "{{ Request::url() }}/photo/upload",
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function(response){
                    return callback(response, false);
                },
                error: function(response){
                    return callback(null, response);
                }
            });
        }
    }

    var loading = {
        'draw': function(message, percentage){
            message = message || false;
            percentage = percentage || false;

            var loading = $('.overlay');
            if(loading.length == 0){
                $('body').append(
                    '<div class="overlay">' +
                        '<div class="preloader-wrapper active">' +
                            '<div class="spinner-layer spinner-blue-only">' +
                                '<div class="circle-clipper left">' +
                                    '<div class="circle"></div>' +
                                '</div>' +
                                '<div class="gap-patch">' +
                                    '<div class="circle"></div>' +
                                '</div>' +
                                '<div class="circle-clipper right">' +
                                    '<div class="circle"></div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
            }
            if(message != false){
                var messageElem = $('.overlay .message');
                if(messageElem.length > 0){
                    $(messageElem).html(message);
                }else{
                    $('.overlay').append('<h6 class="message">' + message + '</h6>');
                }
            }
            if(percentage != false){
                var percentageElem = $('.overlay .percentage');
                if(percentageElem.length > 0){
                    $(percentageElem).html(percentage + '%');
                }else{
                    $('.overlay').append('<h6 class="percentage">' + percentage + '%</h6>');
                }
            }
        },
        'destroy': function(){
            $('.overlay').remove();
        }
    }

    loading.destroy();
    $('#foto').on('change', function(e) {
        M.Toast.dismissAll();
        loading.draw('Memuat file');

        image.load(e.target, function(file, filename, error){
            if(error){
                loading.destroy();
                M.toast({html: error});
            }else{
                loading.draw('Mengunggah gambar', 0);
                image.upload('foto', filename, file, true, function(response, error){
                    loading.destroy();
                    if(response){
                        console.log(response);
                        $('#foto-img').attr('src', response.photo)
                        M.toast({html: 'Gambar berhasil diunggah!'})
                    }else if(error){
                        M.toast({html: 'Terjadi kesalahan saat menggungah!'})
                    }else{
                        M.toast({html: 'Gambar gagal diunggah, silahkan cek jaringan anda!'})
                    }
                })
            }
        });
    });
    $('input[data-length], textarea[data-length]').characterCounter();
</script>

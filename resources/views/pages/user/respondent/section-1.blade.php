<div class="form-content animate__animated animate__fadeInUp" style="margin-bottom: 0">
    <div class="form-content-wrapper" style="margin: 0 auto; max-width: 600px; width: 100%">
        <div class="center-align">
            <div class="circle circle-wrapper grey lighten-1" style="display: inline-block; position: relative; margin: 1em; width:8em; height: 8em;">
                @php
                    $nama_foto = $data['foto']->foto ?? FALSE;
                    $foto = $nama_foto ? url("storage/photo/{$nama_foto}") : "https://www.sunsetlearning.com/wp-content/uploads/2019/09/User-Icon-Grey-300x300.png";
                @endphp
                <img src="{{ $foto }}" alt="userpic"
                id="foto-img"
                class="circle responsive-img"
                style="position: absolute; left: 50%; top:50%; transform: translate(-50%, -50%); width:100%; height: auto;">
                <div id="foto_input" class="custom-input" style="position: absolute; left: 50%; bottom: 0;">
                    <input type="file" id="foto" accept="image/*">
                    <label for="foto" class="btn-floating blue darken-1 waves-effect waves-light"><i class="material-icons">add_a_photo</i></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 grey-text text-darken-2">
                <i class="material-icons prefix">person</i>
                <textarea name="nama" id="nama" maxlength="64" data-length="64" class="materialize-textarea" required
                >{{ $data['responden']->nama ?? NULL }}</textarea>
                <label for="nama">Nama Responden <strong class="red-text">*</strong></label>
            </div>
            <div class="input-field col s12 grey-text text-darken-2">
                <i class="material-icons prefix">call</i>
                <input name="hp" id="hp" type="number" min="0" class="validate" data-length="15"
                value="{{ $data['responden']->hp ?? NULL }}">
                <label for="hp">Nomor Telepon</label>
            </div>
            <div class="input-field col s12 grey-text text-darken-2">
                <i class="material-icons prefix"></i>
                <input name="telp" id="telp" type="number" min="0" class="validate" data-length="15"
                value="{{ $data['responden']->telp ?? NULL }}">
                <label for="telp">Nomor Handphone</label>
            </div>
            <div class="input-field col s12 grey-text text-darken-2">
                <i class="material-icons prefix">work</i>
                <input name="jabatan" id="jabatan" type="text" class="validate" data-length="32"
                value="{{ $data['responden']->jabatan ?? NULL }}">
                <label for="jabatan">Jabatan</label>
            </div>
            <div class="input-field col s12 grey-text text-darken-2">
                <i class="material-icons prefix"></i>
                <input name="bagian" id="bagian" type="text" class="validate" data-length="32"
                value="{{ $data['responden']->bagian ?? NULL }}">
                <label for="bagian">Bagian</label>
            </div>
        </div>
    </div>
</div>

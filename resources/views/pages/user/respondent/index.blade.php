@extends('layouts.user')
@section('title','Respondent · ')
@section('id', 'questionnaire respondent')

@section('navbar')
<div id="appbar" class="navbar-fixed">
    <nav class="blue darken-1 white-text">
        <a class="btn-floating btn-flat waves-effect waves-light blue darken-1 white-text"
            style="position: absolute; top: 50%; left: 2rem; transform: translate(-50%, -50%);"
            href="{{ url("rs/{$data['key']['keytahun']}/{$data['key']['keyrs']}") }}">
            <i class="material-icons" style="line-height:40px; height: 40x; font-size:200%;">chevron_left</i>
        </a>
        <h5 class="light truncate" style="position: absolute; margin: unset; top:50%; left: 4rem; right: 1rem; transform: translateY(-50%);">Profil Responden</h5>
    </nav>
</div>
@stop

@section('contents')
<form class="form" style="margin: unset;" method="POST">
    @csrf
    @include('pages.user.respondent.section-1')
    <div class="navbottom-fixed">
        <div class="navbottom">
            <div class="navbottom-wrapper" style="margin: 0 auto; max-width: 600px; width: 100%; display: flex">
                <button type="submit"
                    class="control-next btn btn-flat waves-effect waves-light blue darken-1 white-text"
                    style="margin-left: auto">
                    <span style="vertical-align: middle">Simpan</span>
                    <i class="material-icons" style="vertical-align: middle">save</i>
                </button>
            </div>
        </div>
    </div>
</form>
@endsection
@section('body-end')
@include('pages.user.respondent.script')
@endsection

@extends('layouts.user')
@section('title','Documentation · ')
@section('id', 'questionnaire documentation')

@section('navbar')
<div id="appbar" class="navbar-fixed">
    <nav class="red lighten-1 white-text">
        <a class="btn-floating btn-flat waves-effect waves-light red lighten-1 white-text"
            style="position: absolute; top: 50%; left: 2rem; transform: translate(-50%, -50%);"
            href="{{ url("rs/{$data['key']['keytahun']}/{$data['key']['keyrs']}") }}">
            <i class="material-icons" style="line-height:40px; height: 40x; font-size:200%;">chevron_left</i>
        </a>
        <h5 class="light truncate" style="position: absolute; margin: unset; top:50%; left: 4rem; right: 1rem; transform: translateY(-50%);">Dokumentasi Foto</h5>
    </nav>
</div>
@stop

@section('contents')
<div class="container" id="images-container" style="padding-left: 1em; padding-right: 1em;">
    @if(count($data['foto']) == 0)
    <div class="center-align" style="padding-top: 4rem" id="no-photo">
        <h5 class="light">Belum ada foto</h5>
        <h5 class="light">klik tombol <span class="btn-floating btn-flat red lighten-1" style="cursor: auto"><i class="material-icons" style="vertical-align:middle; line-height: inherit !important;">add_a_photo</i></span> untuk menambahkan foto</h5>
    </div>
    @else
        <div class="row" id="images">
            @foreach ($data['foto'] as $item)
                <div class="col s12 m6 l4" style="padding: 0.75rem;">
                    <div style="position: relative">
                        <img class="materialboxed responsive-img" src="{{ url("storage/photo/{$item->foto}") }}" alt=""
                        data-caption="Diupload pada {{ $item->created_at->isoFormat('D MMM Y HH:mm') }}"
                        style="width: 100%;">
                        <div class="light white-text" style="position: absolute; bottom: 0; padding: 0.25rem 0.75rem; right: 0; left: 0; background: rgba(0, 0, 0, 0.35)">
                            {{ $item->created_at->isoFormat('D MMM Y HH:mm') }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
    <form class="fixed-action-btn">
        @csrf
        <input type="file" id="foto" accept="image/*" hidden>
        <label for="foto" class="btn-floating btn-large red lighten-1 waves-effect waves-light"><i class="material-icons">add_a_photo</i></label>
    </form>
</div>
@endsection

@section('body-end')
@include('pages.user.documentation.script')
@endsection

<div class="form-content question-set">
    <h5 class="light">Pendapatan</h5>
    <h6 class="light">Kolom bertanda bintang(<strong class="red-text">*</strong>) wajib diisi</h6>
    <br>
    <div class="row">
        <div class="input-field col s12 grey-text text-darken-2">
            <i class="material-icons prefix">equalizer</i>
            <input name="frekuensi" id="frekuensi" type="number" class="validate" data-length="6"
            value="{{ $data['kuesioner']->frekuensi ?? NULL }}" style="padding-right: 4rem;">
            <label for="frekuensi">FREKUENSI KUNJUNGAN SALES <strong class="red-text">*</strong></label>
            <h6 class="light center-align" style="position: absolute; top:0; right: 1rem; line-height:3rem; min-width: 3rem; margin: 0;">/ bulan</h6>
        </div>
        <div class="input-field col s12 grey-text text-darken-2">
            <i class="material-icons prefix">event</i>
            <textarea class="materialize-textarea" name="program" id="program" data-length="65535"
            >{{ $data['kuesioner']->program ?? NULL }}</textarea>
            <label for="program">PROGRAM SALES TAHUN {{ $data['rs']->tahun }}</label>
        </div>

        <div class="col s12 grey-text text-darken-2" style="position: relative; margin: 1rem 0;">
            <div style="position: relative; padding-left: 3rem;">
                <i class="material-icons" style="position: absolute; width: 3rem; font-size: 2rem; top: 0; left: 0;">show_chart</i>
                <h6 class="light" style="margin: 0; line-height: 2rem;">KONDISI PENJUALAN</h6>
                <p>
                    <label>
                        <input name="kondisi" type="radio" value="1"
                        {{ ($data['kuesioner']->pendapatan ?? 0) >= 0 ? 'checked' : '' }}/>
                        <span>MENINGKAT</span>
                    </label>
                <p>
                    <label>
                        <input name="kondisi" type="radio" value="2"
                        {{ ($data['kuesioner']->pendapatan ?? 0) < 0 ? 'checked' : '' }}/>
                        <span>MENURUN</span>
                    </label>
                </p>
            </div>
        </div>
        <div class="col s12">
            <div style="position: relative; padding-left: 3rem;">
                <h6 class="light grey-text text-darken-2" style="margin: 0; line-height: 2rem">SEBESAR</h6>
                <div class="input-field" style="position: relative">
                    <input name="pendapatan" id="pendapatan" type="number" class="validate" data-length="6" maxlength="6" max="100.00" step="0.2"
                    style="width: calc(100% - 3rem); margin-right: 3rem"
                    value="{{ $data['kuesioner']->pendapatan ?? NULL }}">
                    <h5 class="light center-align" style="position: absolute; top:0; right: 0; line-height:3rem; min-width: 3rem; margin: 0;">%</h5>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var aniMate = {
        'initialize': function(){

            $('.question-set').addClass('hide').removeClass('active');
            $('.question-set:eq(0)').addClass('active animate__animated animate__fadeInRight').removeClass('hide');
            $('.question-control .control-prev').addClass('hide').prop('disabled', true);
            aniMate.updateLoading();

            $('.question-control .control-next').on('click', function(e){
                aniMate.next();
            });
            $('.question-control .control-prev').on('click', function(e){
                aniMate.prev();
            });
        },
        'updateLoading': function(){
            var length = $('.question-set').length;
            var index = $('.question-set').index($('.question-set.active')) + 1;
            var percentage = (index / length * 100) - 2;

            $('.progress .determinate').css('width', percentage + '%');
        },
        'next': function(target){
            var parent = $('.question-set.active');
            var target = $(parent).next('.question-set');
            if(target){
                var target = $(target);
            }
            aniMate.requiredDetect(parent, function(){
                if($(target).length > 0){
                    $('.question-control .control-prev').removeClass('hide').addClass('animate__animated animate__fadeIn');
                    $('.question-control').find( '.control-prev, .control-next').prop('disabled', true);
                    $(parent).removeClass('active animate__animated animate__fadeInRight');
                    $(parent).addClass('animate__animated animate__fadeOutLeft');
                    $(target).addClass('active');
                    aniMate.updateLoading();

                    setTimeout(function(){
                        $(parent).removeClass('animate__animated animate__fadeOutLeft').addClass('hide');
                    }, 500);

                    setTimeout(function(){
                        $(target).removeClass('hide').addClass('animate__animated animate__fadeInRight');
                        if($(target).next('.question-set').length == 0){
                            $('.question-control .control-next').attr('type','submit');
                            $('.question-control .control-next[type=submit]').on('click.submit', function(e){
                                aniMate.requiredDetect();
                                $(e.target).off('click.submit')
                            })
                        }
                    }, 500);

                    setTimeout(function(){
                        $(target).removeClass('animate__animated animate__fadeInRight');
                        $('.question-control').find( '.control-prev, .control-next').prop('disabled', false);
                    }, 1000);

                    if($(target).next('.question-set').length == 0){
                        $('.question-control .control-next').removeClass('white deep-purple-text text-darken-1 waves-dark').addClass('deep-purple darken-1 white-text waves-light');
                        $('.question-control .control-next').html('<i class="material-icons" style="vertical-align: middle">save</i><span style="vertical-align: middle"> Simpan</span>');
                    }
                }
            });
        },
        'requiredDetect': function(target, callback){
            var valid = true;
            $.each($(target).find('[required]'), function(i, item){
                var value = $(item).val();
                if( (value == '') || (value == null) || (typeof(value) == undefined) ){
                    valid = false;
                    var form = $(item).parent('.question-set');
                    var text = $(item).siblings('label').text();
                    if(text == ''){
                        text = $(item).attr('placeholder');
                    }
                    text = text.replace(' *', '');
                    if(text == ''){
                        M.toast({html: 'Kolom bertanda (<strong class="red-text">*</strong>) wajib diisi!'})
                    }else{
                        M.toast({html: '<strong class="red-text">' + text + '</strong> wajib diisi!', displayLength: 2000})
                    }
                }
            });
            if(valid){
                callback();
            }
        },
        'prev': function(target){
            var parent = $('.question-set.active');
            var target = $(parent).prev('.question-set');
            if(target){
                var target = $(target);
            }
            if($(target).length > 0){
                $('.question-control').find( '.control-prev, .control-next').prop('disabled', true);
                $(parent).removeClass('active animate__animated animate__fadeInLeft');
                $(parent).addClass('animate__animated animate__fadeOutRight');
                $(target).addClass('active');
                aniMate.updateLoading();

                setTimeout(function(){
                    $(parent).removeClass('animate__animated animate__fadeOutRight').addClass('hide');
                }, 500);

                setTimeout(function(){
                    $(target).removeClass('hide').addClass('animate__animated animate__fadeInLeft');
                    $('.question-control .control-next').attr('type','button');
                    if($(target).prev('.question-set').length == 0){
                        $('.question-control .control-prev').addClass('hide').removeClass('animate__animated animate__fadeOut');
                    }
                }, 500);

                setTimeout(function(){
                    $(target).removeClass('animate__animated animate__fadeInLeft');
                    $('.question-control').find( '.control-prev, .control-next').prop('disabled', false);
                }, 1000);

                $('.question-control .control-next').addClass('white deep-purple-text text-darken-1 waves-dark').removeClass('deep-purple darken-1 white-text waves-light');
                $('.question-control .control-next').html('<span style="vertical-align: middle">Lanjut</span><i class="material-icons" style="vertical-align: middle">chevron_right</i>');

                if($(target).prev('.question-set').length == 0){
                    $('.question-control .control-prev').removeClass('animate__fadeIn').addClass('animate__animated animate__fadeOut');
                }
            }
        }
    }

    initialize();


    function initialize() {
        aniMate.initialize();
        picker();
        numberStrict();
        characterCounter();
    }


    function characterCounter(){
        const elems = $('input[data-length], textarea[data-length]');
        $(elems).characterCounter();
        $(elems).on('keypress keyup change', function(e) {
            var value = $(e.target).val();
            var length = value.length;
            var maxLength = $(e.target).attr('data-length');

            if((length >= maxLength) && !([8, 16, 17, 35, 36, 37, 38, 39, 40, 65].includes(e.keyCode))){
                $(e.target).val(value.substring(0, maxLength));
                return false;
            }
            return true;
        })
    }
    function numberStrict(){
        const elems = $('input[type=text], input[type=number]');
        $(elems).on('keyup', function(e) {
            if(![45, 190].includes(e.keyCode)){
                var value = $(e.target).val();
                $(e.target).val(value);
            }
        });
        $(elems).on('keypress keyup change', function(e) {
            var type = $(e.target).attr('type');
            var minZero = $(e.target).attr('min') > 0;
            var notDecimal = $(e.target).val() %1 == 0;
            var other_valid = [8, 16, 17, 46];
            if(!minZero){
                other_valid.push(45);
            }
            if(type == 'number'){
                if(!(notDecimal) && e.keyCode == 190){
                    return false;
                }
                var valid = (e.keyCode >= 48 && e.keyCode <= 57) ||
                            other_valid.includes(e.keyCode);
                return valid;
            }
        })
    }
    function detectMob() {
        const toMatch = [
            /Android/i,
            /webOS/i,
            /iPhone/i,
            /iPad/i,
            /iPod/i,
            /BlackBerry/i,
            /Windows Phone/i
        ];

        return toMatch.some((toMatchItem) => {
            return navigator.userAgent.match(toMatchItem);
        });
    }


    function picker(){
        if(!detectMob()){
            $('input[type=date]').attr('class', 'datepicker').attr('type', 'text').removeAttr('placeholder');
            $('input[type=time]').attr('class', 'timepicker').attr('type', 'text').removeAttr('placeholder');

            var date_elems = document.querySelectorAll('.datepicker');
            var time_elems = document.querySelectorAll('.timepicker');
            var date_instances = M.Datepicker.init(date_elems, {
                'format': 'dd/mm/yyyy',
                'container': 'body',
            })
            var time_instances = M.Timepicker.init(time_elems, {
                'twelveHour': false,
                'container': 'body'
            });
        }
    }


</script>

@extends('layouts.user')
@section('title','Questionnaire · ')
@section('id', 'questionnaire')

@section('navbar')
<div id="appbar" class="navbar-fixed">
    <nav class="deep-purple darken-2 white-text">
        <a class="btn-floating btn-flat waves-effect waves-light deep-purple darken-2 white-text"
            style="position: absolute; top: 50%; left: 2rem; transform: translate(-50%, -50%);"
            href="{{ url("rs/{$data['key']['keytahun']}/{$data['key']['keyrs']}") }}">
            <i class="material-icons" style="line-height:40px; height: 40x; font-size:200%;">chevron_left</i>
        </a>
        <h5 class="light truncate" style="position: absolute; margin: unset; top:50%; left: 4rem; right: 1rem; transform: translateY(-50%);">Profil Responden</h5>
        <div class="progress deep-purple lighten-4">
            <div id="progress-bar" class="determinate deep-purple lighten-1" style="width: 0%"></div>
        </div>
    </nav>
</div>
@stop
@section('contents')
<form class="form" style="margin: unset; display:flex; flex-direction: column; min-height: calc(100vh - 56px);" method="POST">
    @csrf
    @include('pages.user.questionnaire.section-1')
    @include('pages.user.questionnaire.section-2')
    @include('pages.user.questionnaire.section-3')
    <div class="navbottom question-control" style="margin-top: auto">
        <div class="navbottom-wrapper">
                <button type="button"
                    class="control-prev btn btn-flat waves-effect waves-dark white deep-purple-text text-darken-1"
                    style="margin-right: auto">
                    <i class="material-icons" style="vertical-align: middle">chevron_left</i>
                    <span style="vertical-align: middle">Kembali</span>
                </button>
                <button type="button"
                    class="control-next btn btn-flat waves-effect waves-dark white deep-purple-text text-darken-1"
                    style="margin-left: auto">
                    <span style="vertical-align: middle">Lanjut</span>
                    <i class="material-icons" style="vertical-align: middle">chevron_right</i>
                </button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('body-end')
@include('pages.user.questionnaire.script')
@endsection

<div class="form-content question-set">
    <section class="section">
        <h5 class="light">Identifikasi Rumah Sakit</h5>
        <h6 class="light">Kolom bertanda bintang(<strong class="red-text">*</strong>) wajib diisi</h6>
    </section>
    <div class="row">
        <div class="input-field col s12 grey-text text-darken-2">
            <i class="material-icons prefix">local_hospital</i>
            <textarea name="nama_rs" id="nama_rs" maxlength="64" data-length="64" class="materialize-textarea" required
            >{{ $data['rs']->nama_rs ?? NULL }}</textarea>
            <label for="nama_rs">NAMA <strong class="red-text">*</strong></label>
        </div>
        <div class="input-field col s12 grey-text text-darken-2">
            <i class="material-icons prefix">place</i>
            <textarea name="alamat" id="alamat" class="materialize-textarea" maxlength="255" data-length="255" required
            >{{ $data['rs']->alamat ?? NULL }}</textarea>
            <label for="alamat">ALAMAT <strong class="red-text">*</strong></label>
        </div>
        <div class="input-field col s12 grey-text text-darken-2" style="margin-top: 0">
            <i class="material-icons prefix"></i>
            <select class="browser-default" name="kota" id="kota" required>
                <option value="" disabled selected>PILIH KOTA</option>
                @foreach ($data['kota'] as $id => $option)
                    <option value="{{$id}}"
                    {{ strtolower($data['rs']->kota) == strtolower($option) ? 'selected' : '' }}
                    >{{$option}}</option>
                @endforeach
            </select>
            <label for="kota">Kota <strong class="red-text">*</strong></label>
        </div>
        <div class="input-field col s7 grey-text text-darken-2">
            <i class="material-icons prefix">apartment</i>
            <select class="browser-default" name="jenis" id="jenis" required>
                <option value="" disabled selected>PILIH JENIS</option>
                @foreach ($data['jenis'] as $id => $option)
                    <option value="{{$id}}"
                    {{ $data['rs']->status_pengelolaan == $id ? 'selected' : '' }}>{{$option}}</option>
                @endforeach
            </select>
            <label for="kelas">JENIS <strong class="red-text">*</strong></label>
        </div>
        <div class="input-field col s5 grey-text text-darken-2">
            <select class="browser-default" name="kelas" id="kelas" required>
                <option value="" disabled selected>PILIH KELAS</option>
                @foreach ($data['kelas'] as $id => $option)
                    <option value="{{$id}}"
                    {{ $data['rs']->kelas == $id ? 'selected' : '' }}>{{$option}}</option>
                @endforeach
            </select>
            <label for="kelas">KELAS <strong class="red-text">*</strong></label>
        </div>
        <div class="input-field col s12 m7 grey-text text-darken-2">
            <i class="material-icons prefix">home_work</i>
            <input name="luas" id="luas" type="number" class="validate" data-length="6" maxlength="6" max="99999.99" step="0.20" style="padding-right:2rem"
            value="{{ $data['kuesioner']->luas ?? NULL }}">
            <h6 class="light center-align" style="position: absolute; top:0; right: 0; line-height:3rem; min-width: 3rem; margin: 0;">m<sup>2</sup></h6>
            <label for="luas">LUAS</label>
        </div>
        <div class="input-field col s12 m5 grey-text text-darken-2" style="position: relative;">
            <i class="material-icons prefix">event</i>
            <input name="tahun_berdiri" id="tahun_berdiri" type="number" class="validate" data-length="4"
            value="{{ $data['kuesioner']->tahun_berdiri ?? NULL }}">
            <label for="tahun_berdiri">TAHUN BERDIRI</label>
        </div>
    </div>
</div>

<div class="form-content question-set">
    <section class="section">
        <h5 class="light">Data Wawancara</h5>
    </section>
    <div class="row">
        <div class="input-field col s12 grey-text text-darken-2">
            <i class="material-icons prefix">person</i>
            <textarea name="interviewer" id="interviewer" maxlength="64" data-length="64" class="materialize-textarea"
            >{{ $data['kuesioner']->interviewer ?? NULL }}</textarea>
            <label for="interviewer">INTERVIEWER</label>
        </div>
        <div class="input-field col s12 m4 grey-text text-darken-2">
            <i class="material-icons prefix">event</i>
            <input name="tanggal" id="tanggal" type="date" class="validate" placeholder=""
            value="{{ $data['kuesioner']->tanggal ?? NULL }}">
            <label for="tanggal">TANGGAL</label>
        </div>
        <div class="input-field col s7 m4 grey-text text-darken-2">
            <i class="material-icons prefix">schedule</i>
            <input name="waktu_mulai" id="waktu_mulai" type="time" class="validate" placeholder=""
            value="{{ $data['kuesioner']->waktu_mulai ?? NULL }}">
            <label for="waktu_mulai">WAKTU MULAI</label>
        </div>
        <div class="input-field col s5 m4 grey-text text-darken-2">
            <input name="waktu_selesai" id="waktu_selesai" type="time" class="validate" placeholder=""
            value="{{ $data['kuesioner']->waktu_selesai ?? NULL }}">
            <label for="waktu_selesai">WAKTU SELESAI</label>
        </div>
    </div>
</div>

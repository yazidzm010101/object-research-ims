@extends('layouts.user')
@section('title','Respondent · ')
@section('id', 'revenue')

@section('head-end')
<style>
    body{
        height: 100vh;
        max-height: 100vh;
        overflow: hidden;
    }
    tr.active input::placeholder{
        color: #424242 !important;
    }
</style>
@endsection

@section('navbar')
<div id="navbar" class="navbar-fixed">
    <nav class="green lighten-1 white-text">
        <div class="nav-wrapper">

            @section('nav-wrapper')
            <div class="brand-logo" style="position: unset;">
                <a href="#" data-target="slide-out" class="white-text hide-on-large-only sidenav-trigger" style="margin: 0;">
                    <i class="material-icons">menu</i>
                </a>
                <a href="#" data-target="slide-out" class="white-text hide-on-med-and-down sidenav-toggle" style="float: left; margin: 0;">
                    <i class="material-icons">menu</i>
                </a>
                <span class="white-text light truncate" style="padding-bottom: unset; display: block">Revenue - {{ $data['rs']->nama_rs }}</span>
            </div>
            @show

        </div>
    </nav>
</div>
@stop

@section('contents')
@include('pages.user.revenue.sidebar')
@include('pages.user.revenue.data')
@endsection

@section('body-end')
@include('pages.user.revenue.script')
@endsection

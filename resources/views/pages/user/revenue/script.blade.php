@php
$url = url("rs/{$data['keytahun']}/{$data['keyrs']}/revenue");
$ajaxUrl = url('/data/grup_2');
@endphp
<script>
    slideToggle();

    function slideToggle(){
        $('.sidenav-toggle').on('click', function(e){
            var targetId = '#' + $(e.target).attr('data-target');
            $(targetId).toggleClass('sidenav-toggled');
            $('.contents, #navbar nav, .modal-wrapper .modal').toggleClass('side-toggled');
        })
    }
</script>
<script>
    url = "{{ $url }}";

    toggleRow('[name*="ketersediaan"]');

    //   AJAX GRUP1
    $('#grup_1 .list-group-item').click(function (e) {
        updateGrup2($(e.target), '#grup_2');
    });

    $('#back-btn').click(function(e) {
        e.preventDefault();
        $('#slide-out').removeClass('toggled');
    })

    $('[name*="ketersediaan"]').change(function(e){
        toggleRow($(e.target));
    });

    $('input[name*=jumlah], input[name*=harga]').on('keyup', function(e){
        autoMultiply($(e.target));
    });

    $('#add-data').on('click', function(e){
        var row_length = $('[name*=row][name*=ketersediaan]').length + 1;
        var html =
            `<tr>
                <td class="center-align">
                    <button type="button" class="btn-floating btn-flat white waves-effect waves-dark"
                        onclick="$(this).parents('tr').remove()">
                        <i class="material-icons red-text">delete</i>
                    </button>
                </td>
                <td class="center-align">
                <input type="text" name="row[${row_length}][nama_grup_3]" placeholder="Nama Manufacture"/></td>
                <td class="center-align">
                    <label>
                        <input type="checkbox" id="row[${row_length}][ketersediaan]" name="row[${row_length}][ketersediaan]" class="filled-in"
                        value = 1
                        checked
                        style="position:relative"/>
                        <span></span>
                    </label>
                </td>
                <td><input type="text" name="row[${row_length}][brand]" value="" placeholder="Nama Brand Tanpa () Misal: Polifix"></td>
                <td><input type="text" name="row[${row_length}][ukuran]" value="" placeholder="Ukuran"></td>
                <td>
                <select name=row[${row_length}][tipe] class="browser-default">
                    <option value="" selected></option>
                    <option value="0">Box</option>
                    <option value="1">Rol</option>
                    <option value="2">Pcs</option>
                </select>
                </td>


                <td><input type="number" min="0" name="row[${row_length}][jumlah]" value="" class="total"></td>
                <td>
                <select name=row[${row_length}][satuan] class="browser-default">
                    <option value="" selected></option>
                    <option value="0">Box</option>
                    <option value="1">Rol</option>
                    <option value="2">Pcs</option>
                </select>
                </td>
                <td><input type="number" min="0" name="row[${row_length}][harga]" value="" class="total"></td>
                <td>
                    <input min="0" type="number" name="row[${row_length}][total]" class="total" readonly>
                </td>


                <td><input type="number" min="0" name="row[${row_length}][jumlah_q1]" value="" class="total1"></td>
                <td>
                <select name=row[${row_length}][satuan_q1] class="browser-default">
                    <option value="" selected></option>
                    <option value="0">Box</option>
                    <option value="1">Rol</option>
                    <option value="2">Pcs</option>
                </select>
                </td>
                <td><input type="number" min="0" name="row[${row_length}][harga_q1]" value="" class="total1"></td>
                <td>
                    <input min="0" type="number" name="row[${row_length}][total_q1]" class="total1" readonly>
                </td>


                <td><input type="number" min="0" name="row[${row_length}][jumlah_q2]" value="" class="total2"></td>
                <td>
                <select name=row[${row_length}][satuan_q2] class="browser-default">
                    <option value="" selected></option>
                    <option value="0">Box</option>
                    <option value="1">Rol</option>
                    <option value="2">Pcs</option>
                </select>
                </td>
                <td><input type="number" min="0" name="row[${row_length}][harga_q2]" value="" class="total2"></td>
                <td>
                    <input min="0" type="number" name="row[${row_length}][total_q2]" class="total2" readonly>
                </td>
            </tr>`;

        $('#grup_3').append(html);

        toggleRow('[name*="ketersediaan"]');

        $('[name*="ketersediaan"]').change(function(e){
            toggleRow($(e.target));
        });

        $('input[name*=jumlah], input[name*=harga]').on('keyup', function(e){
            autoMultiply($(e.target));
        });
    })

    function validate(selector, state){
        var siblings = 'input:not([name*=ketersediaan]), select:not([name*=ketersediaan])';
        if(state){
            $(selector).parents('tr').find(siblings).on('click.validate', function (e) {
                M.toast({
                    html: 'Centang kolom ketersediaan sebelum mulai mengisi!'
                })
            })
        }
        else{
            $(selector).parents('tr').find(siblings).off('click.validate');
        }
    }

    function toggleRow(selector){
        var select = $(selector);
        $.each(select, function(index, item){
            var value = $(item).prop('checked');
            if(!value){
                $(item).parents('tr').not('.data-new').removeClass('active');
                $(item).parents('tr').find('input, select').not('[name*="ketersediaan"]').attr('readonly', '').find('option').addClass('d-none');
                validate(item, true);
            }else{
                $(item).parents('tr').not('.data-new').addClass('active');
                $(item).parents('tr').find('input, select').not('[name*="ketersediaan"], [name*="total"]').removeAttr('readonly').find('option').removeClass('d-none');
                validate(item, false);
            }
        });
    }


    function autoMultiply(selector){
        var className = $(selector).attr('class').split(' ').filter(function(className){
            return className.indexOf("total") >= 0
        })[0];
        var unit = parseInt($(selector).parents('tr').find(`.${className}[name*="jumlah"]`).val());
        var harga = parseInt($(selector).parents('tr').find(`.${className}[name*="harga"]`).val());
        var result = parseInt(unit * harga) || 0;
        $(selector).parents('tr').find(`.${className}[name*="total"]`).val(result);
    }

    // change-in-production
    function updateGrup2(grup1, grup2){
        var id_grup_1 = $(grup1).attr('data-grup1');
        $(grup2).find('.sidebar-heading').nextAll().remove();
        var keyrs = "{{ $key['keyrs'] }}";

        $.ajax({
            url: "{{ $ajaxUrl }}/" + id_grup_1 +'/'+ keyrs,
            type: 'get',
            dataType: 'json',
            beforeSend: function() {
                $('#slide-out').append(
                    '<div class="overlay" style="z-index: 1000; position:fixed; top: 0: left: 0; height: 100%; width: 100%;">' +
                        '<div class="preloader-wrapper active">' +
                            '<div class="spinner-layer spinner-blue-only">' +
                                '<div class="circle-clipper left">' +
                                    '<div class="circle"></div>' +
                                '</div>' +
                                '<div class="gap-patch">' +
                                    '<div class="circle"></div>' +
                                '</div>' +
                                '<div class="circle-clipper right">' +
                                    '<div class="circle"></div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
            },
            success: function (response) {
                $('#slide-out .overlay').remove();
                // if the data requested was not null update
                if (response['data'].length > 0) {
                    $('#slide-out').addClass('toggled');
                    // iterate throug response and append it to group 2 select input
                    $.each(response['data'], function(index, value){
                        var id_grup_2 = value.id_grup_2;
                        var name = value.nama_grup_2;
                        var is_null = value.is_null;
                        var list = `<li><a href="${url}?g1=${id_grup_1}&g2=${id_grup_2}" class="list-group-item waves-effect truncate ${ is_null != null ? "blue-text text-darken-1" : '' }">${name}</a></li>`;
                        $(grup2).append(list);
                    });
                }else{
                    updateGrup3(id_grup_1, 0);
                }
            },
            error: function() {
                $('#slide-out .overlay').remove();
                M.toast({html: 'Terjadi kesalahan saat mengakses subkategori!'})
            }
        });
    }

    function updateGrup3(id_grup_1, id_grup_2){
        window.location.replace(`${url}?g1=${id_grup_1}&g2=${id_grup_2}`);
    }

</script>

<div class="contents">
    @if(count($data['data_lab']) > 0)
    <form action="" method='POST'>
        @csrf
        @if($grup_1 = $data['grup_1_aktif']->nama_grup_1 ?? FALSE)
        <section class="section">
            <h5 class="center-align light">
                {{ucwords(strtolower($grup_1))}}
                @if($grup_2 = $data['grup_2_aktif']->nama_grup_2 ?? FALSE)
                    - {{$grup_2}}
                @endif
            </h5>
            <p class="center-align">Save terlebih dahulu apabila ingin berpindah halaman.</p>
        </section>
        @endif
        <div class="fixed-action-btn">
            <button type="submit" class="btn-floating btn-large green lighten-1 white-text waves-effect waves-light">
                <i class="large material-icons">save</i>
            </button>
        </div>
        <div style="overflow: auto; min-width: 100%; max-height: 65vh;">
            <table class="hightlight">
                <thead class="grey darken-3 white-text light" style="z-index: 2">
                    <tr>
                        <th scope="col" class="center-align align-middle th-sm border-right-light" rowspan="2">#</th>
                        <th scope="col" class="center-align align-middle th-md" rowspan="2" id="deskripsiTest">Manufacture</th>
                        <th scope="col" class="center-align align-middle th-sm border-right-light" rowspan="2">Ketersediaan</th>
                        <th scope="col" class="center-align align-middle border-right-light" colspan="3">Product</th>
                        <th scope="col" class="center-align align-middle border-right-light" colspan="4" id="rev">Revenue
                            {{$rs['tahun'] - 1}}</th>
                        <th scope="col" class="center-align align-middle border-right-light" colspan="4" id="rev-q1">Revenue
                            Q1-{{$rs['tahun']}}<br>January - March</th>
                        <th scope="col" class="center-align align-middle" colspan="4" id="rev-q1">Revenue
                            Q2-{{$rs['tahun']}}<br>January - March</th>
                    </tr>
                    <tr>
                        <th scope="col" class="center-align align-middle th-lg" rowspan="2">Brand</th>
                        <th scope="col" class="center-align align-middle th-md" rowspan="2">Size</th>
                        <th scope="col" class="center-align align-middle th-sm border-right-light" rowspan="2">Pack</th>
                        <th scope="col" class="center-align align-middle th-sm">Jumlah Unit</th>
                        <th scope="col" class="center-align align-middle th-sm">Satuan</th>
                        <th scope="col" class="center-align align-middle th-md">Harga Perunit</th>
                        <th scope="col" class="center-align align-middle th-md border-right-light">Total</th>
                        <th scope="col" class="center-align align-middle th-sm">Jumlah Unit</th>
                        <th scope="col" class="center-align align-middle th-sm">Satuan</th>
                        <th scope="col" class="center-align align-middle th-md">Harga Perunit</th>
                        <th scope="col" class="center-align align-middle th-md border-right-light">Total</th>
                        <th scope="col" class="center-align align-middle th-sm">Jumlah Unit</th>
                        <th scope="col" class="center-align align-middle th-sm">Satuan</th>
                        <th scope="col" class="center-align align-middle th-md">Harga Perunit</th>
                        <th scope="col" class="center-align align-middle th-md">Total</th>
                    </tr>
                </thead>

                <tbody id="grup_3">
                    @php $row_id = 0 @endphp
                    @foreach($data['data_lab'] as $id => $item)
                    @php $row_id += 1 @endphp
                    <tr class="{{ $item->status == 1 ? 'data-new' : '' }}">
                        <td class="center-align">
                            {{$loop->iteration}}
                            <input type='text' name='row[{{$loop->iteration}}][id_grup_all]'
                                value="{{$item->id_grup_all_vw}}" hidden />
                        </td>
                        <td class="center-align">{{$item->nama_manufacture}}</td>
                        <td class="center-align">
                            <label>
                                <input type="checkbox" class="filled-in" value="1"
                                    name="row[{{$loop->iteration}}][ketersediaan]"
                                    {{ ($item->ketersediaan ?? '') == '1' ? 'checked' : ''  }}
                                    style="position:relative">
                                <span></span>
                            </label>
                        </td>
                        <td class="center-align th-lg">
                            {{$item->nama_brand}}
                        </td>
                        <td class="center-align th-md">
                            {{$item->ukuran}}
                        </td>
                        <td>
                            {{$item->nama_tipe}}
                        </td>
                        <td>
                            <input type="number" min="0" name="row[{{$loop->iteration}}][jumlah]"
                                value="{{$item->jumlah ?? ''}}" class="total">
                        </td>
                        <td>
                            <select name="row[{{$loop->iteration}}][satuan]" class="browser-default">
                                <option {{ $item->satuan == NULL ? 'selected' : '' }}></option>
                                @foreach(['Box', 'Rol', 'Pcs'] as $id => $option)
                                <option value="{{$id}}" {{ $item->satuan === "$id" ? 'selected' : '' }}>
                                    {{$option}}
                                </option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="number" min="0" name="row[{{$loop->iteration}}][harga]"
                                value="{{$item->harga ?? ''}}" class="total">
                        </td>
                        <td>
                            <input type="number" name="row[{{$loop->iteration}}][total]"
                            value="{{$item->total ?? ''}}" readonly class="total">
                        </td>
                        <td>
                            <input type="number" min="0" name="row[{{$loop->iteration}}][jumlah_q1]"
                                value="{{$item->jumlah_q1 ?? ''}}" class="total1">
                        </td>
                        <td>
                            <select name="row[{{$loop->iteration}}][satuan_q1]" class="browser-default">
                                <option {{ $item->satuan_q1 == NULL ? 'selected' : '' }}></option>
                                @foreach(['Box', 'Rol', 'Pcs'] as $id => $option)
                                <option value="{{$id}}" {{ $item->satuan_q1 === "$id" ? 'selected' : '' }}>
                                    {{$option}}
                                </option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="number" min="0" name="row[{{$loop->iteration}}][harga_q1]"
                                value="{{$item->harga_q1 ?? ''}}" class="total1">
                        </td>
                        <td>
                            <input type="number" name="row[{{$loop->iteration}}][total_q1]"
                            value="{{$item->total_q1 ?? ''}}" class="total1" readonly>
                        </td>
                        <td>
                            <input type="number" min="0" name="row[{{$loop->iteration}}][jumlah_q2]"
                                value="{{$item->jumlah_q2 ?? ''}}" class="total2">
                        </td>
                        <td>
                            <select name="row[{{$loop->iteration}}][satuan_q2]" class="browser-default">
                                <option {{ $item->satuan_q2 == NULL ? 'selected' : '' }}></option>
                                @foreach(['Box', 'Rol', 'Pcs'] as $id => $option)
                                <option value="{{$id}}" {{ $item->satuan_q2 === "$id" ? 'selected' : '' }}>
                                    {{$option}}
                                </option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="number" min="0" name="row[{{$loop->iteration}}][harga_q2]"
                                value="{{$item->harga_q2 ?? ''}}" class="total2">
                        </td>
                        <td>
                            <input type="number" name="row[{{$loop->iteration}}][total_q2]"
                            value="{{$item->total_q2 ?? ''}}" class="total2" readonly>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="19">
                            <button type="button" class="btn btn-flat btn grey darken-3 green-text text-lighten-1 waves-effect waves-light" id="add-data">
                                <i class="material-icons">add</i>
                                <span class="d-inline-block" style="line-height:42px; vertical-align:top">
                                    Lain-lain
                                </span>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="center-align">
        {{ $data['data_lab']->links('vendor.pagination.default') }}
    </div>
    @else
    <h5 class="center-align light" style="padding-top: 4rem">
        Silahkan pilih kategori terlebih dahulu pada menu <i class="material-icons" style="vertical-align:middle; line-height: inherit !important;">menu</i>
    </h5>
    @endif
</div>


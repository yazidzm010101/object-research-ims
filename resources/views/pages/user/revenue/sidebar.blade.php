@php $url = url("rs/{$data['keytahun']}/{$data['keyrs']}") @endphp
<ul id="slide-out" style="white-space: nowrap; overflow-x: hidden"
    class="sidenav sidenav-fixed {{ count($data['grup2']) > 0 ? 'toggled' : '' }}">
    <a type="button" class="btn-floating btn-flat white waves-effect waves-light"
        href="{{ $url }}"
        style="position: absolute; top: 0; right:0; background: transparent !important; padding:unset">
        <i class="material-icons white-text">home</i>
    </a>
    <div id="grup_1" style="width: 100%; vertical-align:top">
        <li class="sidebar-heading">
            <div class="user-view grey darken-3" style="padding: 2em 1em;">
                <h5 class="light white-text center-align" style="vertical-align: middle">Medical Category</h5>
            </div>
        </li>
        @foreach($data['grup1'] as $item)
            <li class="{{ $item->id_grup_1 == $data['id_grup_1'] ? 'active' : ''}}">
                <a
                    href="#"
                    class="waves-effect list-group-item {{ $item->is_null != NULL ? 'blue-text text-darken-1' : '' }}"
                    data-grup1="{{ $item->id_grup_1 }}">{{ $item->nama_grup_1}}</a>
            </li>
        @endforeach
    </div>
    <div id="grup_2" style="width: 100%; vertical-align:top">
        <li class="sidebar-heading">
            <div class="user-view grey darken-3" style="padding: 2em 1em;">
                <button type="button" id="back-btn" class="btn-floating btn-flat white waves-effect waves-light"
                style="position: absolute; top: 0; left:0; background: transparent !important;">
                    <i class="material-icons white-text large">chevron_left</i>
                </button>
                <h5 class="light white-text center-align" style="vertical-align: middle">Product Category</h5>
            </div>
        </li>
        @foreach($data['grup2'] as $item)
            <li class="{{ $item->id_grup_2 == $data['id_grup_2'] ? 'active' : '' }}">
                <a  href="{{ "{$url}/revenue?g1={$data['id_grup_1']}&g2={$item['id_grup_2']}" }}"
                    class="truncate waves-effect {{ $item->is_null != NULL ? "blue-text text-darken-1" : '' }}"
                    >{{ $item->nama_grup_2}}</a>
            </li>
        @endforeach
    </div>
</ul>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.min.css') }}">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>

    <title>@yield('title')IQVIA</title>
    @yield('head-end')
</head>

<body>
    @include('layouts.noscript')

    <div id="app" class="hide @yield('id')">

        @section('navbar')
        <div id="navbar" class="navbar-fixed">
            <nav class="white grey-text text-darken-4">
                <div class="nav-wrapper">
                    @section('nav-wrapper')
                    <div class="brand-logo" style="display: flex; width: 100%; align-items:center; padding: 0 1rem">
                        <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-large-only sidenav-trigger"
                            style="margin: 0;">
                            <i class="material-icons">menu</i>
                        </a>
                        <a href="#" data-target="slide-out" class="grey-text text-darken-1 hide-on-med-and-down sidenav-toggle"
                            style="margin: 0;">
                            <i class="material-icons">menu</i>
                        </a>
                        <span class="grey-text text-darken-2 light truncate">@yield('nav-title')</span>
                    </div>
                    @show
                </div>
            </nav>
        </div>
        @show

        @section('sidebar')
        <ul id="slide-out" class="sidenav sidenav-fixed">
            <li>
                <div class="user-view">
                    <div class="background">
                        <img class="responsive-img" src="https://i.pinimg.com/originals/85/2f/2d/852f2d88fe3ec8144d1d407eb5fcb3e3.png">
                    </div>
                    <a href="#user"><img class="circle" src="https://www.sunsetlearning.com/wp-content/uploads/2019/09/User-Icon-Grey-300x300.png"></a>
                    @guest
                    @else
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"
                        style="position: absolute; top:4px; right: 4px; background: transparent !important; line-height: 40px; height: 40px;"
                        class="btn-floating btn-flat waves-effect waves-light white">
                        <i class="material-icons">logout</i>
                    </a>
                    <a href="#name"><span class="white-text name">{{ Auth::user()->name }}</span></a>
                    <a href="#email"><span class="white-text email">{{ Auth::user()->email }}</span></a>
                    @endguest
                </div>
            </li>
            <li>
                <a href="#!">
                    <img class="responsive-img" src="{{asset('img/logo.png')}}" alt="IQVIA" style="max-height: 1.5em;">
                </a>
            </li>
            <li>
                <div class="divider"></div>
            </li>
            @if($nav ?? FALSE)
                @foreach ($nav as $item)
                    <li @if(Request::is($item['url'])) class="active"  @endif>
                        <a href="{{ url("{$item['url']}") }}" class="waves-effect">
                            <i class="material-icons">{{ $item['icon'] }}</i>{{$item['text']}}
                        </a>
                    </li>
                @endforeach
            @endif
            @if($subnav ?? FALSE)
                <li>
                    <div class="divider"></div>
                </li>
                <li class="active">
                    <a href="{{ url("{$subnav['head']['url']}") }}" class="waves-effect truncate">
                        {{$subnav['head']['text']}}
                    </a>
                </li>
                @foreach ($subnav['body'] as $item)
                    <li @if(Request::is($item['url'])) class="active" @endif>
                        <a href="{{ url("{$item['url']}") }}" class="waves-effect" style="padding-left: 3rem">
                            {{$item['text']}}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
        @show

        <div class="contents" style="min-width: 100%">
            @yield('contents')
        </div>

    </div>

    {{-- SCRIPT --}}
    <script>
        $('#app').removeClass('hide');
        M.AutoInit();
    </script>

    @section('body-end')
    <script>
        nav();
        slideToggle();

        function slideToggle(){
            $('.sidenav-toggle').on('click', function(e){
                var targetId = '#' + $(e.target).attr('data-target');
                $(targetId).toggleClass('sidenav-toggled');
                $('.contents, #navbar nav, .modal-wrapper .modal').toggleClass('side-toggled');
            })
        }

        function nav(){
            var y = 0;
            var y2 = 0;
            $(window).scroll(function(){
                var scrollTop = $(document).scrollTop();
                if(scrollTop > y){
                    if(y2 >= 64){
                        $('#navbar nav').css('transform', 'translateY(-64px)');
                        $('.fixed-action-btn').css('transform', 'scale(0)');
                    }else{
                        y2 += (scrollTop - y);
                    }
                }else{
                    y2 = 0;
                    $('#navbar nav').css('transform', 'translateY(0)');
                    $('.fixed-action-btn').css('transform', 'scale(1)');
                }
                y = scrollTop;
            })
        }
    </script>
    @show
    @yield('body-end-extends')
    @include('layouts.messages')

</body>

</html>

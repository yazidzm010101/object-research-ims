@if ($message = Session::get('success'))
  <script>
      M.toast({
          html: "<strong>@php echo($message); @endphp</strong>",
          classes: 'green-text text-lighten-1'
      })
  </script>
@endif

@if ($message = Session::get('error'))
  <script>
      M.toast({
          html: "<strong>@php echo($message); @endphp</strong>",
          classes: 'red-text text-lighten-1'
      })
  </script>
@endif

@if ($message = Session::get('warning'))
    <script>
        M.toast({
            html: "<strong>@php echo($message); @endphp</strong>",
            classes: 'yellow-text'
        })
    </script>
@endif

@if ($message = Session::get('info'))
    <script>
        M.toast({
            html: "<strong>@php echo($message); @endphp</strong>",
            classes: 'blue-text text-lighten-1'
        })
    </script>
@endif

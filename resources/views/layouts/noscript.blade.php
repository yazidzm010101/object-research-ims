<noscript>
    <div style="position: relative; min-height: 100vh; min-width: 100vw;">
        <div class="container center-align" style="position: absolute; top: 40%; left: 50%; transform: translate(-50%, -50%); max-width: 720px;">
            <img src="{{asset('img/logo.png')}}" class="img-fluid mb-4" alt="IQVIA" style="max-height: 3em">
            <h3 style="letter-spacing: 0.5em; margin-left:0.5em;">
                Javascript Tidak Aktif
            </h3>
            <h5 class="light">
                Halaman ini membutuhkan javascript untuk berjalan dengan baik, harap aktifkan javascript atau gunakan browser yang mendukung javascript untuk mengakses kembali
            </h5>
        </div>
    </div>
</noscript>

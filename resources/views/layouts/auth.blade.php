@extends('layouts.admin')
@section('id', 'login')
@section('head-end')
<style>
    #app{
        display: flex;
        align-items: center;
    }
    main{
        min-width: 100vw;
    }
    .card{
        border-radius: 0.5rem !important;
    }
    .card-content{
        padding: 3rem!important;
    }
    .card .material-icons{
        color: #606060;
    }
    @media only screen and (max-width: 575px){
        .container{
            width: 100%;
        }
        .card-content{
            padding: 3rem 1.5rem!important;
        }
        .card{
            box-shadow: unset;
            border: unset !important;
        }
        #app{
            align-items: flex-start;
        }

    }
</style>
@endsection

@section('navbar')
@stop

@section('sidebar')
@stop

@section('contents')
    <div class="container" style="max-width: 420px; margin:0 auto;">
        <div class="card">
            <div class="card-content">
                @yield('content')
            </div>
        </div>
    </div>
@endsection

@section('body-end')
@stop

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/index.min.css') }}">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script src="{{ asset('js/exif.min.js') }}"></script>
    <title>@yield('title')IQVIA</title>
    @yield('head-end')
</head>

<body>
    @include('layouts.noscript')
    {{-- APP --}}
    <div id="app" class="hide @yield('id')">
        @section('navbar')
        <div id="navbar" class="navbar-fixed">
            <nav class="white grey-text text-darken-4">
                <div class="nav-wrapper container">
                    <a href="#" class="brand-logo grey-text text-darken-3">
                        <img class="responsive-img" src="{{asset('img/logo.png')}}" alt="IQVIA">
                        <span class="hide-on-small-only">Medical Survey</span>
                    </a>
                    @if($rs_nama = $rs['nama'] ?? FALSE )
                        <ul id="nav-mobile" class="right">
                            <li>
                                <a href="sass.html" class="grey-text text-darken-2">
                                    <i class="material-icons" style="display: inline">account_circle</i>
                                    <span class="hide-on-small-only">{{ $rs_nama }}</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                </div>
            </nav>
        </div>
        @show
        @yield('contents')
    </div>

    {{-- SCRIPT --}}
    <script>
        $('#app').removeClass('hide');
        M.AutoInit();
    </script>
    @yield('body-end')
    @include('layouts.messages')

</body>

</html>

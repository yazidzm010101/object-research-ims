<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/index.min.css')}}">
    <title>@yield('title')IQVIA</title>
</head>
<body>
    <div style="position: relative; min-height: 100vh; min-width: 100vw;">
        <div class="container center-align" style="position: absolute; top: 40%; left: 50%; transform: translate(-50%, -50%); max-width: 720px;">
            <img src="{{asset('img/logo.png')}}" class="img-fluid mb-4" alt="IQVIA" style="max-height: 3em">
            <h3 style="letter-spacing: 0.5em; margin-left:0.5em;">
                @yield('status')
            </h3>
            <h5 class="light">
                @yield('message')
            </h5>
        </div>
    </div>
</body>
</html>

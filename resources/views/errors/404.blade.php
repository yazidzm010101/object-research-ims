@extends('layouts.errors')
@section('title', '404 Not Found · ')
@section('status', '404')
@section('message', 'Halaman yang anda cari tidak dapat ditemukan')

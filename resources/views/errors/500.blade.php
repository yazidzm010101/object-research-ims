@extends('layouts.errors')
@section('title', '500 Something Went Wrong · ')
@section('status', '500')
@section('message', 'Maaf terjadi kesalahaan, silahkan coba akses beberapa saat lagi')

-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 18, 2020 at 04:42 PM
-- Server version: 10.3.24-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jrxhmpdw_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2015_07_29_128731_create_tbl_config', 1),
(3, '2020_07_29_121925_create_tbl_main_rs', 1),
(4, '2020_07_29_122101_create_tbl_grup_1', 1),
(5, '2020_07_29_122124_create_tbl_grup_2', 1),
(6, '2020_07_29_122148_create_tbl_grup_3', 1),
(7, '2020_07_29_122158_create_tbl_grup_all', 1),
(8, '2020_07_29_122161_create_tbl_log_edit', 1),
(9, '2020_07_29_122211_create_tbl_main_audit', 1),
(10, '2020_07_29_127410_create_tbl_form', 1),
(11, '2020_07_29_128229_create_vw_grup_all', 1),
(12, '2020_07_29_130812_create_vw_raw_data', 1),
(13, '2020_07_29_133352_create_vw_log_edit', 1),
(14, '2020_07_31_232818_create_vw_grup_1_All', 1),
(15, '2020_08_01_000848_create_vw_grup_2_All', 1),
(16, '2020_08_01_001153_create_vw_grup_revenue', 1),
(17, '2020_08_02_095335_create_respondens_table', 1),
(18, '2020_08_02_095352_create_kuesioners_table', 1),
(19, '2020_08_02_103308_create_fotos_table', 1),
(20, '2020_08_04_063358_create_vw_last_progress', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `id_config` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_config`
--

INSERT INTO `tbl_config` (`id_config`, `status`) VALUES
(1, 0),
(2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form`
--

CREATE TABLE `tbl_form` (
  `tahun` year(4) NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_form`
--

INSERT INTO `tbl_form` (`tahun`, `link`, `status`) VALUES
(2020, 'http://localhost:8000/form/11469560', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_1`
--

CREATE TABLE `tbl_grup_1` (
  `id_grup_1` int(10) UNSIGNED NOT NULL,
  `nama_grup_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_1`
--

INSERT INTO `tbl_grup_1` (`id_grup_1`, `nama_grup_1`) VALUES
(1, 'ACUTE WOUND CARE'),
(2, 'ADVANCE WOUND CARE '),
(3, 'PHYSIOTHERAPY & BANDAGIING'),
(4, 'FRACTURE MANAGEMENT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_2`
--

CREATE TABLE `tbl_grup_2` (
  `id_grup_2` int(10) UNSIGNED NOT NULL,
  `nama_grup_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lini` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_grup_2` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_2`
--

INSERT INTO `tbl_grup_2` (`id_grup_2`, `nama_grup_2`, `nama_lini`, `status_grup_2`) VALUES
(1, 'Wide Area Fixation (Plester penutup luka lebar berwarna putih)', 'FIXATION', 0),
(2, 'Rubber Tapes ( Plester medis roll berwarna coklat)', 'FIXATION', 0),
(3, 'Special Fixation ( Pleseter Infus)', 'FIXATION', 0),
(4, 'Retention Bandage (Cohesive bandage) Perban Elastis Kohesif', 'FIXATION', 0),
(5, 'Low Allergies Tape  ( Plester  roll kertas untuk kulit sensitif) ', 'FIXATION', 0),
(6, 'Sterile Post Op (Plester Pasca Operasi)', 'DRESSING', 0),
(7, 'Surgical dressing ( Perban operasi)', 'DRESSING', 0),
(8, 'Urology/Folley Catheter ( Selang Kateter)', 'MISCELLANEOUS WOUNDCARE ', 0),
(9, 'Antimicrobial (Dressing Luka Anti Baketri)', 'WOUND BED PREPARATION', 0),
(10, 'Impregnated sterile dressing ( Tulle dressing berlapis bahan cair )', 'Advanced Wound Dressings', 0),
(11, 'Hydrocolloid/hydropolymers (Dressing hidrokoloid)', 'Advanced Wound Dressings', 0),
(12, 'Hydrogels ', 'Advanced Wound Dressings', 0),
(13, 'Alginates/hydrofibers (Pembalut Alginat)', 'Advanced Wound Dressings', 0),
(14, 'Foam Dressing (Pembalut penyerap eksudat)', 'Advanced Wound Dressings', 0),
(15, 'Non Adhesive Bandage/Crepe Bandage (Elastik perban tidak berperekat)', 'NON ADHESIVE SUPPORT ', 0),
(16, 'STRAPPING TAPES (Taping Kinesiologi)', 'Actimove', 0),
(17, 'Plaster Of Paris/POP Cast (Gips POP)', 'CAST', 0),
(18, 'Rigid-fiberglass casting (Casting Sintetik tahan air)', 'CAST', 0),
(19, 'Syntetic Splints (Splinting sintetik)', 'SPLINT', 0),
(20, 'Padding (Padding untuk gips)', 'Accessories', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_3`
--

CREATE TABLE `tbl_grup_3` (
  `id_grup_3` int(10) UNSIGNED NOT NULL,
  `nama_grup_3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_grup_3` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_3`
--

INSERT INTO `tbl_grup_3` (`id_grup_3`, `nama_grup_3`, `brand`, `status_grup_3`) VALUES
(1, 'Hypafix', 'BSN MEDICAL', 0),
(2, 'Polifix', 'SNAM', 0),
(3, 'Pharmafix ', 'Pharmaplast/ Global Dispo Medika', 0),
(4, 'Plesterin', 'OneMed', 0),
(5, 'Soft Cloth', '3M', 0),
(6, 'Fixomull Stretch', 'BSN Medical', 0),
(7, 'Ultrafix', 'OneMed', 0),
(8, 'Leukoplast', 'BSN MEDICAL', 0),
(9, 'Hansaplast', 'Beirsdorf', 0),
(10, 'Fescoplast ', 'FRESCO', 0),
(11, 'Leukomed IV Film', 'BSN MEDICAL', 0),
(12, 'Opsite IV 3000', 'Smith & Nephew', 0),
(13, '1624R Tegaderm IV Film', '3M', 0),
(14, 'Onemed Dermafix S IV', 'OneMed', 0),
(15, 'BBRAUN Askina Soft Clear IV', 'BBraun', 0),
(16, 'CANNOFIX-PU 6 CM X 8 CM', 'PHARMAPLAST S.A.E,EGYPT', 0),
(17, 'Elastomull Haft', 'BSN MEDICAL', 0),
(18, 'Pdsive', 'Pro Device', 0),
(19, 'Mediflex', 'Onemed', 0),
(20, 'Leukopor', 'BSN Medical', 0),
(21, 'Micropore', '3M', 0),
(22, 'Masterplast', 'Masterplast', 0),
(23, 'Leukomed T Plus ', 'BSN MEDICAL', 0),
(24, 'Opsite Post op ', 'Smith & Nephew', 0),
(25, 'Tegaderm? Absorbent Clear Acrylic Dressing', '3M', 0),
(26, 'Askina Soft & Clear', 'BBraun', 0),
(27, 'Dermafix', 'Onemed', 0),
(28, 'Curapor Transparent (Lohmann & Rauscher)            ', 'Lohmann & Rauscher', 0),
(29, 'Transparent Wound Dressing with Pad', 'Wayson Medical Semillas', 0),
(30, 'Leukomed Sorbact', 'BSN MEDICAL', 0),
(31, 'Aquacel Ag Surgical - Hydrofiber technology with polyurethane film', 'Convatec', 0),
(32, 'Postofix-PU', 'Pharmaplast', 0),
(33, 'Askina Soft', 'Bbraun', 0),
(34, 'Cutisorb Sterile', 'BSN Medical', 0),
(35, 'Askina Absorb plus ', 'Bbraun', 0),
(36, 'Super Absorbent', 'Winner', 0),
(37, 'Norta ', 'BSN MEDICAL', 0),
(38, 'Urocare Folley Catheter 2 Way ', 'Onemed', 0),
(39, '2 Ways, Folley Catheter', 'Remedi', 0),
(40, 'Euromed Foley Catheter', 'Uromed', 0),
(41, 'Folley Catheter 2 Way', 'Eskamed', 0),
(42, 'Latex Foley Catheter 2 Way ', 'Fesco', 0),
(43, 'Foleycath, 2 way latex ', 'Aximed', 0),
(44, 'Foley Catheter 2 Way', 'Bard Bardia', 0),
(45, '2Way Standard Foley Catheter Silicone Coated ', 'Idealcare', 0),
(46, 'Welford Medical Foley catheter 2 way ', 'Welforf Medical', 0),
(47, 'Foley Balloon Catheter 2 Way ', 'WRP', 0),
(48, 'RUSCH foley catheter ', 'RUSCH', 0),
(49, 'Cutimed Sorbact ', 'BSN MEDICAL', 0),
(50, 'Acticoat Flex 3 ', 'Smith & Nephew', 0),
(51, 'Askina Calgitrol Ag ', 'Bbraun', 0),
(52, 'Aquacell Ag Extra ', 'Convatec', 0),
(53, 'Pharma Super Foam Carbon Silver ', 'Pharmaplast', 0),
(54, 'Kill Bac ', 'KALBE', 0),
(55, 'CUTIMED SORBACT Gel ', 'BSN MEDICAL', 0),
(56, 'Idoform ', 'KALBE', 0),
(57, 'Cuticell Classic ', 'BSN MEDICAL', 0),
(58, 'Bactigrass ', 'Smith & Nephew', 0),
(59, 'Sofratulle ', 'Sanofi', 0),
(60, 'Daryantulle ', 'Darya-Varia', 0),
(61, 'Lomatulle ', 'Lohmann & Rauscher', 0),
(62, 'Framycetin Sulfat ', 'Darya Varia', 0),
(63, 'Excel Tulle ', 'Exceltis', 0),
(64, 'Cutimed Hydro L ', 'BSN MEDICAL', 0),
(65, 'Duoderm ', 'Convatec', 0),
(66, 'Suprasorb H ', 'Lohmann Rauscher', 0),
(67, 'Elect Hydro ', 'Smith Nephew', 0),
(68, 'Exelcare extra thin ', 'Exeltis', 0),
(69, 'Modress hydrocolloid ', 'SOHO', 0),
(70, 'CUTIMED Gel ', 'BSN MEDICAL', 0),
(71, 'Intrasite Gel ', 'Smith Nephew', 0),
(72, 'Askina Gel ', 'Bbraun', 0),
(73, 'Prontosan Wound Gel ', 'Bbraun', 0),
(74, 'Duoderm hydro active gel ', 'Convatec', 0),
(75, 'Exelcare hydrogel ', 'Exeltis', 0),
(76, 'Cavidagel ', 'Global Dispomedika', 0),
(77, 'Cutimed Alginate ', 'BSN MEDICAL', 0),
(78, 'Durafiber ', 'Smith Nephew', 0),
(79, 'Modress alginate', 'SOHO', 0),
(80, 'Kaltostat ', 'Convatec', 0),
(81, 'Suprasorb A ', 'Lohmann & Rauscher', 0),
(82, 'Cutimed Siltec ', 'BSN MEDICAL', 0),
(83, 'Allevyn non adhesive, Allevyn life ', 'Smith & Nephew', 0),
(84, 'Askina Foam ', 'Bbraun', 0),
(85, 'Aquacell Foam Dressing non adhesive  ', 'Convatec', 0),
(86, 'Tegaderm? High Performance Foam Non-Adhesive Dressing ', '3M', 0),
(87, 'Exelcare dressing foam ', 'Exeltis', 0),
(88, 'Silicone Foam Dressing ', 'Winner', 0),
(89, 'Super Foam Carbon ', 'Winner', 0),
(90, 'Modress foam ', 'SOHO', 0),
(91, 'Suprasorb P ', 'Lohmann & Rauscher', 0),
(92, 'Therasorb ', 'Darya Varia', 0),
(93, 'W-Care  ', 'Mahakam', 0),
(94, 'Tensocrepe ', 'BSN MEDICAL', 0),
(95, 'Leukocrepe ', 'BSN MEDICAL', 0),
(96, 'Uniflex ', 'BSN MEDICAL', 0),
(97, 'PD crepe ', 'Pro Device', 0),
(98, 'Policrepe ', 'SNA Medika', 0),
(99, 'FM Crepe ', 'SNA Medika', 0),
(100, 'Pro Crepe ', 'SNA Medika', 0),
(101, ' LR Crepe ', 'Lohmann & Rauscher', 0),
(102, 'High Elastic Bandage', 'Winner', 0),
(103, 'Leukotape - K ', 'BSN MEDICAL', 0),
(104, 'KT Gold ', 'Kinesiology Corp', 0),
(105, 'Kindmax Kinesiology Tape ', 'Kindmax', 0),
(106, 'SPOL ', 'Spol', 0),
(107, ' N- Tape ', 'Gandasari', 0),
(108, 'Gypsona ', 'BSN MEDICAL', 0),
(109, 'Polygip ', 'SNA Medika', 0),
(110, 'Eko Gips ', 'NewMaw Medical Ltd', 0),
(111, 'Cellona ', 'Pro Device', 0),
(112, 'Delta lite Conformable ', 'BSN MEDICAL', 0),
(113, 'SNA Cast ', 'SNA Medika', 0),
(114, 'Orthocast ', 'SOHO', 0),
(115, 'Cellacast ', 'Pro Device', 0),
(116, 'Dynacast Prelude ', 'BSN MEDICAL', 0),
(117, 'Ortho Splint', 'SOHO', 0),
(118, 'Medisplint ', 'MEDISPLIN', 0),
(119, 'Soffban', 'BSN MEDICAL', 0),
(120, 'Poliban', 'SNA Medika', 0),
(121, 'Mediban', 'Onemed', 0),
(122, 'Delta Dry', 'BSN MEDICAL', 0),
(123, 'Merek lainnya', 'GEA', 1),
(124, 'Smith & Nephew', 'OpsiTG post-op', 1),
(125, 'BSN Medical', 'Hansau USA', 1),
(126, 'onemed', 'dermatix', 1),
(127, 'GEA', 'Foley 2 Way', 1),
(128, 'GEA', 'Foley 2 Way', 1),
(129, 'GEA', 'Foley 2 Way', 1),
(130, 'GEA', 'Foley 2 Way', 1),
(131, 'GEA', 'Foley 2 Way', 1),
(132, 'GEA', 'ASD', 1),
(133, '3M', 'Micropool', 1),
(134, 'GEA', 'Foley Catheter', 1),
(135, 'GEA', 'Folay Chateter', 1),
(136, 'GEA', 'Foley Catheter', 1),
(137, 'GEA', 'Foley Catheter', 1),
(138, 'GEA', 'Foley Catheter', 1),
(139, 'GEA', 'Foley Catheter', 1),
(140, 'GEA', 'Foley 2 Way', 1),
(141, 'GEA', 'Foley 2 Way', 1),
(142, 'GEA', 'Foley 2 Way', 1),
(143, 'GEA', 'Foley 2 Way', 1),
(144, 'GEA', 'Foley 2 Way', 1),
(145, 'OneMed', 'Ultrafix', 1),
(146, 'Bard Bardia', 'Foley Catheter', 1),
(147, 'Bard Bardia', 'Foley Catheter', 1),
(148, 'Bard Bardia', 'Foley Catheter', 1),
(149, 'Bard Bardia', 'Foley Catheter', 1),
(150, 'Global Dispomedika', 'Cavidagel [ Global Dispomedikal ]', 1),
(151, 'BSN MEDICAL', 'Leukomed IV Film (BSN MEDICAL)', 1),
(152, 'RUSCH', 'RUSCH foley catheter (RUSCH)', 1),
(153, 'RUSCH', 'RUSCH foley catheter (RUSCH)', 1),
(154, 'GEA', 'lainnya', 1),
(155, 'FRESCO', 'Handiplast', 1),
(156, 'Wellkad', 'Foley Catheter', 1),
(157, 'Wellkad', 'Foley Catheter', 1),
(158, 'Wellkad', 'Foley Catheter', 1),
(159, 'Wellkad', 'Foley Catheter', 1),
(160, 'Wellkad', 'Foley Catheter', 1),
(161, 'Wellkad', 'Foley Catheter', 1),
(162, 'Wellkad', 'Foley Catheter', 1),
(163, 'Wellkad', 'Foley Catheter', 1),
(164, 'GEA', 'Foley Catheter', 1),
(165, 'Onemed', 'Medikan', 1),
(166, 'Merk Lainnya', 'Bard Bardia', 1),
(167, 'GEA', 'Foley Catheter', 1),
(168, 'GEA', 'Foley Catheter', 1),
(169, 'Foley Catheter', 'GEA (Foley Catheter)', 1),
(170, 'leukovix', 'leukovix', 1),
(171, 'Hansaplast', 'Hansaplast', 1),
(172, 'Opsite', 'Opsite', 1),
(173, 'FM crepe', 'FM crepe', 1),
(174, 'FM crepe', 'FM crepe', 1),
(175, 'FM crepe', 'FM crepe', 1),
(176, 'KALBE', 'Klink Bac [KALBE]', 1),
(177, 'KALBE MEDICAL', 'Cutimed Sorbact Gel [BSN Me', 1),
(178, 'OneMed', 'Isoplast', 1),
(179, 'Smith & Nephew', 'Opsite Post op', 1),
(180, 'Smith & Nephew', 'Opsite IV 3000 [Smith&Nephew', 1),
(181, 'Convatec', 'Duoderm hydro active gel [convatec', 1),
(182, 'Onemed', 'Dermafix (Onemed)', 1),
(183, '3M', 'Tegaderm? Absorbent Clear Acrylic Dressing', 1),
(184, '3M', 'Tegaderm? Absorbent Clear Acrylic Dressing', 1),
(185, '3M', 'Tega Derm', 1),
(186, '3M', 'Tega Derm', 1),
(187, 'Darya Varia', 'Therasorb Algiplus', 1),
(188, '3M', 'Micropore', 1),
(189, '3M', 'Micropore', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grup_all`
--

CREATE TABLE `tbl_grup_all` (
  `id_grup_all` int(10) UNSIGNED NOT NULL,
  `id_grup_1` int(10) UNSIGNED NOT NULL,
  `id_grup_2` int(10) UNSIGNED NOT NULL,
  `id_grup_3` int(10) UNSIGNED NOT NULL,
  `ukuran` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_grup_all`
--

INSERT INTO `tbl_grup_all` (`id_grup_all`, `id_grup_1`, `id_grup_2`, `id_grup_3`, `ukuran`, `tipe`) VALUES
(1, 1, 1, 1, '5 CM X 1 M ', 0),
(2, 1, 1, 1, '5 CM X 5 M', 0),
(3, 1, 1, 1, '10 CM X 5 M', 0),
(4, 1, 1, 1, '15 CM X 5 M', 0),
(5, 1, 1, 1, '20 CM X 5 M', 0),
(6, 1, 1, 2, '10 CM X 5 M', 0),
(7, 1, 1, 2, '15 CM X 5 M', 0),
(8, 1, 1, 2, '21 CM X 5 M', 0),
(9, 1, 1, 3, '5 CM X 5 M', 0),
(10, 1, 1, 3, '10 CM X 5 M', 0),
(11, 1, 1, 3, '10 CM X 10 M', 0),
(12, 1, 1, 3, '15 CM X 10 M', 0),
(13, 1, 1, 4, '5 CM X 5 M', 0),
(14, 1, 1, 4, '10 CM X 5 M', 0),
(15, 1, 1, 5, '5cm x 5m', 0),
(16, 1, 1, 5, '10cm x 5m', 0),
(17, 1, 1, 6, '5cm x 5m', 0),
(18, 1, 1, 6, '10cm x 5m', 0),
(19, 1, 1, 6, '15cm x 5m', 0),
(20, 1, 1, 7, '5cm x 5m', 0),
(21, 1, 1, 7, '10cm x 5m', 0),
(22, 1, 1, 7, '15cm x 5m', 0),
(23, 1, 2, 8, '1621H (0.5cm x 5m)', 0),
(24, 1, 2, 8, '1562 (1cm x 1m)', 0),
(25, 1, 2, 8, '1622 (1cm x 5m)', 0),
(26, 1, 2, 8, '1622H (1cm x 5m)', 0),
(27, 1, 2, 8, '1624 (2cm x 5m)', 0),
(28, 1, 2, 8, '1624H (2cm x 5m)', 0),
(29, 1, 2, 8, '1625 (3cm x 5m)', 0),
(30, 1, 2, 8, '1625H (3cm x 5m)', 0),
(31, 1, 2, 8, '1624H (5cm x 4,5m)', 0),
(32, 1, 2, 8, '7.5 CM x 4.5 M', 0),
(33, 1, 2, 8, '1625 H (5cm x 4 5m)', 0),
(34, 1, 2, 8, '1562 (5cmx1m) EDITION', 0),
(35, 1, 2, 9, '1,25 CM X 1 M', 0),
(36, 1, 2, 9, '1,25 CM X 5 M', 0),
(37, 1, 2, 10, '1.25 cm X 4.5 M', 0),
(38, 1, 2, 10, '2,5cm x 4,5m', 0),
(39, 1, 2, 10, '5cmx4.5m', 0),
(40, 1, 3, 11, '6 x 8 cm', 0),
(41, 1, 3, 12, '5 CM X 6 cm', 0),
(42, 1, 3, 12, '7 cm x 9 cm', 0),
(43, 1, 3, 13, '6cm x 7 cm', 0),
(44, 1, 3, 14, '6cm x 7 cm', 0),
(45, 1, 3, 15, '8cm x 6cm', 0),
(46, 1, 3, 16, '6cm x 8cm', 0),
(47, 1, 4, 17, '6cm x 4m', 0),
(48, 1, 4, 17, '8cm x 4m\n', 0),
(49, 1, 4, 17, '10cm x 4m\n', 0),
(50, 1, 4, 17, '6cm x 20m', 0),
(51, 1, 4, 17, '8cm x 20m', 0),
(52, 1, 4, 17, '10cm x 20m', 0),
(53, 1, 4, 18, '10cmx4m', 0),
(54, 1, 4, 19, '2,5cm', 0),
(55, 1, 4, 19, '5cm', 0),
(56, 1, 4, 19, '7,5cm', 0),
(57, 1, 4, 19, '10cm', 0),
(58, 1, 5, 20, '2453 ( 1.25CM x 9.2M', 0),
(59, 1, 5, 20, '2454 (2,5CM x 9.2M)\n', 0),
(60, 1, 5, 20, '2455 (5CM x 9.2M', 0),
(61, 1, 5, 20, '2456 (7,5cm x 9,2m)', 0),
(62, 1, 5, 21, '2.5 CM X 9.1 M', 0),
(63, 1, 5, 22, '0,5 x 10yds', 0),
(64, 1, 6, 23, '7.2cm x 5cm\n', 0),
(65, 1, 6, 23, '8cm x 10cm\n', 0),
(66, 1, 6, 23, '8cm x 15cm\n', 0),
(67, 1, 6, 23, '10cm x 25cm\n', 0),
(68, 1, 6, 23, '10cm x 30cm\n', 0),
(69, 1, 6, 23, '10cm x 35cm', 0),
(70, 1, 6, 24, '6.5 CM X 5 CM', 0),
(71, 1, 6, 24, '9.5 CM X 8.5 CM', 0),
(72, 1, 6, 24, '15 CM X 10 CM', 0),
(73, 1, 6, 24, '20 CM X 10 CM', 0),
(74, 1, 6, 25, '4,5 x 5cm', 0),
(75, 1, 6, 25, '7.6 x 9.5cm', 0),
(76, 1, 6, 26, '7.5 X 5 CM', 0),
(77, 1, 6, 26, '9 X 10 CM', 0),
(78, 1, 6, 26, '9 X 15 CM', 0),
(79, 1, 6, 26, '9 X 20CM', 0),
(80, 1, 6, 26, '9 X 25CM', 0),
(81, 1, 6, 26, '9 X 30CM', 0),
(82, 1, 6, 27, '5cm x 7cm', 0),
(83, 1, 6, 27, '10cm x 12cm', 0),
(84, 1, 6, 27, '10cm x 20cm', 0),
(85, 1, 6, 28, '10 x 15cm', 0),
(86, 1, 6, 28, '10 x 20cm', 0),
(87, 1, 6, 29, '6 x 7 cm', 0),
(88, 1, 6, 30, '8x15cm', 0),
(89, 1, 6, 31, '9cm x 10cm', 0),
(90, 1, 6, 31, '9cm x 15cm', 0),
(91, 1, 6, 31, '9cm x 20cm', 0),
(92, 1, 6, 32, '10 X 10CM', 0),
(93, 1, 6, 32, '10 X 15CM', 0),
(94, 1, 6, 32, '10 X 20CM', 0),
(95, 1, 6, 33, '7,5 x 5cm', 0),
(96, 1, 6, 33, '9 X 10 CM', 0),
(97, 1, 6, 33, '9 X 15 CM', 0),
(98, 1, 6, 33, '9 X 20CM', 0),
(99, 1, 6, 33, '9 X 25CM', 0),
(100, 1, 6, 33, '9 X 30CM', 0),
(101, 1, 7, 34, '10 x 10cm', 0),
(102, 1, 7, 34, '10 x 20cm', 0),
(103, 1, 7, 35, '10cm x 10cm', 0),
(104, 1, 7, 35, '10cm x 20cm', 0),
(105, 1, 7, 35, '20cm x 20cm', 0),
(106, 1, 7, 36, '7,5 X 7,5CM', 0),
(107, 1, 7, 36, '10 X 10CM', 0),
(108, 1, 7, 36, '10 X 20CM', 0),
(109, 1, 8, 37, '2WAY CH.14 (9385)', 0),
(110, 1, 8, 37, '2WAY CH.16 (9385)', 0),
(111, 1, 8, 37, '2WAY CH.18 (9385)', 0),
(112, 1, 8, 37, '2WAY CH.20 (9385)', 0),
(113, 1, 8, 37, '3WAY CH.22 (9388)', 0),
(114, 1, 8, 37, '3WAY CH.24 (9388)', 0),
(115, 1, 8, 37, '2WAY PED. CH6 (9413)', 0),
(116, 1, 8, 37, '2WAY PED. CH8 (9413)', 0),
(117, 1, 8, 37, '2WAY PED. CH10 (9413)', 0),
(118, 1, 8, 38, 'ch14', 0),
(119, 1, 8, 38, 'ch-16', 0),
(120, 1, 8, 38, 'ch-18', 0),
(121, 1, 8, 39, '10FR', 0),
(122, 1, 8, 39, '12FR', 0),
(123, 1, 8, 39, '16FR', 0),
(124, 1, 8, 39, '18FR', 0),
(125, 1, 8, 39, '20FR', 0),
(126, 1, 8, 40, '6FR', 0),
(127, 1, 8, 40, '8FR', 0),
(128, 1, 8, 40, '10FR', 0),
(129, 1, 8, 40, '12FR', 0),
(130, 1, 8, 40, '14FR', 0),
(131, 1, 8, 40, '16FR', 0),
(132, 1, 8, 40, '18FR', 0),
(133, 1, 8, 40, '20FR', 0),
(134, 1, 8, 40, '22FR', 0),
(135, 1, 8, 40, '24FR', 0),
(136, 1, 8, 41, '', 0),
(137, 1, 8, 42, '16FR', 0),
(138, 1, 8, 42, '18FR', 0),
(139, 1, 8, 43, '14FR', 0),
(140, 1, 8, 43, '16FR', 0),
(141, 1, 8, 43, '18FR', 0),
(142, 1, 8, 44, '', 0),
(143, 1, 8, 45, '14FR', 0),
(144, 1, 8, 45, '16FR', 0),
(145, 1, 8, 45, '18FR', 0),
(146, 1, 8, 46, '', 0),
(147, 1, 8, 47, '', 0),
(148, 1, 8, 48, '', 0),
(149, 2, 9, 49, '7 cm x 9 cm', 0),
(150, 2, 9, 50, '10 x 10cm', 0),
(151, 2, 9, 51, '10 x 10cm', 0),
(152, 2, 9, 51, '15 x 15cm', 0),
(153, 2, 9, 51, '20 x 20cm', 0),
(154, 2, 9, 52, '', 0),
(155, 2, 9, 53, '10 cm x 10 cm', 0),
(156, 2, 9, 53, '10cm x 20cm', 0),
(157, 2, 9, 54, '', 0),
(158, 2, 9, 55, '7,5cm x 7,5cm', 0),
(159, 2, 9, 55, '7,5cm x 15cm', 0),
(160, 2, 9, 56, '', 0),
(161, 2, 10, 57, '10cm x 10cm', 0),
(162, 2, 10, 57, '10cm x 40cm', 0),
(163, 2, 10, 58, '10cm x 10cm', 0),
(164, 2, 10, 59, '10cm x 10cm', 0),
(165, 2, 10, 60, '', 0),
(166, 2, 10, 61, '10cm x 10cm', 0),
(167, 2, 10, 62, '', 0),
(168, 2, 10, 63, '10cm x 10cm', 0),
(169, 2, 11, 64, '10cm x 10cm', 0),
(170, 2, 11, 65, '', 0),
(171, 2, 11, 66, '10 x 10cm', 0),
(172, 2, 11, 67, '10 x 10cm', 0),
(173, 2, 11, 68, '10 x 10cm', 0),
(174, 2, 11, 69, '', 0),
(175, 2, 12, 70, '1 x 25gr', 0),
(176, 2, 12, 71, '1 x15gr', 0),
(177, 2, 12, 72, '1 x15gr', 0),
(178, 2, 12, 73, '1 x 30ml', 0),
(179, 2, 12, 74, '1 x 30gr', 0),
(180, 2, 12, 75, '1 x 30gr', 0),
(181, 2, 12, 76, '1 x 15gr', 0),
(182, 2, 12, 76, '1x 30gr', 0),
(183, 2, 13, 77, '10cm x 10cm', 0),
(184, 2, 13, 78, '10cm x 10cm', 0),
(185, 2, 13, 79, '', 0),
(186, 2, 13, 80, '5x5cm', 0),
(187, 2, 13, 80, '7.5x12cm', 0),
(188, 2, 13, 80, '10x20cm', 0),
(189, 2, 13, 81, '5 x 5cm', 0),
(190, 2, 13, 81, '10 x 10cm', 0),
(191, 2, 14, 82, '10cm x 10cm', 0),
(192, 2, 14, 83, '5cm x 5cm', 0),
(193, 2, 14, 83, '10cm x 10cm', 0),
(194, 2, 14, 83, '15cm x 15cm', 0),
(195, 2, 14, 83, '20cm x 20cm', 0),
(196, 2, 14, 84, '5 cm x 7 cm', 0),
(197, 2, 14, 84, '10 cm x 10 cm', 0),
(198, 2, 14, 84, '10 cm x 20 cm', 0),
(199, 2, 14, 84, '20 cm x 20 cm', 0),
(200, 2, 14, 85, '10cm x 10cm', 0),
(201, 2, 14, 85, '17,5cm x 17,5cm', 0),
(202, 2, 14, 86, '', 0),
(203, 2, 14, 87, '10 x 10 cm', 0),
(204, 2, 14, 87, '15 x 15 cm', 0),
(205, 2, 14, 87, '20 x 20 cm', 0),
(206, 2, 14, 88, '10x10cm', 0),
(207, 2, 14, 88, '15 x 15 cm', 0),
(208, 2, 14, 89, '', 0),
(209, 2, 14, 90, '', 0),
(210, 2, 14, 91, '7,5 X 7,5cm', 0),
(211, 2, 14, 91, '10 X 10cm', 0),
(212, 2, 14, 91, '15 X 20cm', 0),
(213, 2, 14, 92, '10 x 10cm', 0),
(214, 2, 14, 93, '10 x 10cm', 0),
(215, 3, 15, 94, '7,5 x 2,3m', 0),
(216, 3, 15, 94, '10cm x 4.5m', 0),
(217, 3, 15, 94, '15cm x 4,5m', 0),
(218, 3, 15, 95, '7,5cm x 4,5m', 0),
(219, 3, 15, 95, '10cm x 4,5m', 0),
(220, 3, 15, 95, '15cm x 4,5m', 0),
(221, 3, 15, 96, '7,5cm x 4,5m', 0),
(222, 3, 15, 96, '10cm x 4,5m', 0),
(223, 3, 15, 96, '15cm x 4,5m', 0),
(224, 3, 15, 97, '7,5cm x 4,5m', 0),
(225, 3, 15, 97, '10cm x 4,5m', 0),
(226, 3, 15, 98, '7,5cm x 4,5cm', 0),
(227, 3, 15, 99, '7,5? cm X 4,5-4,55 m', 0),
(228, 3, 15, 99, '10 ? cm X 4,5-4,55 m', 0),
(229, 3, 15, 99, '15 ? cm X 4,5-4,55 m', 0),
(230, 3, 15, 100, '10cm x 4,5m', 0),
(231, 3, 15, 101, '', 0),
(232, 3, 15, 102, '', 0),
(233, 3, 16, 103, '5CMx5M BLUE 72978.21', 0),
(234, 3, 16, 103, '5CMx5M RED 72978.16', 0),
(235, 3, 16, 103, '5CMx5M SKIN 72978.11', 0),
(236, 3, 16, 103, 'Blue 5CMx5M 72978.28', 0),
(237, 3, 16, 103, 'Light Red 5CMx5M 72978.25', 0),
(238, 3, 16, 104, '', 0),
(239, 3, 16, 105, '5CM X 5M', 0),
(240, 3, 16, 106, '', 0),
(241, 3, 16, 107, '', 0),
(242, 3, 17, 108, '7,5cm x 2,75m', 0),
(243, 3, 17, 108, '7cm x 3,5m', 0),
(244, 3, 17, 108, '10cm x 3,5m', 0),
(245, 3, 17, 108, '15cm x 3,5m', 0),
(246, 3, 17, 108, '20cm x 3,5m', 0),
(247, 3, 17, 109, '', 0),
(248, 3, 17, 110, '', 0),
(249, 4, 17, 111, '', 0),
(250, 4, 18, 112, '5cm X 3.6m', 0),
(251, 4, 18, 112, '7.5cm x 3.6m', 0),
(252, 4, 18, 112, '10cm x 3.6m', 0),
(253, 4, 18, 112, '12.5cm x 3.6m', 0),
(254, 4, 18, 113, '16cm x 16cm', 0),
(255, 4, 18, 114, '', 0),
(256, 4, 18, 115, '', 0),
(257, 4, 19, 116, '2.5cm x 4.6m', 0),
(258, 4, 19, 116, '5cm x 4.6m', 0),
(259, 4, 19, 116, '7.5cm x 4.6m', 0),
(260, 4, 19, 116, '10cm x 4.6m', 0),
(261, 4, 19, 116, '12.5cm x 4.6m', 0),
(262, 4, 19, 116, '15cm x 4.6m', 0),
(263, 4, 19, 117, '', 0),
(264, 4, 19, 118, '', 0),
(265, 4, 20, 119, '5cm x 2.7m', 0),
(266, 4, 20, 119, '7.5cm x 2.7m', 0),
(267, 4, 20, 119, '10cm x 2.7m', 0),
(268, 4, 20, 119, '15cm x 2.7m', 0),
(269, 4, 20, 120, '7.5 cm x 2.7 m?', 0),
(270, 4, 20, 120, '10 cm x 2.7 m', 0),
(271, 4, 20, 120, '15 cm x 2.7 m', 0),
(272, 4, 20, 121, '7,5cm x 2,75m', 0),
(273, 4, 20, 121, '15cm x 2,7m', 0),
(274, 4, 20, 122, '5cm x 2.4m', 0),
(275, 4, 20, 122, '7.5cm x 2.4m', 0),
(276, 4, 20, 122, '10cm x 2.4m', 0),
(277, 1, 8, 123, '16', 2),
(278, 1, 6, 124, '25 X 10CM', 0),
(279, 1, 7, 125, '10 X 10CM', 2),
(280, 1, 6, 126, '10 x 25CM', NULL),
(281, 1, 8, 127, '8FR', 0),
(282, 1, 8, 128, '10FR', 0),
(283, 1, 8, 129, '14FR', 0),
(284, 1, 8, 130, '16FR', 0),
(285, 1, 8, 131, '18FR', 0),
(286, 1, 8, 132, NULL, NULL),
(287, 1, 2, 133, '2,5CM X 9m', 0),
(288, 1, 8, 134, '18', 2),
(289, 1, 7, 135, '18', NULL),
(290, 1, 8, 136, '8', 2),
(291, 1, 8, 137, '14', 2),
(292, 1, 8, 138, '16', 2),
(293, 1, 8, 139, '22', 2),
(294, 1, 8, 140, '8', 0),
(295, 1, 8, 141, '10', 0),
(296, 1, 8, 142, '14', 0),
(297, 1, 8, 143, '16', 0),
(298, 1, 8, 144, '18', 0),
(299, 1, 1, 145, '20cm x 5m', 0),
(300, 1, 8, 146, '16', 2),
(301, 1, 8, 147, '18', 2),
(302, 1, 8, 148, '20', 2),
(303, 1, 8, 149, '22', 2),
(304, 1, 5, 150, '1x15gr', NULL),
(305, 1, 3, 151, '10 x 25 cm', 0),
(306, 1, 8, 152, '16 FR', 2),
(307, 1, 8, 153, '18 FR', 2),
(308, 1, 8, 154, '18FR', 2),
(309, 1, 2, 155, '19mx65m', NULL),
(310, 1, 8, 156, '6', NULL),
(311, 1, 8, 157, '8', NULL),
(312, 1, 8, 158, '10', NULL),
(313, 1, 8, 159, '12', NULL),
(314, 1, 8, 160, '14', NULL),
(315, 1, 8, 161, '22', NULL),
(316, 1, 8, 162, '18', NULL),
(317, 1, 8, 163, '20', NULL),
(318, 1, 8, 164, '16', NULL),
(319, 4, 20, 165, '10 x 2,7m', NULL),
(320, 1, 8, 166, '14', NULL),
(321, 1, 8, 167, '16', NULL),
(322, 1, 8, 168, '18', NULL),
(323, 1, 8, 169, '20', NULL),
(324, 1, 1, 170, NULL, 1),
(325, 1, 2, 171, 'strip', NULL),
(326, 1, 3, 172, '2,5 x 10 cm', 2),
(327, 1, 4, 173, '3 inch', 1),
(328, 1, 4, 174, '4 inch', 1),
(329, 1, 4, 175, '6 inch', 1),
(330, 1, 6, 176, 'Box', NULL),
(331, 1, 6, 177, '7,5cm x 7,5cm', NULL),
(332, 1, 2, 178, NULL, 1),
(333, 1, 6, 179, '25 cm x 10 cm', 2),
(334, 1, 7, 180, '7cm x 9cm', 0),
(335, 1, 5, 181, '1 x 30gr', 0),
(336, 1, 6, 182, '10cm x 25cm', 0),
(337, 1, 6, 183, '10 x 12cm', 0),
(338, 1, 6, 184, '3.5 x 8.2cm', 0),
(339, 1, 6, 185, '3.5 x 8.6cm', 2),
(340, 1, 6, 186, '3.5 x 9.1cm', 2),
(341, 2, 14, 187, '10 x 10cm', 2),
(342, 1, 5, 188, '1 inchi', 2),
(343, 1, 5, 189, '0,5 inchi', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_edit`
--

CREATE TABLE `tbl_log_edit` (
  `id_log_edit` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_grup_2` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_log_edit`
--

INSERT INTO `tbl_log_edit` (`id_log_edit`, `id_rs`, `lokasi`, `id_grup_2`, `ip`, `created_at`, `updated_at`) VALUES
(1, 210, 'Palembang', 0, '112.215.175.4', '2020-08-14 02:52:05', '2020-08-14 02:52:05'),
(5, 33, 'Jakarta', 0, '125.161.138.221', '2020-08-14 21:43:30', '2020-08-14 21:43:30'),
(6, 33, 'Jakarta', 0, '125.161.138.221', '2020-08-14 21:47:55', '2020-08-14 21:47:55'),
(7, 33, 'Jakarta', 2, '125.161.138.221', '2020-08-14 21:49:42', '2020-08-14 21:49:42'),
(8, 33, 'Jakarta', 3, '125.161.138.221', '2020-08-14 21:50:35', '2020-08-14 21:50:35'),
(9, 36, 'Jakarta', 0, '125.161.138.221', '2020-08-14 21:51:57', '2020-08-14 21:51:57'),
(10, 33, 'Jakarta', 6, '125.161.138.221', '2020-08-14 21:53:02', '2020-08-14 21:53:02'),
(11, 33, 'Jakarta', 8, '125.161.138.221', '2020-08-14 21:55:56', '2020-08-14 21:55:56'),
(12, 33, 'Jakarta', 10, '125.161.138.221', '2020-08-14 21:57:43', '2020-08-14 21:57:43'),
(13, 17, 'Jakarta', 0, '125.161.138.221', '2020-08-14 21:57:52', '2020-08-14 21:57:52'),
(14, 33, 'Jakarta', 10, '125.161.138.221', '2020-08-14 21:57:58', '2020-08-14 21:57:58'),
(15, 36, 'Jakarta', 0, '125.161.138.221', '2020-08-14 21:58:21', '2020-08-14 21:58:21'),
(16, 33, 'Jakarta', 15, '125.161.138.221', '2020-08-14 22:00:20', '2020-08-14 22:00:20'),
(17, 33, 'Jakarta', 17, '125.161.138.221', '2020-08-14 22:02:45', '2020-08-14 22:02:45'),
(18, 33, 'Jakarta', 20, '125.161.138.221', '2020-08-14 22:03:50', '2020-08-14 22:03:50'),
(19, 36, 'Jakarta', 1, '125.161.138.221', '2020-08-14 22:04:46', '2020-08-14 22:04:46'),
(20, 33, 'Jakarta', 20, '125.161.138.221', '2020-08-14 22:05:24', '2020-08-14 22:05:24'),
(21, 36, 'Jakarta', 6, '125.161.138.221', '2020-08-14 22:06:16', '2020-08-14 22:06:16'),
(22, 17, 'Jakarta', 0, '125.161.138.221', '2020-08-14 22:06:42', '2020-08-14 22:06:42'),
(23, 17, 'Jakarta', 0, '125.161.138.221', '2020-08-14 22:06:43', '2020-08-14 22:06:43'),
(24, 36, 'Jakarta', 6, '125.161.138.221', '2020-08-14 22:06:57', '2020-08-14 22:06:57'),
(25, 17, 'Jakarta', 1, '125.161.138.221', '2020-08-14 22:09:44', '2020-08-14 22:09:44'),
(26, 36, 'Jakarta', 6, '125.161.138.221', '2020-08-14 22:11:04', '2020-08-14 22:11:04'),
(27, 184, 'Jakarta', 0, '125.161.138.221', '2020-08-14 22:11:19', '2020-08-14 22:11:19'),
(28, 17, 'Jakarta', 2, '125.161.138.221', '2020-08-14 22:11:32', '2020-08-14 22:11:32'),
(29, 36, 'Jakarta', 6, '125.161.138.221', '2020-08-14 22:12:16', '2020-08-14 22:12:16'),
(30, 17, 'Jakarta', 4, '125.161.138.221', '2020-08-14 22:14:11', '2020-08-14 22:14:11'),
(31, 36, 'Jakarta', 0, '125.161.138.221', '2020-08-14 22:14:24', '2020-08-14 22:14:24'),
(32, 36, 'Jakarta', 0, '125.161.138.221', '2020-08-14 22:14:46', '2020-08-14 22:14:46'),
(33, 184, 'Jakarta', 0, '125.161.138.221', '2020-08-14 22:15:26', '2020-08-14 22:15:26'),
(34, 17, 'Jakarta', 6, '125.161.138.221', '2020-08-14 22:16:01', '2020-08-14 22:16:01'),
(35, 17, 'Jakarta', 7, '125.161.138.221', '2020-08-14 22:21:22', '2020-08-14 22:21:22'),
(36, 17, 'Jakarta', 7, '125.161.138.221', '2020-08-14 22:25:29', '2020-08-14 22:25:29'),
(37, 17, 'Jakarta', 8, '125.161.138.221', '2020-08-14 23:37:00', '2020-08-14 23:37:00'),
(38, 17, 'Jakarta', 8, '125.161.138.221', '2020-08-14 23:39:03', '2020-08-14 23:39:03'),
(39, 17, 'Jakarta', 15, '125.161.138.221', '2020-08-14 23:40:33', '2020-08-14 23:40:33'),
(40, 17, 'Jakarta', 15, '125.161.138.221', '2020-08-14 23:41:03', '2020-08-14 23:41:03'),
(41, 184, 'Jakarta', 1, '125.161.138.221', '2020-08-14 23:42:48', '2020-08-14 23:42:48'),
(42, 184, 'Jakarta', 6, '125.161.138.221', '2020-08-14 23:45:23', '2020-08-14 23:45:23'),
(43, 25, 'Jakarta', 0, '125.161.138.221', '2020-08-14 23:46:12', '2020-08-14 23:46:12'),
(44, 184, 'Jakarta', 8, '125.161.138.221', '2020-08-14 23:46:55', '2020-08-14 23:46:55'),
(45, 184, 'Jakarta', 8, '125.161.138.221', '2020-08-14 23:48:02', '2020-08-14 23:48:02'),
(46, 25, 'Jakarta', 0, '125.161.138.221', '2020-08-14 23:48:35', '2020-08-14 23:48:35'),
(47, 25, 'Jakarta', 1, '125.161.138.221', '2020-08-14 23:50:55', '2020-08-14 23:50:55'),
(48, 184, 'Jakarta', 15, '125.161.138.221', '2020-08-14 23:51:19', '2020-08-14 23:51:19'),
(49, 24, 'Jakarta', 0, '125.161.138.221', '2020-08-14 23:51:32', '2020-08-14 23:51:32'),
(50, 184, 'Jakarta', 15, '125.161.138.221', '2020-08-14 23:51:34', '2020-08-14 23:51:34'),
(51, 25, 'Jakarta', 2, '125.161.138.221', '2020-08-14 23:51:52', '2020-08-14 23:51:52'),
(52, 24, 'Jakarta', 0, '125.161.138.221', '2020-08-14 23:52:14', '2020-08-14 23:52:14'),
(53, 184, 'Jakarta', 17, '125.161.138.221', '2020-08-14 23:54:09', '2020-08-14 23:54:09'),
(54, 25, 'Jakarta', 4, '125.161.138.221', '2020-08-14 23:54:39', '2020-08-14 23:54:39'),
(55, 25, 'Jakarta', 6, '125.161.138.221', '2020-08-14 23:55:41', '2020-08-14 23:55:41'),
(56, 184, 'Jakarta', 20, '125.161.138.221', '2020-08-14 23:56:02', '2020-08-14 23:56:02'),
(57, 24, 'Jakarta', 0, '125.161.138.221', '2020-08-14 23:56:29', '2020-08-14 23:56:29'),
(58, 24, 'Jakarta', 1, '125.161.138.221', '2020-08-14 23:58:32', '2020-08-14 23:58:32'),
(59, 25, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:00:23', '2020-08-15 00:00:23'),
(60, 164, 'Jakarta', 0, '125.161.138.221', '2020-08-15 00:00:31', '2020-08-15 00:00:31'),
(61, 25, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:00:37', '2020-08-15 00:00:37'),
(62, 25, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:01:14', '2020-08-15 00:01:14'),
(63, 24, 'Jakarta', 2, '125.161.138.221', '2020-08-15 00:02:26', '2020-08-15 00:02:26'),
(64, 164, 'Jakarta', 0, '125.161.138.221', '2020-08-15 00:03:33', '2020-08-15 00:03:33'),
(65, 25, 'Jakarta', 10, '125.161.138.221', '2020-08-15 00:04:10', '2020-08-15 00:04:10'),
(66, 24, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:06:05', '2020-08-15 00:06:05'),
(67, 164, 'Jakarta', 1, '125.161.138.221', '2020-08-15 00:06:38', '2020-08-15 00:06:38'),
(68, 25, 'Jakarta', 17, '125.161.138.221', '2020-08-15 00:07:31', '2020-08-15 00:07:31'),
(69, 164, 'Jakarta', 2, '125.161.138.221', '2020-08-15 00:07:36', '2020-08-15 00:07:36'),
(70, 164, 'Jakarta', 6, '125.161.138.221', '2020-08-15 00:08:52', '2020-08-15 00:08:52'),
(71, 44, 'Jakarta', 0, '125.161.138.221', '2020-08-15 00:09:18', '2020-08-15 00:09:18'),
(72, 9, 'Jakarta', 0, '125.161.138.221', '2020-08-15 00:09:41', '2020-08-15 00:09:41'),
(73, 164, 'Jakarta', 7, '125.161.138.221', '2020-08-15 00:10:14', '2020-08-15 00:10:14'),
(74, 44, 'Jakarta', 0, '125.161.138.221', '2020-08-15 00:11:23', '2020-08-15 00:11:23'),
(75, 164, 'Jakarta', 10, '125.161.138.221', '2020-08-15 00:11:37', '2020-08-15 00:11:37'),
(76, 164, 'Jakarta', 10, '125.161.138.221', '2020-08-15 00:12:07', '2020-08-15 00:12:07'),
(77, 44, 'Jakarta', 1, '125.161.138.221', '2020-08-15 00:21:34', '2020-08-15 00:21:34'),
(78, 9, 'Jakarta', 0, '125.161.138.221', '2020-08-15 00:21:37', '2020-08-15 00:21:37'),
(79, 44, 'Jakarta', 2, '125.161.138.221', '2020-08-15 00:22:44', '2020-08-15 00:22:44'),
(80, 44, 'Jakarta', 2, '125.161.138.221', '2020-08-15 00:23:04', '2020-08-15 00:23:04'),
(81, 9, 'Jakarta', 1, '125.161.138.221', '2020-08-15 00:23:59', '2020-08-15 00:23:59'),
(82, 9, 'Jakarta', 2, '125.161.138.221', '2020-08-15 00:25:48', '2020-08-15 00:25:48'),
(83, 44, 'Jakarta', 3, '125.161.138.221', '2020-08-15 00:26:30', '2020-08-15 00:26:30'),
(84, 9, 'Jakarta', 4, '125.161.138.221', '2020-08-15 00:27:27', '2020-08-15 00:27:27'),
(85, 44, 'Jakarta', 4, '125.161.138.221', '2020-08-15 00:28:33', '2020-08-15 00:28:33'),
(86, 44, 'Jakarta', 6, '125.161.138.221', '2020-08-15 00:29:51', '2020-08-15 00:29:51'),
(87, 9, 'Jakarta', 6, '125.161.138.221', '2020-08-15 00:30:16', '2020-08-15 00:30:16'),
(88, 9, 'Jakarta', 6, '125.161.138.221', '2020-08-15 00:31:26', '2020-08-15 00:31:26'),
(89, 9, 'Jakarta', 6, '125.161.138.221', '2020-08-15 00:32:03', '2020-08-15 00:32:03'),
(90, 44, 'Jakarta', 6, '125.161.138.221', '2020-08-15 00:32:04', '2020-08-15 00:32:04'),
(91, 9, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:38:56', '2020-08-15 00:38:56'),
(92, 9, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:39:14', '2020-08-15 00:39:14'),
(93, 9, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:39:23', '2020-08-15 00:39:23'),
(94, 44, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:41:35', '2020-08-15 00:41:35'),
(95, 9, 'Jakarta', 12, '125.161.138.221', '2020-08-15 00:42:58', '2020-08-15 00:42:58'),
(96, 44, 'Jakarta', 9, '125.161.138.221', '2020-08-15 00:43:50', '2020-08-15 00:43:50'),
(97, 44, 'Jakarta', 10, '125.161.138.221', '2020-08-15 00:44:36', '2020-08-15 00:44:36'),
(98, 44, 'Jakarta', 12, '125.161.138.221', '2020-08-15 00:45:13', '2020-08-15 00:45:13'),
(99, 9, 'Jakarta', 14, '125.161.138.221', '2020-08-15 00:45:32', '2020-08-15 00:45:32'),
(100, 44, 'Jakarta', 13, '125.161.138.221', '2020-08-15 00:45:56', '2020-08-15 00:45:56'),
(101, 44, 'Jakarta', 15, '125.161.138.221', '2020-08-15 00:47:44', '2020-08-15 00:47:44'),
(102, 9, 'Jakarta', 15, '125.161.138.221', '2020-08-15 00:52:51', '2020-08-15 00:52:51'),
(103, 9, 'Jakarta', 16, '125.161.138.221', '2020-08-15 00:54:25', '2020-08-15 00:54:25'),
(104, 25, 'Jakarta', 8, '125.161.138.221', '2020-08-15 00:55:22', '2020-08-15 00:55:22'),
(105, 9, 'Jakarta', 17, '125.161.138.221', '2020-08-15 00:55:27', '2020-08-15 00:55:27'),
(106, 9, 'Jakarta', 18, '125.161.138.221', '2020-08-15 01:00:06', '2020-08-15 01:00:06'),
(107, 9, 'Jakarta', 18, '125.161.138.221', '2020-08-15 01:00:41', '2020-08-15 01:00:41'),
(108, 56, 'Jakarta', 0, '125.161.138.221', '2020-08-15 01:05:05', '2020-08-15 01:05:05'),
(109, 9, 'Jakarta', 19, '125.161.138.221', '2020-08-15 01:09:16', '2020-08-15 01:09:16'),
(110, 9, 'Jakarta', 19, '125.161.138.221', '2020-08-15 01:09:38', '2020-08-15 01:09:38'),
(111, 56, 'Jakarta', 0, '125.161.138.221', '2020-08-15 01:09:53', '2020-08-15 01:09:53'),
(112, 9, 'Jakarta', 20, '125.161.138.221', '2020-08-15 01:12:05', '2020-08-15 01:12:05'),
(113, 56, 'Jakarta', 1, '125.161.138.221', '2020-08-15 01:14:05', '2020-08-15 01:14:05'),
(114, 56, 'Jakarta', 1, '125.161.138.221', '2020-08-15 01:14:10', '2020-08-15 01:14:10'),
(115, 56, 'Jakarta', 2, '125.161.138.221', '2020-08-15 01:16:17', '2020-08-15 01:16:17'),
(116, 56, 'Jakarta', 2, '125.161.138.221', '2020-08-15 01:17:01', '2020-08-15 01:17:01'),
(117, 56, 'Jakarta', 3, '125.161.138.221', '2020-08-15 01:18:21', '2020-08-15 01:18:21'),
(118, 56, 'Jakarta', 4, '125.161.138.221', '2020-08-15 01:19:07', '2020-08-15 01:19:07'),
(119, 56, 'Jakarta', 5, '125.161.138.221', '2020-08-15 01:19:37', '2020-08-15 01:19:37'),
(120, 56, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:20:58', '2020-08-15 01:20:58'),
(121, 56, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:21:30', '2020-08-15 01:21:30'),
(122, 44, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:24:17', '2020-08-15 01:24:17'),
(123, 44, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:25:04', '2020-08-15 01:25:04'),
(124, 56, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:26:31', '2020-08-15 01:26:31'),
(125, 56, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:27:56', '2020-08-15 01:27:56'),
(126, 56, 'Jakarta', 10, '125.161.138.221', '2020-08-15 01:31:38', '2020-08-15 01:31:38'),
(127, 56, 'Jakarta', 15, '125.161.138.221', '2020-08-15 01:32:58', '2020-08-15 01:32:58'),
(128, 56, 'Jakarta', 20, '125.161.138.221', '2020-08-15 01:37:47', '2020-08-15 01:37:47'),
(129, 56, 'Jakarta', 20, '125.161.138.221', '2020-08-15 01:38:10', '2020-08-15 01:38:10'),
(130, 5, 'Jakarta', 0, '125.161.138.221', '2020-08-15 01:39:13', '2020-08-15 01:39:13'),
(131, 5, 'Jakarta', 0, '125.161.138.221', '2020-08-15 01:42:24', '2020-08-15 01:42:24'),
(132, 5, 'Jakarta', 1, '125.161.138.221', '2020-08-15 01:44:36', '2020-08-15 01:44:36'),
(133, 5, 'Jakarta', 2, '125.161.138.221', '2020-08-15 01:45:57', '2020-08-15 01:45:57'),
(134, 5, 'Jakarta', 3, '125.161.138.221', '2020-08-15 01:47:03', '2020-08-15 01:47:03'),
(135, 5, 'Jakarta', 4, '125.161.138.221', '2020-08-15 01:48:50', '2020-08-15 01:48:50'),
(136, 5, 'Jakarta', 5, '125.161.138.221', '2020-08-15 01:49:31', '2020-08-15 01:49:31'),
(137, 5, 'Jakarta', 6, '125.161.138.221', '2020-08-15 01:50:32', '2020-08-15 01:50:32'),
(138, 5, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:58:16', '2020-08-15 01:58:16'),
(139, 5, 'Jakarta', 8, '125.161.138.221', '2020-08-15 01:58:35', '2020-08-15 01:58:35'),
(140, 5, 'Jakarta', 10, '125.161.138.221', '2020-08-15 02:00:36', '2020-08-15 02:00:36'),
(141, 5, 'Jakarta', 11, '125.161.138.221', '2020-08-15 02:01:20', '2020-08-15 02:01:20'),
(142, 5, 'Jakarta', 11, '125.161.138.221', '2020-08-15 02:01:35', '2020-08-15 02:01:35'),
(143, 5, 'Jakarta', 15, '125.161.138.221', '2020-08-15 02:03:39', '2020-08-15 02:03:39'),
(144, 5, 'Jakarta', 17, '125.161.138.221', '2020-08-15 02:04:29', '2020-08-15 02:04:29'),
(145, 125, 'Jakarta', 0, '125.161.138.221', '2020-08-15 02:05:09', '2020-08-15 02:05:09'),
(146, 5, 'Jakarta', 18, '125.161.138.221', '2020-08-15 02:06:17', '2020-08-15 02:06:17'),
(147, 125, 'Jakarta', 0, '125.161.138.221', '2020-08-15 02:06:29', '2020-08-15 02:06:29'),
(148, 5, 'Jakarta', 19, '125.161.138.221', '2020-08-15 02:07:58', '2020-08-15 02:07:58'),
(149, 5, 'Jakarta', 19, '125.161.138.221', '2020-08-15 02:08:08', '2020-08-15 02:08:08'),
(150, 125, 'Jakarta', 1, '125.161.138.221', '2020-08-15 02:10:23', '2020-08-15 02:10:23'),
(151, 213, 'Jakarta', 0, '114.4.78.41', '2020-08-15 08:29:48', '2020-08-15 08:29:48'),
(152, 213, 'Jakarta', 1, '114.4.78.41', '2020-08-15 08:43:01', '2020-08-15 08:43:01'),
(153, 213, 'Jakarta', 2, '114.4.78.41', '2020-08-15 08:45:33', '2020-08-15 08:45:33'),
(154, 213, 'Jakarta', 3, '114.4.78.41', '2020-08-15 08:46:43', '2020-08-15 08:46:43'),
(155, 213, 'Jakarta', 6, '114.4.78.41', '2020-08-15 08:47:48', '2020-08-15 08:47:48'),
(156, 213, 'Jakarta', 8, '114.4.78.41', '2020-08-15 08:49:46', '2020-08-15 08:49:46'),
(157, 213, 'Jakarta', 10, '114.4.78.41', '2020-08-15 08:51:49', '2020-08-15 08:51:49'),
(158, 213, 'Jakarta', 12, '114.4.78.41', '2020-08-15 08:52:54', '2020-08-15 08:52:54'),
(159, 213, 'Jakarta', 12, '114.4.78.41', '2020-08-15 08:53:10', '2020-08-15 08:53:10'),
(160, 213, 'Jakarta', 15, '114.4.78.41', '2020-08-15 08:54:42', '2020-08-15 08:54:42'),
(161, 213, 'Jakarta', 17, '114.4.78.41', '2020-08-15 08:55:43', '2020-08-15 08:55:43'),
(162, 213, 'Jakarta', 20, '114.4.78.41', '2020-08-15 08:56:24', '2020-08-15 08:56:24'),
(163, 213, 'Jakarta', 20, '114.4.78.41', '2020-08-15 08:56:53', '2020-08-15 08:56:53'),
(164, 217, 'Bontang', 0, '114.125.188.240', '2020-08-15 10:18:19', '2020-08-15 10:18:19'),
(165, 217, 'Bontang', 1, '114.125.188.240', '2020-08-15 10:35:56', '2020-08-15 10:35:56'),
(166, 217, 'Bontang', 1, '114.125.188.240', '2020-08-15 10:38:14', '2020-08-15 10:38:14'),
(167, 217, 'Bontang', 1, '114.125.188.240', '2020-08-15 10:38:53', '2020-08-15 10:38:53'),
(168, 217, 'Bontang', 1, '114.125.188.240', '2020-08-15 10:38:53', '2020-08-15 10:38:53'),
(169, 217, 'Bontang', 1, '114.125.188.240', '2020-08-15 10:38:53', '2020-08-15 10:38:53'),
(170, 217, 'Bontang', 10, '114.125.188.240', '2020-08-15 10:56:30', '2020-08-15 10:56:30'),
(171, 217, 'Bontang', 9, '114.125.188.240', '2020-08-15 11:02:35', '2020-08-15 11:02:35'),
(172, 217, 'Bontang', 4, '114.125.188.240', '2020-08-15 11:06:10', '2020-08-15 11:06:10'),
(173, 217, 'Bontang', 5, '114.125.188.240', '2020-08-15 11:08:51', '2020-08-15 11:08:51'),
(174, 217, 'Bontang', 5, '114.125.188.240', '2020-08-15 11:16:51', '2020-08-15 11:16:51'),
(175, 217, 'Bontang', 20, '114.125.188.240', '2020-08-15 11:18:46', '2020-08-15 11:18:46'),
(176, 217, 'Bontang', 20, '114.125.188.240', '2020-08-15 11:21:50', '2020-08-15 11:21:50'),
(177, 219, 'Bontang', 0, '114.125.188.240', '2020-08-15 11:46:54', '2020-08-15 11:46:54'),
(178, 219, 'Bontang', 1, '114.125.188.240', '2020-08-15 11:49:00', '2020-08-15 11:49:00'),
(179, 219, 'Bontang', 1, '114.125.188.240', '2020-08-15 11:51:21', '2020-08-15 11:51:21'),
(180, 219, 'Bontang', 5, '114.125.188.240', '2020-08-15 11:58:35', '2020-08-15 11:58:35'),
(181, 220, 'Bontang', 0, '114.125.188.240', '2020-08-15 12:08:17', '2020-08-15 12:08:17'),
(182, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:11:44', '2020-08-15 12:11:44'),
(183, 220, 'Bontang', 1, '114.125.188.240', '2020-08-15 12:11:54', '2020-08-15 12:11:54'),
(184, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:12:03', '2020-08-15 12:12:03'),
(185, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:12:13', '2020-08-15 12:12:13'),
(186, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:19:55', '2020-08-15 12:19:55'),
(187, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:24:17', '2020-08-15 12:24:17'),
(188, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:24:47', '2020-08-15 12:24:47'),
(189, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:25:25', '2020-08-15 12:25:25'),
(190, 221, 'Jakarta', 0, '36.68.235.43', '2020-08-15 12:25:39', '2020-08-15 12:25:39'),
(191, 221, 'Medan', 1, '114.125.47.253', '2020-08-15 12:55:38', '2020-08-15 12:55:38'),
(192, 215, 'Jakarta', 0, '114.5.219.33', '2020-08-15 17:33:46', '2020-08-15 17:33:46'),
(193, 215, 'Jakarta', 1, '114.5.219.33', '2020-08-15 17:36:41', '2020-08-15 17:36:41'),
(194, 215, 'Jakarta', 1, '114.5.219.33', '2020-08-15 17:36:58', '2020-08-15 17:36:58'),
(195, 135, 'Jakarta', 0, '114.5.219.33', '2020-08-15 17:48:27', '2020-08-15 17:48:27'),
(196, 135, 'Jakarta', 1, '114.5.219.33', '2020-08-15 17:51:57', '2020-08-15 17:51:57'),
(197, 135, 'Jakarta', 3, '114.5.219.33', '2020-08-15 17:54:14', '2020-08-15 17:54:14'),
(198, 135, 'Jakarta', 6, '114.5.219.33', '2020-08-15 17:56:04', '2020-08-15 17:56:04'),
(199, 135, 'Jakarta', 8, '114.5.219.33', '2020-08-15 17:59:33', '2020-08-15 17:59:33'),
(200, 135, 'Jakarta', 8, '114.5.219.33', '2020-08-15 18:01:18', '2020-08-15 18:01:18'),
(201, 135, 'Jakarta', 8, '114.5.219.33', '2020-08-15 18:02:37', '2020-08-15 18:02:37'),
(202, 135, 'Jakarta', 8, '114.5.219.33', '2020-08-15 18:04:57', '2020-08-15 18:04:57'),
(203, 135, 'Jakarta', 8, '114.5.219.33', '2020-08-15 18:06:47', '2020-08-15 18:06:47'),
(204, 135, 'Jakarta', 8, '114.5.219.33', '2020-08-15 18:07:08', '2020-08-15 18:07:08'),
(205, 135, 'Jakarta', 17, '114.5.219.33', '2020-08-15 18:09:15', '2020-08-15 18:09:15'),
(206, 222, 'Jakarta', 0, '114.5.219.33', '2020-08-15 18:13:40', '2020-08-15 18:13:40'),
(207, 222, 'Jakarta', 1, '114.5.219.33', '2020-08-15 18:18:13', '2020-08-15 18:18:13'),
(208, 222, 'Jakarta', 1, '114.5.219.33', '2020-08-15 18:19:02', '2020-08-15 18:19:02'),
(209, 222, 'Jakarta', 2, '114.5.219.33', '2020-08-15 18:21:21', '2020-08-15 18:21:21'),
(210, 222, 'Jakarta', 6, '114.5.219.33', '2020-08-15 18:22:25', '2020-08-15 18:22:25'),
(211, 222, 'Jakarta', 6, '114.5.219.33', '2020-08-15 18:22:36', '2020-08-15 18:22:36'),
(212, 223, 'Bakau', 0, '114.125.188.36', '2020-08-15 22:12:43', '2020-08-15 22:12:43'),
(213, 223, 'Bakau', 1, '114.125.188.36', '2020-08-15 22:21:00', '2020-08-15 22:21:00'),
(214, 223, 'Bakau', 2, '114.125.188.36', '2020-08-15 22:30:53', '2020-08-15 22:30:53'),
(215, 223, 'Bakau', 2, '114.125.188.36', '2020-08-15 22:32:58', '2020-08-15 22:32:58'),
(216, 223, 'Bakau', 2, '114.125.188.36', '2020-08-15 22:35:55', '2020-08-15 22:35:55'),
(217, 223, 'Bakau', 6, '114.125.188.36', '2020-08-15 22:40:23', '2020-08-15 22:40:23'),
(218, 223, 'Bakau', 10, '114.125.188.36', '2020-08-15 22:45:13', '2020-08-15 22:45:13'),
(219, 223, 'Bakau', 10, '114.125.188.36', '2020-08-15 22:46:15', '2020-08-15 22:46:15'),
(220, 223, 'Bakau', 9, '114.125.188.36', '2020-08-15 22:47:47', '2020-08-15 22:47:47'),
(221, 223, 'Bakau', 8, '114.125.188.36', '2020-08-15 22:51:10', '2020-08-15 22:51:10'),
(222, 223, 'Bakau', 16, '114.125.188.36', '2020-08-15 22:53:23', '2020-08-15 22:53:23'),
(223, 223, 'Bakau', 17, '114.125.188.36', '2020-08-15 22:54:28', '2020-08-15 22:54:28'),
(224, 223, 'Bakau', 20, '114.125.188.36', '2020-08-15 22:58:26', '2020-08-15 22:58:26'),
(225, 224, 'Balikpapan', 0, '180.248.117.127', '2020-08-15 23:38:08', '2020-08-15 23:38:08'),
(226, 224, 'Balikpapan', 1, '180.248.117.127', '2020-08-15 23:40:36', '2020-08-15 23:40:36'),
(227, 224, 'Balikpapan', 2, '180.248.117.127', '2020-08-15 23:43:29', '2020-08-15 23:43:29'),
(228, 224, 'Balikpapan', 6, '180.248.117.127', '2020-08-15 23:45:22', '2020-08-15 23:45:22'),
(229, 224, 'Balikpapan', 10, '180.248.117.127', '2020-08-15 23:49:04', '2020-08-15 23:49:04'),
(230, 224, 'Balikpapan', 11, '180.248.117.127', '2020-08-15 23:49:50', '2020-08-15 23:49:50'),
(231, 224, 'Balikpapan', 9, '180.248.117.127', '2020-08-15 23:55:54', '2020-08-15 23:55:54'),
(232, 225, 'Bogor', 0, '111.95.14.22', '2020-08-16 00:02:30', '2020-08-16 00:02:30'),
(233, 224, 'Balikpapan', 7, '180.248.117.127', '2020-08-16 00:08:07', '2020-08-16 00:08:07'),
(234, 224, 'Balikpapan', 4, '180.248.117.127', '2020-08-16 00:13:33', '2020-08-16 00:13:33'),
(235, 225, 'Bogor', 0, '111.95.14.22', '2020-08-16 00:18:43', '2020-08-16 00:18:43'),
(236, 224, 'Balikpapan', 14, '180.248.117.127', '2020-08-16 00:21:38', '2020-08-16 00:21:38'),
(237, 225, 'Bogor', 1, '111.95.14.22', '2020-08-16 00:22:43', '2020-08-16 00:22:43'),
(238, 225, 'Bogor', 2, '111.95.14.22', '2020-08-16 00:24:30', '2020-08-16 00:24:30'),
(239, 225, 'Bogor', 4, '111.95.14.22', '2020-08-16 00:26:16', '2020-08-16 00:26:16'),
(240, 225, 'Bogor', 4, '111.95.14.22', '2020-08-16 00:27:43', '2020-08-16 00:27:43'),
(241, 225, 'Bogor', 5, '111.95.14.22', '2020-08-16 00:28:23', '2020-08-16 00:28:23'),
(242, 225, 'Bogor', 5, '111.95.14.22', '2020-08-16 00:28:56', '2020-08-16 00:28:56'),
(243, 224, 'Balikpapan', 20, '180.248.117.127', '2020-08-16 00:29:29', '2020-08-16 00:29:29'),
(244, 225, 'Bogor', 8, '111.95.14.22', '2020-08-16 00:42:05', '2020-08-16 00:42:05'),
(245, 225, 'Bogor', 10, '111.95.14.22', '2020-08-16 00:43:08', '2020-08-16 00:43:08'),
(246, 225, 'Bogor', 10, '111.95.14.22', '2020-08-16 00:44:13', '2020-08-16 00:44:13'),
(247, 225, 'Bogor', 15, '111.95.14.22', '2020-08-16 00:45:45', '2020-08-16 00:45:45'),
(248, 225, 'Bogor', 15, '111.95.14.22', '2020-08-16 00:50:24', '2020-08-16 00:50:24'),
(249, 225, 'Bogor', 17, '111.95.14.22', '2020-08-16 00:52:08', '2020-08-16 00:52:08'),
(250, 225, 'Bogor', 20, '111.95.14.22', '2020-08-16 00:54:53', '2020-08-16 00:54:53'),
(251, 226, 'Bogor', 0, '111.95.14.22', '2020-08-16 01:05:10', '2020-08-16 01:05:10'),
(252, 226, 'Bogor', 0, '111.95.14.22', '2020-08-16 01:09:02', '2020-08-16 01:09:02'),
(253, 226, 'Bogor', 1, '111.95.14.22', '2020-08-16 01:11:27', '2020-08-16 01:11:27'),
(254, 226, 'Bogor', 2, '111.95.14.22', '2020-08-16 01:17:24', '2020-08-16 01:17:24'),
(255, 226, 'Bogor', 6, '111.95.14.22', '2020-08-16 01:18:49', '2020-08-16 01:18:49'),
(256, 226, 'Bogor', 8, '111.95.14.22', '2020-08-16 01:23:13', '2020-08-16 01:23:13'),
(257, 226, 'Bogor', 8, '111.95.14.22', '2020-08-16 01:27:16', '2020-08-16 01:27:16'),
(258, 182, 'Bogor', 0, '111.95.14.22', '2020-08-16 06:58:21', '2020-08-16 06:58:21'),
(259, 182, 'Bogor', 1, '111.95.14.22', '2020-08-16 07:02:13', '2020-08-16 07:02:13'),
(260, 182, 'Bogor', 5, '111.95.14.22', '2020-08-16 07:03:00', '2020-08-16 07:03:00'),
(261, 182, 'Bogor', 6, '111.95.14.22', '2020-08-16 07:05:10', '2020-08-16 07:05:10'),
(262, 193, 'Pekanbaru', 0, '182.1.33.90', '2020-08-16 07:08:57', '2020-08-16 07:08:57'),
(263, 193, 'Pekanbaru', 0, '182.1.33.90', '2020-08-16 07:09:15', '2020-08-16 07:09:15'),
(264, 193, 'Pekanbaru', 1, '182.1.33.90', '2020-08-16 07:10:12', '2020-08-16 07:10:12'),
(265, 182, 'Bogor', 8, '111.95.14.22', '2020-08-16 07:10:16', '2020-08-16 07:10:16'),
(266, 193, 'Pekanbaru', 3, '182.1.33.90', '2020-08-16 07:11:00', '2020-08-16 07:11:00'),
(267, 193, 'Pekanbaru', 6, '182.1.33.90', '2020-08-16 07:11:58', '2020-08-16 07:11:58'),
(268, 193, 'Pekanbaru', 8, '182.1.33.90', '2020-08-16 07:14:29', '2020-08-16 07:14:29'),
(269, 182, 'Bogor', 8, '111.95.14.22', '2020-08-16 07:14:33', '2020-08-16 07:14:33'),
(270, 193, 'Pekanbaru', 8, '182.1.33.90', '2020-08-16 07:14:50', '2020-08-16 07:14:50'),
(271, 182, 'Bogor', 17, '111.95.14.22', '2020-08-16 07:16:05', '2020-08-16 07:16:05'),
(272, 182, 'Bogor', 17, '111.95.14.22', '2020-08-16 07:17:22', '2020-08-16 07:17:22'),
(273, 45, 'Pekanbaru', 0, '182.1.33.90', '2020-08-16 07:22:35', '2020-08-16 07:22:35'),
(274, 45, 'Pekanbaru', 1, '182.1.33.90', '2020-08-16 07:27:33', '2020-08-16 07:27:33'),
(275, 45, 'Jakarta', 1, '114.5.211.151', '2020-08-16 07:30:07', '2020-08-16 07:30:07'),
(276, 45, 'Jakarta', 1, '114.5.211.151', '2020-08-16 07:31:12', '2020-08-16 07:31:12'),
(277, 45, 'Jakarta', 1, '114.5.211.151', '2020-08-16 07:31:44', '2020-08-16 07:31:44'),
(278, 45, 'Jakarta', 1, '114.5.211.151', '2020-08-16 07:32:21', '2020-08-16 07:32:21'),
(279, 45, 'Jakarta', 2, '114.5.211.151', '2020-08-16 07:35:41', '2020-08-16 07:35:41'),
(280, 45, 'Jakarta', 2, '114.5.211.151', '2020-08-16 07:37:53', '2020-08-16 07:37:53'),
(281, 45, 'Jakarta', 3, '114.5.211.151', '2020-08-16 07:40:00', '2020-08-16 07:40:00'),
(282, 45, 'Jakarta', 3, '114.5.211.151', '2020-08-16 07:41:28', '2020-08-16 07:41:28'),
(283, 45, 'Jakarta', 4, '114.5.211.151', '2020-08-16 07:45:15', '2020-08-16 07:45:15'),
(284, 45, 'Jakarta', 6, '114.5.211.151', '2020-08-16 07:46:51', '2020-08-16 07:46:51'),
(285, 45, 'Jakarta', 8, '114.5.211.151', '2020-08-16 07:57:13', '2020-08-16 07:57:13'),
(286, 45, 'Jakarta', 10, '114.5.211.151', '2020-08-16 07:58:42', '2020-08-16 07:58:42'),
(287, 45, 'Jakarta', 11, '114.5.211.151', '2020-08-16 07:59:26', '2020-08-16 07:59:26'),
(288, 45, 'Medan', 15, '182.1.49.249', '2020-08-16 08:03:57', '2020-08-16 08:03:57'),
(289, 45, 'Medan', 17, '182.1.49.249', '2020-08-16 08:05:17', '2020-08-16 08:05:17'),
(290, 45, 'Medan', 20, '182.1.49.249', '2020-08-16 08:06:38', '2020-08-16 08:06:38'),
(291, 82, 'Medan', 0, '182.1.49.249', '2020-08-16 08:09:32', '2020-08-16 08:09:32'),
(292, 82, 'Medan', 1, '182.1.49.249', '2020-08-16 08:11:23', '2020-08-16 08:11:23'),
(293, 82, 'Medan', 2, '182.1.49.249', '2020-08-16 08:12:04', '2020-08-16 08:12:04'),
(294, 82, 'Medan', 6, '182.1.49.249', '2020-08-16 08:12:51', '2020-08-16 08:12:51'),
(295, 227, 'Balikpapan', 0, '180.248.117.127', '2020-08-16 08:53:12', '2020-08-16 08:53:12'),
(296, 227, 'Balikpapan', 1, '180.248.117.127', '2020-08-16 08:58:48', '2020-08-16 08:58:48'),
(297, 227, 'Balikpapan', 3, '180.248.117.127', '2020-08-16 09:04:06', '2020-08-16 09:04:06'),
(298, 227, 'Balikpapan', 6, '180.248.117.127', '2020-08-16 09:21:08', '2020-08-16 09:21:08'),
(299, 125, 'Jakarta', 1, '125.161.131.75', '2020-08-16 09:50:55', '2020-08-16 09:50:55'),
(300, 125, 'Jakarta', 1, '125.161.131.75', '2020-08-16 09:51:18', '2020-08-16 09:51:18'),
(301, 125, 'Jakarta', 1, '125.161.131.75', '2020-08-16 09:51:50', '2020-08-16 09:51:50'),
(302, 125, 'Jakarta', 3, '125.161.131.75', '2020-08-16 09:52:46', '2020-08-16 09:52:46'),
(303, 125, 'Jakarta', 10, '125.161.131.75', '2020-08-16 09:54:31', '2020-08-16 09:54:31'),
(304, 227, 'Balikpapan', 6, '180.248.117.127', '2020-08-16 10:00:09', '2020-08-16 10:00:09'),
(305, 228, 'Jakarta', 0, '125.161.131.75', '2020-08-16 10:01:11', '2020-08-16 10:01:11'),
(306, 228, 'Jakarta', 0, '125.161.131.75', '2020-08-16 10:03:24', '2020-08-16 10:03:24'),
(307, 228, 'Jakarta', 1, '125.161.131.75', '2020-08-16 10:04:07', '2020-08-16 10:04:07'),
(308, 228, 'Jakarta', 2, '125.161.131.75', '2020-08-16 10:06:07', '2020-08-16 10:06:07'),
(309, 228, 'Jakarta', 6, '125.161.131.75', '2020-08-16 10:08:37', '2020-08-16 10:08:37'),
(310, 228, 'Jakarta', 8, '125.161.131.75', '2020-08-16 10:11:31', '2020-08-16 10:11:31'),
(311, 228, 'Jakarta', 8, '125.161.131.75', '2020-08-16 10:13:03', '2020-08-16 10:13:03'),
(312, 228, 'Jakarta', 8, '125.161.131.75', '2020-08-16 10:14:59', '2020-08-16 10:14:59'),
(313, 228, 'Jakarta', 10, '125.161.131.75', '2020-08-16 10:16:32', '2020-08-16 10:16:32'),
(314, 228, 'Jakarta', 8, '125.161.131.75', '2020-08-16 10:19:39', '2020-08-16 10:19:39'),
(315, 227, 'Makassar', 10, '114.125.173.165', '2020-08-16 13:18:33', '2020-08-16 13:18:33'),
(316, 227, 'Makassar', 12, '114.125.173.165', '2020-08-16 13:20:12', '2020-08-16 13:20:12'),
(317, 227, 'Makassar', 9, '114.125.173.165', '2020-08-16 13:22:45', '2020-08-16 13:22:45'),
(318, 227, 'Makassar', 8, '114.125.173.165', '2020-08-16 13:37:50', '2020-08-16 13:37:50'),
(319, 227, 'Makassar', 7, '114.125.173.165', '2020-08-16 14:04:53', '2020-08-16 14:04:53'),
(320, 227, 'Makassar', 5, '114.125.173.165', '2020-08-16 14:06:30', '2020-08-16 14:06:30'),
(321, 227, 'Makassar', 5, '114.125.173.165', '2020-08-16 14:14:28', '2020-08-16 14:14:28'),
(322, 227, 'Makassar', 14, '114.125.173.165', '2020-08-16 14:16:55', '2020-08-16 14:16:55'),
(323, 227, 'Makassar', 14, '114.125.173.165', '2020-08-16 14:17:54', '2020-08-16 14:17:54'),
(324, 227, 'Makassar', 15, '114.125.173.165', '2020-08-16 14:21:58', '2020-08-16 14:21:58'),
(325, 227, 'Makassar', 16, '114.125.173.165', '2020-08-16 14:23:34', '2020-08-16 14:23:34'),
(326, 227, 'Makassar', 17, '114.125.173.165', '2020-08-16 14:27:03', '2020-08-16 14:27:03'),
(327, 227, 'Makassar', 20, '114.125.173.165', '2020-08-16 14:31:05', '2020-08-16 14:31:05'),
(328, 229, 'Makassar', 0, '114.125.173.165', '2020-08-16 14:37:32', '2020-08-16 14:37:32'),
(329, 229, 'Makassar', 1, '114.125.173.165', '2020-08-16 14:38:46', '2020-08-16 14:38:46'),
(330, 229, 'Makassar', 2, '114.125.173.165', '2020-08-16 14:39:50', '2020-08-16 14:39:50'),
(331, 229, 'Makassar', 6, '114.125.173.165', '2020-08-16 14:41:19', '2020-08-16 14:41:19'),
(332, 229, 'Makassar', 10, '114.125.173.165', '2020-08-16 14:42:54', '2020-08-16 14:42:54'),
(333, 229, 'Makassar', 10, '114.125.173.165', '2020-08-16 14:43:19', '2020-08-16 14:43:19'),
(334, 229, 'Makassar', 11, '114.125.173.165', '2020-08-16 14:43:44', '2020-08-16 14:43:44'),
(335, 229, 'Makassar', 9, '114.125.173.165', '2020-08-16 14:45:09', '2020-08-16 14:45:09'),
(336, 229, 'Makassar', 7, '114.125.173.165', '2020-08-16 14:46:25', '2020-08-16 14:46:25'),
(337, 229, 'Makassar', 4, '114.125.173.165', '2020-08-16 14:47:32', '2020-08-16 14:47:32'),
(338, 229, 'Makassar', 14, '114.125.173.165', '2020-08-16 14:49:39', '2020-08-16 14:49:39'),
(339, 229, 'Makassar', 17, '114.125.173.165', '2020-08-16 14:51:44', '2020-08-16 14:51:44'),
(340, 229, 'Makassar', 20, '114.125.173.165', '2020-08-16 14:55:12', '2020-08-16 14:55:12'),
(341, 230, 'Medan', 0, '140.213.155.174', '2020-08-16 23:22:07', '2020-08-16 23:22:07'),
(342, 230, 'Jakarta', 0, '125.161.138.25', '2020-08-17 21:11:21', '2020-08-17 21:11:21'),
(343, 230, 'Jakarta', 0, '125.161.138.25', '2020-08-17 21:14:53', '2020-08-17 21:14:53'),
(344, 230, 'Jakarta', 1, '125.161.138.25', '2020-08-17 21:20:27', '2020-08-17 21:20:27'),
(345, 230, 'Jakarta', 2, '125.161.138.25', '2020-08-17 21:21:35', '2020-08-17 21:21:35'),
(346, 230, 'Jakarta', 2, '125.161.138.25', '2020-08-17 21:21:46', '2020-08-17 21:21:46'),
(347, 230, 'Jakarta', 5, '125.161.138.25', '2020-08-17 21:22:33', '2020-08-17 21:22:33'),
(348, 230, 'Jakarta', 5, '125.161.138.25', '2020-08-17 21:22:42', '2020-08-17 21:22:42'),
(349, 230, 'Jakarta', 5, '125.161.138.25', '2020-08-17 21:22:47', '2020-08-17 21:22:47'),
(350, 230, 'Jakarta', 6, '125.161.138.25', '2020-08-17 21:25:14', '2020-08-17 21:25:14'),
(351, 230, 'Jakarta', 6, '125.161.138.25', '2020-08-17 21:25:43', '2020-08-17 21:25:43'),
(352, 230, 'Jakarta', 8, '125.161.138.25', '2020-08-17 21:29:33', '2020-08-17 21:29:33'),
(353, 230, 'Jakarta', 8, '125.161.138.25', '2020-08-17 21:32:27', '2020-08-17 21:32:27'),
(354, 230, 'Jakarta', 8, '125.161.139.172', '2020-08-17 22:01:24', '2020-08-17 22:01:24'),
(355, 230, 'Jakarta', 10, '125.161.139.172', '2020-08-17 22:03:13', '2020-08-17 22:03:13'),
(356, 230, 'Jakarta', 10, '125.161.139.172', '2020-08-17 22:04:06', '2020-08-17 22:04:06'),
(357, 230, 'Jakarta', 15, '125.161.139.172', '2020-08-17 22:06:43', '2020-08-17 22:06:43'),
(358, 230, 'Jakarta', 15, '125.161.139.172', '2020-08-17 22:07:36', '2020-08-17 22:07:36'),
(359, 230, 'Jakarta', 20, '125.161.139.172', '2020-08-17 22:09:36', '2020-08-17 22:09:36'),
(360, 213, 'Semarang', 0, '114.79.23.52', '2020-08-17 22:10:43', '2020-08-17 22:10:43'),
(361, 230, 'Jakarta', 20, '125.161.139.172', '2020-08-17 22:14:34', '2020-08-17 22:14:34'),
(362, 210, 'Jakarta', 0, '125.161.139.172', '2020-08-17 22:58:48', '2020-08-17 22:58:48'),
(363, 210, 'Jakarta', 0, '125.161.139.172', '2020-08-17 23:01:28', '2020-08-17 23:01:28'),
(364, 231, 'Semarang', 0, '114.79.23.52', '2020-08-17 23:01:56', '2020-08-17 23:01:56'),
(365, 210, 'Jakarta', 1, '125.161.139.172', '2020-08-17 23:02:45', '2020-08-17 23:02:45'),
(366, 210, 'Jakarta', 2, '125.161.139.172', '2020-08-17 23:04:27', '2020-08-17 23:04:27'),
(367, 210, 'Jakarta', 2, '125.161.139.172', '2020-08-17 23:04:39', '2020-08-17 23:04:39'),
(368, 210, 'Jakarta', 6, '125.161.139.172', '2020-08-17 23:05:49', '2020-08-17 23:05:49'),
(369, 210, 'Jakarta', 8, '125.161.139.172', '2020-08-17 23:08:23', '2020-08-17 23:08:23'),
(370, 194, 'Jakarta', 0, '125.161.139.172', '2020-08-17 23:14:20', '2020-08-17 23:14:20'),
(371, 194, 'Jakarta', 0, '125.161.139.172', '2020-08-17 23:16:22', '2020-08-17 23:16:22'),
(372, 194, 'Jakarta', 1, '125.161.139.172', '2020-08-17 23:18:20', '2020-08-17 23:18:20'),
(373, 194, 'Jakarta', 2, '125.161.139.172', '2020-08-17 23:19:53', '2020-08-17 23:19:53'),
(374, 194, 'Jakarta', 3, '125.161.139.172', '2020-08-17 23:24:03', '2020-08-17 23:24:03'),
(375, 194, 'Jakarta', 5, '125.161.139.172', '2020-08-17 23:25:16', '2020-08-17 23:25:16'),
(376, 194, 'Jakarta', 5, '125.161.139.172', '2020-08-17 23:25:37', '2020-08-17 23:25:37'),
(377, 194, 'Jakarta', 6, '125.161.139.172', '2020-08-17 23:26:45', '2020-08-17 23:26:45'),
(378, 194, 'Jakarta', 7, '125.161.139.172', '2020-08-17 23:27:42', '2020-08-17 23:27:42'),
(379, 194, 'Jakarta', 7, '125.161.139.172', '2020-08-17 23:27:48', '2020-08-17 23:27:48'),
(380, 194, 'Jakarta', 8, '125.161.139.172', '2020-08-17 23:32:09', '2020-08-17 23:32:09'),
(381, 194, 'Jakarta', 8, '125.161.139.172', '2020-08-17 23:35:40', '2020-08-17 23:35:40'),
(382, 194, 'Jakarta', 9, '125.161.139.172', '2020-08-17 23:37:19', '2020-08-17 23:37:19'),
(383, 194, 'Jakarta', 11, '125.161.139.172', '2020-08-17 23:38:10', '2020-08-17 23:38:10'),
(384, 194, 'Jakarta', 11, '125.161.139.172', '2020-08-17 23:38:21', '2020-08-17 23:38:21'),
(385, 194, 'Jakarta', 12, '125.161.139.172', '2020-08-17 23:39:17', '2020-08-17 23:39:17'),
(386, 194, 'Jakarta', 12, '125.161.139.172', '2020-08-17 23:47:04', '2020-08-17 23:47:04'),
(387, 194, 'Jakarta', 12, '125.161.139.172', '2020-08-17 23:47:40', '2020-08-17 23:47:40'),
(388, 194, 'Jakarta', 12, '125.161.139.172', '2020-08-17 23:47:42', '2020-08-17 23:47:42'),
(389, 194, 'Jakarta', 12, '125.161.139.172', '2020-08-17 23:47:42', '2020-08-17 23:47:42'),
(390, 186, 'Jakarta', 0, '125.161.139.172', '2020-08-17 23:58:21', '2020-08-17 23:58:21'),
(391, 186, 'Jakarta', 0, '125.161.139.172', '2020-08-18 00:02:07', '2020-08-18 00:02:07'),
(392, 186, 'Jakarta', 1, '125.161.139.172', '2020-08-18 00:04:18', '2020-08-18 00:04:18'),
(393, 186, 'Jakarta', 1, '125.161.139.172', '2020-08-18 00:05:27', '2020-08-18 00:05:27'),
(394, 186, 'Jakarta', 2, '125.161.139.172', '2020-08-18 00:07:55', '2020-08-18 00:07:55'),
(395, 186, 'Jakarta', 2, '125.161.139.172', '2020-08-18 00:07:55', '2020-08-18 00:07:55'),
(396, 186, 'Jakarta', 2, '125.161.139.172', '2020-08-18 00:08:47', '2020-08-18 00:08:47'),
(397, 186, 'Jakarta', 2, '125.161.139.172', '2020-08-18 00:08:56', '2020-08-18 00:08:56'),
(398, 186, 'Jakarta', 3, '125.161.139.172', '2020-08-18 00:11:05', '2020-08-18 00:11:05'),
(399, 186, 'Jakarta', 4, '125.161.139.172', '2020-08-18 00:13:43', '2020-08-18 00:13:43'),
(400, 186, 'Jakarta', 5, '125.161.139.172', '2020-08-18 00:17:38', '2020-08-18 00:17:38'),
(401, 231, 'Surabaya', 0, '112.215.240.176', '2020-08-18 00:25:19', '2020-08-18 00:25:19'),
(402, 231, 'Surabaya', 2, '112.215.240.176', '2020-08-18 00:28:14', '2020-08-18 00:28:14'),
(403, 231, 'Surabaya', 1, '112.215.240.176', '2020-08-18 00:29:39', '2020-08-18 00:29:39'),
(404, 231, 'Surabaya', 2, '112.215.240.176', '2020-08-18 00:30:51', '2020-08-18 00:30:51'),
(405, 231, 'Surabaya', 10, '112.215.240.176', '2020-08-18 00:33:43', '2020-08-18 00:33:43'),
(406, 231, 'Surabaya', 15, '112.215.240.176', '2020-08-18 00:35:51', '2020-08-18 00:35:51'),
(407, 231, 'Surabaya', 15, '112.215.240.176', '2020-08-18 00:36:17', '2020-08-18 00:36:17'),
(408, 186, 'Jakarta', 6, '125.161.139.172', '2020-08-18 00:51:00', '2020-08-18 00:51:00'),
(409, 186, 'Jakarta', 6, '125.161.139.172', '2020-08-18 00:55:00', '2020-08-18 00:55:00'),
(410, 186, 'Jakarta', 8, '125.161.139.172', '2020-08-18 01:01:53', '2020-08-18 01:01:53'),
(411, 232, 'Surabaya', 0, '112.215.240.176', '2020-08-18 01:13:39', '2020-08-18 01:13:39'),
(412, 232, 'Surabaya', 0, '112.215.240.176', '2020-08-18 01:14:34', '2020-08-18 01:14:34'),
(413, 186, 'Jakarta', 8, '125.161.139.172', '2020-08-18 01:15:17', '2020-08-18 01:15:17'),
(414, 186, 'Jakarta', 8, '125.161.139.172', '2020-08-18 01:15:21', '2020-08-18 01:15:21'),
(415, 186, 'Jakarta', 8, '125.161.139.172', '2020-08-18 01:18:13', '2020-08-18 01:18:13'),
(416, 186, 'Jakarta', 8, '125.161.139.172', '2020-08-18 01:19:49', '2020-08-18 01:19:49'),
(417, 232, 'Surabaya', 0, '112.215.240.176', '2020-08-18 01:21:52', '2020-08-18 01:21:52'),
(418, 232, 'Surabaya', 1, '112.215.240.176', '2020-08-18 01:23:27', '2020-08-18 01:23:27'),
(419, 232, 'Surabaya', 1, '112.215.240.176', '2020-08-18 01:25:21', '2020-08-18 01:25:21'),
(420, 232, 'Surabaya', 1, '112.215.240.176', '2020-08-18 01:26:54', '2020-08-18 01:26:54'),
(421, 232, 'Surabaya', 2, '112.215.240.176', '2020-08-18 01:28:18', '2020-08-18 01:28:18'),
(422, 232, 'Surabaya', 3, '112.215.240.176', '2020-08-18 01:29:26', '2020-08-18 01:29:26'),
(423, 232, 'Surabaya', 4, '112.215.240.176', '2020-08-18 01:30:25', '2020-08-18 01:30:25'),
(424, 232, 'Surabaya', 5, '112.215.240.176', '2020-08-18 01:31:25', '2020-08-18 01:31:25'),
(425, 232, 'Surabaya', 6, '112.215.240.176', '2020-08-18 01:32:27', '2020-08-18 01:32:27'),
(426, 232, 'Surabaya', 8, '112.215.240.176', '2020-08-18 01:33:35', '2020-08-18 01:33:35'),
(427, 186, 'Jakarta', 10, '125.161.139.172', '2020-08-18 01:33:43', '2020-08-18 01:33:43'),
(428, 232, 'Surabaya', 10, '112.215.240.176', '2020-08-18 01:35:18', '2020-08-18 01:35:18'),
(429, 186, 'Jakarta', 12, '125.161.139.172', '2020-08-18 01:35:47', '2020-08-18 01:35:47'),
(430, 186, 'Jakarta', 12, '125.161.139.172', '2020-08-18 01:36:07', '2020-08-18 01:36:07'),
(431, 232, 'Surabaya', 11, '112.215.240.176', '2020-08-18 01:36:43', '2020-08-18 01:36:43'),
(432, 186, 'Jakarta', 14, '125.161.139.172', '2020-08-18 01:36:58', '2020-08-18 01:36:58'),
(433, 232, 'Surabaya', 17, '112.215.240.176', '2020-08-18 01:39:33', '2020-08-18 01:39:33'),
(434, 186, 'Jakarta', 14, '125.161.139.172', '2020-08-18 01:39:45', '2020-08-18 01:39:45'),
(435, 232, 'Surabaya', 20, '112.215.240.176', '2020-08-18 01:41:33', '2020-08-18 01:41:33'),
(436, 232, 'Surabaya', 20, '112.215.240.176', '2020-08-18 01:41:38', '2020-08-18 01:41:38'),
(437, 233, 'Jakarta', 0, '125.161.139.172', '2020-08-18 01:42:24', '2020-08-18 01:42:24'),
(438, 186, 'Jakarta', 14, '125.161.139.172', '2020-08-18 01:43:26', '2020-08-18 01:43:26'),
(439, 186, 'Jakarta', 15, '125.161.139.172', '2020-08-18 01:45:54', '2020-08-18 01:45:54'),
(440, 233, 'Jakarta', 0, '125.161.139.172', '2020-08-18 01:46:13', '2020-08-18 01:46:13'),
(441, 186, 'Jakarta', 17, '125.161.139.172', '2020-08-18 01:48:10', '2020-08-18 01:48:10'),
(442, 233, 'Jakarta', 1, '125.161.139.172', '2020-08-18 01:48:14', '2020-08-18 01:48:14'),
(443, 186, 'Jakarta', 19, '125.161.139.172', '2020-08-18 01:51:28', '2020-08-18 01:51:28'),
(444, 233, 'Jakarta', 2, '125.161.139.172', '2020-08-18 01:52:09', '2020-08-18 01:52:09'),
(445, 186, 'Jakarta', 20, '125.161.139.172', '2020-08-18 01:54:08', '2020-08-18 01:54:08'),
(446, 186, 'Jakarta', 20, '125.161.139.172', '2020-08-18 01:54:16', '2020-08-18 01:54:16'),
(447, 233, 'Jakarta', 4, '125.161.139.172', '2020-08-18 01:54:55', '2020-08-18 01:54:55'),
(448, 233, 'Jakarta', 5, '125.161.139.172', '2020-08-18 02:00:29', '2020-08-18 02:00:29'),
(449, 233, 'Jakarta', 6, '125.161.139.172', '2020-08-18 02:03:16', '2020-08-18 02:03:16'),
(450, 233, 'Jakarta', 8, '125.161.139.172', '2020-08-18 02:05:14', '2020-08-18 02:05:14'),
(451, 233, 'Jakarta', 11, '125.161.139.172', '2020-08-18 02:11:26', '2020-08-18 02:11:26'),
(452, 233, 'Jakarta', 13, '125.161.139.172', '2020-08-18 02:14:00', '2020-08-18 02:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_audit`
--

CREATE TABLE `tbl_main_audit` (
  `id_audit` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `id_grup_all` int(10) UNSIGNED NOT NULL,
  `ketersediaan` tinyint(1) NOT NULL DEFAULT 0,
  `jumlah` int(10) UNSIGNED DEFAULT NULL,
  `satuan` smallint(5) UNSIGNED DEFAULT NULL,
  `harga` bigint(20) UNSIGNED DEFAULT NULL,
  `total` bigint(20) UNSIGNED DEFAULT NULL,
  `jumlah_q1` int(10) UNSIGNED DEFAULT NULL,
  `satuan_q1` smallint(5) UNSIGNED DEFAULT NULL,
  `harga_q1` bigint(20) UNSIGNED DEFAULT NULL,
  `total_q1` bigint(20) UNSIGNED DEFAULT NULL,
  `jumlah_q2` int(10) UNSIGNED DEFAULT NULL,
  `satuan_q2` smallint(5) UNSIGNED DEFAULT NULL,
  `harga_q2` bigint(20) UNSIGNED DEFAULT NULL,
  `total_q2` bigint(20) UNSIGNED DEFAULT NULL,
  `id_log_edit` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_main_audit`
--

INSERT INTO `tbl_main_audit` (`id_audit`, `id_rs`, `id_grup_all`, `ketersediaan`, `jumlah`, `satuan`, `harga`, `total`, `jumlah_q1`, `satuan_q1`, `harga_q1`, `total_q1`, `jumlah_q2`, `satuan_q2`, `harga_q2`, `total_q2`, `id_log_edit`) VALUES
(1, 33, 32, 1, 240, 1, 48000, 11520000, 55, 1, 48000, 2640000, 50, 1, 48000, 2640000, 7),
(2, 33, 40, 1, 130, 0, 153000, 19890000, 35, 0, 153000, 5355000, 35, 0, 153000, 5355000, 8),
(3, 33, 83, 1, 360, 2, 20000, 7200000, 200, 2, 20000, 4000000, 210, 2, 20000, 4000000, 10),
(4, 33, 84, 1, 360, 2, 25000, 9000000, 300, 2, 25000, 7500000, 300, 2, 25000, 7500000, 10),
(5, 33, 277, 1, 600, 2, 15000, 9000000, 150, 2, 15000, 2250000, 100, 2, 15000, 2250000, 11),
(6, 33, 167, 1, 145, 2, 22000, 3190000, 30, 2, 22000, 660000, 15, 2, 22000, 660000, 14),
(7, 33, 229, 1, 100, 1, 80000, 8000000, 25, 1, 80000, 2000000, 12, 1, 80000, 2000000, 16),
(8, 33, 232, 1, 115, 1, 25000, 2875000, 35, 1, 25000, 875000, 20, 1, 25000, 875000, 16),
(9, 33, 247, 1, 80, 1, 40000, 3200000, 20, 1, 40000, 800000, 15, 1, 40000, 800000, 17),
(10, 33, 270, 1, 70, 1, 38000, 2660000, 20, 1, 38000, 760000, 14, 1, 38000, 760000, 20),
(11, 36, 2, 1, 180, 0, 210000, 37800000, 0, NULL, 0, 0, 45, 0, 210000, 0, 19),
(12, 36, 13, 1, 180, 0, 27500, 4950000, 0, NULL, 0, 0, 45, 0, 27500, 0, 19),
(13, 36, 82, 1, 40, 0, 115000, 4600000, 0, NULL, 0, 0, 12, 0, 115000, 0, 29),
(14, 17, 1, 1, 120, 0, 80000, 9600000, 30, 0, 80000, 2400000, 40, 0, 80000, 2400000, 25),
(15, 17, 2, 1, 180, 0, 110000, 19800000, 40, 0, 110000, 4400000, 60, 0, 110000, 4400000, 25),
(16, 17, 20, 1, 50, 0, 70000, 3500000, 30, 0, 70000, 2100000, 10, 0, 70000, 2100000, 25),
(17, 36, 278, 1, 50, 0, 48000, 2400000, 0, NULL, 0, 0, 12, 0, 48000, 0, 29),
(18, 17, 32, 1, 10, 1, 38000, 380000, 12, 1, 38000, 456000, 20, 1, 38000, 456000, 28),
(19, 17, 37, 1, 20, 1, 5000, 100000, 4, 1, 5000, 20000, 2, 1, 5000, 20000, 28),
(20, 17, 47, 1, 60, 0, 45000, 2700000, 20, 0, 45000, 900000, 10, 0, 45000, 900000, 30),
(21, 17, 84, 1, 35, 2, 50000, 1750000, 10, 2, 50000, 500000, 8, 2, 50000, 500000, 34),
(22, 17, 279, 1, 100, 2, 18000, 1800000, 60, 2, 18000, 1080000, 60, 2, 18000, 1080000, 36),
(23, 17, 138, 1, 46, 0, 130002, 5980092, 20, 0, 130000, 2600000, 10, 0, 130000, 2600000, 38),
(24, 17, 141, 1, 50, 0, 120000, 6000000, 10, 0, 120000, 1200000, 10, 0, 120000, 1200000, 38),
(25, 17, 143, 1, 13, 2, 8000, 104000, 10, 2, 8000, 80000, 10, 2, 8000, 80000, 38),
(26, 17, 215, 1, 84, 2, 70000, 5880000, 7, 2, 70000, 490000, 10, 2, 70000, 490000, 40),
(27, 184, 3, 1, 756, 1, 85000, 64260000, 270, 1, 85000, 22950000, 72, 1, 85000, 22950000, 41),
(28, 184, 280, 1, 4000, 2, 12000, 48000000, 1000, 2, 12000, 12000000, 400, 2, 12000, 12000000, 42),
(29, 184, 277, 1, 3800, 2, 9000, 34200000, 800, 2, 9000, 7200000, 415, 2, 9000, 7200000, 45),
(30, 25, 4, 1, 250, 0, 80000, 20000000, 130, 0, 80000, 10400000, 60, 0, 80000, 10400000, 47),
(31, 25, 5, 1, 250, 0, 130000, 32500000, 130, 0, 130000, 16900000, 65, 0, 130000, 16900000, 47),
(32, 184, 216, 1, 60, 1, 55000, 3300000, 15, 1, 55000, 825000, 10, 1, 55000, 825000, 50),
(33, 184, 217, 1, 120, 1, 87500, 10500000, 30, 1, 87500, 2625000, 12, 1, 87500, 2625000, 50),
(34, 184, 218, 1, 72, 1, 66500, 4788000, 18, 1, 66500, 1197000, 13, 1, 66500, 1197000, 50),
(35, 25, 32, 1, 10, 1, 30000, 300000, 4, 1, 30000, 120000, 2, 1, 30000, 120000, 51),
(36, 184, 242, 1, 48, 1, 30000, 1440000, 12, 1, 30000, 360000, 6, 1, 30000, 360000, 53),
(37, 184, 243, 1, 60, 1, 38000, 2280000, 15, 1, 38000, 570000, 10, 1, 38000, 570000, 53),
(38, 184, 244, 1, 36, 1, 49000, 1764000, 9, 1, 49000, 441000, 5, 1, 49000, 441000, 53),
(39, 25, 48, 1, 100, 0, 38000, 3800000, 60, 0, 38000, 2280000, 40, 0, 38000, 2280000, 54),
(40, 25, 49, 1, 250, 0, 60000, 15000000, 150, 0, 60000, 9000000, 60, 0, 60000, 9000000, 54),
(41, 25, 82, 1, 75, 0, 25000, 1875000, 16, 0, 25000, 400000, 14, 0, 25000, 400000, 55),
(42, 184, 265, 1, 24, 1, 27000, 648000, 8, 1, 27000, 216000, 5, 1, 27000, 216000, 56),
(43, 184, 266, 1, 12, 1, 38000, 456000, 4, 1, 38000, 152000, 1, 1, 38000, 152000, 56),
(44, 184, 267, 1, 15, 1, 40000, 600000, 5, 1, 40000, 200000, 5, 1, 40000, 200000, 56),
(45, 24, 21, 1, 240, 0, 27000, 6480000, 20, 0, 27000, 540000, 10, 0, 27000, 540000, 58),
(46, 25, 281, 0, 2, 0, 6000, 12000, 1, 0, 6000, 6000, 0, NULL, 0, 0, 104),
(47, 25, 282, 0, 4, 0, 6000, 24000, 1, 0, 6000, 6000, 1, 0, 6000, 6000, 104),
(48, 25, 283, 0, 80, 0, 6000, 480000, 25, 0, 6000, 150000, 10, 0, 6000, 150000, 104),
(49, 25, 284, 0, 60, 0, 6000, 360000, 15, 0, 6000, 90000, 5, 0, 6000, 90000, 104),
(50, 25, 285, 0, 30, 0, 6000, 180000, 10, 0, 6000, 60000, 5, 0, 6000, 60000, 104),
(51, 24, 287, 1, 5, 0, 240000, 1200000, 2, 0, 240000, 480000, 1, 0, 240000, 480000, 63),
(52, 25, 162, 1, 300, 2, 8000, 2400000, 70, 2, 8000, 560000, 30, 2, 8000, 560000, 65),
(53, 24, 288, 1, 40, 2, 6000, 240000, 15, 2, 6000, 90000, 10, 2, 6000, 90000, 66),
(54, 164, 3, 1, 3, 0, 150000, 450000, 1, 0, 150000, 150000, 1, 0, 150000, 150000, 67),
(55, 164, 14, 1, 5, 0, 30000, 150000, 1, 0, 30000, 30000, 0, NULL, 0, 0, 67),
(56, 25, 242, 1, 60, 1, 35000, 2100000, 20, 1, 35000, 700000, 10, 1, 35000, 700000, 68),
(57, 25, 243, 1, 40, 1, 50000, 2000000, 15, 1, 50000, 750000, 5, 1, 50000, 750000, 68),
(58, 25, 244, 1, 60, 1, 65000, 3900000, 20, 1, 65000, 1300000, 10, 1, 65000, 1300000, 68),
(59, 164, 32, 1, 12, 1, 60000, 720000, 3, 1, 60000, 180000, 0, NULL, 0, 0, 69),
(60, 164, 280, 1, 10, 0, 35000, 350000, 3, 2, 35000, 105000, 1, 2, 35000, 105000, 70),
(61, 164, 289, 1, 20, 2, 15000, 300000, 7, 2, 15000, 105000, 4, 2, 15000, 105000, 73),
(62, 164, 166, 1, 300, 2, 47000, 14100000, 50, 2, 47000, 2350000, 25, 2, 47000, 2350000, 76),
(63, 44, 2, 1, 6, 0, 75000, 450000, 1, 0, 75000, 75000, 0, NULL, 0, 0, 77),
(64, 44, 3, 1, 50, 0, 121000, 6050000, 10, 0, 121000, 1210000, 4, 1, 121000, 1210000, 77),
(65, 44, 6, 1, 6, 0, 144000, 864000, 1, 0, 144000, 144000, 1, 0, 144000, 144000, 77),
(66, 44, 7, 1, 3, 0, 200000, 600000, 1, 0, 200000, 200000, 0, NULL, 0, 0, 77),
(67, 44, 14, 1, 5, 0, 42000, 210000, 0, NULL, 0, 0, 1, NULL, 42000, 0, 77),
(68, 44, 21, 1, 75, 0, 27500, 2062500, 30, 0, 27000, 810000, 15, 0, 27000, 810000, 77),
(69, 44, 22, 1, 60, 0, 85000, 5100000, 10, 0, 85000, 850000, 5, 0, 85000, 850000, 77),
(70, 44, 32, 1, 120, 1, 38000, 4560000, 10, 1, 38000, 380000, 10, 1, 38000, 380000, 80),
(71, 9, 19, 1, 384, 0, 159000, 61056000, 96, 0, 159000, 15264000, 90, 0, 159000, 15264000, 81),
(72, 9, 33, 1, 48, 1, 45000, 2160000, 12, 1, 45000, 540000, 12, 1, 45000, 540000, 82),
(73, 44, 40, 1, 5, 0, 5000, 25000, 1, 0, 5000, 5000, 1, 0, 5000, 5000, 83),
(74, 44, 43, 1, 500, 2, 15000, 7500000, 40, 2, 15000, 600000, 10, 2, 15000, 600000, 83),
(75, 9, 48, 1, 480, 1, 45000, 21600000, 120, 1, 45000, 5400000, 120, 1, 45000, 5400000, 84),
(76, 9, 49, 1, 600, 1, 50000, 30000000, 150, 1, 50000, 7500000, 152, 1, 50000, 7500000, 84),
(77, 44, 49, 1, 50, 2, 65000, 3250000, 15, 2, 65000, 975000, 5, 2, 65000, 975000, 85),
(78, 44, 65, 1, 1, 0, 20000, 20000, 1, 0, 20000, 20000, 0, NULL, 0, 0, 90),
(79, 44, 66, 1, 1, 0, 30000, 30000, 1, 0, 30000, 30000, 0, NULL, 0, 0, 90),
(80, 44, 67, 1, 3, 0, 45000, 135000, 1, 0, 45000, 45000, 0, NULL, 0, 0, 90),
(81, 9, 70, 1, 420, 2, 13000, 5460000, 60, 2, 13000, 780000, 55, 2, 13000, 780000, 89),
(82, 9, 71, 1, 60, 2, 25000, 1500000, 15, 2, 25000, 375000, 15, 2, 25000, 375000, 89),
(83, 9, 73, 1, 600, 2, 45000, 27000000, 150, 2, 45000, 6750000, 100, 2, 45000, 6750000, 89),
(84, 9, 93, 1, 120, 2, 30000, 3600000, 30, 2, 30000, 900000, 10, 2, 30000, 900000, 89),
(85, 44, 88, 1, 60, 2, 38000, 2280000, 15, 2, 38000, 570000, 7, 2, 38000, 570000, 90),
(86, 9, 142, 1, 80, 2, 35000, 2800000, 30, 2, 35000, 1050000, 30, 2, 35000, 1050000, 93),
(87, 44, 288, 1, 1200, 2, 8000, 9600000, 400, 2, 8000, 3200000, 215, 2, 80000, 32000000, 123),
(88, 44, 290, 1, 10, 2, 8000, 80000, 3, 2, 8000, 24000, 2, 2, 8000, 24000, 123),
(89, 44, 291, 1, 20, 2, 8000, 160000, 8, 2, 8000, 64000, 2, 2, 8000, 64000, 123),
(90, 44, 292, 1, 40, 2, 8000, 320000, 152, 2, 8000, 1216000, 45, 2, 8000, 1216000, 123),
(91, 44, 293, 1, 10, 2, 8000, 80000, 2, 2, 8000, 16000, 3, 2, 8000, 16000, 123),
(92, 9, 176, 1, 30, 2, 90000, 2700000, 10, 2, 90000, 900000, 20, 2, 90000, 900000, 95),
(93, 44, 149, 1, 30, 2, 150000, 4500000, 5, 2, 150000, 750000, 3, 2, 150000, 750000, 96),
(94, 44, 161, 1, 300, 2, 15000, 4500000, 100, 2, 15000, 1500000, 20, 2, 15000, 1500000, 97),
(95, 44, 175, 1, 4, 0, 120000, 480000, 1, 0, 120000, 120000, 1, 0, 120000, 120000, 98),
(96, 9, 193, 1, 24, 2, 70000, 1680000, 6, 2, 70000, 420000, 6, 2, 70000, 420000, 99),
(97, 9, 200, 1, 60, 2, 230000, 13800000, 15, 2, 230000, 3450000, 10, 2, 230000, 3450000, 99),
(98, 44, 183, 1, 2, 0, 65000, 130000, 1, 0, 65000, 65000, 0, NULL, 0, 0, 100),
(99, 44, 229, 1, 40, 1, 40000, 1600000, 11, 1, 40000, 440000, 5, 1, 40000, 440000, 101),
(100, 44, 230, 1, 40, 1, 58500, 2340000, 11, 1, 58000, 638000, 5, 1, 58000, 638000, 101),
(101, 9, 218, 1, 480, 1, 70000, 33600000, 120, 1, 70000, 8400000, 30, 1, 70000, 8400000, 102),
(102, 9, 219, 1, 600, 1, 85000, 51000000, 100, 1, 85000, 8500000, 70, 1, 85000, 8500000, 102),
(103, 9, 220, 1, 360, 1, 90000, 32400000, 110, 1, 90000, 9900000, 80, 1, 90000, 9900000, 102),
(104, 9, 224, 1, 216, 1, 50000, 10800000, 50, 1, 50000, 2500000, 50, 1, 50000, 2500000, 102),
(105, 9, 225, 1, 216, 1, 60000, 12960000, 30, 1, 60000, 1800000, 30, 1, 60000, 1800000, 102),
(106, 9, 235, 1, 600, 1, 130000, 78000000, 150, 1, 130000, 19500000, 100, 1, 130000, 19500000, 103),
(107, 25, 294, 1, 2, 0, 6000, 12000, 1, 0, 6000, 6000, 0, NULL, 0, 0, 104),
(108, 25, 295, 1, 4, 0, 6000, 24000, 1, 0, 6000, 6000, 1, 0, 6000, 6000, 104),
(109, 25, 296, 1, 80, 0, 6000, 480000, 25, 0, 6000, 150000, 10, 0, 6000, 150000, 104),
(110, 25, 297, 1, 60, 0, 6000, 360000, 15, 0, 6000, 90000, 5, 0, 6000, 90000, 104),
(111, 25, 298, 1, 30, 0, 6000, 180000, 10, 0, 6000, 60000, 5, 0, 6000, 60000, 104),
(112, 9, 242, 1, 60, 1, 30000, 1800000, 15, 1, 30000, 450000, 10, 1, 30000, 450000, 105),
(113, 9, 251, 1, 84, 1, 180000, 15120000, 21, 1, 180000, 3780000, 20, 1, 180000, 3780000, 107),
(114, 9, 252, 1, 108, 1, 198000, 21384000, 27, 1, 198000, 5346000, 25, 1, 198000, 5346000, 107),
(115, 9, 253, 1, 48, 1, 230000, 11040000, 12, 1, 230000, 2760000, 12, 1, 230000, 2760000, 107),
(116, 9, 259, 1, 6, 0, 1700000, 10200000, 3, 0, 1700000, 5100000, 1, 0, 1700000, 5100000, 110),
(117, 9, 260, 1, 6, 0, 1840000, 11040000, 3, 0, 1840000, 5520000, 1, 0, 1840000, 5520000, 110),
(118, 9, 261, 1, 6, 0, 1999000, 11994000, 3, 0, 1999000, 5997000, 1, 0, 1999000, 5997000, 110),
(119, 9, 274, 1, 120, 1, 85000, 10200000, 30, 1, 85000, 2550000, 25, 1, 85000, 2550000, 112),
(120, 9, 275, 1, 120, 1, 97000, 11640000, 30, 1, 97000, 2910000, 25, 1, 97000, 2910000, 112),
(121, 9, 276, 1, 120, 1, 115000, 13800000, 30, 1, 115000, 3450000, 25, 1, 115000, 3450000, 112),
(122, 56, 5, 1, 50, 0, 235900, 11795000, 10, 0, 235900, 2359000, 5, 0, 235900, 2359000, 114),
(123, 56, 13, 1, 50, 0, 52000, 2600000, 15, 0, 52000, 780000, 10, 0, 52000, 780000, 114),
(124, 56, 14, 1, 60, 0, 60000, 3600000, 24, 0, 60000, 1440000, 11, 0, 60000, 1440000, 114),
(125, 56, 299, 1, 50, 0, 52000, 2600000, 10, NULL, 52000, 520000, 8, 0, 52000, 520000, 114),
(126, 56, 28, 1, 50, 1, 19750, 987500, 13, 1, 19750, 256750, 10, 1, 19750, 256750, 116),
(127, 56, 32, 1, 260, 1, 55000, 14300000, 40, 1, 55000, 2200000, 35, 1, 55000, 2200000, 116),
(128, 56, 41, 1, 7, 0, 954000, 6678000, 2, 0, 954000, 1908000, 1, 0, 954000, 1908000, 117),
(129, 56, 49, 1, 120, 0, 64350, 7722000, 20, 0, 64350, 1287000, 10, 0, 64350, 1287000, 118),
(130, 56, 62, 1, 8, 0, 25525, 204200, 2, 0, 25525, 51050, 1, 0, 25525, 51050, 119),
(131, 56, 115, 1, 2, 0, 734250, 1468500, 3, 0, 734250, 2202750, 4, 0, 734250, 2202750, 125),
(132, 56, 120, 1, 5, 2, 8800, 44000, 2, 2, 8800, 17600, 2, 2, 8800, 17600, 125),
(133, 44, 141, 0, 5, 2, 8800, 44000, 4, 2, 8800, 35200, 4, 2, 8800, 35200, 123),
(134, 56, 141, 1, 5, 2, 8800, 44000, 4, 2, 8800, 35200, 4, 2, 8800, 35200, 125),
(135, 56, 288, 1, 200, 2, 7500, 1500000, 65, 2, 7500, 487500, 40, 2, 7500, 487500, 125),
(136, 56, 292, 1, 180, 2, 7500, 1350000, 55, 2, 7500, 412500, 30, 2, 7500, 412500, 125),
(137, 56, 161, 1, 2, 0, 13000, 26000, 1, 0, 13000, 13000, 1, 0, 13000, 13000, 126),
(138, 56, 163, 1, 4, 0, 742500, 2970000, 2, 0, 742500, 1485000, 1, 0, 742500, 1485000, 126),
(139, 56, 165, 1, 2, 0, 220000, 440000, 2, 0, 220000, 440000, 1, 0, 220000, 440000, 126),
(140, 56, 166, 1, 2, 0, 93500, 187000, 2, 0, 93500, 187000, 1, 0, 93500, 187000, 126),
(141, 56, 167, 1, 5, 0, 110000, 550000, 3, 0, 110000, 330000, 2, 0, 110000, 330000, 126),
(142, 56, 219, 1, 70, 1, 85850, 6009500, 20, 1, 85850, 1717000, 5, 1, 85850, 1717000, 127),
(143, 56, 220, 1, 24, 1, 100000, 2400000, 9, 1, 100000, 900000, 4, 1, 100000, 900000, 127),
(144, 56, 267, 1, 12, 1, 37400, 448800, 5, 1, 37400, 187000, 2, 1, 37400, 187000, 129),
(145, 56, 268, 1, 36, 1, 50600, 1821600, 15, 1, 50600, 759000, 8, 1, 50600, 759000, 129),
(146, 5, 3, 1, 710, 0, 139500, 99045000, 50, 0, 139500, 6975000, 50, 0, 139500, 6975000, 132),
(147, 5, 4, 1, 1239, 0, 193000, 239127000, 250, 0, 193000, 48250000, 200, 0, 193000, 48250000, 132),
(148, 5, 33, 1, 30, 1, 40000, 1200000, 6, 1, 40000, 240000, 6, 1, 40000, 240000, 133),
(149, 5, 34, 1, 130, 1, 60000, 7800000, 6, 1, 60000, 360000, 6, 1, 60000, 360000, 133),
(150, 5, 43, 1, 1600, 2, 15000, 24000000, 400, 2, 15000, 6000000, 232, 2, 15000, 6000000, 134),
(151, 5, 48, 1, 12, 0, 47300, 567600, 24, 0, 47300, 1135200, 10, 0, 47300, 1135200, 135),
(152, 5, 49, 1, 12, 0, 56700, 680400, 12, 0, 56700, 680400, 5, 0, 56700, 680400, 135),
(153, 5, 62, 1, 240, 1, 25000, 6000000, 0, NULL, 0, 0, 30, 1, 25000, 0, 136),
(154, 5, 71, 1, 20, 2, 25000, 500000, 10, 2, 25000, 250000, 5, 2, 25000, 250000, 137),
(155, 5, 141, 1, 421, 2, 67500, 28417500, 100, 2, 67500, 6750000, 84, 2, 67500, 6750000, 139),
(156, 5, 300, 1, 500, 2, 67500, 33750000, 124, 2, 67500, 8370000, 100, 2, 67500, 8370000, 139),
(157, 5, 301, 1, 1274, 2, 67500, 85995000, 346, 2, 67500, 23355000, 300, 2, 67500, 23355000, 139),
(158, 5, 302, 1, 100, 2, 67500, 6750000, 25, 2, 67500, 1687500, 29, 2, 67500, 1687500, 139),
(159, 5, 303, 1, 123, 2, 292000, 35916000, 27, 2, 292000, 7884000, 25, 2, 292000, 7884000, 139),
(160, 5, 163, 1, 1100, 2, 18000, 19800000, 300, 2, 18000, 5400000, 275, 2, 18000, 5400000, 140),
(161, 5, 166, 1, 5880, 2, 45000, 264600000, 4040, 2, 45000, 181800000, 1201, 2, 45000, 181800000, 140),
(162, 5, 170, 1, 0, NULL, 0, 0, 4, 2, 250000, 1000000, 1, 2, 250000, 1000000, 142),
(163, 5, 218, 1, 149, 1, 142000, 21158000, 84, 1, 142000, 11928000, 50, 1, 142000, 11928000, 143),
(164, 5, 219, 1, 483, 1, 168000, 81144000, 100, 1, 168000, 16800000, 95, 1, 168000, 16800000, 143),
(165, 5, 220, 1, 20, 1, 215000, 4300000, 0, NULL, 0, 0, 0, NULL, 0, 0, 143),
(166, 5, 244, 1, 77, 1, 50000, 3850000, 36, 1, 50000, 1800000, 10, 1, 50000, 1800000, 144),
(167, 5, 251, 1, 15, 1, 286000, 4290000, 2, 1, 286000, 572000, 1, 1, 286000, 572000, 146),
(168, 5, 252, 1, 17, 1, 291000, 4947000, 2, 1, 291000, 582000, 1, 1, 291000, 582000, 146),
(169, 5, 253, 1, 7, 1, 347000, 2429000, 4, 1, 347000, 1388000, 3, 1, 347000, 1388000, 146),
(170, 5, 259, 1, 4, 0, 6000, 24000, 1, 0, 6000, 6000, 1, 0, 6000, 6000, 149),
(171, 5, 260, 1, 4, 0, 7000, 28000, 1, 0, 7000, 7000, 1, 0, 7000, 7000, 149),
(172, 125, 13, 1, 860, 0, 20000, 17200000, 300, 0, 20000, 6000000, 0, NULL, 0, 0, 301),
(173, 125, 14, 1, 400, 0, 25000, 10000000, 250, 0, 25000, 6250000, 0, NULL, 0, 0, 301),
(174, 213, 6, 1, 360, 0, 75000, 27000000, 90, 0, 75000, 6750000, 92, 0, 75000, 6750000, 152),
(175, 213, 13, 1, 48, 0, 30000, 1440000, 12, NULL, 30000, 360000, 12, 0, 30000, 360000, 152),
(176, 213, 27, 1, 16, 1, 31000, 496000, 12, 1, 31000, 372000, 12, 1, 31000, 372000, 153),
(177, 213, 36, 1, 48, 1, 93000, 4464000, 12, 1, 93000, 1116000, 12, 1, 93000, 1116000, 153),
(178, 213, 41, 1, 96, 0, 20000, 1920000, 32, 0, 20000, 640000, 32, 0, 20000, 640000, 154),
(179, 213, 70, 1, 90, 0, 23000, 2070000, 12, 0, 23000, 276000, 12, 0, 23000, 276000, 155),
(180, 213, 118, 1, 96, 2, 17000, 1632000, 24, 2, 17000, 408000, 24, 2, 17000, 408000, 156),
(181, 213, 119, 1, 144, 2, 54000, 7776000, 36, 2, 54000, 1944000, 36, 2, 54000, 1944000, 156),
(182, 213, 120, 1, 96, 2, 60000, 5760000, 48, 2, 60000, 2880000, 48, 2, 60000, 2880000, 156),
(183, 213, 164, 1, 240, 0, 35000, 8400000, 60, 0, 35000, 2100000, 60, 0, 35000, 2100000, 157),
(184, 213, 166, 1, 144, 0, 15000, 2160000, 60, 0, 15000, 900000, 60, 0, 15000, 900000, 157),
(185, 213, 178, 1, 48, 0, 270000, 12960000, 12, 0, 270000, 3240000, 12, 0, 270000, 3240000, 159),
(186, 213, 215, 1, 48, 1, 85000, 4080000, 24, 1, 85000, 2040000, 24, 1, 85000, 2040000, 160),
(187, 213, 219, 1, 144, 1, 102000, 14688000, 12, 1, 102000, 1224000, 12, 1, 102000, 1224000, 160),
(188, 213, 247, 1, 288, 0, 55000, 15840000, 24, 0, 5500, 132000, 24, 0, 55000, 1320000, 161),
(189, 213, 267, 1, 96, 1, 34200, 3283200, 24, 1, 34200, 820800, 0, NULL, 0, 0, 163),
(190, 217, 10, 1, 90, NULL, 0, 0, 0, NULL, 0, 0, 90, 2, 61950, 0, 169),
(191, 217, 12, 1, 93, NULL, 0, 0, 0, NULL, 0, 0, 93, 2, 107854, 0, 169),
(192, 217, 1, 1, 5, NULL, 0, 0, 0, NULL, 0, 0, 10, 2, 8275, 0, 169),
(193, 217, 163, 1, 1313, NULL, 0, 0, 0, NULL, 0, 0, 1500, NULL, 13017, 0, 170),
(194, 217, 149, 1, 11, NULL, 0, 0, 0, NULL, 0, 0, 11, NULL, 39606, 0, 171),
(195, 217, 47, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 327, NULL, 44231, 0, 172),
(196, 217, 48, 1, 327, NULL, 0, 0, 0, NULL, 0, 0, 0, NULL, 0, 0, 172),
(197, 217, 58, 1, 62, NULL, 0, 0, 0, NULL, 0, 0, 65, NULL, 8275, 0, 174),
(198, 217, 304, 1, 12, NULL, 0, 0, 0, NULL, 0, 0, 15, NULL, 65157, 0, 174),
(199, 217, 265, 1, 6, NULL, 0, 0, 0, NULL, 0, 0, 10, NULL, 18333, 0, 176),
(200, 217, 269, 1, 62, NULL, 0, 0, 0, NULL, 0, 0, 60, NULL, 17418, 0, 176),
(201, 217, 270, 1, 60, NULL, 0, 0, 0, NULL, 0, 0, 60, NULL, 20455, 0, 176),
(202, 217, 271, 1, 115, NULL, 0, 0, 0, NULL, 0, 0, 100, NULL, 23371, 0, 176),
(203, 219, 1, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 100203, NULL, 21000, 0, 179),
(204, 219, 21, 1, 30, NULL, 0, 0, 0, NULL, 0, 0, 305011, NULL, 34000, 0, 179),
(205, 219, 58, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 40, 1, 9500, 0, 180),
(206, 220, 1, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 150, 2, 21000, 0, 183),
(207, 220, 18, 1, 229, 0, 99175, 22711075, 0, NULL, 0, 0, 67, 0, 99175, 0, 183),
(208, 221, 2, 1, 12, 0, 83000, 996000, 4, 0, 83000, 332000, 0, NULL, 0, 0, 191),
(209, 221, 3, 1, 22, 0, 138000, 3036000, 12, 0, 138000, 1656000, 0, NULL, 0, 0, 191),
(210, 221, 4, 1, 27, 0, 185000, 4995000, 8, 0, 185000, 1480000, 0, NULL, 0, 0, 191),
(211, 221, 13, 1, 18, 1, 250, 4500, 6, 1, 0, 0, 0, NULL, 0, 0, 191),
(212, 215, 3, 1, 50, 0, 142500, 7125000, 15, 0, 142500, 2137500, 20, 0, 142500, 2137500, 194),
(213, 215, 21, 1, 55, 0, 75000, 4125000, 15, 0, 75000, 1125000, 18, 0, 75000, 1125000, 194),
(214, 135, 2, 1, 12, 1, 75794, 909528, 10, 1, 75794, 757940, 14, 1, 75794, 757940, 196),
(215, 135, 3, 1, 6, 1, 143748, 862488, 4, 1, 143748, 574992, 7, 1, 143748, 574992, 196),
(216, 135, 4, 1, 3, 1, 202554, 607662, 2, 1, 202554, 405108, 5, 1, 202554, 405108, 196),
(217, 135, 305, 1, 2, 0, 1072500, 2145000, 2, 0, 1072500, 2145000, 1, 0, 1072500, 2145000, 197),
(218, 135, 67, 1, 2, 0, 1072500, 2145000, 2, 0, 1072500, 2145000, 1, 0, 1072500, 2145000, 198),
(219, 135, 84, 1, 3, 0, 214500, 643500, 2, 0, 214500, 429000, 2, 0, 214500, 429000, 198),
(220, 135, 306, 1, 35, 2, 17050, 596750, 23, 2, 17050, 392150, 12, 2, 17050, 392150, 204),
(221, 135, 307, 1, 20, 2, 17050, 341000, 25, 2, 17050, 426250, 16, 2, 17050, 426250, 204),
(222, 135, 277, 1, 16, 2, 11000, 176000, 20, 2, 11000, 220000, 25, 2, 11000, 220000, 204),
(223, 135, 308, 1, 15, 2, 11000, 165000, 23, 2, 11000, 253000, 20, 2, 11000, 253000, 204),
(224, 135, 243, 1, 6, 1, 52000, 312000, 5, 1, 52000, 260000, 4, 1, 52000, 260000, 205),
(225, 135, 244, 1, 8, 1, 52000, 416000, 6, 1, 52000, 312000, 4, 1, 52000, 312000, 205),
(226, 135, 245, 1, 3, 1, 52000, 156000, 2, 1, 52000, 104000, 2, 1, 52000, 104000, 205),
(227, 222, 2, 1, 45, 1, 82500, 3712500, 11, 1, 83000, 913000, 12, 1, 83000, 913000, 208),
(228, 222, 3, 1, 103, 1, 156200, 16088600, 22, 1, 143000, 3146000, 22, 1, 143000, 3146000, 208),
(229, 222, 4, 1, 97, 1, 221100, 21446700, 23, 1, 209000, 4807000, 23, 1, 209000, 4807000, 208),
(230, 222, 8, 1, 210, 0, 1500000, 315000000, 0, NULL, 0, 0, 0, NULL, 0, 0, 208),
(231, 222, 32, 1, 84, 1, 46500, 3906000, 24, 1, 48000, 1152000, 23, 1, 48000, 1152000, 209),
(232, 222, 67, 1, 43, 2, 34000, 1462000, 7, 2, 35000, 245000, 8, 2, 35000, 245000, 211),
(233, 223, 6, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 30, 1, 34000, 0, 213),
(234, 223, 32, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 5, 1, 84600, 0, 214),
(235, 223, 309, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 20, 0, 66880, 0, 216),
(236, 223, 70, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 10, 2, 10200, 0, 217),
(237, 223, 163, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 20, 2, 15300, 0, 219),
(238, 223, 149, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 5, 2, 57200, 0, 220),
(239, 223, 118, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 10, 2, 39930, 0, 221),
(240, 223, 119, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 10, 2, 39900, 0, 221),
(241, 223, 120, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 10, 2, 39930, 0, 221),
(242, 223, 235, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 3, 1, 192300, 0, 222),
(243, 223, 242, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 2, 1, 60000, 0, 223),
(244, 223, 266, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 1, 1, 31810, 0, 224),
(245, 223, 267, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 1, 1, 38636, 0, 224),
(246, 223, 268, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 1, 1, 37900, 0, 224),
(247, 224, 1, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 110, 1, 21000, 0, 226),
(248, 224, 3, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 100, 1, 40500, 0, 226),
(249, 224, 4, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 150, 1, 55000, 0, 226),
(250, 224, 23, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 105, 1, 8275, 0, 227),
(251, 224, 64, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 60, 0, 7930, 0, 228),
(252, 224, 65, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 60, 0, 16162, 0, 228),
(253, 224, 163, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 45, 0, 13000, 0, 229),
(254, 224, 101, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 150, 0, 40100, 0, 233),
(255, 225, 1, 1, 360, 0, 17500, 6300000, 298, 0, 17500, 5215000, 150, 0, 17500, 5215000, 237),
(256, 225, 2, 1, 30, 0, 73400, 2202000, 40, 0, 73400, 2936000, 15, 0, 73400, 2936000, 237),
(257, 225, 3, 1, 20, 0, 144800, 2896000, 30, 0, 144800, 4344000, 10, 0, 144800, 4344000, 237),
(258, 225, 14, 1, 756, 0, 39300, 29710800, 376, 0, 39300, 14776800, 215, 0, 39300, 14776800, 237),
(259, 225, 23, 1, 364, 1, 10000, 3640000, 250, 1, 10000, 2500000, 115, 1, 10000, 2500000, 238),
(260, 225, 32, 1, 610, 1, 50000, 30500000, 170, 1, 50000, 8500000, 100, 1, 50000, 8500000, 238),
(261, 225, 53, 1, 147, 1, 51900, 7629300, 70, 1, 51900, 3633000, 40, 1, 51900, 3633000, 240),
(262, 225, 62, 1, 4, 0, 336000, 1344000, 1, 0, 336000, 336000, 0, NULL, 0, 0, 242),
(263, 225, 310, 1, 10, 2, 55000, 550000, 20, 2, 55000, 1100000, 5, 2, 55000, 1100000, 244),
(264, 225, 311, 1, 20, 2, 50500, 1010000, 30, 2, 50500, 1515000, 10, 2, 50500, 1515000, 244),
(265, 225, 312, 1, 30, 2, 40500, 1215000, 30, 2, 40500, 1215000, 8, 2, 40500, 1215000, 244),
(266, 225, 313, 1, 57, 2, 15000, 855000, 40, 2, 15000, 600000, 10, 2, 15000, 600000, 244),
(267, 225, 314, 1, 153, 2, 15000, 2295000, 20, 2, 15000, 300000, 6, 2, 15000, 300000, 244),
(268, 225, 315, 1, 0, NULL, 0, 0, 10, 2, 15000, 150000, 5, 2, 15000, 150000, 244),
(269, 225, 316, 1, 1350, 2, 15000, 20250000, 300, 2, 15000, 4500000, 122, 2, 15000, 4500000, 244),
(270, 225, 317, 1, 30, 2, 15000, 450000, 0, NULL, 0, 0, 5, 2, 15000, 0, 244),
(271, 225, 318, 1, 2643, 2, 15000, 39645000, 500, 2, 15000, 7500000, 200, 2, 15000, 7500000, 244),
(272, 225, 161, 1, 90, 2, 13200, 1188000, 80, 2, 13200, 1056000, 40, 2, 13200, 1056000, 246),
(273, 225, 215, 1, 138, 0, 73000, 10074000, 12, 0, 73000, 876000, 6, 0, 73000, 876000, 248),
(274, 225, 216, 1, 328, 0, 88000, 28864000, 24, 0, 88000, 2112000, 10, 0, 88000, 2112000, 248),
(275, 225, 217, 1, 24, 0, 116000, 2784000, 24, 0, 116000, 2784000, 10, 0, 116000, 2784000, 248),
(276, 225, 242, 1, 48, 1, 32600, 1564800, 0, NULL, 0, 0, 10, 1, 32600, 0, 249),
(277, 225, 244, 1, 72, 1, 48000, 3456000, 36, 1, 48000, 1728000, 10, 1, 48000, 1728000, 249),
(278, 225, 245, 1, 36, 1, 63000, 2268000, 12, 1, 63000, 756000, 5, 1, 63000, 756000, 249),
(279, 225, 272, 1, 60, 1, 27000, 1620000, 48, 1, 27000, 1296000, 15, 1, 27000, 1296000, 250),
(280, 225, 273, 1, 120, 1, 48000, 5760000, 48, 1, 48000, 2304000, 10, 1, 48000, 2304000, 250),
(281, 225, 319, 1, 228, 1, 34300, 7820400, 48, 1, 27000, 1296000, 16, 1, 34300, 1646400, 250),
(282, 226, 21, 1, 45, 0, 30800, 1386000, 15, 0, 30800, 462000, 5, 0, 30800, 462000, 253),
(283, 226, 280, 1, 410, 2, 21450, 8794500, 50, 2, 21450, 1072500, 50, 2, 21450, 1072500, 255),
(284, 226, 142, 1, 380, 2, 26000, 9880000, 0, NULL, 0, 0, 50, 2, 26000, 0, 257),
(285, 226, 320, 1, 3, 2, 28000, 84000, 0, NULL, 0, 0, 0, NULL, 0, 0, 257),
(286, 182, 1, 1, 13, 0, 109000, 1417000, 12, 0, 109000, 1308000, 12, 0, 109000, 1308000, 259),
(287, 182, 2, 1, 14, 0, 62000, 868000, 20, 0, 62000, 1240000, 12, 0, 62000, 1240000, 259),
(288, 182, 3, 1, 22, 0, 117000, 2574000, 7, 0, 117000, 819000, 8, 0, 117000, 819000, 259),
(289, 182, 4, 1, 12, 0, 133000, 1596000, 6, 0, 133000, 798000, 8, 0, 133000, 798000, 259),
(290, 182, 62, 1, 23, 0, 23000, 529000, 15, 0, 23000, 345000, 7, 0, 23000, 345000, 260),
(291, 182, 83, 1, 43, 0, 150000, 6450000, 25, 0, 180000, 4500000, 29, 0, 180000, 4500000, 261),
(292, 182, 84, 1, 52, 0, 190000, 9880000, 36, 0, 220000, 7920000, 30, 0, 220000, 7920000, 261),
(293, 193, 3, 1, 45, 0, 150000, 6750000, 12, 0, 150000, 1800000, 20, 0, 150000, 1800000, 264),
(294, 182, 137, 1, 15, 2, 11000, 165000, 0, NULL, 0, 0, 0, NULL, 0, 0, 269),
(295, 182, 138, 1, 25, 2, 10000, 250000, 0, NULL, 0, 0, 0, NULL, 0, 0, 269),
(296, 182, 139, 1, 25, 2, 15000, 375000, 0, NULL, 0, 0, 0, NULL, 0, 0, 269),
(297, 182, 140, 1, 25, 2, 15000, 375000, 0, NULL, 0, 0, 0, NULL, 0, 0, 269),
(298, 182, 142, 1, 15, 2, 11000, 165000, 41, 2, 11000, 451000, 30, 2, 11000, 451000, 269),
(299, 182, 321, 1, 20, 2, 15000, 300000, 15, 2, 15000, 225000, 38, 2, 15000, 225000, 269),
(300, 182, 322, 1, 20, 2, 15000, 300000, 20, 2, 15000, 300000, 30, 2, 15000, 300000, 269),
(301, 193, 43, 1, 20, 0, 293000, 5860000, 7, 0, 293000, 2051000, 8, 0, 293000, 2051000, 266),
(302, 193, 67, 1, 423, 2, 28460, 12038580, 22, 2, 28460, 626120, 21, 2, 28460, 626120, 267),
(303, 193, 110, 1, 784, 2, 19000, 14896000, 238, 2, 19000, 4522000, 419, 2, 19000, 4522000, 270),
(304, 182, 243, 1, 5, 0, 85000, 425000, 2, 0, 85000, 170000, 2, 0, 85000, 170000, 272),
(305, 45, 3, 1, 134, 0, 136000, 18224000, 72, 0, 136000, 9792000, 70, 0, 136000, 9792000, 278),
(306, 45, 13, 1, 36, 0, 31000, 1116000, 8, 0, 31000, 248000, 11, 0, 31000, 248000, 278),
(307, 45, 18, 1, 9, 0, 461000, 4149000, 4, 0, 461000, 1844000, 4, 0, 461000, 1844000, 278),
(308, 45, 21, 1, 0, NULL, 0, 0, 10, 0, 47000, 470000, 10, 0, 47000, 470000, 278),
(309, 45, 22, 1, 0, NULL, 0, 0, 2, 1, 17240, 34480, 3, 1, 17240, 34480, 278),
(310, 45, 324, 1, 6, 1, 22500, 135000, 0, NULL, 0, 0, 0, NULL, 0, 0, 278),
(311, 45, 29, 1, 2, 1, 54000, 108000, 4, 1, 54000, 216000, 5, 1, 54000, 216000, 280),
(312, 45, 32, 1, 2, 1, 83000, 166000, 0, NULL, 0, 0, 0, NULL, 0, 0, 280),
(313, 45, 35, 1, 0, 1, 0, 0, 9, 1, 7600, 68400, 9, 1, 7600, 68400, 280),
(314, 45, 325, 1, 1123, NULL, 400, 449200, 348, NULL, 400, 139200, 350, NULL, 400, 139200, 280),
(315, 45, 326, 1, 63, 2, 52430, 3303090, 15, 2, 52430, 786450, 14, 2, 52430, 786450, 282),
(316, 45, 327, 1, 23, 1, 79000, 1817000, 5, 1, 79000, 395000, 5, 1, 79000, 395000, 283),
(317, 45, 328, 1, 14, 1, 84000, 1176000, 1, 1, 84000, 84000, 1, 1, 84000, 84000, 283),
(318, 45, 329, 1, 4, 1, 91000, 364000, 1, 1, 91000, 91000, 1, 1, 91000, 91000, 283),
(319, 45, 71, 1, 3, 2, 28400, 85200, 0, NULL, 0, 0, 0, NULL, 0, 0, 284),
(320, 45, 73, 1, 63, 2, 52430, 3303090, 15, 2, 52430, 786450, 15, 2, 52430, 786450, 284),
(321, 45, 110, 1, 0, NULL, 0, 0, 4, 2, 10900, 43600, 5, 2, 10900, 43600, 285),
(322, 45, 111, 1, 0, NULL, 0, 0, 43, 2, 13200, 567600, 43, 2, 13200, 567600, 285),
(323, 45, 113, 1, 1, 2, 63400, 63400, 0, NULL, 0, 0, 0, NULL, 0, 0, 285),
(324, 45, 114, 1, 0, NULL, 0, 0, 4, 2, 63400, 253600, 4, 2, 63400, 253600, 285),
(325, 45, 122, 1, 5, 2, 21000, 105000, 3, 2, 21000, 63000, 3, 2, 21000, 63000, 285),
(326, 45, 145, 1, 1, 2, 19000, 19000, 14, 2, 19000, 266000, 13, 2, 19000, 266000, 285),
(327, 45, 146, 1, 884, 2, 27500, 24310000, 273, 2, 27500, 7507500, 273, 2, 27500, 7507500, 285),
(328, 45, 167, 1, 42, 2, 12500, 525000, 11, 2, 12500, 137500, 11, 2, 12500, 137500, 286),
(329, 45, 171, 1, 16, 2, 276000, 4416000, 2, 2, 276000, 552000, 2, 2, 276000, 552000, 287),
(330, 45, 227, 1, 23, 1, 79000, 1817000, 5, 1, 79000, 395000, 5, 1, 79000, 395000, 288),
(331, 45, 228, 1, 14, 1, 84000, 1176000, 1, 1, 84000, 84000, 1, 1, 84000, 84000, 288),
(332, 45, 229, 1, 4, 1, 91000, 364000, 1, 1, 91000, 91000, 1, 1, 91000, 91000, 288),
(333, 45, 242, 1, 3, 1, 32000, 96000, 0, NULL, 0, 0, 0, NULL, 0, 0, 289),
(334, 45, 244, 1, 2, 1, 44000, 88000, 1, 1, 44000, 44000, 1, 1, 44000, 44000, 289),
(335, 45, 245, 1, 6, 1, 58000, 348000, 0, NULL, 0, 0, 0, NULL, 0, 0, 289),
(336, 45, 269, 1, 2, 1, 25500, 51000, 0, NULL, 0, 0, 0, NULL, 0, 0, 290),
(337, 45, 270, 1, 1, 1, 27500, 27500, 0, NULL, 0, 0, 0, NULL, 0, 0, 290),
(338, 45, 271, 1, 4, 1, 38000, 152000, 1, 1, 38000, 38000, 1, 1, 38000, 38000, 290),
(339, 82, 20, 1, 12, 0, 30000, 360000, 6, 0, 30000, 180000, 6, 0, 30000, 180000, 292),
(340, 82, 22, 1, 12, 0, 80000, 960000, 6, 0, 80000, 480000, 6, 0, 80000, 480000, 292),
(341, 82, 36, 1, 10, 1, 35000, 350000, 5, 1, 35000, 175000, 5, 1, 35000, 175000, 293),
(342, 82, 69, 1, 20, 0, 85000, 1700000, 10, 0, 85000, 850000, 10, 0, 85000, 850000, 294),
(343, 227, 1, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 150, 2, 21000, 0, 296),
(344, 227, 18, 1, 229, 0, 99075, 22688175, 0, NULL, 0, 0, 65, 0, 99175, 0, 296),
(345, 227, 43, 1, 922, 1, 20661, 19049442, 0, NULL, 0, 0, 271, 1, 20661, 0, 297),
(346, 227, 70, 1, 79, 2, 8543, 674897, 0, NULL, 0, 0, 18, 2, 8543, 0, 304),
(347, 227, 71, 1, 99, 2, 17087, 1691613, 0, NULL, 0, 0, 14, 2, 17087, 0, 304),
(348, 227, 72, 1, 108, 2, 21833, 2357964, 0, NULL, 0, 0, 17, 2, 21833, 0, 304),
(349, 227, 73, 1, 1050, 2, 36073, 37876650, 0, NULL, 0, 0, 280, 2, 36073, 0, 304),
(350, 125, 21, 1, 500, 0, 20000, 10000000, 200, 0, 20000, 4000000, 0, NULL, 0, 0, 301),
(351, 125, 44, 1, 1400, 0, 30000, 42000000, 600, 0, 30000, 18000000, 0, NULL, 0, 0, 302),
(352, 125, 167, 1, 800, 0, 10000, 8000000, 200, 0, 10000, 2000000, 0, NULL, 0, 0, 303),
(353, 227, 330, 1, 8, 2, 122100, 976800, 0, NULL, 0, 0, 0, NULL, 0, 0, 304),
(354, 227, 331, 1, 15, 2, 55660, 834900, 0, NULL, 0, 0, 0, NULL, 0, 0, 304),
(355, 228, 3, 1, 10, 0, 204000, 2040000, 2, 0, 204000, 408000, 0, NULL, 0, 0, 307),
(356, 228, 332, 1, 3, 1, 54000, 162000, 1, 1, 54000, 54000, 0, NULL, 0, 0, 308),
(357, 228, 333, 1, 35, 2, 90000, 3150000, 15, 2, 90000, 1350000, 0, NULL, 0, 0, 309),
(358, 228, 166, 1, 60, 2, 21000, 1260000, 17, 2, 21000, 357000, 0, NULL, 0, 0, 313),
(359, 228, 137, 1, 60, 2, 14000, 840000, 15, 2, 14000, 210000, 0, NULL, 0, 0, 314),
(360, 227, 163, 1, 1390, 2, 12815, 17812850, 0, NULL, 0, 0, 376, 2, 12815, 0, 315),
(361, 227, 175, 1, 4, 2, 70840, 283360, 0, NULL, 0, 0, 0, NULL, 0, 0, 316),
(362, 227, 150, 1, 7, 2, 208846, 1461922, 0, NULL, 0, 0, 0, NULL, 0, 0, 317),
(363, 227, 126, 1, 3, 2, 166237, 498711, 0, NULL, 0, 0, 0, 2, 166237, 0, 318),
(364, 227, 127, 1, 26, 2, 53625, 1394250, 0, NULL, 0, 0, 7, 2, 53625, 0, 318),
(365, 227, 128, 1, 33, 2, 53625, 1769625, 0, NULL, 0, 0, 13, 2, 21450, 0, 318),
(366, 227, 129, 1, 44, 2, 20450, 899800, 0, NULL, 0, 0, 9, 2, 20900, 0, 318),
(367, 227, 130, 1, 227, 2, 209000, 47443000, 0, NULL, 0, 0, 167, 2, 20900, 0, 318),
(368, 227, 131, 1, 1013, 2, 20900, 21171700, 0, NULL, 0, 0, 169, 2, 20900, 0, 318),
(369, 227, 132, 1, 120, 2, 20900, 2508000, 0, NULL, 0, 0, 40, 2, 20900, 0, 318),
(370, 227, 334, 1, 149, 2, 6221, 926929, 0, NULL, 0, 0, 61, 2, 5221, 0, 319),
(371, 227, 59, 1, 74, 2, 31460, 2328040, 0, NULL, 0, 0, 12, 2, 31460, 0, 321),
(372, 227, 335, 1, 21, 2, 52136, 1094856, 0, NULL, 0, 0, 1, 2, 52136, 0, 321),
(373, 227, 191, 1, 1, 2, 91765, 91765, 0, NULL, 0, 0, 0, 2, 91765, 0, 323),
(374, 227, 193, 1, 73, 2, 67603, 4935019, 0, NULL, 0, 0, 4, 2, 63603, 0, 323),
(375, 227, 215, 1, 56, 2, 34486, 1931216, 0, NULL, 0, 0, 14, 2, 34486, 0, 324),
(376, 227, 216, 1, 71, 2, 44032, 3126272, 0, NULL, 0, 0, 18, 2, 44032, 0, 324),
(377, 227, 217, 1, 69, 2, 57747, 3984543, 0, NULL, 0, 0, 8, 2, 57747, 0, 324),
(378, 227, 235, 1, 200, 2, 29753, 5950600, 0, NULL, 0, 0, 0, 2, 29753, 0, 325),
(379, 227, 242, 1, 44, 2, 26060, 1146640, 0, NULL, 0, 0, 11, 2, 26060, 0, 326),
(380, 227, 244, 1, 34, 2, 36485, 1240490, 0, NULL, 0, 0, 13, 2, 36485, 0, 326),
(381, 227, 245, 1, 13, 2, 47844, 621972, 0, NULL, 0, 0, 8, 2, 47844, 0, 326),
(382, 227, 266, 1, 31, 2, 21960, 680760, 0, NULL, 0, 0, 6, 2, 21960, 0, 327),
(383, 227, 267, 1, 22, 2, 28184, 620048, 0, NULL, 0, 0, 8, 2, 28148, 0, 327),
(384, 227, 268, 1, 15, 2, 39416, 591240, 0, NULL, 0, 0, 2, 2, 39416, 0, 327),
(385, 229, 23, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 185, 1, 8275, 0, 330),
(386, 229, 64, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 60, 0, 7930, 0, 331),
(387, 229, 65, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 60, 0, 16162, 0, 331),
(388, 229, 163, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 45, 0, 13000, 0, 333),
(389, 229, 101, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 150, 1, 40100, 0, 336),
(390, 229, 266, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 110, 0, 17500, 0, 340),
(391, 229, 267, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 40, 0, 21000, 0, 340),
(392, 229, 268, 1, 0, NULL, 0, 0, 0, NULL, 0, 0, 48, 0, 25000, 0, 340),
(393, 230, 2, 1, 144, 2, 18500, 2664000, 36, 2, 18500, 666000, 24, 2, 18500, 666000, 344),
(394, 230, 3, 1, 144, 2, 85000, 12240000, 36, 2, 85000, 3060000, 24, 2, 85000, 3060000, 344),
(395, 230, 4, 1, 144, 2, 100000, 14400000, 36, 2, 100000, 3600000, 24, 2, 100000, 3600000, 344),
(396, 230, 5, 1, 288, 2, 130000, 37440000, 36, 2, 130000, 4680000, 26, 2, 130000, 4680000, 344),
(397, 230, 32, 1, 48, 1, 35000, 1680000, 12, 1, 35000, 420000, 10, 1, 35000, 420000, 346),
(398, 230, 62, 1, 60, 0, 360000, 21600000, 15, 0, 360000, 5400000, 12, 0, 360000, 5400000, 349),
(399, 230, 336, 1, 48, 0, 130000, 6240000, 12, 0, 130000, 1560000, 9, 0, 130000, 1560000, 351),
(400, 230, 110, 1, 24, 0, 70000, 1680000, 6, 0, 70000, 420000, 6, 0, 70000, 420000, 354),
(401, 230, 111, 1, 36, 0, 70000, 2520000, 9, 0, 70000, 630000, 7, 0, 70000, 630000, 354),
(402, 230, 112, 1, 12, 0, 70000, 840000, 3, 0, 70000, 210000, 2, 0, 70000, 210000, 354),
(403, 230, 113, 1, 10, 0, 70000, 700000, 3, 0, 70000, 210000, 1, 0, 70000, 210000, 354),
(404, 230, 114, 1, 10, 0, 70000, 700000, 3, 0, 70000, 210000, 1, 0, 70000, 210000, 354),
(405, 230, 142, 1, 80, 2, 22000, 1760000, 50, 2, 22000, 1100000, 35, 2, 22000, 1100000, 354),
(406, 230, 288, 1, 100, 2, 7000, 700000, 70, 2, 7000, 490000, 50, 2, 7000, 490000, 354),
(407, 230, 161, 1, 30, 0, 5, 150, 9, 0, 1200000, 10800000, 4, 0, 1200000, 10800000, 356),
(408, 230, 166, 1, 24, 0, 150000, 3600000, 4, 0, 150000, 600000, 2, 0, 150000, 600000, 356),
(409, 230, 218, 1, 36, 1, 50000, 1800000, 6, 1, 50000, 300000, 5, 1, 50000, 300000, 358),
(410, 230, 219, 1, 36, 1, 86000, 3096000, 8, 1, 86000, 688000, 6, 1, 86000, 688000, 358),
(411, 230, 220, 1, 36, 1, 100100, 3603600, 9, 1, 100100, 900900, 7, 1, 100000, 900000, 358),
(412, 230, 267, 1, 24, 1, 75000, 1800000, 6, 1, 75000, 450000, 5, 1, 75000, 450000, 361),
(413, 230, 268, 1, 24, 1, 83400, 2001600, 6, 1, 83400, 500400, 4, 1, 83400, 500400, 361),
(414, 210, 3, 1, 30, 0, 110000, 3300000, 10, 0, 110000, 1100000, 8, 0, 114500, 1145000, 365),
(415, 210, 32, 1, 60, 1, 38000, 2280000, 30, 1, 40000, 1200000, 15, 1, 40000, 1200000, 367),
(416, 210, 336, 1, 1000, 2, 15000, 15000000, 245, 2, 15000, 3675000, 181, 2, 15000, 3675000, 368),
(417, 210, 118, 1, 20, 0, 70000, 1400000, 10, 0, 102000, 1020000, 7, 0, 102000, 1020000, 369),
(418, 210, 119, 1, 20, 0, 70000, 1400000, 10, 0, 102000, 1020000, 6, 0, 102000, 1020000, 369),
(419, 210, 120, 1, 20, 0, 85000, 1700000, 10, 0, 102000, 1020000, 10, 0, 102000, 1020000, 369),
(420, 194, 10, 1, 4510, 0, 30000, 135300000, 2685, 0, 30000, 80550000, 2751, 0, 30000, 80550000, 372),
(421, 194, 18, 1, 659, 0, 90000, 59310000, 192, 0, 90000, 17280000, 198, 0, 90000, 17280000, 372),
(422, 194, 32, 1, 95, 0, 37371, 3550245, 105, 0, 37371, 3923955, 208, 0, 37371, 3923955, 373),
(423, 194, 40, 1, 1560, 0, 270000, 421200000, 320, 0, 270000, 86400000, 350, 0, 270000, 86400000, 374),
(424, 194, 41, 1, 704, 0, 520000, 366080000, 350, 0, 520000, 182000000, 372, 0, 520000, 182000000, 374),
(425, 194, 62, 1, 144, 1, 20000, 2880000, 36, 1, 20000, 720000, 50, 1, 20000, 720000, 376),
(426, 194, 93, 1, 35, 0, 180000, 6300000, 10, 0, 180000, 1800000, 14, 0, 180000, 1800000, 377),
(427, 194, 103, 1, 865, 2, 70000, 60550000, 240, 2, 70000, 16800000, 250, 2, 70000, 16800000, 379),
(428, 194, 110, 1, 10150, 2, 13050, 132457500, 1100, 2, 13050, 14355000, 1510, 2, 13050, 14355000, 381),
(429, 194, 111, 1, 1751, 2, 13050, 22850550, 417, 2, 13050, 5441850, 545, 2, 13050, 5441850, 381),
(430, 194, 112, 1, 872, 2, 13050, 11379600, 450, 2, 13050, 5872500, 450, 2, 13050, 5872500, 381),
(431, 194, 116, 1, 347, 2, 50300, 17454100, 125, 2, 50300, 6287500, 150, 2, 50300, 6287500, 381),
(432, 194, 117, 1, 300, 2, 42800, 12840000, 85, 2, 42800, 3638000, 136, 2, 42800, 3638000, 381),
(433, 194, 139, 1, 318, 2, 16000, 5088000, 100, 2, 16000, 1600000, 151, 2, 16000, 1600000, 381),
(434, 194, 140, 1, 1837, 2, 11000, 20207000, 580, 2, 11000, 6380000, 695, 2, 11000, 6380000, 381),
(435, 194, 141, 1, 3015, 2, 11000, 33165000, 1040, 2, 11000, 11440000, 1254, 2, 11000, 11440000, 381),
(436, 194, 143, 1, 40, 2, 57670, 2306800, 10, 2, 57670, 576700, 17, 2, 57670, 576700, 381),
(437, 194, 149, 1, 374, 2, 90000, 33660000, 100, 2, 90000, 9000000, 130, 2, 90000, 9000000, 382),
(438, 194, 170, 1, 232, 2, 210000, 48720000, 130, 2, 210000, 27300000, 164, 2, 210000, 27300000, 384),
(439, 194, 175, 1, 486, 2, 120000, 58320000, 290, 2, 120000, 34800000, 317, 2, 120000, 34800000, 389),
(440, 186, 3, 1, 1000, 0, 223000, 223000000, 350, 0, 223000, 78050000, 300, 0, 223000, 78050000, 393),
(441, 186, 4, 1, 2000, 0, 294000, 588000000, 730, 0, 294000, 214620000, 700, 0, 294000, 214620000, 393),
(442, 186, 29, 1, 1200, 2, 57383, 68859600, 120, 2, 57383, 6885960, 100, 2, 57383, 6885960, 397),
(443, 186, 29, 1, 1200, 2, 57383, 68859600, 120, 2, 57383, 6885960, 100, 2, 57383, 6885960, 397),
(444, 186, 41, 1, 2000, 2, 8250, 16500000, 5000, 2, 8250, 41250000, 4000, 2, 8250, 41250000, 398),
(445, 186, 48, 1, 110, 0, 55872, 6145920, 25, 0, 55872, 1396800, 20, 0, 55872, 1396800, 399),
(446, 186, 49, 1, 120, 0, 87120, 10454400, 30, 0, 87120, 2613600, 25, 0, 87120, 2613600, 399),
(447, 186, 62, 1, 45, 0, 360000, 16200000, 15, 0, 360000, 5400000, 8, 0, 360000, 5400000, 400),
(448, 231, 4, 1, 24, 0, 121000, 2904000, 8, 0, 121000, 968000, 7, 0, 121000, 968000, 403),
(449, 231, 5, 1, 24, 0, 154300, 3703200, 8, 0, 154300, 1234400, 6, 0, 154300, 1234400, 403),
(450, 231, 29, 1, 24, 1, 41800, 1003200, 8, 1, 41800, 334400, 6, 1, 41800, 334400, 404),
(451, 231, 165, 1, 28, 0, 22500, 630000, 10, 0, 22500, 225000, 9, 0, 22500, 225000, 405),
(452, 231, 217, 1, 17, 1, 91431, 1554327, 6, 1, 91431, 548586, 6, 1, 91431, 548586, 407),
(453, 186, 337, 1, 250, 2, 22869, 5717250, 60, 2, 22869, 1372140, 55, 2, 22869, 1372140, 409),
(454, 186, 338, 1, 220, 2, 18643, 4101460, 50, 2, 18643, 932150, 40, 2, 18643, 932150, 409),
(455, 186, 339, 1, 100, 2, 40394, 4039400, 26, 2, 40394, 1050244, 25, 2, 40394, 1050244, 409),
(456, 186, 340, 1, 150, 2, 80789, 12118350, 35, 2, 80789, 2827615, 30, 2, 80789, 2827615, 409),
(457, 186, 300, 1, 101, 2, 37180, 3755180, 25, 2, 37180, 929500, 30, 2, 37180, 929500, 416),
(458, 186, 301, 1, 126, 2, 37752, 4756752, 25, 2, 37752, 943800, 28, 2, 37752, 943800, 416),
(459, 186, 302, 1, 119, 2, 42900, 5105100, 129, 2, 42900, 5534100, 31, 2, 42900, 5534100, 416),
(460, 232, 1, 1, 6, 0, 19000, 114000, 2, 0, 19000, 38000, 2, 0, 19000, 38000, 420),
(461, 232, 5, 1, 150, 0, 221000, 33150000, 40, 0, 221000, 8840000, 40, 0, 221000, 8840000, 420),
(462, 232, 13, 1, 60, 0, 28000, 1680000, 15, 0, 28000, 420000, 15, 0, 28000, 420000, 420),
(463, 232, 32, 1, 80, 1, 41000, 3280000, 20, 1, 41000, 820000, 18, 1, 41000, 820000, 421),
(464, 232, 44, 1, 15, 0, 69000, 1035000, 5, 0, 69000, 345000, 4, 0, 69000, 345000, 422),
(465, 232, 49, 1, 60, 0, 52000, 3120000, 15, 0, 52000, 780000, 13, 0, 52000, 780000, 423),
(466, 232, 62, 1, 10, 0, 220000, 2200000, 2, 0, 220000, 440000, 2, 0, 220000, 440000, 424),
(467, 232, 84, 1, 100, 0, 100000, 10000000, 25, 0, 100000, 2500000, 22, 0, 100000, 2500000, 425),
(468, 232, 119, 1, 180, 2, 11000, 1980000, 45, 2, 11000, 495000, 40, 2, 11000, 495000, 426),
(469, 186, 161, 1, 1500, 2, 15101, 22651500, 4000, 2, 15101, 60404000, 3000, 2, 15101, 60404000, 427),
(470, 186, 163, 1, 1600, 2, 15101, 24161600, 3500, 2, 19305, 67567500, 3000, 2, 19305, 67567500, 427),
(471, 232, 165, 1, 80, 0, 200000, 16000000, 20, 0, 200000, 4000000, 17, 0, 200000, 4000000, 428),
(472, 232, 166, 1, 10, 0, 225000, 2250000, 3, 0, 225000, 675000, 2, 0, 225000, 675000, 428),
(473, 186, 176, 1, 28, 0, 130680, 3659040, 8, 0, 130680, 1045440, 5, 0, 130680, 1045440, 430),
(474, 186, 179, 1, 6, 0, 140910, 845460, 4, 0, 140910, 563640, 3, 0, 140910, 563640, 430),
(475, 232, 170, 1, 10, 0, 200000, 2000000, 3, 0, 200000, 600000, 2, 0, 200000, 600000, 431),
(476, 186, 193, 1, 1000, 2, 97284, 97284000, 350, 2, 97284, 34049400, 300, 2, 97284, 34049400, 438),
(477, 232, 244, 1, 100, 1, 32000, 3200000, 25, 1, 32000, 800000, 25, 1, 32000, 800000, 433),
(478, 232, 245, 1, 150, 1, 60000, 9000000, 35, 1, 60000, 2100000, 35, 1, 60000, 2100000, 433),
(479, 232, 266, 1, 100, 1, 22000, 2200000, 25, 1, 22000, 550000, 25, 1, 22000, 550000, 436),
(480, 232, 267, 1, 100, 1, 28000, 2800000, 25, 1, 28000, 700000, 22, 1, 28000, 700000, 436),
(481, 232, 268, 1, 80, 1, 38000, 3040000, 20, 1, 38000, 760000, 20, 1, 38000, 760000, 436),
(482, 186, 341, 1, 250, 2, 108900, 27225000, 90, 2, 108900, 9801000, 80, 2, 108900, 9801000, 438),
(483, 186, 218, 1, 20, 1, 89089, 1781780, 7, 1, 89089, 623623, 6, 1, 89089, 623623, 439),
(484, 186, 219, 1, 40, 1, 105534, 4221360, 9, 1, 105534, 949806, 10, 1, 105534, 949806, 439),
(485, 186, 220, 1, 50, 1, 147534, 7376700, 15, 1, 147534, 2213010, 17, 1, 147534, 2213010, 439),
(486, 186, 243, 1, 100, 0, 40857, 4085700, 30, 0, 40857, 1225710, 25, 0, 40857, 1225710, 441),
(487, 186, 244, 1, 80, 0, 54478, 4358240, 15, 0, 54478, 817170, 20, 0, 54478, 817170, 441),
(488, 186, 245, 1, 45, 0, 71438, 3214710, 12, 0, 71438, 857256, 10, 0, 71438, 857256, 441),
(489, 233, 14, 1, 48, 0, 20000, 960000, 10, 0, 20000, 200000, 10, 0, 20000, 200000, 442),
(490, 186, 259, 1, 2, 1, 3268760, 6537520, 0, 1, 3268760, 0, 1, 1, 3268760, 0, 443),
(491, 186, 261, 1, 2, 1, 3845600, 7691200, 1, 1, 3845600, 3845600, 0, 1, 3845600, 3845600, 443),
(492, 233, 32, 1, 60, 1, 35000, 2100000, 3, 1, 35000, 105000, 2, 1, 35000, 105000, 444),
(493, 186, 266, 1, 60, 1, 24300, 1458000, 15, 1, 24300, 364500, 12, 1, 24300, 364500, 446),
(494, 186, 267, 1, 50, 1, 30750, 1537500, 12, 1, 30750, 369000, 11, 1, 30750, 369000, 446),
(495, 186, 268, 1, 18, 1, 44492, 800856, 5, 1, 44492, 222460, 3, 1, 44492, 222460, 446),
(496, 233, 49, 1, 400, 2, 51500, 20600000, 25, 2, 51500, 1287500, 25, 2, 51500, 1287500, 447),
(497, 233, 342, 1, 587, 2, 18500, 10859500, 100, 2, 18500, 1850000, 75, 2, 18500, 1850000, 448),
(498, 233, 343, 1, 335, 2, 18500, 6197500, 100, 2, 18500, 1850000, 60, 2, 18500, 1850000, 448),
(499, 233, 340, 1, 50, 2, 81000, 4050000, 15, 2, 81000, 1215000, 8, 2, 81000, 1215000, 449),
(500, 233, 322, 1, 1080, 2, 6500, 7020000, 38, 2, 6500, 247000, 20, 2, 6500, 247000, 450),
(501, 233, 169, 1, 71, 0, 57000, 4047000, 20, 0, 57200, 1144000, 10, 0, 57200, 1144000, 451),
(502, 233, 170, 1, 172, 0, 75537, 12992364, 50, 0, 75537, 3776850, 50, 0, 75537, 3776850, 451),
(503, 233, 183, 1, 46, 2, 57200, 2631200, 15, 2, 57200, 858000, 7, 2, 57200, 858000, 452);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_rs`
--

CREATE TABLE `tbl_main_rs` (
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `id_rawdata` bigint(20) UNSIGNED NOT NULL,
  `tahun` smallint(5) UNSIGNED DEFAULT NULL,
  `nama_rs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelompok_kota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_pengelolaan` smallint(5) UNSIGNED DEFAULT NULL,
  `kelas` smallint(5) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_main_rs`
--

INSERT INTO `tbl_main_rs` (`id_rs`, `id_rawdata`, `tahun`, `nama_rs`, `alamat`, `kota`, `kelompok_kota`, `status_pengelolaan`, `kelas`, `status`) VALUES
(1, 143, 2020, 'RS MEDISTRA', 'JL GATOT SUBROTO KAV.59 JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(2, 903, 2020, 'SANSANI', 'JL. SOEKARNO HATTA ATAS PKU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(3, 145, 2020, 'RS HARAPAN BUNDA', 'JL. RAYA BOGOR KM.22 NO. 24 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(4, 1002, 2020, 'RSUD SITI FATIMAH PROVINSI SUMSEL', 'Jl. Kol. H. Burlian Km 6', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(5, 821, 2020, 'RSU ROYAL PRIMA', 'Jl. Ayahanda No.68A, Sei Putih Tengah, Kec. Medan Petisah, Kota Medan, Sumatera Utara 20118', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(6, 818, 2020, 'RS RIDOS', 'JL MENTENG 7 NO 62', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(7, 160, 2020, 'RSUD TARAKAN', 'JL. KYAI CARINGIN NO.7   ', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(8, 1302, 2020, 'RSU PERMATA BUNDA', 'DI PINGKA KLADUIDAJ', 'Manado', 'Makassar+Manado', 0, 0, 0),
(9, 822, 2020, 'COLUMBIA ASIA MEDAN', 'Jl. Listrik No.2A, Petisah Tengah, Kec. Medan Petisah, Kota Medan, Sumatera Utara 20112', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(10, 1428, 2020, 'RS. PARU DR M GUNAWAN CISARUA', 'JL. PUNCAK RAYA KM 83 CIBEREM CISARUA', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(11, 910, 2020, 'RSIA BUDHI MULIA', 'PEKAN BARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(12, 202, 2020, 'RS Umum Tk IV 03.07.03Sariningsih', 'JL. LL. RR MARTADINATA. BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(13, 402, 2020, 'RSK TUGU REJO', 'JL RAYA TUGUREJO TAMBAK AJI NGALINGAN', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(14, 206, 2020, 'RSIA MELINDA', 'Jl. Pajajaran No.46 Kel Pasirkaliki, Kec. Cicendo, Kota Bandung', 'Bandung', 'Bandung', 0, 0, 0),
(15, 825, 2020, 'RS MARTHA FRISKA', 'KOMPLEK MULTATULI', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(16, 1011, 2020, 'RS KARYA ASIH CHARITAS', 'PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(17, 819, 2020, 'RSU ESTOMIHI', 'JL SM RAJA NO 235', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(18, 146, 2020, 'RSU KARTIKA PULOMAS JAKARTA TIMUR', 'JL. PULOMAS BARAT IV C NO. 1 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(19, 403, 2020, 'RS KARIADI', 'JL DR SUTOMO NO 16 RANDUSARI', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(20, 308, 2020, 'RS WIYUNG SEJAHTERA', 'JL WARENGAN', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(21, 1403, 2020, 'RSIA TUMBUH KEMBANG ANAK', 'JL. RAYA BOGOR KM 31 NO. 23 CIMANGGIS DEPOK', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(22, 1402, 2020, 'BOGOR SENIOR HOSPITAL', 'JALAN RAYA TAJUR NO. 168 BOGOR', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(23, 318, 2020, 'RS PREMIERE SURABAYA', 'JL NGINDEN INTAN BARAT BLOK B JAWA TIMUR SUKOLILO', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(24, 811, 2020, 'RS SUFIWA AZIZ', 'Jalan Karya Baru, Kelurahan No.1, Helvetia Tim., Kec. Medan Helvetia, Kota Medan, Sumatera Utara 20124', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(25, 817, 2020, 'RSU IMELDA', 'JL. BILAL NO. 616', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(26, 606, 2020, 'RSIA PERMATA BUNDA', 'JL. NGEKSIGONDO NO. 56 YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(27, 101, 2020, 'RS J DR SOEHARTO HERDJAN', 'JL PROF LATUMENJEN NO 1 JAKARTA BARAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(28, 1101, 2020, 'RS PERMATA HATI', 'JL IMAM BONJOL NO 1', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(29, 397, 2020, 'RS DARMO', 'JL RAYA DARMO NO 90 DARMO', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(30, 1401, 2020, 'RS. UMUM HARAPAN DEPOK', 'JL. PEMUDA NO. 10 DEPOK', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(31, 162, 2020, 'RS SINT CAROLLUS', 'JL. SALEMBARAYA NO. 41 JAKARTA PUSAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(32, 324, 2020, 'RS WIJAYA', 'JL. MENGAUDI 398 WIYUNG SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(33, 812, 2020, 'RSU SUNDARI', 'JL TB SIMATUPANG/ JL PINANG BARAT NO 31 MEDAN SUNGGAL', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(34, 1425, 2020, 'RS MULIA PAJAJARAN', 'JL RAYA PAJAJARAN NO 98 RT02/RW03 BANTARJATI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(35, 127, 2020, 'RS UMUM TEBET', 'JL HARYONO MT NO 8 JAKSEL', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(36, 1009, 2020, 'RSIA AZ-ZAHRA PALEMBANG', 'Jl.Brigjend Hasan Kasim No. 1-2 Kel. Bukit Sangkal Kec. Kalidoni Palembang', 'Palembang', 'Medan+Palembang+Pekanbaru', 2, 4, 0),
(37, 106, 2020, 'RS. SILOAM CILANDAK JAKARTA SELATAN', 'JL. RA. KARTINI NO. 8 CILANDAK RT.010/RW.004. CILANDAK BARAT. JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(38, 110, 2020, 'RS PATRIA IKKT', 'KOMP HAMKAM JL CENDRAWASIH NO 1 SLIPI PALMERAH RT 5 RW2', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(39, 115, 2020, 'RS Umum Daerah Kecamatan Tebet', 'JL PROF SOEPOMO SH NO 54 TEBET RT 13 RW02', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(40, 1404, 2020, 'RSU JATI SAMPURNA', 'JL STUDIO ANTEVE JATIRADEN JATI SAMPURNA KOTA BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(41, 503, 2020, 'CAKRA HUSADA', 'Jl. Merbabu klaten', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(42, 805, 2020, 'RSU MARTHA  FIRISKA P BRAYAN', 'JLN YOS SUDARSO', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(43, 808, 2020, 'rsu bakti medan', 'jl hm jhoni no 64', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(44, 820, 2020, 'RSU VINA ESTETIKA', 'JL. ISKANDAR MUDA NO. 119 MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(45, 904, 2020, 'RS BINA KASIH', 'JL DR SAMANHUDI NO 3 . 5 SAGO', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(46, 163, 2020, 'RS PANTAI INDAH KAPUK', 'JL PANTAI INDAH UTARA 3 JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(47, 209, 2020, 'RSUD AL IHSAN BALE ENDAH', 'JL. KI ASTRA MANGALA KAMPUNG BALE BALE BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(48, 1303, 2020, 'SITI MARYAM', 'JL PAGIDAN TUMITING', 'Manado', 'Makassar+Manado', 0, 0, 0),
(49, 304, 2020, 'RSI JEMUR SARI', 'RSI JEMUR SARI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(50, 139, 2020, 'RSU CINTA KASIH TZU CHI', 'JL KAMAL RAYA UOTER RING ROAD CENGKARENG JAKARTA BARAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(51, 111, 2020, 'RSUPN DR. CIPTO MANGUNKUSUMO', 'JL. PANGERAN DIPONEGORO NO.71 RW. 05 KENARI. SENEN KENARI. JAKARTA PUSAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(52, 205, 2020, 'RSU KARISMA', 'Jl. Raya Cimareme No. 235 RT. 03 RW. 01 Kec.Ngamprah', 'Bandung', 'Bandung', 0, 0, 0),
(53, 1012, 2020, 'RS KHUSUS MATA PROVINSI SUMATERA SELATAN', 'JL. ANGKATAN 45 IR. HARAPAN NO. 10 PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(54, 322, 2020, 'RS DELTA SURYA', 'RS DELTA SURYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(55, 806, 2020, 'rsu prof boloni', 'jl walter mongonsidi no 11', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(56, 807, 2020, 'RSU PERMATA BUNDA', 'JL. SISINGAMANGARAJA NO. 7 MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(57, 1417, 2020, 'RS ANANDA BEKASI', 'JL. SULTAN AGUNG NO. 173 BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(58, 909, 2020, 'RSU UNIVERSITAS RIAU', 'Kompleks Bina Widya KM 12,5 Simpang Baru, Panam PEKAN BARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(59, 161, 2020, 'RS ATMAJAYA', 'JL. PLUIT RAYA NO.2 PENJARINGAN JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(60, 330, 2020, 'RS ROYAL SURABAYA', 'JL. RUNGKUT INDUSTRI III SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(61, 702, 2020, 'RS JASEM', 'JL. SAMANHUDI NO. 85 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(62, 335, 2020, 'RS JIWA MENUR', 'JL RAYA MENUR SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(63, 504, 2020, 'RS PKU MUH SOLO', 'JL RONGGO WARSITO', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(64, 133, 2020, 'RSUD BUDI ASIH CAWANG', 'JL DEWI SARTIKA CAWANG', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(65, 108, 2020, 'RS Haji Pondok Gede', 'jl raya pondok gede pinang ranti makassar jaktim', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(66, 1305, 2020, 'RUMKIT TK II R.W MONGONSIDI', 'JL 14 FEBRUARI', 'Manado', 'Makassar+Manado', 0, 0, 0),
(67, 401, 2020, 'RS YAY KES TELOGOREDJO', 'JL KH. AHMAD DAHLAN PEKUNDEN SEMARANG', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(68, 117, 2020, 'RS UMUM TARUMA JAYA', 'JL RAYA TARUMA JAYA NO 18 PANTAI MAKMUR BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(69, 309, 2020, 'RS MANYAR MEDICAL CENTRE', 'JL MANYAR RAYA NO 9', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(70, 329, 2020, 'RS BHAKTI DARMA HUSADA', 'JL RAYA KENDUANG 115 SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(71, 501, 2020, 'RS DENTATAMA', 'JL PERINTIS KEMERDEKAAN SEAGEN', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(72, 1409, 2020, 'RS UMUM ASYIFAH', 'JL RAYA LEUWILIANG CIBEBER 1 KABUPATEN BOGOR', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(73, 712, 2020, 'RSUD SIDOARJO', 'JL MAJAPAHIT 9 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(74, 1307, 2020, 'RSUD PROV SULUT', 'JL BETHESDA NO 77', 'Manado', 'Makassar+Manado', 0, 0, 0),
(75, 204, 2020, 'RSU CAHYA KAWALUYAN', 'JL. PARAHIANGAN KM.3 BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(76, 406, 2020, 'RS COLUMBIA ASIA SMG', 'JL SILIWANGI NO 143 SEMARANG BARAT', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(77, 1004, 2020, 'RSIA BUNDA NONI', 'Jl. Srijaya Negara No.1 Rt.72 Rw.11 Kel. Bukit Lama', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(78, 157, 2020, 'RSUD KECAMATAN KRAMATJATI', 'JL.ROYA INPRES NO.48', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(79, 1005, 2020, 'RSIA RIKA AMELIA', 'JL. SULTAN MACHMUD BADARUDIN II NO. 18 RT.017/RW.004 PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(80, 1007, 2020, 'RSIA YK MADIRA PALEMBANG', 'Jl. Jendral Sudirman no. 1051 C-D-E KM 3.5', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(81, 405, 2020, 'RS PANTI WILASA CITARUM', 'JL CITARUM NO 98 KEBON AGUNG SEMARANG TIMUR', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(82, 907, 2020, 'RSU LANCANG KUNING', 'Jl Ronggowarsito Ujung No.5A Gobah Pekanbaru', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(83, 305, 2020, 'RUMAH SAKIT ISLAM SURABAYA', 'JL A YANI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(84, 311, 2020, 'RS  ISLAM SURABAYA', 'JL A YANI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(85, 1432, 2020, 'RS SANTHA ELISABETH', 'JL. RAYA NAROGONG NO. 202 KEMANG BEKASI', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(86, 323, 2020, 'RS BUNDA SURABAYA', 'JL. KANDANGAN NO.23-24', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(87, 1427, 2020, 'RS BHAKTI YUDA', 'JL SAWANGAN NO2A PANCORAN MAS', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(88, 1204, 2020, 'RS KHODIJAH', 'Jl. RA. Kartini No. 15-17 Makassar', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(89, 1411, 2020, 'RS TIARA', 'JL BERINGIN RAYA NO 3-5', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(90, 107, 2020, 'RS ANDIKA', 'JL WARUNG SILAH NO 8 RT 6 RW4', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(91, 709, 2020, 'RS ISLAM SITI HAJAR', 'JL. RADEN PATAH NO. 70-72 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(92, 713, 2020, 'RSU MITRA KELUARGA WARU SIDOARJO', 'JL RAYA KALIJATEN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(93, 711, 2020, 'RSU ANWAR MEDIKA', 'JL RAYA BY PASS KRIAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(94, 156, 2020, 'RSU KECAMATAN MATRAMAN', 'JL KEBON KELAPA RAYA NO 29, UTAN KAYU UTARA, MATRAMAN RT 1/RW 10 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(95, 158, 2020, 'RS ROYAL TARUNA', 'JL. DAAN MOGOT NO.34 GROGOT PETAMBUTAN JAWA BARAT', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(96, 703, 2020, 'RSU ASSAKINAH MEDIKA', 'JL. BOGON KEBON AGUNG NO. 65 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(97, 714, 2020, 'RUMKITBAN 05 08 03 SIDOARJO', 'JL DR SOETOMO NO 2 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(98, 506, 2020, 'RSIA RESTU IBU', 'JL NGRAMPEL SRAGEN', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(99, 505, 2020, 'RSU KUMALA SIWI MIJEN KUDUS', 'Jl. Jepara', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(100, 327, 2020, 'RS MUJI RAHAYU', 'JL RAYA MANUKAN WETAN SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(101, 312, 2020, 'RS PURA RAHARJA', 'JL PUCANG ADI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(102, 313, 2020, 'RS SILOAM', 'JL RAYA GUBAG NO 70', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(103, 701, 2020, 'RS RAHMAN RAHIM', 'JL. SAUBANG. DESA KEBONAGUNG. SIDOARJO', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(104, 1206, 2020, 'RSU ALIYAH I', 'JL. BUNGGASI', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(105, 1406, 2020, 'RS AMINAH', 'jl hos cokroaminoto no4 larangan banten 15156', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(106, 1006, 2020, 'RS RK CHARITAS', ' Jl. Sudirman 1054,Palembang', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(107, 171, 2020, 'RS. SIAGA RAYA', 'JL. SIAGA RAYA NO KAV 4-8 PASAR MINGGU JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(108, 603, 2020, 'RSIA PKU MUHAMADIYAH', 'JL. KEMASAN NO. 43 PURBAYANI KOTA GEDE YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(109, 1433, 2020, 'RS SENTOSA', 'JL RAYA KEMANG PONDOK UDIK KECAMATAN KEMANG BOGOR', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(110, 326, 2020, 'RS GOTONG ROYONG', 'JL RAYA MAMKA WETAN 68A', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(111, 710, 2020, 'RSIA PRIMA HASADA', 'JL LETJEN SOETOOYO 03', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(112, 1415, 2020, 'rs umum bayangkara tk IV Bogor', 'jl kaptent nuslihad no 18 paledang bogor', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(113, 502, 2020, 'RS PANTI WALUYO', 'JL.A YANI NO. 01 RT. 004 RW. 014 KERTEN-LAWEYAN', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(114, 328, 2020, 'RS HAJI SURABAYA', 'JL MANYAR KESTOADI SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(115, 203, 2020, 'RS AL-ISLAM BANDUNG', 'JL. SOEKARNO HATTA NO. 644 KOTA BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(116, 1208, 2020, 'RS AWAL BROS', 'JL. URIP SUMOHARJO NO. 47 KARUWISI UTARA - PANAKUKANG', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(117, 1102, 2020, 'RS PERTAMINA', 'JL JENDRAL SUDIRMAN NO 1 BALIKPAPAN', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(118, 307, 2020, 'RS BERSALIN ADI GUNA', 'JL ALUN ALUN RANGKAH NO 3', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(119, 708, 2020, 'RS MATA FATIMA', 'JL KALI JATEN 40 SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(120, 407, 2020, 'RS ELIZABETH', 'JL. KAWI NO.1 WONOTINGGAL KANDISARI', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(121, 1431, 2020, 'RS MELATI', 'JL. MERDEKA TANGERANG NO. 92', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(122, 906, 2020, 'RS KHUSUS MATA SMEC', 'Jl Arifin Ahmad No. 92 Kecamatan Marpoyan Damai Kota Pekanbaru', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(123, 1301, 2020, 'PANCARAN KASIH', 'JL SAMRATULANGI', 'Manado', 'Makassar+Manado', 0, 0, 0),
(124, 604, 2020, 'RSU TRIHARSI', 'JL. MONGINSIDI NO. 82 SURAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(125, 1003, 2020, 'RS MUSI MEDIKA CENDIKIA', 'Jl. Demang Lebar Daun Rt.035 Rw.010 Ilir Barat I Kec. Demang Lebar Daun', 'Palembang', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(126, 336, 2020, 'RS Ibu dan Anak Bantuan 05.08.05 Surabaya', 'JL RAYA GUBENG POJOK NO 21', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(127, 130, 2020, 'RS MEDIKA PERMATA HIJAU', 'JL KEB LAMA NO 64 RT6 RW8 SUKABUMI SELATAN KEBON JERUK', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(128, 114, 2020, 'RS PURI INDAH', 'JL PURI INDAH RAYA BLOK S-2 KEMBANGAN RT 1 RW 2', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(129, 605, 2020, 'RS. PRATAMA KOTA YOGYAKARTA', 'JL. KOLONEL SUGIYONO BRONTOKUSUNAB YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(130, 908, 2020, 'RS ERIA BUNDA', 'KH AHMAD DAHLAN NO 163 PEKAN BARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(131, 1416, 2020, 'SENTRA MEDIKA', 'JL RAYA BOGOR KM 33', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(132, 334, 2020, 'RS JIWA MENUR', 'JL RAYA MENUR SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(133, 154, 2020, 'RSUD MAMPANG PRAPATAN', 'JL. MAMPANG PRAPATAN RAYA NO. 9 RT.001/RW.013 MAMPANG PRAPATAN JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(134, 404, 2020, 'RS PERMATA MEDIKA', 'JL MOCH ICHSAN NO 93 -97 NGALINGAN', 'Semarang', 'Semarang+Solo+Yogya', 0, 0, 0),
(135, 913, 2020, 'RS TK 4 PEKANBARU', 'JL. KESEHATAN NO. 2 PEKANBARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 1, 2, 0),
(136, 1216, 2020, 'RS SAYANG RAKYAT', 'JL PAHLAWAN NO 10000 MAKASAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(137, 1103, 2020, 'RDS HARDJANTO', 'JL TANJUNG PURA NO 1 BALIKPAPAN', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(138, 705, 2020, 'RS UMUM BUNDA', 'JL KUNDI NO 70 KEPUH KIRIMAN WARU SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(139, 128, 2020, 'RS SUKMUL', 'JL TAWES NO 18-20 RT4 RW8 TANJUNG PRIOK', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(140, 201, 2020, 'RS UMUM BUNGSU', 'JL VETERAN NO 6 BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(141, 1213, 2020, 'RS FAISAL', 'JL AP PEHARANI MAKASAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(142, 1207, 2020, 'RS BUDI MULIA', 'JL. NIKEL', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(143, 1104, 2020, 'RSB KASIH BUNDA', 'JL LETJEND S.PARMAN NO 06 RT23 KEL GUNUNG SARI', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(144, 608, 2020, 'RS PANTI RAPIH', 'JL. TEUKU CIKDITIRO 30 YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(145, 319, 2020, 'RSIA LOMBOK DUA DUA', 'JL RAYA Lontar', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(146, 320, 2020, 'RSIA LOMBOK DUA DUA', 'JL RAYA Lontar', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(147, 704, 2020, 'RS KIRANA', 'JL NGELOM NO 87 TAMAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(148, 601, 2020, 'RS KHUSUS IBU DAN ANAK FAJAR', 'JL BUGISAN NO 6/9', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(149, 1422, 2020, 'RS BHAKTI ASIH ', 'JL. RADEN SALEH NO.10 CILEDUK KARANG TENGAH', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(150, 1008, 2020, 'RS BHAYANGKARA', 'JL. JENDRAL SUDIRMAN KM. 45 PALEMBANG', 'Palembang', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(151, 1304, 2020, 'RS SILOAM MANADO', 'JL SAM RATULANGI NO 22 WENANG UTARA', 'Manado', 'Makassar+Manado', 0, 0, 0),
(152, 911, 2020, 'RS PROF DR TABRANI', 'JL JENDRAL SUDIRMAN 410 PEKANBARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(153, 1105, 2020, 'RS UMUM DAERAH BERIMAN', 'JL MS SUTOYO NO30 KEL GUNUNG ARI ULU', 'Balikpapan', 'Balikpapan', 0, 0, 0),
(154, 166, 2020, 'RS POLRI BHAYANGKARA SOEKAMTO', 'JL. KRAMATJATI', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(155, 105, 2020, 'RS PS REBO', 'JL TB SIMATUPANG NO 30 PS REBO JAKTIM\\', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(156, 125, 2020, 'RSIA RESTI MULYA', 'JL PAHLAWAN KOMARUDIN RAYA NO 5 RT 011 RW 05 PENGGILINGAN CAKUNG JAKTIM', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(157, 1414, 2020, 'RS CENTRA MEDIKA CIBINONG', 'JL MAYOR O KING JAYA ANDAYA NO 9', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(158, 507, 2020, 'RS KASIH IBU', 'JL BRIG JEND SLAMET RIYADI NO 404 RT01/RW10', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(159, 165, 2020, 'RS FATMAWATI', 'JL. RS FATMAWATI NO. 4 CILANDAK. CILANDAK BARAT JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(160, 1217, 2020, 'RS GRESTELINA', 'JL HERSTANING NO 51 MAKASAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(161, 1205, 2020, 'RSUP DR WAHIDIN SUDIRO HUSODO', 'JL PERINTIS KEMERDEKAAN KM 10 TAMALAHREA', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(162, 901, 2020, 'IBNU SINA', 'JL MELATI NO 60 PKU HARJOSARI', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(163, 815, 2020, 'RUMAH SAKIT SITI HAJAR', 'JL LETJEN JAMIUN GINTING', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(164, 816, 2020, 'RS IBNU SALEH', 'Jl. HM. Joni No.63A, RW.65, Ps. Merah Tim., Kec. Medan Area, Kota Medan, Sumatera Utara', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 4, 0),
(165, 333, 2020, 'RSIA IBI', 'JL DUPAK 15A', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(166, 706, 2020, 'RS IBU DAN ANAK SOERYA', 'JL RAYA KALIJATEN 11-15 TAMAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(167, 707, 2020, 'RSU ANWAR MEDIKA', 'JL RAYA BY PASS KRIAN SIDOARJO', 'Sidoarjo', 'Surabaya+Sidoarjo', 0, 0, 0),
(168, 1203, 2020, 'RS PELAMONIA', 'JENDRAL SUDERMAN NO 27', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(169, 208, 2020, 'BHAYANGKARA TKII SARTIKA ASIH', 'JL MOH TOHA 369', 'Bandung', 'Bandung', 0, 0, 0),
(170, 1215, 2020, 'RSA PERMATA HATI', 'JL BTP BLOK M NO.9 MAKASSAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(171, 153, 2020, 'RS UMUM TRIA DIPA', 'JL. RAYA PASAR MINGGU34 JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(172, 151, 2020, 'RS. SETIA MITRA', 'JL. FATMAWATI NO. 80-82 JAKARTA SELATAN', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(173, 152, 2020, 'RS. KESDAM JAYA', 'JL. MAHONI CIJANTUNG JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(174, 167, 2020, 'RS SATYA NEGARA', 'JL. AGUNG UTARA RAYA SUNTER JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(175, 170, 2020, 'RS ANTAM MEDIKA', 'JL. RAYA PEMUDA NO. 1A PULOGADUNG', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(176, 210, 2020, 'RSIA GRAHA BUNDA', 'JL. TERUSAN JAKARTA NO. 15-17 BABAKAN BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(177, 212, 2020, 'RSIA HANDAYANI', 'JL. GM BATU NO 7 BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(178, 339, 2020, 'RSIA KENDANGSARI MERR SURABAYA', 'JL DR IR H SOEKAMO NO 2 SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(179, 340, 2020, 'RS TNI AU SOEMITRO', 'JL RAYA SERAYU NO 17 SURABAYA', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(180, 607, 2020, 'RSIA RACHMI', 'JL. WAHID HASYIM NO. 47 NOTOPRAJAN NGAMJILAN YOGYAKARTA', 'Yogya', 'Semarang+Solo+Yogya', 0, 0, 0),
(181, 169, 2020, 'RS AQIDAH', 'JL. RADEN FATAH NO. 40', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(182, 804, 2020, 'RSU TERE MARGARETH', 'Jl. Ringroad No.200, Tj. Sari, Kec. Medan Selayang, Kota Medan, Sumatera Utara', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(183, 809, 2020, 'RSU TERE MARGARETH', 'JL RINGROAD', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(184, 814, 2020, 'RSU MADANI', 'Jl. Arief Rahman Hakim No.168, Sukaramai I, Kec. Medan Area, Kota Medan, Sumatera Utara', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 0),
(185, 914, 2020, 'RS PRIMA PEKANBARU', 'JL. BIMA NO. 1 TUANKU TAMBUSAI KELURAHAN DELIMA KECAMATAN TAMPAN KOTA PEKANBARU', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(186, 823, 2020, 'RSU MURNI TEGUH', 'JL JAWA NO 2 GG BUNTU', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(187, 1209, 2020, 'RSP UNHAS MAKASSAR', 'JL. PERINTIS KEMERDEKAAN KM. 11 MAKASSAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(188, 1210, 2020, 'RSIA SENTOSA', 'JL. JENDRAL SUDIRMAN NO. 52 SAWERIGADING MAKASSAR', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(189, 131, 2020, 'RS ISLAM PONDOK KOPI', 'JL RAYA PONDOK KOPI JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(190, 132, 2020, 'RS PELABUHAN JAKARTA', 'JL KERAMAT JAYA 1 TUGU UTARA KOJA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(191, 303, 2020, 'RSIA PUTRI', 'JL ARIF RAHMAN HAKIM', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(192, 207, 2020, 'RS RAJAWALI', 'JL. RAJAWALI BARAT NO. 38 MALEBER. BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(193, 902, 2020, 'RS ANNISA', 'JL GARUDA NO 66 TANGERANG MAPOYAN DAMAI', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 2, 2, 0),
(194, 810, 2020, 'RSU H ADAM MALIK', 'JL BUNGA LAU NO 17', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 1, 0),
(195, 1202, 2020, 'RSUP DR WAHIDIN SUDIRO HUSODO', 'JL PERINTIS KEMERDEKAAN KM 10 TAMALAHREA', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(196, 1413, 2020, 'RS BUAH HATI CIPUTAT', 'JL ARIA PUTRA NO 399 SERUA INDAH CIPUTAT TANGSEL', 'Bodetabek', 'Jabodetabek', 0, 0, 0),
(197, 302, 2020, 'RSIA NUR UMMI NUMBI', 'RSIA NUR UMMI NUMBI', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(198, 103, 2020, 'RS ZAHIRA', 'JL SIRSAK NO 21 RT9 RW 1 JAGAKARSA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(199, 1201, 2020, 'RS BHYANGKARA MAKASSSAR', 'JL.LETJEN ANDI MAPPAODDANG JONGAYA - TAMALATE', 'Makassar', 'Makassar+Manado', 0, 0, 0),
(200, 211, 2020, 'RS PARU DR. HA ROTINSULU', 'JL. BUKIT JARIAN NO. 40 CIPAGANTI BANDUNG', 'Bandung', 'Bandung', 0, 0, 0),
(201, 508, 2020, 'RS UMUM DR KUSTATI', 'JL KAPT MULYADI 249 PS KLIWON SOLO', 'Solo', 'Semarang+Solo+Yogya', 0, 0, 0),
(202, 316, 2020, 'RSIA PUSURA TEGALSARI', 'JL TEGALSARI JAWA TIMUR 60261', 'Surabaya', 'Surabaya+Sidoarjo', 0, 0, 0),
(203, 144, 2020, 'RSJ DHARMAWANGSA', 'JL. DHARMAWANGSA RAYA NO. 13', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(204, 155, 2020, 'RS FIRDAUS', 'KOMPLEK BEA CUKAI JL SIAK NO 14 CILINCING JAKARTA UTARA', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(205, 915, 2020, 'RS SYAFIRA', 'JL. JENDRAL SUDIRMAN NO. 134 PEKANBARU 28252', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(206, 802, 2020, 'RS BUNDA THAMRIN', 'JL SAI BATANG HARI NO 28-30 MEDAN', 'Medan', 'Medan+Palembang+Pekanbaru', 0, 0, 0),
(207, 150, 2020, 'RS. HARAPAN JAYAKARTA', 'JL. BEKASI TIMUR KM. 18 JAKARTA TIMUR', 'Jakarta', 'Jabodetabek', 0, 0, 0),
(210, 826, 2020, 'rsia. badrul aini', 'Jl. Bromo Jl. Syukri No.18, Tegal Sari III, Kec. Medan Area, Kota Medan, Sumatera Utara 20216', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 4, 1),
(213, 916, 2020, 'RS PMC', 'JL. Kav.1/Lembaga pemasyarakatan no 25', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 2, 2, 1),
(214, 917, 2020, 'JMB', NULL, 'Pekanbaru', 'Medan+Palembang+Pekanbaru', NULL, NULL, 1),
(215, 918, 2020, 'RS JMB', 'Jl. Sekolah/khayangan No. 53 Kelurahan Limbungan Baru Kec. Rumbai Pesisir', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 2, 3, 1),
(216, 1106, 2020, 'Rumah sakit Kanujoso', NULL, 'Balikpapan', 'Balikpapan', NULL, NULL, 1),
(217, 1107, 2020, 'Rumah sakit Kanujoso Jl. MT Haryono no 656 balikpapan', 'Jl. MT Haryono no.656', 'Balikpapan', 'Balikpapan', 1, 2, 1),
(218, 1108, 2020, 'Rumah Sakit Balikpapan Baru/PT Medical Helt Countera', NULL, 'Balikpapan', 'Balikpapan', NULL, NULL, 1),
(219, 1109, 2020, 'RSIA Asih', 'Jl. Sepinggan rt5.27 no.104 Sepingggan Baru', 'Balikpapan', 'Balikpapan', 2, 4, 1),
(220, 1110, 2020, 'RS HEMINA BALIKPAPAN', 'Jl. MT Haryono no.45 rt.45', 'Balikpapan', 'Balikpapan', 2, 3, 1),
(221, 1013, 2020, 'RS Umum Sriwijaya', 'Jl. Jendral Sudirman Km.4,5 No. 502 20 Ilir D 4 Kec  Ilir Timur 1', 'Palembang', 'Medan+Palembang+Pekanbaru', 2, 3, 1),
(222, 919, 2020, 'RS ZAINAB', 'Jl. Ronggowarsito  No. 1 kelurahan sukamaju kecamatan sail', 'Pekanbaru', 'Medan+Palembang+Pekanbaru', 2, 2, 1),
(223, 1111, 2020, 'Rumah Sakit Balikpapan Baru', 'Jl. Jokotole 2.Sumberrejo Balikpapan Tengah', 'Balikpapan', 'Balikpapan', 2, 4, 1),
(224, 1112, 2020, 'Rs. Siloam Balikpapan', 'Jl. Haryono Balikpapan Selatan', 'Balikpapan', 'Balikpapan', 2, 2, 1),
(225, 827, 2020, 'Bina Kasih', 'Jl. Jend, Jl. Tahi Bonar Simatupang No.148, Sunggal, Kec. Medan Sunggal, Kota Medan, Sumatera Utara', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 1),
(226, 828, 2020, 'RS SRI RATU MEDAN', 'Jl. Nibung Raya No.204-212, Petisah Tengah, Kec. Medan Petisah, Kota Medan, Sumatera Utara', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 3, 1),
(227, 1113, 2020, 'Rs Hermina', 'Jl. MT Haryono no.45 rt.45', 'Balikpapan', 'Balikpapan', 2, 3, 1),
(228, 1014, 2020, 'RSIA MARISSA', 'Jl. Kapten Abdullah No.1212 RT. 18, Plaju Ilir, Kec. Plaju, Kota Palembang, Sumatera Selatan 30119', 'Palembang', 'Medan+Palembang+Pekanbaru', 2, 4, 1),
(229, 1114, 2020, 'Rs Siloam balikpapan', 'Jl. MT Haryono Balikpapan Selatan', 'Balikpapan', 'Balikpapan', 2, 2, 1),
(230, 829, 2020, 'rsu. herna', 'Jl. Mojopahit No.118A, Petisah Hulu, Kec. Medan Baru, Kota Medan, Sumatera Utara 20152', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 2, 1),
(231, 398, 2020, 'RSIA SITI AISYAH', 'Jl. Pacar Keling 15A', 'Surabaya', 'Surabaya+Sidoarjo', 2, 4, 1),
(232, 715, 2020, 'RS. CITRA MEDIKA', 'JL. Raya Surabaya - Mojokerto KM 44 Kramat Temenggung', 'Sidoarjo', 'Surabaya+Sidoarjo', 2, 3, 1),
(233, 830, 2020, 'RSU Sarah Medan', 'Jl. Baja Raya No.10, Petisah Tengah, Kec. Medan Petisah, Kota Medan, Sumatera Utara 20111', 'Medan', 'Medan+Palembang+Pekanbaru', 2, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quest_foto`
--

CREATE TABLE `tbl_quest_foto` (
  `id_foto` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `jenis` tinyint(4) NOT NULL,
  `foto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_quest_foto`
--

INSERT INTO `tbl_quest_foto` (`id_foto`, `id_rs`, `jenis`, `foto`, `created_at`, `updated_at`) VALUES
(1, 221, 2, '1597519457_Screenshot_20200816_022328.jpg', '2020-08-15 12:24:17', '2020-08-15 12:24:17'),
(2, 221, 2, '1597519486_IMG_20200813_150714.jpg', '2020-08-15 12:24:47', '2020-08-15 12:24:47'),
(3, 221, 2, '1597519524_IMG_20200813_150643.jpg', '2020-08-15 12:25:25', '2020-08-15 12:25:25'),
(4, 221, 2, '1597519539_IMG_20200813_150652.jpg', '2020-08-15 12:25:39', '2020-08-15 12:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quest_kuesioner`
--

CREATE TABLE `tbl_quest_kuesioner` (
  `id_kuesioner` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `interviewer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `waktu_mulai` time DEFAULT NULL,
  `waktu_selesai` time DEFAULT NULL,
  `tahun_berdiri` year(4) DEFAULT NULL,
  `luas` decimal(7,2) UNSIGNED DEFAULT NULL,
  `frekuensi` smallint(5) UNSIGNED DEFAULT NULL,
  `program` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendapatan` decimal(5,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_quest_kuesioner`
--

INSERT INTO `tbl_quest_kuesioner` (`id_kuesioner`, `id_rs`, `interviewer`, `tanggal`, `waktu_mulai`, `waktu_selesai`, `tahun_berdiri`, `luas`, `frekuensi`, `program`, `pendapatan`, `created_at`, `updated_at`) VALUES
(1, 33, 'Lindawati', '1970-01-01', '12:00:00', '14:25:00', NULL, 520.00, 6, 'Dapat discout harga', 50.00, '2020-08-14 21:47:55', '2020-08-14 21:47:55'),
(2, 36, 'Asmani', '1970-01-01', '14:30:00', '15:00:00', 2008, 3048.00, 2, NULL, 15.00, '2020-08-14 21:58:21', '2020-08-14 22:14:46'),
(3, 17, 'Lindawati', '1970-01-01', '10:00:00', '10:25:00', NULL, 500.00, 1, 'One med ada diskon harga', -50.00, '2020-08-14 22:06:42', '2020-08-14 22:06:43'),
(4, 184, 'Lindawati', '1970-01-01', '10:00:00', '10:30:00', NULL, 550.00, 4, 'BSN sales memberikan Souvenir', -50.00, '2020-08-14 22:15:26', '2020-08-14 22:15:26'),
(5, 25, 'Afnizar', '1970-01-01', '11:00:00', '13:20:00', 1983, 400.00, 1, NULL, -40.00, '2020-08-14 23:48:35', '2020-08-14 23:48:35'),
(6, 24, 'Lindawati', '1970-01-01', '13:00:00', '14:50:00', NULL, NULL, 4, 'Tidak ada promosi', -20.00, '2020-08-14 23:56:29', '2020-08-14 23:56:29'),
(7, 164, 'Lindawati', '1970-01-01', '11:15:00', '13:00:00', 1995, 300.00, 1, 'Discount Harga, Paket Lebaran', -40.00, '2020-08-15 00:03:33', '2020-08-15 00:03:33'),
(8, 44, 'Afnizar', '1970-01-01', '10:20:00', '13:15:00', 1993, 400.00, 4, NULL, -50.00, '2020-08-15 00:11:23', '2020-08-15 00:11:23'),
(9, 9, 'Luidawati', '1970-01-01', '12:30:00', '13:00:00', 2002, 25740.00, 3, 'BSN ada discount harga B ada discount harga', 30.00, '2020-08-15 00:21:37', '2020-08-15 00:21:37'),
(10, 56, 'Ivanna', '2020-04-08', '13:00:00', '16:25:00', NULL, 900.00, 4, NULL, -30.00, '2020-08-15 01:09:53', '2020-08-15 01:09:53'),
(11, 5, 'Lindawati', '1970-01-01', '12:00:00', '12:20:00', 2014, 1000.00, 2, 'Discount harga untuk rumah sakit', -25.00, '2020-08-15 01:42:24', '2020-08-15 01:42:24'),
(12, 125, 'Asmuni', '1970-01-01', '12:30:00', '13:00:00', 2017, NULL, 4, NULL, 20.00, '2020-08-15 02:06:29', '2020-08-15 02:06:29'),
(13, 213, 'LESI LESMANA', '1970-01-01', '00:00:11', '12:00:00', 2005, 7433.00, 4, 'tidak ada', 70.00, '2020-08-15 08:29:48', '2020-08-15 08:29:48'),
(14, 217, 'NOVI ADI', '1970-01-01', '10:00:00', '10:30:00', 2000, 4800.00, 6, 'TA', 15.00, '2020-08-15 10:18:19', '2020-08-15 10:18:19'),
(15, 219, 'NOVI ADI', '1970-01-01', '02:00:00', '02:30:00', 2014, 1200.00, 4, 'TA', -10.00, '2020-08-15 11:46:54', '2020-08-15 11:46:54'),
(16, 220, 'NOVI ADI', '1970-01-01', '09:00:00', '09:40:00', 2015, 2400.00, 4, 'TA', -10.00, '2020-08-15 12:08:17', '2020-08-15 12:08:17'),
(17, 221, 'Asmuni', '2020-08-15', '11:19:00', '12:00:00', 2017, 40000.00, 4, 'Tidka tahu', -20.00, '2020-08-15 12:19:55', '2020-08-15 12:19:55'),
(18, 215, 'lesi lesmana', '2020-05-08', '09:00:00', '10:31:00', 2013, 3600.00, 4, 'tidak ada', 45.00, '2020-08-15 17:33:46', '2020-08-15 17:33:46'),
(19, 135, 'Lesi Lesmana', '2020-06-08', '04:41:00', '05:41:00', 1963, 2850.00, 4, 'tidak ada', 45.00, '2020-08-15 17:48:27', '2020-08-15 17:48:27'),
(20, 222, 'Delvie Franika', '2020-08-08', '09:00:00', '10:11:00', 2006, 1500.00, 1, 'tidak ada', -80.00, '2020-08-15 18:13:40', '2020-08-15 18:13:40'),
(21, 223, 'NOVI ADI', '1970-01-01', '03:00:00', '03:31:00', 2001, 1400.00, NULL, 'TA', -10.00, '2020-08-15 22:12:43', '2020-08-15 22:12:43'),
(22, 224, 'NOVI ADI', '2020-03-08', '10:00:00', '10:30:00', 2009, 480.00, 4, 'TA', 10.00, '2020-08-15 23:38:08', '2020-08-15 23:38:08'),
(23, 225, 'Lindawati', '1970-01-01', '14:30:00', '15:00:00', 2005, NULL, 4, 'Wellkad memberikan discount harga, GEA memberikan discount harga, BSN memberikan discont harga', -46.00, '2020-08-16 00:18:43', '2020-08-16 00:18:43'),
(24, 226, 'Lindawati', '2020-04-08', '15:00:00', '15:00:00', 1986, 500.00, 1, NULL, -50.00, '2020-08-16 01:09:02', '2020-08-16 01:09:02'),
(25, 182, 'Ivana', '1970-01-01', '12:00:00', '12:20:00', 2012, 500.00, NULL, 'Sales langsung berhubungan ke owner', 50.00, '2020-08-16 06:58:21', '2020-08-16 06:58:21'),
(26, 193, 'Delvie Franika', '2020-08-08', '10:04:00', '11:45:00', 1997, 625.00, 4, 'tidak ada', 10.00, '2020-08-16 07:08:57', '2020-08-16 07:09:15'),
(27, 45, 'Susanti', '1970-01-01', '14:15:00', '15:16:00', 1992, 690.00, 2, 'promosi produk masing - masing', -10.00, '2020-08-16 07:22:35', '2020-08-16 07:22:35'),
(28, 82, 'lesi lesmana', '2020-06-08', '13:07:00', '14:07:00', 2007, 4500.00, 4, 'seminar produk nestle, pembagian sample gratis bubur bayi, pelatihan APM bagi bidan', 70.00, '2020-08-16 08:09:32', '2020-08-16 08:09:32'),
(29, 227, 'NOVI ADI', '1970-01-01', '09:00:00', '09:40:00', 2016, 2400.00, 4, 'TA', -10.00, '2020-08-16 08:53:12', '2020-08-16 08:53:12'),
(30, 228, 'Asmuni', '2020-03-08', '12:00:00', '13:00:00', 2004, 1660.00, 2, NULL, -50.00, '2020-08-16 10:03:25', '2020-08-16 10:03:25'),
(31, 229, 'NOVI ADI', '2020-03-08', '10:00:00', '10:30:00', 2009, 480.00, 4, 'TA', 10.00, '2020-08-16 14:37:32', '2020-08-16 14:37:32'),
(32, 230, 'Lindawati', '2020-08-08', '11:15:00', '01:00:00', 1970, 650.00, 3, NULL, -15.00, '2020-08-17 21:14:53', '2020-08-17 21:14:53'),
(33, 210, 'Ivanna', '2020-03-08', '10:25:00', '10:20:00', 1986, 405.00, 2, NULL, -50.00, '2020-08-17 23:01:28', '2020-08-17 23:01:28'),
(34, 194, 'Lindawati', '2020-05-08', '12:00:00', '12:45:00', 1993, NULL, 4, 'Discount harga BSN\r\nDIscount harga 3M', 30.00, '2020-08-17 23:16:22', '2020-08-17 23:16:22'),
(35, 186, 'Lindawati', '2020-06-08', '13:00:00', '13:20:00', 2012, 16000.00, 3, 'BSN ada discount harga \r\n3M ada discount harga', -15.00, '2020-08-18 00:02:08', '2020-08-18 00:02:08'),
(36, 231, 'Tuti Wina', '1970-01-01', '09:15:00', '10:05:00', 2000, 600.00, 1, 'TA PROGRAM', 20.00, '2020-08-18 00:25:19', '2020-08-18 00:25:19'),
(37, 232, 'Hans', '2020-03-08', '13:09:00', '13:40:00', 2007, 8000.00, 4, 'TIDAK ADA PROGRAM', -15.00, '2020-08-18 01:21:52', '2020-08-18 01:21:52'),
(38, 233, 'Ivanna', '1970-01-01', '10:00:00', '13:15:00', 1983, 500.00, 4, 'BSN ada discount harga, Onemed ada discount harga', -40.00, '2020-08-18 01:46:13', '2020-08-18 01:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quest_responden`
--

CREATE TABLE `tbl_quest_responden` (
  `id_responden` bigint(20) UNSIGNED NOT NULL,
  `id_rs` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bagian` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_quest_responden`
--

INSERT INTO `tbl_quest_responden` (`id_responden`, `id_rs`, `nama`, `jabatan`, `bagian`, `telp`, `hp`, `created_at`, `updated_at`) VALUES
(1, 210, 'Ulfa', 'asisten apoteker', 'farmasi', '089658383763', '0617350315', '2020-08-14 02:52:05', '2020-08-14 02:52:05'),
(4, 33, 'Wiwik Kusuma wardani', 'instalasi farmasi', 'Asisten Apoteker', '082276081276', NULL, '2020-08-14 21:43:30', '2020-08-14 21:43:30'),
(5, 36, 'Yovi Pranata', 'Apoteker Pendamping', 'Farmasi', '082184002552', '0711822723', '2020-08-14 21:51:57', '2020-08-14 22:14:24'),
(6, 17, 'Nur Zanah', 'Asisten Apoteker', 'Farmasi', '085362144240', '0617861771', '2020-08-14 21:57:52', '2020-08-14 21:57:52'),
(7, 184, 'Karliani', 'Gudang Farmasi', 'Pengadaan Obat', '081260764905', '0617345911', '2020-08-14 22:11:19', '2020-08-14 22:11:19'),
(8, 25, 'Jona', 'Logistik Farmasi', 'Farmasi', '081370301532', '0616610072', '2020-08-14 23:46:12', '2020-08-14 23:46:12'),
(9, 24, 'Safuani', 'Asisten Apoteker', 'Instalasi Farmasi', '081260256125', '0618441111', '2020-08-14 23:51:32', '2020-08-14 23:52:14'),
(10, 164, 'Dame Nasrah Harahap', 'Farmasi', 'Pengadaan Farmasi', '085885507787', '0617342241', '2020-08-15 00:00:31', '2020-08-15 00:00:31'),
(11, 44, 'Marta Sebayang', 'Logistik', 'Farmasi', '08126015790', NULL, '2020-08-15 00:09:18', '2020-08-15 00:09:18'),
(12, 9, 'Adang', 'Carge Store', 'Store', '081397537176', '0614566368', '2020-08-15 00:09:41', '2020-08-15 00:09:41'),
(13, 56, 'Enny Suheni', 'Kepala Seksi Farmasi', 'Farmasi', '081260494066', NULL, '2020-08-15 01:05:05', '2020-08-15 01:05:05'),
(14, 5, 'Reza Septian', 'Staff', 'Gudang farmasi', NULL, '06188813182', '2020-08-15 01:39:13', '2020-08-15 01:39:13'),
(15, 125, 'Heru Susanto', 'Staff', 'Farmasi', '082180012264', '0711446272', '2020-08-15 02:05:10', '2020-08-15 02:05:10'),
(16, 221, 'Mirza Amrina, S Fam, Apt', 'Apoteker Penanggung Jawab', 'Farmasi', '081930852058', '0711419680', '2020-08-15 12:11:44', '2020-08-15 12:11:44'),
(17, 225, 'Irma Suryani', 'Staf', 'Apotik Farmasi', '081262957096', '0618445270', '2020-08-16 00:02:30', '2020-08-16 00:02:30'),
(18, 226, 'Utari Meuthia Khanza', 'Asisten Apoteker', 'Farmasi', '085362849363', '0614521074', '2020-08-16 01:05:10', '2020-08-16 01:05:10'),
(19, 228, 'Meta Wira Yanti', 'Bidan', 'Farmasi', '081270176364', '0711542508', '2020-08-16 10:01:11', '2020-08-16 10:01:11'),
(20, 230, 'Rosmayana situmorang', 'pembelian', 'instalasi farmasi', '08126593001', '0614510766', '2020-08-16 23:22:07', '2020-08-16 23:22:07'),
(21, 213, 'Wahyu Hani Masruroh', 'Asisten Apoteker', 'Instalasi Farmasi', '088803402600', '0315019164', '2020-08-17 22:10:43', '2020-08-17 22:10:43'),
(22, 231, 'WAHYU HANI MASRUROH', 'ASISTEN APOTEKER', 'Instalasi Farmasi', '088803402600', '0315019164', '2020-08-17 23:01:56', '2020-08-17 23:01:56'),
(23, 194, 'Titus', 'Staff', 'Gudang', '081269469007', '061836143', '2020-08-17 23:14:20', '2020-08-17 23:14:20'),
(24, 186, 'Ririn', NULL, 'Farmasi', '082363748319', '06180501888', '2020-08-17 23:58:21', '2020-08-17 23:58:21'),
(25, 232, 'Ibu Rida', 'Kepala Pengadaan', 'Pengadaan', '0', '0321361000', '2020-08-18 01:13:39', '2020-08-18 01:14:34'),
(26, 233, 'Maya', 'Asisten Apoteker', 'Farmasi', NULL, '0614568383', '2020-08-18 01:42:24', '2020-08-18 01:42:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@object-research.com', NULL, '$2y$10$/h44D/vVnH0L297fmd2F4OxTsw0CHD1fW08llR08r87iFF1WCf4Kq', NULL, '2020-08-09 02:33:32', '2020-08-09 02:33:32'),
(2, 'testing1', 'testing1@email.com', NULL, '$2y$10$DGK745WKrB4PwhhwtbJcM.GTbpBov23j2kZPZZhhc2dG9iTu/.Ygi', NULL, '2020-08-09 10:35:26', '2020-08-09 10:35:26'),
(3, 'test', 'test@email.com', NULL, '$2y$10$GnmChtRwu07cmpJLSgteVuOffn7sr3Jo99mgAKMHAkBD1nTunWNTy', NULL, '2020-08-11 01:41:30', '2020-08-11 01:41:30');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_1_all`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_1_all` (
`id_grup_all` int(10) unsigned
,`id_grup_1` int(10) unsigned
,`nama_grup_1` varchar(191)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_2_all`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_2_all` (
`id_grup_all` int(10) unsigned
,`id_grup_1` int(10) unsigned
,`id_grup_2` int(10) unsigned
,`nama_grup_2` varchar(191)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_all`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_all` (
`id_grup_all` int(10) unsigned
,`id_grup_1` int(10) unsigned
,`nama_grup_1` varchar(191)
,`id_grup_2` int(10) unsigned
,`nama_grup_2` varchar(191)
,`nama_lini` varchar(191)
,`id_grup_3` int(10) unsigned
,`nama_grup_3` varchar(191)
,`brand` varchar(191)
,`ukuran` varchar(191)
,`tipe` smallint(5) unsigned
,`nama_tipe` varchar(3)
,`status_grup_2` tinyint(1)
,`status_grup_3` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_grup_fill_revenue`
-- (See below for the actual view)
--
CREATE TABLE `vw_grup_fill_revenue` (
`id_grup_all` int(10) unsigned
,`id_grup_3` int(10) unsigned
,`nama_manufacture` varchar(191)
,`nama_brand` varchar(385)
,`ukuran` varchar(191)
,`tipe` smallint(5) unsigned
,`nama_tipe` varchar(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_last_progress`
-- (See below for the actual view)
--
CREATE TABLE `vw_last_progress` (
`id_rs` bigint(20)
,`id_grup_2` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_log_edit`
-- (See below for the actual view)
--
CREATE TABLE `vw_log_edit` (
`id_log_edit` bigint(20) unsigned
,`id_rs` bigint(20)
,`id_rawdata` bigint(20) unsigned
,`nama_grup_2` varchar(191)
,`nama` varchar(191)
,`tahun` smallint(5) unsigned
,`lokasi` varchar(191)
,`ip` varchar(191)
,`tanggal_akses` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rawdata`
-- (See below for the actual view)
--
CREATE TABLE `vw_rawdata` (
`ketersediaan` varchar(5)
,`id_grup_all` int(10) unsigned
,`tahun` smallint(5) unsigned
,`id_rs` bigint(20) unsigned
,`id_rawdata` bigint(20) unsigned
,`nama_rs` varchar(191)
,`alamat` mediumtext
,`kota` varchar(191)
,`kelompok_kota` varchar(191)
,`status_pengelolaan` varchar(7)
,`kelas` varchar(1)
,`nama_grup_1` varchar(191)
,`nama_lini` varchar(191)
,`nama_grup_2` varchar(191)
,`nama_grup_3` varchar(387)
,`ukuran` varchar(191)
,`brand` varchar(191)
,`satuan_tipe` varchar(3)
,`jumlah` int(10) unsigned
,`satuan` varchar(3)
,`harga` bigint(20) unsigned
,`total` bigint(20) unsigned
,`jumlah_q1` int(10) unsigned
,`satuan_q1` varchar(3)
,`harga_q1` bigint(20) unsigned
,`total_q1` bigint(20) unsigned
,`jumlah_q2` int(10) unsigned
,`satuan_q2` varchar(3)
,`harga_q2` bigint(20) unsigned
,`total_q2` bigint(20) unsigned
,`proporsi` varchar(18)
,`status_grup_2` varchar(3)
,`status_produk` varchar(3)
,`status_rs` varchar(3)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_1_all`
--
DROP TABLE IF EXISTS `vw_grup_1_all`;

CREATE VIEW `vw_grup_1_all`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_1` AS `id_grup_1`,`tbl_grup_1`.`nama_grup_1` AS `nama_grup_1` from (`tbl_grup_all` join `tbl_grup_1` on(`tbl_grup_all`.`id_grup_1` = `tbl_grup_1`.`id_grup_1`)) order by `tbl_grup_1`.`id_grup_1` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_2_all`
--
DROP TABLE IF EXISTS `vw_grup_2_all`;

CREATE VIEW `vw_grup_2_all`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_1` AS `id_grup_1`,`tbl_grup_all`.`id_grup_2` AS `id_grup_2`,`tbl_grup_2`.`nama_grup_2` AS `nama_grup_2` from (`tbl_grup_all` join `tbl_grup_2` on(`tbl_grup_all`.`id_grup_2` = `tbl_grup_2`.`id_grup_2`)) order by `tbl_grup_all`.`id_grup_2` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_all`
--
DROP TABLE IF EXISTS `vw_grup_all`;

CREATE VIEW `vw_grup_all`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_1` AS `id_grup_1`,`tbl_grup_1`.`nama_grup_1` AS `nama_grup_1`,`tbl_grup_all`.`id_grup_2` AS `id_grup_2`,`tbl_grup_2`.`nama_grup_2` AS `nama_grup_2`,`tbl_grup_2`.`nama_lini` AS `nama_lini`,`tbl_grup_all`.`id_grup_3` AS `id_grup_3`,`tbl_grup_3`.`nama_grup_3` AS `nama_grup_3`,`tbl_grup_3`.`brand` AS `brand`,`tbl_grup_all`.`ukuran` AS `ukuran`,`tbl_grup_all`.`tipe` AS `tipe`,case `tbl_grup_all`.`tipe` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `nama_tipe`,`tbl_grup_2`.`status_grup_2` AS `status_grup_2`,`tbl_grup_3`.`status_grup_3` AS `status_grup_3` from (((`tbl_grup_all` join `tbl_grup_1` on(`tbl_grup_all`.`id_grup_1` = `tbl_grup_1`.`id_grup_1`)) join `tbl_grup_2` on(`tbl_grup_all`.`id_grup_2` = `tbl_grup_2`.`id_grup_2`)) join `tbl_grup_3` on(`tbl_grup_all`.`id_grup_3` = `tbl_grup_3`.`id_grup_3`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_grup_fill_revenue`
--
DROP TABLE IF EXISTS `vw_grup_fill_revenue`;

CREATE VIEW `vw_grup_fill_revenue`  AS  select `tbl_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_grup_all`.`id_grup_3` AS `id_grup_3`,`tbl_grup_3`.`brand` AS `nama_manufacture`,concat(`tbl_grup_3`.`nama_grup_3`,' (',`tbl_grup_3`.`brand`,')') AS `nama_brand`,`tbl_grup_all`.`ukuran` AS `ukuran`,`tbl_grup_all`.`tipe` AS `tipe`,case `tbl_grup_all`.`tipe` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `nama_tipe` from (`tbl_grup_all` join `tbl_grup_3` on(`tbl_grup_all`.`id_grup_3` = `tbl_grup_3`.`id_grup_3`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_last_progress`
--
DROP TABLE IF EXISTS `vw_last_progress`;

CREATE VIEW `vw_last_progress`  AS  select `tbl_log_edit`.`id_rs` AS `id_rs`,max(`tbl_log_edit`.`id_grup_2`) AS `id_grup_2` from `tbl_log_edit` group by `tbl_log_edit`.`id_rs` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_log_edit`
--
DROP TABLE IF EXISTS `vw_log_edit`;

CREATE VIEW `vw_log_edit`  AS  select `tbl_log_edit`.`id_log_edit` AS `id_log_edit`,`tbl_log_edit`.`id_rs` AS `id_rs`,`tbl_main_rs`.`id_rawdata` AS `id_rawdata`,`tbl_grup_2`.`nama_grup_2` AS `nama_grup_2`,`tbl_main_rs`.`nama_rs` AS `nama`,`tbl_main_rs`.`tahun` AS `tahun`,`tbl_log_edit`.`lokasi` AS `lokasi`,`tbl_log_edit`.`ip` AS `ip`,`tbl_log_edit`.`updated_at` AS `tanggal_akses` from ((`tbl_log_edit` join `tbl_main_rs` on(`tbl_log_edit`.`id_rs` = `tbl_main_rs`.`id_rs`)) left join `tbl_grup_2` on(`tbl_log_edit`.`id_grup_2` = `tbl_grup_2`.`id_grup_2`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rawdata`
--
DROP TABLE IF EXISTS `vw_rawdata`;

CREATE VIEW `vw_rawdata`  AS  select case `tbl_main_audit`.`ketersediaan` when '0' then 'Tidak' when '1' then 'Iya' end AS `ketersediaan`,`vw_grup_all`.`id_grup_all` AS `id_grup_all`,`tbl_main_rs`.`tahun` AS `tahun`,`tbl_main_rs`.`id_rs` AS `id_rs`,`tbl_main_rs`.`id_rawdata` AS `id_rawdata`,`tbl_main_rs`.`nama_rs` AS `nama_rs`,`tbl_main_rs`.`alamat` AS `alamat`,`tbl_main_rs`.`kota` AS `kota`,`tbl_main_rs`.`kelompok_kota` AS `kelompok_kota`,case `tbl_main_rs`.`status_pengelolaan` when '1' then 'Public' when '2' then 'Private' end AS `status_pengelolaan`,case `tbl_main_rs`.`kelas` when '1' then 'A' when '2' then 'B' when '3' then 'C' when '4' then 'D' end AS `kelas`,`vw_grup_all`.`nama_grup_1` AS `nama_grup_1`,`vw_grup_all`.`nama_lini` AS `nama_lini`,`vw_grup_all`.`nama_grup_2` AS `nama_grup_2`,concat(`vw_grup_all`.`nama_grup_3`,' ( ',`vw_grup_all`.`brand`,' )') AS `nama_grup_3`,`vw_grup_all`.`ukuran` AS `ukuran`,`vw_grup_all`.`brand` AS `brand`,case `vw_grup_all`.`tipe` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `satuan_tipe`,`tbl_main_audit`.`jumlah` AS `jumlah`,case `tbl_main_audit`.`satuan` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `satuan`,`tbl_main_audit`.`harga` AS `harga`,`tbl_main_audit`.`total` AS `total`,`tbl_main_audit`.`jumlah_q1` AS `jumlah_q1`,case `tbl_main_audit`.`satuan_q1` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `satuan_q1`,`tbl_main_audit`.`harga_q1` AS `harga_q1`,`tbl_main_audit`.`total_q1` AS `total_q1`,`tbl_main_audit`.`jumlah_q2` AS `jumlah_q2`,case `tbl_main_audit`.`satuan_q2` when '0' then 'Box' when '1' then 'Rol' when '2' then 'Pcs' end AS `satuan_q2`,`tbl_main_audit`.`harga_q2` AS `harga_q2`,`tbl_main_audit`.`total_q2` AS `total_q2`,concat(`tbl_main_audit`.`jumlah` / `tbl_main_audit`.`jumlah_q1`,' % ') AS `proporsi`,case `vw_grup_all`.`status_grup_2` when '0' then '' when '1' then 'NEW' end AS `status_grup_2`,case `vw_grup_all`.`status_grup_3` when '0' then '' when '1' then 'NEW' end AS `status_produk`,case `vw_grup_all`.`tipe` when '0' then '' when '1' then 'NEW' end AS `status_rs` from ((`tbl_main_audit` join `vw_grup_all` on(`tbl_main_audit`.`id_grup_all` = `vw_grup_all`.`id_grup_all`)) join `tbl_main_rs` on(`tbl_main_audit`.`id_rs` = `tbl_main_rs`.`id_rs`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`id_config`);

--
-- Indexes for table `tbl_form`
--
ALTER TABLE `tbl_form`
  ADD PRIMARY KEY (`tahun`);

--
-- Indexes for table `tbl_grup_1`
--
ALTER TABLE `tbl_grup_1`
  ADD PRIMARY KEY (`id_grup_1`);

--
-- Indexes for table `tbl_grup_2`
--
ALTER TABLE `tbl_grup_2`
  ADD PRIMARY KEY (`id_grup_2`);

--
-- Indexes for table `tbl_grup_3`
--
ALTER TABLE `tbl_grup_3`
  ADD PRIMARY KEY (`id_grup_3`);

--
-- Indexes for table `tbl_grup_all`
--
ALTER TABLE `tbl_grup_all`
  ADD PRIMARY KEY (`id_grup_all`),
  ADD KEY `tbl_grup_all_id_grup_1_foreign` (`id_grup_1`),
  ADD KEY `tbl_grup_all_id_grup_2_foreign` (`id_grup_2`),
  ADD KEY `tbl_grup_all_id_grup_3_foreign` (`id_grup_3`);

--
-- Indexes for table `tbl_log_edit`
--
ALTER TABLE `tbl_log_edit`
  ADD PRIMARY KEY (`id_log_edit`);

--
-- Indexes for table `tbl_main_audit`
--
ALTER TABLE `tbl_main_audit`
  ADD PRIMARY KEY (`id_audit`),
  ADD KEY `tbl_main_audit_id_rs_foreign` (`id_rs`),
  ADD KEY `tbl_main_audit_id_grup_all_foreign` (`id_grup_all`),
  ADD KEY `tbl_main_audit_id_log_edit_foreign` (`id_log_edit`);

--
-- Indexes for table `tbl_main_rs`
--
ALTER TABLE `tbl_main_rs`
  ADD PRIMARY KEY (`id_rs`);

--
-- Indexes for table `tbl_quest_foto`
--
ALTER TABLE `tbl_quest_foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `tbl_quest_foto_id_rs_foreign` (`id_rs`);

--
-- Indexes for table `tbl_quest_kuesioner`
--
ALTER TABLE `tbl_quest_kuesioner`
  ADD PRIMARY KEY (`id_kuesioner`),
  ADD UNIQUE KEY `tbl_quest_kuesioner_id_rs_unique` (`id_rs`);

--
-- Indexes for table `tbl_quest_responden`
--
ALTER TABLE `tbl_quest_responden`
  ADD PRIMARY KEY (`id_responden`),
  ADD UNIQUE KEY `tbl_quest_responden_id_rs_unique` (`id_rs`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_config`
--
ALTER TABLE `tbl_config`
  MODIFY `id_config` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_grup_1`
--
ALTER TABLE `tbl_grup_1`
  MODIFY `id_grup_1` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_grup_2`
--
ALTER TABLE `tbl_grup_2`
  MODIFY `id_grup_2` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_grup_3`
--
ALTER TABLE `tbl_grup_3`
  MODIFY `id_grup_3` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `tbl_grup_all`
--
ALTER TABLE `tbl_grup_all`
  MODIFY `id_grup_all` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=344;

--
-- AUTO_INCREMENT for table `tbl_log_edit`
--
ALTER TABLE `tbl_log_edit`
  MODIFY `id_log_edit` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=453;

--
-- AUTO_INCREMENT for table `tbl_main_audit`
--
ALTER TABLE `tbl_main_audit`
  MODIFY `id_audit` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=504;

--
-- AUTO_INCREMENT for table `tbl_main_rs`
--
ALTER TABLE `tbl_main_rs`
  MODIFY `id_rs` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `tbl_quest_foto`
--
ALTER TABLE `tbl_quest_foto`
  MODIFY `id_foto` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_quest_kuesioner`
--
ALTER TABLE `tbl_quest_kuesioner`
  MODIFY `id_kuesioner` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tbl_quest_responden`
--
ALTER TABLE `tbl_quest_responden`
  MODIFY `id_responden` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_grup_all`
--
ALTER TABLE `tbl_grup_all`
  ADD CONSTRAINT `tbl_grup_all_id_grup_1_foreign` FOREIGN KEY (`id_grup_1`) REFERENCES `tbl_grup_1` (`id_grup_1`),
  ADD CONSTRAINT `tbl_grup_all_id_grup_2_foreign` FOREIGN KEY (`id_grup_2`) REFERENCES `tbl_grup_2` (`id_grup_2`),
  ADD CONSTRAINT `tbl_grup_all_id_grup_3_foreign` FOREIGN KEY (`id_grup_3`) REFERENCES `tbl_grup_3` (`id_grup_3`);

--
-- Constraints for table `tbl_main_audit`
--
ALTER TABLE `tbl_main_audit`
  ADD CONSTRAINT `tbl_main_audit_id_grup_all_foreign` FOREIGN KEY (`id_grup_all`) REFERENCES `tbl_grup_all` (`id_grup_all`),
  ADD CONSTRAINT `tbl_main_audit_id_log_edit_foreign` FOREIGN KEY (`id_log_edit`) REFERENCES `tbl_log_edit` (`id_log_edit`),
  ADD CONSTRAINT `tbl_main_audit_id_rs_foreign` FOREIGN KEY (`id_rs`) REFERENCES `tbl_main_rs` (`id_rs`);

--
-- Constraints for table `tbl_quest_foto`
--
ALTER TABLE `tbl_quest_foto`
  ADD CONSTRAINT `tbl_quest_foto_id_rs_foreign` FOREIGN KEY (`id_rs`) REFERENCES `tbl_main_rs` (`id_rs`);

--
-- Constraints for table `tbl_quest_kuesioner`
--
ALTER TABLE `tbl_quest_kuesioner`
  ADD CONSTRAINT `tbl_quest_kuesioner_id_rs_foreign` FOREIGN KEY (`id_rs`) REFERENCES `tbl_main_rs` (`id_rs`);

--
-- Constraints for table `tbl_quest_responden`
--
ALTER TABLE `tbl_quest_responden`
  ADD CONSTRAINT `tbl_quest_responden_id_rs_foreign` FOREIGN KEY (`id_rs`) REFERENCES `tbl_main_rs` (`id_rs`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

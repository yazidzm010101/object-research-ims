url = $('input[name=url]').val();

$('[name*="readonly"]').attr('readonly', true);

//   AJAX GRUP1
$('#grup_1 .list-group-item').click(function (e) {
    updateGrup2($(e.target), '#grup_2');
});

$('#back-btn').click(function(e) {
    e.preventDefault();
    $('#slide-out').removeClass('toggled');
})

toggleRow('[name*="ketersediaan"]');

$('[name*="ketersediaan"]').change(function(e){
    toggleRow($(e.target));
});

$('#add-data').on('click', function(e){
    var row_length = $('[name*=row][name*=ketersediaan]').length + 1;
    var html =
        `<tr>
            <td><button type="button" class="btn btn-flat btn-danger btn-rounded" onclick="$(this).parents('tr').remove()">-</button>
            </td>
            <td>
            <input type="text" class="form-control" name="row[${row_length}][nama_grup_3]"/></td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="row[${row_length}][ketersediaan]" name="row[${row_length}][ketersediaan]" class="custom-control-input"
                    value=1
                    checked/>
                    <label class="custom-control-label" for="row[${row_length}][ketersediaan]"></label>
                </div>
            </td>
            <td><input type="text" name="row[${row_length}][brand]" value="" class="form-control"></td>
            <td><input type="text" name="row[${row_length}][ukuran]" value="" class="form-control"></td>
            <td>

            <select name=row[${row_length}][tipe] class="custom-select">
                <option value="" selected></option>
                <option value="0">Box</option>
                <option value="1">Rol</option>
                <option value="2">Pcs</option>
            </select>
            </td>
            <td><input type="number" min="0" name="row[${row_length}][jumlah]" value="" class="form-control "></td>
            <td><input type="number" min="0" value="" class="form-control" readonly></td>
            <td><input type="number" min="0" name="row[${row_length}][harga]" value="" class="form-control" ></td>
            <td><input type="number" min="0" name="row[${row_length}][jumlah_q1]" value="" class="form-control "></td>
            <td><input type="number" min="0"  value="" class="form-control" readonly></td>
            <td><input type="number" min="0" name="row[${row_length}][harga_q1]" value="" class="form-control" ></td>
            <td><input type="number" min="0" name="row[${row_length}][jumlah_q2]" value="" class="form-control "></td>
            <td><input type="number" min="0" value="" class="form-control" readonly></td>
            <td><input type="number" min="0" name="row[${row_length}][harga_q2]" value="" class="form-control" ></td>
        </tr>`;

    $('#grup_3').append(html);

})

function toggleRow(selector){
    var select = $(selector);
    $.each(select, function(index, item){
        var value = $(item).prop('checked');
        if(!value){
            $(item).parents('tr').not('.data-new').css('background-color','#ffffff');
            $(item).parents('tr').find('input, select').not('[name*="ketersediaan"],[name*="readonly"]').attr('readonly', '').find('option').addClass('d-none');
        }else{
            $(item).parents('tr').not('.data-new').css('background-color','#e3f2fd');
            $(item).parents('tr').find('input, select').not('[name*="ketersediaan"],[name*="readonly"]').removeAttr('readonly').find('option').removeClass('d-none');
        }
    });
}

// change-in-production
function updateGrup2(grup1, grup2){
    var id_grup_1 = $(grup1).attr('data-grup1');
    $(grup2).find('.list-group').empty();
    var keyrs = $(location).attr("href").split('/');
    var keyrs = keyrs[keyrs.length - 1].split('?')[0].replace( /#/, "" );
    console.log(keyrs);

    $.ajax({
        url: 'http://localhost:8000/data/grup_2/' + id_grup_1+'/'+keyrs,
        type: 'get',
        dataType: 'json',
        success: function (response) {
            // if the data requested was not null update
            if (response['data'].length > 0) {
                $('#slide-out').addClass('toggled');
                // iterate throug response and append it to group 2 select input
                $.each(response['data'], function(index, value){
                    var id_grup_2 = value.id_grup_2;
                    var name = value.nama_grup_2;
                    var is_null = value.is_null;
                    var list = `<a href="${url}?g1=${id_grup_1}&g2=${id_grup_2}" class="list-group-item list-group-item-action bg-light  ${ is_null != null ? "notnull" : '' }">${name}</a>`;
                    $(grup2).find('.list-group').append(list);
                });
            }else{
                updateGrup3(id_grup_1, 0);
            }
        }
    });
}

function updateGrup3(id_grup_1, id_grup_2){
    window.location.replace(`${url}?g1=${id_grup_1}&g2=${id_grup_2}`);
}

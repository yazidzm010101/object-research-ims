var jenisLab = {
    'updateJenis2': function(selector, reset = false){
        jenis2 = $('[name=jenis2]');
        kelas = $('[name=tipe_kelas]');
        value = $(selector).val();
        value2 = $(jenis2).val();
        value3 = $(kelas).val();
        if(value != null){
            $(jenis2).removeAttr('readonly').find('option').prop('disabled',true).addClass('d-none');
            $(kelas).removeAttr('readonly').find('option').prop('disabled',true).addClass('d-none');
            if(value == 2){
                $(jenis2).find('*[value=4], *[value=5]').prop('disabled', false).removeClass('d-none');
                $(kelas).find('*[value=4], *[value=5], *[value=6]').prop('disabled', false).removeClass('d-none');
                $(kelas).val(reset ? 4 : value3)
                $(jenis2).val(reset ? 4 : value2);
            }else{
                if(value == 1){
                    $(jenis2).find('*[value=1], *[value=3]').prop('disabled', false).removeClass('d-none');
                    $(jenis2).val(reset ? 1 : value2);
                }else{
                    $(jenis2).find('*[value=1], *[value=2]').prop('disabled', false).removeClass('d-none');
                    $(jenis2).val(reset ? 1 : value2);
                }
                $(kelas).find('*[value=1], *[value=2], *[value=3]').prop('disabled', false).removeClass('d-none');
                $(kelas).val(reset ? 1 : value3)
            }
        }else{
            $(jenis2).attr('readonly','').find('option').prop('disabled',true).addClass('d-none');
            $(kelas).attr('readonly','').find('option').prop('disabled',true).addClass('d-none');
        }
    },
}

var helper = {
    'imageInput': function(input, target){
        if(input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(target).attr('src', e.target.result).removeClass('d-none');
            }

            reader.readAsDataURL(input.files[0]);
        }
    },
}
var aniMate = {
    'initialize': function(first){
        $('.question-set').addClass('d-none');
        $(first).removeClass('d-none');
        $('.question-set *[data-next]').on('click', function(e){
            aniMate.next(e.target);
        })
        $('.question-set *[data-prev]').on('click', function(e){
            aniMate.prev(e.target);
        })
        $('.question-set *[type=submit]').on('click', function(e){
            e.preventDefault();
            var parent = $(e.target).parents('form');
            parent.addClass('animate__animated animate__fadeOutLeft');
            setTimeout(function(){
                $(parent).submit();
            }, 800);
        })
    },
    'next': function(selector){
        var parent = $(selector).parents('.question-set');
        var target = $(selector).attr('data-next');
        if(target){
            $(parent).removeClass('animate__animated animate__fadeInRight');
            $(parent).addClass('animate__animated animate__fadeOutLeft');
            setTimeout(function(){
                $(parent).removeClass('animate__animated animate__fadeOutLeft').addClass('d-none');
            }, 1000);
            setTimeout(function(){
                $(target).removeClass('d-none').addClass('animate__animated animate__fadeInRight');
            }, 800);
            setTimeout(function(){
                $(target).removeClass('animate__animated animate__fadeInRight');
            }, 1800);
        }
    },
    'prev': function(selector){
        var parent = $(selector).parents('.question-set');
        var target = $(selector).attr('data-prev');
        if(target){
            $(parent).addClass('animate__animated animate__fadeOutRight');
            setTimeout(function(){
                $(parent).removeClass('animate__animated animate__fadeOutRight').addClass('d-none');
            }, 1000);
            setTimeout(function(){
                $(target).removeClass('d-none').addClass('animate__animated animate__fadeInLeft');
            }, 800);
            setTimeout(function(){
                $(target).removeClass('animate__animated animate__fadeInLeft');
            }, 1800);
        }
    }
}

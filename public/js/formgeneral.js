$('#keyword').on('focus focusin keyup change',function(e){
    var value = $(e.target).val();
    var kota = $('#question-1 #kota').val();
    var url = $('#url').val() + '/' + kota;
    if(!($(e.target).prop('readonly'))){
        $('#results').addClass('show');
        $('#results-header').text(`Loading...`);
        $('#results-body').html('');
        $.ajax({
            type : 'get',
            url : url,
            dataType: 'json',
            data: {'keyword': value},
            success: function(data){
                var total = data['total'];
                var total_display = data['total_display'];

                if(total_display == 1){
                    $('#id_rs').val(data['data'][0]['id_rs']);
                }else{
                    $('#id_rs').val('');
                }

                $('#results-header').text(`Menampilkan maksimal ${total_display} dari ${total} total hasil pencarian`)
                $('#results-body').html('');
                $.each(data['data'], function(index, item){
                    $('#results-body').append(`<div class="dropdown-item list-group-item" style="width:auto !important" data-idrs="${item['id_rs']}">${item['nama']}</div>`);
                })
                $('#results-body .dropdown-item[data-idrs]').on('click', function(e){
                    $('#id_rs').val($(e.target).attr('data-idrs'));
                    $('#keyword').val($(e.target).text());
                    $('#results').removeClass('show');
                });
            },
            error: function(){
                $('#results-header').text(`Terjadi kesalahan, silahkan coba beberapa saat lagi`);
                $('#results-body').html('');
            }
        });

        $('#question-1 #kota').on('change', function(){
            $('#id_rs, #keyword').val('');
            $('#results').removeClass('show');
        })
    }
});

$(document).click(function(e){
    var resultsHeader = $('#results-header');
    var keyword = $('#keyword');
    if( !(resultsHeader.is(e.target) || keyword.is(e.target)) ){
        $('#results').removeClass('show');
    }
})

var valueKota1 = $('[name=kota]:eq(0)').val() == null;
var valueKota2 = $('[name=kota]:eq(1)').val() == null;
console.log(valueKota1);
$('#keyword').prop('readonly', valueKota1);
$('#nama').prop('readonly', valueKota2);

$('[name=kota]:eq(0)').change(function(e) {
    var value = $(e.target).val() == null;
    $('#keyword').prop('readonly', value);
})
$('[name=kota]:eq(1)').change(function(e) {
    var value = $(e.target).val() == null;
    $('#nama').prop('readonly', value);
})

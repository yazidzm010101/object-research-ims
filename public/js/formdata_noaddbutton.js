url = $('input[name=url]').val();
bpjs_rs = $('input[name=bpjs_rs]').val() == 1 ? 'BPJS' : 'Non BPJS';

//   AJAX GRUP1
$('#grup_1 .list-group-item').click(function (e) {
    updateGrup2($(e.target), '#grup_2');
});

$('#back-btn').click(function(e) {
    e.preventDefault();
    $('#sidebar-wrapper').removeClass('toggled');
})

toggleRow('select[name*="melayani_tes"]');

$('select[name*=pengerjaan_lab]').change(function(e){
    toggleInhouse($(e.target));
})

$('select[name*="melayani_tes"]').change(function(e){
    toggleRow($(e.target));
})

$('input[name*=unit], input[name*=harga]').on('keyup', function(e){
    autoMultiply($(e.target));
})

function toggleBpjs(){
    if(bpjs_rs == 'BPJS'){
        $('input[name*="_bpjs_"]:not([name*="nilai"])').prop('readonly', false);
    }else{
        $('input[name*="_bpjs_"]').prop('readonly', true);
    }
}

function toggleInhouse(selector){
    var select = $(selector);
    $.each(select, function(index, item){
        var value = $(item).children("option:selected").val() == 0;
        var selectorSiblings = `input[name*=nama_lab], input[name*=tahun_kerjasama], select[name*=bpjs]`;
        var siblings = $(item).parent().parent().find(selectorSiblings);
        $(siblings).prop('readonly', value);
    })
}

function toggleRow(selector){
    var select = $(selector);
    $.each(select, function(index, item){
        var value = $(item).val() == 1 ;
        if(!value){
            $(item).parent().parent().not('.data-new').css('background-color','#ffffff');
            $(item).parent().parent().find('input, select').not('[name*="melayani_tes"]').attr('readonly', '');
        }else{
            $(item).parent().parent().not('.data-new').css('background-color','#e3f2fd');
            $(item).parent().parent().find('input, select').not('[name*="melayani_tes"], [name*="nilai"]').removeAttr('readonly');
            toggleInhouse($(item).parent().parent().find('select[name*=pengerjaan_lab]'));
            toggleBpjs();
        }
    });
}

function autoMultiply(selector){
    var className = $(selector).attr('class').split(' ').filter(function(className){
        return className.indexOf("bpjs") >= 0
    })[0];
    var unit = parseInt($(selector).parent().parent().find(`.${className}[name*="unit"]`).val());
    var harga = parseInt($(selector).parent().parent().find(`.${className}[name*="harga"]`).val());
    var result = parseInt(unit * harga);
    $(selector).parent().parent().find(`.${className}[name*="nilai"]`).val(result);
}

// change-in-production
function updateGrup2(grup1, grup2){
    var id_grup_1 = $(grup1).attr('data-grup1');
    $(grup2).find('.list-group').empty();
    $.ajax({
        url: 'http://localhost:8000/data/grup_2/' + id_grup_1,
        type: 'get',
        dataType: 'json',
        success: function (response) {
            // if the data requested was not null update
            if (response['data'].length > 0) {
                $('#sidebar-wrapper').addClass('toggled');
                // iterate throug response and append it to group 2 select input
                $.each(response['data'], function(index, value){
                    var id_grup_2 = value.id_grup_2;
                    var name = value.nama;
                    var list = `<a href="${url}?g1=${id_grup_1}&g2=${id_grup_2}" class="list-group-item list-group-item-action bg-light">${name}</a>`;
                    $(grup2).find('.list-group').append(list);
                });
            }else{
                updateGrup3(id_grup_1, 0);
            }
            // $(grup1).parent().find('.list-group-item').removeClass('active');
            // $(grup1).addClass('active');
        }
    });
}

function updateGrup3(id_grup_1, id_grup_2){
    window.location.replace(`${url}?g1=${id_grup_1}&g2=${id_grup_2}`);
}

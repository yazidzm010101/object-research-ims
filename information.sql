CREATE VIEW vw_grup_all AS
SELECT 
id_grup_all, 
tbl_grup_all.id_grup_1, tbl_grup_1.nama_grup_1,
tbl_grup_all.id_grup_2, tbl_grup_2.nama_grup_2, nama_lini,
tbl_grup_all.id_grup_3, tbl_grup_3.nama_grup_3, brand,
ukuran, tipe,
CASE tipe
    when '0' then 'Box'
    when '1' then 'Rol'
    when '2' then 'Pcs'
END as nama_tipe ,
tbl_grup_2.status_grup_2, tbl_grup_3.status_grup_3 from tbl_grup_all
INNER JOIN tbl_grup_1 ON tbl_grup_all.id_grup_1 = tbl_grup_1.id_grup_1
INNER JOIN tbl_grup_2 ON tbl_grup_all.id_grup_2 = tbl_grup_2.id_grup_2
INNER JOIN tbl_grup_3 ON tbl_grup_all.id_grup_3 = tbl_grup_3.id_grup_3;

CREATE VIEW vw_rawdata AS
SELECT 
tbl_main_audit.ketersediaan, vw_grup_all.id_grup_all, tbl_main_rs.tahun, tbl_main_audit.id_rs, nama_rs, alamat, kota, kelompok_kota,status_pengelolaan, kelas,
vw_grup_all.nama_grup_1, vw_grup_all.nama_lini, vw_grup_all.nama_grup_2, 
CONCAT (vw_grup_all.nama_grup_3 ,' ( ',vw_grup_all.brand,' )') as nama_grup_3, 
CONCAT (vw_grup_all.nama_grup_3 ,' ( ',vw_grup_all.brand,' ) ',' ',vw_grup_all.ukuran) as nama_grup_3_ukuran, vw_grup_all.brand, CASE vw_grup_all.tipe
    when '0' then 'Box'
    when '1' then 'Rol'
    when '2' then 'Pcs'
END as satuan, 
jumlah, harga,jumlah*harga as total,
jumlah_q1, harga_q1, jumlah_q1*harga_q1 as total_q1,
CONCAT (jumlah/jumlah_q1 ,' % ') as proporsi,
CASE vw_grup_all.status_grup_2
    when '0' then ''
    when '1' then 'NEW'
END as status_grup_2
,
CASE vw_grup_all.status_grup_3
    when '0' then ''
    when '1' then 'NEW'
END as status_produk
, CASE vw_grup_all.tipe
    when '0' then ''
    when '1' then 'NEW'
END as status_rs
from tbl_main_audit
INNER JOIN vw_grup_all ON tbl_main_audit.id_grup_all = vw_grup_all.id_grup_all
INNER JOIN tbl_main_rs ON tbl_main_audit.id_rs = tbl_main_rs.id_rs;

CREATE VIEW vw_log_edit AS
select tbl_log_edit.id_log_edit,
tbl_log_edit.id_rs AS id_rs,
id_rawdata,
nama_grup_2,
tbl_main_rs.nama_rs AS `nama`,
tbl_main_rs.tahun,
tbl_log_edit.lokasi,
tbl_log_edit.ip,
tbl_log_edit.updated_at AS `tanggal_akses` 
from tbl_log_edit
INNER JOIN tbl_main_rs ON tbl_log_edit.id_rs = tbl_main_rs.id_rs
INNER JOIN tbl_grup_2 ON tbl_grup_2.id_grup_2 = tbl_grup_2.id_grup_2;

CREATE VIEW vw_grup_1_All AS
SELECT 
tbl_grup_all.id_grup_all, 
tbl_grup_all.id_grup_1, tbl_grup_1.nama_grup_1 from tbl_grup_all
INNER JOIN tbl_grup_1 ON tbl_grup_all.id_grup_1 = tbl_grup_1.id_grup_1
ORDER BY tbl_grup_1.id_grup_1;

CREATE VIEW vw_grup_2_All AS
SELECT 
tbl_grup_all.id_grup_all, 
tbl_grup_all.id_grup_1, 
tbl_grup_all.id_grup_2, tbl_grup_2.nama_grup_2
from tbl_grup_all
INNER JOIN tbl_grup_2 ON tbl_grup_all.id_grup_2 = tbl_grup_2.id_grup_2
ORDER BY tbl_grup_all.id_grup_2;

CREATE VIEW vw_grup_fill_revenue AS
SELECT 
id_grup_all, 
tbl_grup_all.id_grup_3, tbl_grup_3.nama_grup_3,  CONCAT (tbl_grup_3.nama_grup_3 ,' (',brand,')') as nama_brand,
ukuran, tipe,
CASE tipe
    when '0' then 'Box'
    when '1' then 'Rol'
    when '2' then 'Pcs'
END as nama_tipe from tbl_grup_all
INNER JOIN tbl_grup_3 ON tbl_grup_all.id_grup_3 = tbl_grup_3.id_grup_3;

CREATE VIEW vw_last_progress AS
select id_rs, max(id_grup_2) as id_grup_2 from `tbl_log_edit` group by `id_rs`

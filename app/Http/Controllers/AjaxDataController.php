<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Audit;
use App\Models\Grup_2;
use App\Models\Grup_2_All;
use App\Models\Grup_3;
use App\Models\Grup;
use App\Models\RumahSakit;

class AjaxDataController extends Controller
{

    function liveSearch(Request $request)
    {
        $kota = [
            1 => 'Jakarta',
            14 => 'Bodetabek',
            2 => 'Bandung',
            3 => 'Surabaya',
            4 => 'Semarang',
            5 => 'Solo',
            6 => 'Yogya',
            7 => 'Sidoarjo',
            8 =>'Medan',
            9 => 'Pekanbaru',
            10 =>'Palembang',
            11 => 'Balikpapan',
            12 => 'Makassar',
            13 => 'Manado'
        ];
        $nama_kota = $kota[$request->kota] ?? FALSE;
        $keyword = $request->keyword;
        $results['total'] = 0;
        $results['data'] = [];
        if($nama_kota !== FALSE){
            $results['total'] = RumahSakit::selectRaw("id_rs, CONCAT(nama_rs, ' | ', COALESCE(`alamat`,' - ')) AS nama")
            ->whereRaw("LOWER(CONCAT(`nama_rs`, ' | ', COALESCE(`alamat`,' - '))) LIKE ? " , ['%'. strtolower($keyword).'%'])
            ->where('kota', '=', $nama_kota)
            ->count();

            $results['data'] = RumahSakit::selectRaw("id_rs, CONCAT(nama_rs, ' | ', COALESCE(`alamat`,' - ')) AS nama")
            ->whereRaw("LOWER(CONCAT(`nama_rs`, ' | ', COALESCE(`alamat`,' - '))) LIKE ? " , ['%'. strtolower($keyword).'%'])
            ->where('kota', '=', $nama_kota)->get();
        }
        echo json_encode($results);
        exit;
    }

    function getDataGrup2($id_grup_1=0,$keyid_rs=0)
    {
        if(base64_decode($keyid_rs, true)){
            $id_rs=base64_decode($keyid_rs)/config('global.key_crypt');
            $userData['data'] = Grup_2_All::selectRaw("nama_grup_2, id_grup_2 , tbl_main_audit.id_grup_all AS is_null")
            ->leftJoin('tbl_main_audit', function ($join) use ($id_rs) {
                $join->on('vw_grup_2_all.id_grup_all', '=', 'tbl_main_audit.id_grup_all')
                ->where('tbl_main_audit.id_rs', '=', $id_rs);
            })
            ->where('vw_grup_2_all.id_grup_1', '=', $id_grup_1)
            ->groupBy('vw_grup_2_all.id_grup_2')
            ->orderBy('vw_grup_2_all.id_grup_2')
            ->get();
            echo json_encode($userData);
            exit;
        }
    }
}

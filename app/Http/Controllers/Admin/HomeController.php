<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\RumahSakit;
use App\Models\Audit;
use App\Models\LastProgress;

use App\Exports\RawDataExport;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends BaseController
{
    public function index(Request $request)
    {
        if($keyword = $request->k){
            $data['keyword'] = $request->k;
            $data['dataRumahSakit'] = DB::table('vw_log_edit')
            ->where('nama','like',"%$keyword%")
            ->orWhere('tahun','like',"%$keyword%")
            ->orderByRaw('tanggal_akses DESC, tahun DESC')
            ->groupBy('id_rs')
            ->paginate(10);
        }else{
            $data['dataRumahSakit'] = DB::table('vw_log_edit')
            ->orderByRaw('tanggal_akses DESC, tahun DESC')
            ->groupBy('id_rs')
            ->paginate(10);
        }
        return view('pages.admin.dashboard.index', [
            'data' => $data,
        ]);
    }

    public function showReport(){
        $data['totalRevenuePerKota'] = Audit::join('tbl_main_rs', 'tbl_main_rs.id_rs', '=', 'tbl_main_audit.id_rs')
            ->groupBy('kota')->select('kota', DB::raw('count(*) as total'))->get();
        $data['totalPerKota'] = Audit::join('tbl_main_rs', 'tbl_main_rs.id_rs', '=', 'tbl_main_audit.id_rs')
            ->groupBy('kota')->select('kota', DB::raw('count(distinct(tbl_main_rs.id_rs)) as total'))->get();

        $data['totalMengisiProfil'] = LastProgress::where('id_grup_2','=','0')->count();
        $data['totalMengisiAudit'] = Audit::distinct('id_rs')->count();

        $data['totalRS'] = RumahSakit::count();
        $data['totalRSCeil'] = ceil($data['totalRS'] / 50) * 50;
        $data['totalRSBaru'] = RumahSakit::where('status', 1)->count();
        $data['totalRSLama'] = RumahSakit::where('status', 0)->count();
        return view('pages.admin.report.index', [
            'data' => $data
        ]);
    }

    public function exportAllRevenue(Request $request)
    {
        $tahun = $request->tahun;
        return Excel::download(new RawDataExport($tahun, 0),
            "Export Raw Data BSN - {$tahun}.xlsx"
        );
    }
}

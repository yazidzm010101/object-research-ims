<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

use App\Models\RumahSakit;
use App\Models\Responden;
use App\Models\Kuesioner;
use App\Models\Foto;

use App\Exports\RawDataExport;
use Maatwebsite\Excel\Facades\Excel;

class RumahsakitController extends BaseController
{
    protected $rs;

    function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->initializeRumahsakit($request->id_rs, $request->tahun);
        $this->initializeKey();
        $this->initializeNav();
        $this->initializeSubNav();
    }

    public function index(Request $request)
    {
        // GET RUMAHSAKIT
        $id_rs = $this->rs->id_rs;
        $data['rs'] = $this->rs;
        $data['key'] = $this->key;

        // GET RESPONDEN & KUESIONER
        $data['responden'] = Responden::where('id_rs', $id_rs)->first();
        $data['kuesioner'] = Kuesioner::where('id_rs', $id_rs)->first();

        // GET FOTO
        $data['foto_rs'] = Foto::where('id_rs', $id_rs)
        ->where('jenis', 2)
        ->get();
        //
        $data['foto_latest'] = Foto::where('id_rs', $id_rs)
        ->where('jenis', 2)
        ->latest()
        ->limit(3)
        ->get();
        //
        $data['foto_responden'] = Foto::where('id_rs', $id_rs)
        ->where('jenis', 1)
        ->first();
        //
        return view('pages.admin.rumahsakit.index', [
            'data' => $data
        ]);
    }

    function showRevenue()
    {
        // GET RUMAHSAKIT
        $id_rs = $this->rs->id_rs;
        $data['rs'] = $this->rs;
        $data['key'] = $this->key;
        $data['audit']= DB::table('vw_rawdata')
        ->select('id_grup_all','ketersediaan','brand as nama_manufacture','nama_grup_3 as nama_brand','ukuran','satuan_tipe as nama_tipe','jumlah','satuan','harga','total','jumlah_q1','satuan_q1','harga_q1','total_q1','jumlah_q2','satuan_q2','harga_q2','total_q2','status_produk')
        ->where('tahun', $data['rs']->tahun)
        ->where('id_rs', $data['rs']->id_rs)
        ->orderBy('id_grup_all')
        ->paginate(10);
        //
        return view('pages.admin.detail.index', [
            'data' => $data
        ]);
    }

    function showLog()
    {
        $data['rs'] = $this->rs;
        $data['log'] = DB::table('vw_log_edit')
                        ->where('tahun',$data['rs']->tahun)
                        ->where('id_rs',$data['rs']->id_rs)
                        ->orderByRaw('tahun DESC, tanggal_akses DESC')
                        ->paginate(10);
        //
        return view('pages.admin.log.index', [
            'data' => $data
        ]);
    }

    function exportRevenue()
    {
        $id_rs = $this->rs->id_rs;
        $tahun = $this->rs->tahun;
        $nama = $this->rs->nama_rs;
        $kota = $this->rs->kota;
        //
        return Excel::download(
            new RawDataExport($tahun, $id_rs),
            "Export Raw Data BSN - {$tahun}_{$kota}_{$nama}.xlsx"
        );
    }

    public function initializeRumahsakit($id_rs, $tahun)
    {
        $rs = RumahSakit::where('id_rs',$id_rs)
        ->where('tahun', $tahun)->first();
        if($rs){
            $this->rs = $rs;
            return;
        }
        return abort(404);

    }

    public function initializeSubNav()
    {
        $rs = $this->rs;
        $base_url = "dashboard/rs/{$rs->tahun}/{$rs->id_rs}";
        $subnav = [
            'head' => [
                'text' => $rs->nama_rs,
                'url' => $base_url,
            ],
            'body'=> [
                [
                    'text' => 'Revenue',
                    'url' => "{$base_url}/revenue",
                ],
                [
                    'text' => 'Log',
                    'url' => "{$base_url}/log",
                ]
            ]
        ];
        View::share('subnav', $subnav);
    }

    public function initializeKey()
    {
        // ENCRYPTION KEY
        $key = config('global.key_crypt');

        $enc_key['keytahun'] = $this->rs->tahun * $key;
        $enc_key['keyrs'] = base64_encode($this->rs->id_rs * $key);

        $this->key = $enc_key;
    }
}

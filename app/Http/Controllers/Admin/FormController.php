<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\RumahSakit;
use App\Models\Audit;
use App\Models\LastProgress;

class FormController extends BaseController
{
    function index()
    {
        $data['form'] = Form::paginate(6);
        return view('pages.admin.form.index', [
            'data' => $data
        ]);
    }

    function create(Request $request)
    {
        $tahun = $request->tahun;
        $status = $request->status;
        //
        $key = config('global.key_crypt');
        $keytahun = $tahun * $key;
        //
        $form = Form::where('tahun', $request->tahun)->count();
        if($form <= 0){
            $form = Form::create([
                'tahun' => $tahun,
                'status' => $status,
                'link' => url("/form/{$keytahun}"),
            ]);
            return redirect()->route('adminFormIndex')->with(['success' => "Formulir pengisian {$tahun} berhasil dibuat"]);
        }
        return redirect()->route('adminFormIndex')->with(['error' => "Formulir pengisian {$tahun} sudah ada!"]);
    }

    function update(Request $request)
    {
        $form = Form::where('tahun', $request->tahun)->first();
        $form->update([
            'status' => $request->status
        ]);
        //
        $tahun = $form->tahun ;
        $status = ($request->status == 1) ? 'aktif' : 'nonaktif';
        //
        return redirect()->route('adminFormIndex')->with(['success' => "Formulir pengisian {$tahun} berhasil di{$status}kan"]);
    }


    function destroy(Request $request)
    {
        $form = Form::where('tahun', $request->tahun);
        $form->delete();
        return redirect()->route('adminFormIndex')->with(['success' => 'Data berhasil dihapus']);
    }
}

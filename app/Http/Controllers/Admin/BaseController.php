<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->initializeNav();
        $this->initializeSubNav();
    }

    public function initializeNav()
    {
        $nav = [
            [
                'text' => 'Daftar Rumah Sakit',
                'url' => 'dashboard',
                'icon' => 'local_hospital'
            ],
            [
                'text' => 'Perolehan',
                'url' => 'dashboard/report',
                'icon' => 'analytics'
            ],
            [
                'text' => 'Formulir Pengisian',
                'url' => 'dashboard/form',
                'icon' => 'assignment'
            ],
            [
                'text' => 'Pengaturan',
                'url' => 'dashboard/settings',
                'icon' => 'settings'
            ]
        ];
        View::share('nav', $nav);
    }

    public function initializeSubNav()
    {

    }
}

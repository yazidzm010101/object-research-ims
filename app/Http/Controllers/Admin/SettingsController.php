<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends BaseController
{
    function index()
    {
        $data['register'] = DB::table('tbl_config')->select('*')->where('id_config', 1)->first();
        $data['validate'] = DB::table('tbl_config')->select('*')->where('id_config', 2)->first();
        return view('pages.admin.settings.index', [
            'data' => $data
        ]);
    }

    function update(Request $request)
    {
        $registerConfig = DB::table('tbl_config')->where('id_config', 1);
        $validateConfig = DB::table('tbl_config')->where('id_config', 2);
        $registerConfig->update([
            'status' => $request->register_status
        ]);
        $validateConfig->update([
            'status' => $request->validate_status
        ]);
        return redirect()->route('adminSettingsIndex')->with(['success' => 'Pengaturan berhasil disimpan']);
    }
}

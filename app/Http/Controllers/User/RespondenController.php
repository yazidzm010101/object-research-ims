<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Models\Responden;
use App\Models\Foto;
use Exception;

class RespondenController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['key'] = $this->key;
        //
        $data['responden'] = Responden::
        where('id_rs', $this->rs->id_rs)
        ->first();
        //
        $data['foto'] = Foto::
        where('id_rs', $this->rs->id_rs)
        ->where('jenis', 1)
        ->first();
        //
        return view('pages.user.respondent.index', [
            'data' => $data,
        ]);
    }

    public function updateOrCreate(Request $request)
    {
        $this->createLog(request()->ip(), $this->rs->id_rs, 0);

        Responden::updateOrCreate(
            [
                'id_rs'     => $this->rs->id_rs,
            ],
            [
                'nama'      => $request->nama ?? NULL,
                'jabatan'   => $request->jabatan ?? NULL,
                'bagian'    => $request->bagian ?? NULL,
                'telp'      => $request->telp ?? NULL,
                'hp'        => $request->hp ?? NULL
            ]
        );
        return redirect()->route('userHome', $this->key)->with(['success' => "Data responden berhasil diperbarui!"]);
    }

    public function uploadPhoto(Request $request)
    {
        if($request->hasFile('foto'))
        {
            try
            {
                $filename = $this->storePhoto($request->file('foto'), [
                    'rectangle' => TRUE
                ]);

                $this->createLog(request()->ip(), $this->rs->id_rs, 0);

                Foto::updateOrCreate(
                    [
                        'id_rs'     => $this->rs->id_rs,
                    ],
                    [
                        'jenis'     => '1',
                        'foto'      => $filename
                    ]
                );
                return response()->json(['photo' => url("storage/photo/${filename}")]);
            }
            catch(Exception $e)
            {
                return response()->json(['message' => $e->getMessage()], 400);
            }
        }
        return response()->json(['message' => 'Tidak ada foto yang diupload'], 400);
        //
    }
}

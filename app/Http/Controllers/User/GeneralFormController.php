<?php

namespace App\Http\Controllers\User;

use App\Models\Kuesioner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\RumahSakit;
use App\Models\Responden;
use App\Models\LogEdit;
use Validator;


class GeneralFormController extends BaseController
{
    protected $form;
    protected $options;

    function __construct(Request $request)
    {
        $this->initializeForm($request->keytahun);
        $this->initializeOptions();
    }

    function index()
    {
        $key = config('global.key_crypt');

        $data['kota'] = $this->options['kota'];
        $data['form'] = $this->form;
        $data['keytahun'] = $this->form->tahun * $key;

        return view('pages.user.generalform.index', [
            'data' => $data
        ]);
    }

    function index_alt()
    {
        $key = config('global.key_crypt');

        $data['kota'] = $this->options['kota'];
        $data['form'] = $this->form;
        $data['keytahun'] = $this->form->tahun * $key;

        return view('pages.user.generalform.create', [
            'data' => $data
        ]);
    }

    function update(Request $request)
    {
        // VALIDATE FIRST
        $validator = Validator::make($request->all(), [
            'kota' => 'required',
            'id_rs' => 'required'
        ]);
        //
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Mohon data di cek kembali!');
        }

        // SEARCH THE DATA
        if ($request->id_rs) {
            $dataRumahSakit = RumahSakit::where('id_rs', $request->id_rs)->first();
            if($dataRumahSakit){
                $validateConfig = $this->validateConfig($dataRumahSakit->id_rs);
                if($validateConfig){
                    $key = $this->encryptUrl($this->form->tahun, $dataRumahSakit->id_rs);
                    return redirect()->route('userHome', $key);
                }
                return redirect()->back()->with('error', 'Data rumah sakit ini sudah diisi!, silahkan akses melalui link yang diunduh sebelumnya');
            }
        }
        //
        return redirect()->back()->with('error', 'Mohon data di cek kembali!');

    }

    function validateConfig($id_rs)
    {
        $config = DB::table('tbl_config')->where('id_config', 2)->first();

        if($config->status == 1){
            $tbl_log_edit = LogEdit::where('id_rs', $id_rs)->count();
            if($tbl_log_edit == 0){
                return TRUE;
            }
            return FALSE;
        }
        return TRUE;
    }

    function create(Request $request)
    {
        // FIELD VALIDATION
        //
        $validator = Validator::make($request->all(), [
            'kota' => 'required',
            'nama_rs' => 'required'
        ]);
        //
        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Mohon data di cek kembali.');
        }

        // CHECK FIRST IF THE DATA WAS EXIST
        $dataRumahSakitExist = RumahSakit::where('kota', $this->options['kota'][$request->kota])
        ->where('nama_rs', $request->nama_rs)
        ->whereNull('alamat')
        ->first();
        
        if($dataRumahSakitExist) {
            return redirect()->back()->with('error', "<span><span class='white-text'>{$request->nama_rs} | {$this->options['kota'][$request->kota]}</span> sudah terdaftar,<br>harap masuk melalui halaman pencarian<br>atau melalui link yang sudah disimpan sebelumnya!</span>");
        }

        // DETERMINE NEW ID_RAWDATA
        //
        //SELECT * FROM `tbl_main_rs` WHERE id_rawdata >= 1400 AND id_rawdata <= 1499
        $dataRumahSakitLast = RumahSakit::
        select('id_rawdata')
        ->where('id_rawdata', '>', $request->kota * 100)
        ->where('id_rawdata', '<=', ($request->kota * 100) + 99)
        ->orderBy('id_rawdata', 'DESC')
        ->first();
        //
        $id_rawdata = min(($dataRumahSakitLast->id_rawdata ?? 0) + 1, ($request->kota * 100) + 99);


        // CREATE NEW DATA
        //
        $dataRumahSakit = RumahSakit::create([
            'id_rawdata' => $id_rawdata,
            'nama_rs' => $request->nama_rs,
            'kota' => $this->options['kota'][$request->kota],
            'kelompok_kota' => $this->options['kelompok_kota'][$request->kota],
            'tahun' => $this->form['tahun'],
            'status' => 1
        ]);
        //
        if($dataRumahSakit){
            $key = $this->encryptUrl($this->form->tahun, $dataRumahSakit->id);
            return redirect()->route('userHome', $key);
        }
        //
        return redirect()->back()->with('error', 'Mohon data di cek kembali.');
    }

    function useValidation(){}
}

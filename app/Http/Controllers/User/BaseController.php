<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\RumahSakit;
use App\Models\Form;
use App\Models\LogEdit;

use Exception;
use Image;

class BaseController extends Controller
{
    protected $rs;
    protected $form;
    protected $key;
    protected $options;

    public function __construct(Request $request)
    {
        $this->initializeForm($request->keytahun);
        $this->initializeHospital($request->keyrs, $this->form->tahun);
        $this->initializeOptions();

        $this->key = ['keytahun' => $request->keytahun, 'keyrs' => $request->keyrs];
    }

    public function initializeForm($keytahun)
    {
        // ENCRYPTION KEY
        $key = config('global.key_crypt');
        $is_real = $keytahun > 0;
        $is_dividable = ($keytahun % $key) == 0;

        // FORM VALIDATION HERE
        if($is_real && $is_dividable)
        {
            $tahun = $keytahun / $key;
            $form = Form::select('*')->where('tahun', $tahun)->first();
            if($form->status ?? FALSE){
                $this->form = $form;
                return;
            }
        }
        return abort(404);
    }

    public function initializeHospital($keyrs, $tahun)
    {
        // ENCRYPTION KEY
        $key = config('global.key_crypt');

        // HOSPITAL VALIDATION HERE

        $id_rs = base64_decode($keyrs) ?? FALSE;
        if($id_rs && is_numeric($id_rs))
        {
            $is_real = $id_rs > 0;
            $is_dividable = ($id_rs % $key) == 0;
            if($is_real && $is_dividable)
            {
                $id_rs = $id_rs / $key;
                $rs = RumahSakit::where('id_rs', $id_rs)
                ->where('tahun', $tahun)
                ->first();
                if($rs){
                    $this->rs = $rs;
                    return;
                }
            }
        }
        return abort(404);
    }

    public function encryptUrl($tahun, $id_rs)
    {
        // ENCRYPTION KEY
        $key = config('global.key_crypt');

        $enc_key['keytahun'] = $tahun * $key;
        $enc_key['keyrs'] = base64_encode($id_rs * $key);

        return $enc_key;
    }

    public function initializeOptions()
    {
        $options['kelas'] = [
            1 => 'A',
            2 => 'B',
            3 => 'C',
            4 => 'D'
        ];
        $options['jenis'] = [
            1 => 'Pemerintah',
            2 => 'Swasta'
        ];
        $options['kota']  = [
            1 => 'Jakarta',
            14 => 'Bodetabek',
            2 => 'Bandung',
            3 => 'Surabaya',
            4 => 'Semarang',
            5 => 'Solo',
            6 => 'Yogya',
            7 => 'Sidoarjo',
            8 =>'Medan',
            9 => 'Pekanbaru',
            10 =>'Palembang',
            11 => 'Balikpapan',
            12 => 'Makassar',
            13 => 'Manado'
        ];
        $options['kelompok_kota'] = $this->mergeColumn($options['kota'], [[12, 13], [8, 10, 9], [4, 5, 6], [3, 7]]);
        $options['kelompok_kota'][1] = 'Jabodetabek';
        $options['kelompok_kota'][14] = 'Jabodetabek';

        $this->options = $options;
    }

    public function mergeColumn($columns, $keys_group = [[]])
    {
        $columns_merged = $columns;
        foreach($keys_group as $keys)
        {
            //assign
            $columns_group = [];
            foreach($keys as $key)
            {
                array_push($columns_group, $columns[$key]);
            }

            //implode as astring & reassign
            $value = implode('+', $columns_group);
            $columns_group = array_fill_keys($keys, $value);
            $columns_merged = array_replace($columns_merged, $columns_group);
        }
        return $columns_merged;
    }

    public function storePhoto($file, $options = [])
    {
        try
        {
            // configuration
            $img_valid = $options['extension'] ?? ['jpg', 'jpeg', 'png'];
            $img_rect = $options['rectangle'] ?? FALSE;
            $img_fit = $options['fit'] ?? FALSE;
            $img_blur = $options['blur'] ?? FALSE;
            $img_dir = $options['directory'] ?? 'storage/photo';
            $img_prefix = $options['prefix'] ?? '';

            // preprocess the file
            $file_name = ($img_prefix != '' ? $img_prefix.'_' : '') .
                         time() . "_" . $file->getClientOriginalName();
            $file_ext = $file->getClientOriginalExtension();

            // validate if extension is valid
            if (!in_array(strtolower($file_ext), $img_valid)) {
                throw new Exception("File extension doesn't valid");
            }

            // create the image
            $img = Image::make($file->getRealPath());

            // crop image to rectangle
            if($img_rect) {
                $img_width = Image::make($file->getRealPath())->width();
                $img_height = Image::make($file->getRealPath())->height();
                $img->fit(min($img_width, $img_height));
            }else if($img_fit) {
                $img->fit($img_fit[0], $img_fit[1]);
            }
            if($img_blur) {
                $img->blur($img_blur);
            }

            $img->save($img_dir.'/'.$file_name);

            return $file_name;

        }catch(Exception $e){
            throw $e;
        }
    }

    public function createLog($ip, $id_rs, $id_grup_2 = 0)
    {
        $dataLocation = \Location::get($ip);
        $location = $dataLocation->cityName ?? '';
        LogEdit::create([
            'id_rs' => $id_rs,
            'lokasi' => $location,
            'id_grup_2' => $id_grup_2,
            'ip' => $ip
        ]);
    }
}

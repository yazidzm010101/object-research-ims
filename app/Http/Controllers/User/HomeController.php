<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\DB;
use App\Models\Responden;
use App\Models\Kuesioner;
use App\Models\Foto;
use Response;


class HomeController extends BaseController
{
    public function index()
    {
        $data['rs'] = $this->rs;

        // Badge
        $data['badge']['responden'] = $this->percentage(
            Responden::
            select(['nama', 'jabatan', 'bagian', 'telp', 'hp', 'foto'])
            ->where('tbl_quest_responden.id_rs', $this->rs->id_rs)
            ->leftJoin('tbl_quest_foto', function ($join) {
                $join->on('tbl_quest_foto.id_rs', '=', 'tbl_quest_responden.id_rs')
                        ->where('tbl_quest_foto.jenis', '=', 1);
            })->first() ??
            Foto::leftJoin('tbl_quest_responden', 'tbl_quest_responden.id_rs', '=', 'tbl_quest_foto.id_rs')
            ->select(['nama', 'jabatan', 'bagian', 'telp', 'hp', 'foto'])
            ->where('tbl_quest_foto.id_rs', $this->rs->id_rs)
            ->where('tbl_quest_foto.jenis', '=', 1)
            ->first()
        );
        //
        $data['badge']['kuesioner'] = $this->percentage(
            Kuesioner::where('id_rs', $this->rs->id_rs)->first()
        );
        //
        $data['badge']['dokumentasi'] = Foto::where('id_rs', $this->rs->id_rs)
        ->where('jenis', 2)->count();
        //
        $data['badge']['revenue'] = DB::table('vw_log_edit')->where('id_rs', $this->rs->id_rs)
        ->whereNotNull('nama_grup_2')->count();

        return view('pages.user.dashboard.index', [
            'data' => $data
        ]);
    }

    function percentage($data)
    {
        if($data){
            $data = $data->toArray();
            $length = count($data);
            $notNull = 0;
            foreach(array_keys($data) as $key){
                if($data[$key] != ''){
                    $notNull += 1;
                }
            }
            return (round(($notNull/$length) * 100));
        }
        return 0;
    }

    function getURL()
    {
        $url = url("rs/{$this->key['keytahun']}/{$this->key['keyrs']}");
        $nama = $this->rs->nama_rs;
        $alamat = $this->rs->alamat;
        $kota = $this->rs->kota;
        $content = "Link Kuesioner {$nama} \n {$alamat} \n $kota \n (Silahkan salin URL ini pada browser anda) \n $url";

        $fileName = "Link - {$kota}_{$nama}.txt";
        $headers = [
            'Content-type' => 'text/plain',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        ];
        return Response::make($content, 200, $headers);
    }
}

<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Audit;
use App\Models\Grup_1;
use App\Models\Grup_2;
use App\Models\Grup_3;
use App\Models\Grup_1_All;
use App\Models\Grup_2_All;
use App\Models\LogEdit;
use App\Models\Grup;
use App\Models\GrupAll;
use App\Models\GrupFillingRev;
use Exception;
use Validator;

class RevenueController extends BaseController
{
    function index(Request $request)
    {
        // VARIABEL PENTING
        $data['keytahun'] = $this->key['keytahun'];
        $data['keyrs'] = $this->key['keyrs'];
        $data['rs'] = $this->rs;
        $data['id_grup_1'] = $request->g1 ?? FALSE;
        $data['id_grup_2'] = $request->g2 ?? FALSE;

        // GET DATA GRUP 1
        $data['grup1'] = Grup_1_All::selectRaw('nama_grup_1, id_grup_1, tbl_main_audit.id_grup_all AS is_null')
        ->leftJoin('tbl_main_audit', function ($join) {
            $join->on('vw_grup_1_all.id_grup_all', '=', 'tbl_main_audit.id_grup_all')
                 ->where('tbl_main_audit.id_rs', '=', $this->rs->id_rs);
        })
        ->groupBy('vw_grup_1_all.id_grup_1')
        ->orderBy('vw_grup_1_all.id_grup_1')
        ->get();

        // GET DATA GRUP 2
        $data['grup2'] = Grup_2_All::selectRaw("nama_grup_2, id_grup_2 , tbl_main_audit.id_grup_all AS is_null")
        ->leftJoin('tbl_main_audit', function ($join) {
            $join->on('vw_grup_2_all.id_grup_all', '=', 'tbl_main_audit.id_grup_all')
            ->where('tbl_main_audit.id_rs', '=', $this->rs->id_rs);
        })
        ->where('vw_grup_2_all.id_grup_1', '=', $data['id_grup_1'])
        ->groupBy('vw_grup_2_all.id_grup_2')
        ->orderBy('vw_grup_2_all.id_grup_2')
        ->get();

        // GET DATA GRUP 3 FROM GRUP 2
        $data['grup3'] = Grup::select("id_grup_3")
        ->where('vw_grup_all.id_grup_2', '=', $data['id_grup_2'])
        ->distinct()
        ->get();


        // JIKA ADA DATA PADA GRUP 2
        if($data['grup2'])
        {
            // SELECT DATA DARI GRUP 3 LEFT JOIN AUDIT
            $data['grup_1_aktif'] = Grup_1_All::where('id_grup_1', $data['id_grup_1'])->first();
            $data['grup_2_aktif'] = Grup_2_All::where('id_grup_2', $data['id_grup_2'])->first();
            $data['data_lab'] = GrupFillingRev::
            select([
                'vw_grup_fill_revenue.id_grup_all as id_grup_all_vw'       ,
                'vw_grup_fill_revenue.id_grup_3'       ,
                'vw_grup_fill_revenue.nama_manufacture'       ,
                'vw_grup_fill_revenue.nama_brand'       ,
                'vw_grup_fill_revenue.ukuran'       ,
                'vw_grup_fill_revenue.nama_tipe'       ,
                'tbl_main_audit.id_rs'           ,
                'tbl_main_audit.ketersediaan'           ,
                'tbl_main_audit.id_grup_all'         ,
                'tbl_main_audit.jumlah'        ,
                'tbl_main_audit.satuan'            ,
                'tbl_main_audit.harga'            ,
                'tbl_main_audit.total'            ,
                'tbl_main_audit.jumlah_q1'        ,
                'tbl_main_audit.satuan_q1'            ,
                'tbl_main_audit.harga_q1'            ,
                'tbl_main_audit.total_q1'            ,
                'tbl_main_audit.jumlah_q2'        ,
                'tbl_main_audit.satuan_q2'            ,
                'tbl_main_audit.harga_q2'            ,
                'tbl_main_audit.total_q2'            ,
            ])->
            leftJoin('tbl_main_audit', function ($join) {
                $join->on('tbl_main_audit.id_grup_All', '=', 'vw_grup_fill_revenue.id_grup_All')
                     ->where('tbl_main_audit.id_rs', '=', $this->rs->id_rs);
            })
            ->whereIn('vw_grup_fill_revenue.id_grup_3', $data['grup3'])
            ->orderBy('vw_grup_fill_revenue.id_grup_all')
            ->paginate(30)
            ->appends(request()->query());
        }

        return view('pages.user.revenue.index', [
            'data' => $data,
            'rs' => $this->rs,
            'key' => $this->key
        ]);
    }

    function updateOrCreate(Request $request)
    {
        //VALIDATOR FIRST
        $validator = Validator::make($request->all(), [
            'row' => 'required',
            'g1' => 'required',
            'g2' => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->with('error', 'Gunakan tombol save apabila telah melakukan perubahan / penambahan minimal 1 data. Untuk penambahan data lain - lain, silahkan pilih golongan grup terlebih dahulu dengan cara memilih grup yang tersedia dibagian sisi kiri.');
        }

        // COUNTER
        $count_create = 0;
        $count_update = 0;
        $count_failed = 0;

        // UPDATE LOG
        // change-in-production
        $ip = $request->ip();
        $dataLocation = \Location::get($ip);
        $location = $dataLocation->cityName ?? '';
        $dataLog = LogEdit::create([
            'id_rs' => $this->rs->id_rs,
            'lokasi' => $location,
            'id_grup_2' => $request->g2,
            'ip' => $ip
        ]);

        // ENUMERATE SETIAP ARRAY REQUEST
        foreach ($request->row as $id => $item)
        {
            $count = $this->auditHandler($item, [
                'id_rs'     => $this->rs->id_rs,
                'id_log'    => $dataLog->id,
                'id_grup_1' => $request->g1,
                'id_grup_2' => $request->g2
            ]);
            $count_create += $count[0];
            $count_update += $count[1];
            $count_failed += $count[2];
        }

        // GENERATE PESAN
        $message =  ($count_create > 0 ? "{$count_create} data berhasil ditambahkan ." : "").
                    ($count_update > 0 ? "{$count_update} data berhasil diperbarui ." : "").
                    ($count_failed > 0 ? "{$count_failed} data tidak valid ." : "");

        // REDIRECT KEMBALI
        return redirect()->back()->with(['info' => $message]);
    }

    function auditHandler($item, $data)
    {
        // VARIABEL PENTING
        $id_grup_all = isset($item['id_grup_all']) ? $item['id_grup_all'] : NULL;
        $ketersediaan = isset($item['ketersediaan']) ? $item['ketersediaan'] : 0;

        // JIKA MELAYANI TES 1 DAN PADA TERDAPAT NAMA DESKRIPSI PADA ARRAY
        // MAKA KITA ANGGAP SEBAGAI DESKRIPSI TES LAIN-LAIN

        if( ($ketersediaan == 1) && isset($item['nama_grup_3']) )
        {
            $grup_3_baru = Grup_3::create(
            [
                'nama_grup_3'          => $item['nama_grup_3'],
                'brand'                => $item['brand'],
                'status'               => 1
            ]);
            $grup_all_baru = GrupAll::create(
            [
                'id_grup_1'             => $data['id_grup_1'],
                'id_grup_2'             => $data['id_grup_2'],
                'id_grup_3'             => $grup_3_baru->id,
                'ukuran'                => $item['ukuran'],
                'tipe'                  => $item['tipe'],
                'status'                => 1
            ]);
            $id_grup_all = $grup_all_baru->id;
        }
        // JIKA TIDAK CEK KEMBALI JIKA ID DESKRIPSI TES TIDAK KOSONG

        if($id_grup_all != NULL)
        {
            // CEK DATA TERLEBIH DAHULU SUDAH EXIST PADA AUDIT ATAU BELUM
            $getAudit = Audit::select('id_grup_all')
            ->where('id_rs', $data['id_rs'])
            ->where('id_grup_all', $id_grup_all)
            ->count();

            $jumlah = $item['jumlah'] ?? 0;
            $harga = $item['harga'] ?? 0;
            $total = $jumlah * $harga;
            $satuan = $item['satuan'] ?? NULL;

            $jumlah_q1 = $item['jumlah_q1'] ?? 0;
            $harga_q1 = $item['harga_q1'] ?? 0;
            $total_q1 = $jumlah_q1 * $harga_q1;
            $satuan_q1 = $item['satuan_q1'] ?? NULL;

            $jumlah_q2 = $item['jumlah_q2'] ?? 0;
            $harga_q2 = $item['harga_q2'] ?? 0;
            $total_q2 = $jumlah_q1 * $harga_q2;
            $satuan_q2 = $item['satuan_q2'] ?? NULL;

            // DATA DIANGGAP VALID APABILA SETIAP HARGA MEMILIKI SETIDAKNYA SATU VALUE
            if( ($jumlah != 0) || ($jumlah_q1 != 0) || ($jumlah_q2 != 0) )
            {
                // INISIALISASI FORM
                $form = [
                    'id_rs'             => $data['id_rs'],
                    'id_grup_all'         => $id_grup_all,

                    'ketersediaan'      => $ketersediaan,

                    'jumlah'    => $jumlah,
                    'harga'   => $harga,
                    'total' => $total,
                    'satuan' => $satuan,

                    'jumlah_q1'    => $jumlah_q1 ,
                    'harga_q1'   => $harga_q1,
                    'total_q1' => $total_q1,
                    'satuan_q1' => $satuan_q1,

                    'jumlah_q2'    => $jumlah_q2,
                    'harga_q2'   => $harga_q2,
                    'total_q2' => $total_q2,
                    'satuan_q2' => $satuan_q2,

                    'id_log_edit'       => $data['id_log']
                ];

                // TRY CATCH DI BAGIAN INI
                // JANGAN SAMPAI KARENA SATU DATA TIDAK VALID MENGINTERUPSI KESELURUHAN INPUT
                // BESAR KEMUNGKINAN TERJADI EKSEPSI DARI USER YANG MENGINPUT DATA TIDAK VALID
                try
                {
                    // JIKA GET AUDIT MENYIMPAN DEKSRIPSI TES(SUDAH ADA DATA DI AUDIT)
                    // MAKA UPDATE AUDIT TERSBUT
                    if( $getAudit > 0 )
                    {
                        Audit::
                        where('id_rs', $data['id_rs'])
                        ->where('id_grup_all', $id_grup_all)
                        ->update($form);
                        return [0, 1, 0];
                    }
                    // JIKA TIDAK MAKA BUAT AUDIT BARU
                    Audit::create($form);
                    return [1, 0, 0];
                }
                // JIKA TERJADI KESALAHAN
                catch(Exception $e)
                {
                    // echo($e->getMessage());
                    return [0, 0, 1];
                }
            }
        }
        // RETURN 0 JIKA TIDAK MEMENUHI SEMUA KONDISI YANG TERJADI
        return [0, 0, 0];
    }
}

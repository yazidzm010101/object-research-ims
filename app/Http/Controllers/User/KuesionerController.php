<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Models\Kuesioner;
use App\Models\RumahSakit;

class KuesionerController extends BaseController
{
    public function index()
    {
        $data['key'] = $this->key;
        $data['rs'] = $this->rs;
        $data['kuesioner'] = Kuesioner::where('id_rs', $this->rs->id_rs)->first();
        $data['kota'] = $this->options['kota'];
        $data['jenis'] = $this->options['jenis'];
        $data['kelas'] = $this->options['kelas'];
        return view('pages.user.questionnaire.index', [
            'data' => $data,
        ]);
    }

    public function updateOrCreate(Request $request)
    {
        $sign_value = ($request->kondisi ?? 1) == 1 ? 1 : -1;

        $this->createLog(request()->ip(), $this->rs->id_rs, 0);

        Kuesioner::updateOrCreate(
            [
                'id_rs' => $this->rs->id_rs,
            ],
            [
                'interviewer'   => $request->interviewer ?? NULL,
                'tanggal'       => date('Y-m-d', strtotime($request->tanggal ?? NULL)),
                'waktu_mulai'   => $request->waktu_mulai ?? NULL,
                'waktu_selesai' => $request->waktu_selesai ?? NULL,
                'tahun_berdiri' => $request->tahun_berdiri ?? NULL,
                'luas'          => $request->luas ?? NULL,
                'frekuensi'     => $request->frekuensi ?? NULL,
                'program'       => $request->program ?? NULL,
                'pendapatan'    => $sign_value * ($request->pendapatan ?? 0),
            ]
        );
        RumahSakit::where('id_rs', $this->rs->id_rs)
        ->update([
            'alamat' => $request->alamat,
            'kelas' => $request->kelas,
            'status_pengelolaan' => $request->jenis
        ]);

        return redirect(route('userHome', $this->key))->with(['success' => "Data kuesioner berhasil diperbarui!"]);;
    }
}

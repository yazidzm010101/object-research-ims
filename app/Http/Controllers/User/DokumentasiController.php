<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Models\Foto;
use Exception;

class DokumentasiController extends BaseController
{
    public function index()
    {
        $data['key'] = $this->key;
        //
        $data['foto'] = Foto::where('id_rs', $this->rs['id_rs'])
        ->where('jenis', 2)
        ->get();
        //
        return view('pages.user.documentation.index', [
            'data'=> $data,
        ]);
    }


    public function create(Request $request)
    {
        if($request->hasFile('foto'))
        {
            try
            {
                $filename = $this->storePhoto($request->file('foto'), [
                    'fit' => [640, 360]
                ]);

                $this->createLog(request()->ip(), $this->rs['id_rs'], 0);

                if($filename){
                    $foto = Foto::create(
                        [
                            'id_rs'     => $this->rs['id_rs'],
                            'jenis'     => '2',
                            'foto'      => $filename
                        ]
                    );
                    return response()->json(
                        [
                            'photo' => url("storage/photo/${filename}"),
                            'created_at' => $foto->created_at->isoFormat('D MMM Y HH:mm'),
                            'message' => 'Gambar berhasil diunggah!',
                            'status' => 200
                        ]
                    );
                }else{
                    return response()->json(
                        [
                            'message' => 'Foto gagal diproses',
                            'status' => 400
                        ]);
                }
            }
            catch(Exception $e)
            {
                return response()->json(
                    [
                        'message' => $e->getMessage(),
                        'status' => 400
                    ]
                );
            }
        }
        return response()->json(
            [
                'message' => 'Tidak ada foto yang diupload',
                'status' => 400
            ]
        );
    }
}

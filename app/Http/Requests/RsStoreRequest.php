<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tahun' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'kabupaten' => 'required',
            'kota' => 'required',
            'kelompok_kota' => 'required',
            'jenis' => 'required',
            'jenis2' => 'required',
            'tipe_kelas' => 'required',
            'bpjs' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'tahun' => 'Tahun dilarang kosong',
            'nama' => 'Nama dilarang kosong',
            'alamat' => 'Alamat dilarang kosong',
            'kabupaten' => 'Kabupaten dilarang kosong',
            'kota' => 'Kota dilarang kosong',
            'kelompok_kota' => 'Kelompok kota dilarang kosong',
            'jenis' => 'Jenis dilarang kosong',
            'jenis2' => 'Jenis dilarang kosong',
            'tipe_kelas' => 'Tipe kelas dilarang kosong',
            'bpjs' => 'BPJS dilarang kosong'
        ];
    }
}

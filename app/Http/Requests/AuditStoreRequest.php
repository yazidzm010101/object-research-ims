<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuditStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_rs' => 'required',
            'id_deskripsi_tes' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'id_rs' => 'Rumah sakit / lab tak terdeteksi, Harap refresh browser',
            'id_deskripsi_tes' => 'Deskripsi tes tidak terdeteksi, Harap refresh browser'
        ];
    }
}

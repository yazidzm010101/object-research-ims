<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grup_2 extends Model
{
    public $timestamps = false;
    protected $table = "tbl_grup_2";
    protected $fillable = [
        'nama_grup_2',
        'nama_lini',
        'status_grup_2'
    ];
}

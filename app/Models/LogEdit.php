<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogEdit extends Model
{
    public $timestamps = true;
    protected $table = "tbl_log_edit";
    protected $fillable = [
        'id_rs',
        'lokasi',
        'id_grup_2',
        'ip'
    ];
}

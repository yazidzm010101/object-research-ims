<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kuesioner extends Model
{
    protected $table = 'tbl_quest_kuesioner';
    protected $primaryKey = 'id_kuesioner';
    protected $fillable = [
        'id_rs',
        'interviewer',
        'tanggal',
        'waktu_mulai',
        'waktu_selesai',
        'tahun_berdiri',
        'luas',
        'frekuensi',
        'program',
        'pendapatan'
    ];
}

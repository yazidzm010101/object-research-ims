<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $table = 'tbl_quest_foto';
    protected $primaryKey = 'id_foto';
    protected $fillable = [
        'id_rs',
        'jenis',
        'foto'
    ];
}

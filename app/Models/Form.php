<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'tahun';
    protected $table = "tbl_form";
    protected $fillable = ['tahun','link','status'];
}

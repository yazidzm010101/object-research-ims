<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LastProgress extends Model
{
    protected $table = "vw_last_progress";
}

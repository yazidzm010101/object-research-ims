<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grup_1 extends Model
{
    public $timestamps = false;
    protected $table = "tbl_grup_1";
    protected $fillable = [
        'nama_grup_1'
    ];
}

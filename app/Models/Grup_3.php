<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grup_3 extends Model
{
    public $timestamps = false;
    protected $table = "tbl_grup_3";
    protected $fillable = [
        'nama_grup_3',
        'brand',
        'status_grup_3'
    ];
}

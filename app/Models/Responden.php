<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Responden extends Model
{
    protected $table = 'tbl_quest_responden';
    protected $primaryKey = 'id_responden';
    protected $fillable = [
        'id_rs',
        'nama',
        'jabatan',
        'bagian',
        'telp',
        'hp',
        'foto'
    ];
}

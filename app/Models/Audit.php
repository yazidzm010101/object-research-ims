<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    public $timestamps = false;
    public $primaryKey = 'id_rs';
    protected $table = "tbl_main_audit";
    protected $fillable = [
        'id_rs',
        'id_grup_all',
        'ketersediaan',
        'jumlah',
        'satuan',
        'harga',
        'total',
        'jumlah_q1',
        'satuan_q1',
        'harga_q1',
        'total_q1',
        'jumlah_q2',
        'satuan_q2',
        'harga_q2',
        'total_q2',
        'id_log_edit'
    ];
}

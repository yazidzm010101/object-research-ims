<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupAll extends Model
{
    public $timestamps = false;
    protected $table = "tbl_grup_all";
    protected $fillable = [
        'id_grup_1',
        'id_grup_2',
        'id_grup_3',
        'ukuran',
        'tipe',
        'status_grup_all'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RumahSakit extends Model
{
    public $timestamps = false;
    protected $table = "tbl_main_rs";
    protected $fillable = [
        'id_rawdata',
        'tahun',
        'nama_rs',
        'alamat',
        'kota',
        'kelompok_kota',
        'status'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawData extends Model
{
    public $timestamps = false;
    protected $table = "vw_rawdata";
}

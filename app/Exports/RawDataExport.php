<?php

namespace App\Exports;

use App\Models\RawData;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RawDataExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $tahun;
    protected $prev_tahun;
    protected $id_rs;

    public function __construct(int $tahun, int $id_rs)
    {
        $this->tahun = $tahun;
        $this->prev_tahun = $tahun - 1;
        $this->id_rs = $id_rs;
    }

    public function query()
    {
        if($this->id_rs!="0"){
            $data= RawData::select('ketersediaan','id_rawdata','nama_rs','alamat','kota','kelompok_kota','status_pengelolaan','kelas','nama_grup_1','nama_lini','nama_grup_2','nama_grup_3','ukuran','nama_lini','satuan_tipe','jumlah','satuan','harga','total','jumlah_q1','satuan_q1','harga_q1','total_q1','jumlah_q2','satuan_q2','harga_q2','total_q2','proporsi','status_rs','status_grup_2','status_produk')
            ->where('tahun','=',$this->tahun)
            ->where('id_rs','=',$this->id_rs)
            ->orderByRaw('`id_grup_all` ASC, `status_rs` ASC, `id_grup_all` ASC');
        } else {
            $data= RawData::select('ketersediaan','id_rawdata','nama_rs','alamat','kota','kelompok_kota','status_pengelolaan','kelas','nama_grup_1','nama_lini','nama_grup_2','nama_grup_3','ukuran','nama_lini','satuan_tipe','jumlah','satuan','harga','total','jumlah_q1','satuan_q1','harga_q1','total_q1','jumlah_q2','satuan_q2','harga_q2','total_q2','proporsi','status_rs','status_grup_2','status_produk')
            ->where('tahun','=',$this->tahun)
            ->orderByRaw('`id_grup_all` ASC, `status_rs` ASC, `id_grup_all` ASC');
        }
        return $data;
    }
    public function headings(): array
    {
        return [
            'KETERSEDIAAN',
            'ID1','RUMAH SAKIT',
            'ALAMAT','KOTA','GROUP CITY',
            'PRIVATE / PUBLIC','Kelas/Tipe',
            'CATEGORY','SUB LINI','SUB CATEGORY','PRODUCT IN DETAIL','BRAND','SATUAN',
            "TOTAL QUANTITY {$this->prev_tahun}","SATUAN UNIT {$this->prev_tahun}",	"PRICE PER QUANTITY {$this->prev_tahun}", "TOTAL REVENUE {$this->prev_tahun}",
            "TOTAL QUANTITY Q1 {$this->tahun}","SATUAN UNIT {$this->tahun} Q1" ,	"PRICE PER QUANTITY Q1 {$this->tahun}", "TOTAL REVENUE Q1 {$this->tahun}",
            "TOTAL QUANTITY Q2 {$this->tahun}","STAUAN UNIT {$this->tahun} Q2" ,	"PRICE PER QUANTITY Q2 {$this->tahun}", "TOTAL REVENUE Q2 {$this->tahun}",
            "PROPOSI {$this->prev_tahun} / {$this->tahun} Q1","STATUS_RS","STATUS_GRUP_2","STATUS_PRODUCT"
        ];
    }
}
